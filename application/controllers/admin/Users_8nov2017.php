<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
class Users extends CI_Controller {

    var $user_id;

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('pagination');
        $this->load->library("Nusoap_lib");
        $this->load->library('form_validation');

        $this->load->model(array('users_model', 'payment_transaction_fss_model', 'role_model', 'tickets_model', 'wallet_model', 'wallet_trans_model', 'state_model', 'wallet_topup_model', 'credit_account_detail_model', 'state_master_model', 'city_master_model', 'agent_docs_master_model', 'office_master_model','fund_request_model','common_model','user_email_model', 'user_otp_model', 'user_tpin_model', 'user_password_model'));

        is_logged_in();
        $this->load->library('image_lib');
        $this->load->library('cca_payment_gateway');
        /* $this->user_id = $this->session->userdata('user_id');
          if ($this->user_id == "") {
          redirect(base_url());
          } */
    }

    /* @function    : users
     * @param       : 
     * @detail      : all users list.
     *                 
     */

    public function index() {

        $roles = "";
        $user_id = strtolower($this->session->userdata('user_id'));
        $role_name = strtolower($this->session->userdata('role_name'));
        $chck_multi_office = $this->users_model->where(array('id' => $this->session->userdata('user_id'), 'role_id' => ORS_AGENT_ROLE_ID, 'multioffice_flag' => 'Y'))->find_all();
        if ($role_name == "admin" || $role_name == "super admin") {
            $roles = $this->role_model->select();
            $data["roles"] = $roles;
            load_back_view(USERS_VIEW, $data);
        } else if ($role_name == ORS_AGENT_ROLE_NAME && $chck_multi_office) {
            $this->users_model->where("id", $user_id);
            $data["user_info"] = $this->users_model->as_array()->select();
            $data["office_info"] = $this->office_master_model->get_office_agentwise($user_id);
            load_back_view(USERS_VIEW, $data);
        } else if ($role_name == DISTRIBUTOR_ROLE_NAME) {
            $this->users_model->where("id", $user_id);
            $data["user_info"] = $this->users_model->as_array()->select();
            $data["office_info"] = $this->office_master_model->get_office_agentwise($user_id);
            $data['states'] = $this->state_master_model->get_all_state();
            $data['city'] = $this->city_master_model->get_all_city();
            load_back_view(USERS_VIEW, $data);
        } else {
            load_back_view(NOT_AUTHORIZED_USERS);
        }
    }

    /*
     * @function    : get_list
     * @param       : 
     * @detail      : all users list according to role(operator,agent).
     *                 
     */

    function get_list() {
        $role_name = $this->session->userdata('role_name');
        
        $role_id = $this->session->userdata('user_id');
        $this->datatables->select("u.id as id,u.display_name,u.email,u.mobile_no,ur.role_name as role_name,IF(u.office_code ='' OR u.office_code IS NULL,u.firm_name,om.office_name)   
AS Office,u.status,u.kyc_verify_status as kyc_status,u.email_verify_status");
        $this->datatables->from("users u");
        $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'left');
        $this->datatables->join('office_master om', 'om.office_code=u.office_code', 'left');
        if (strtolower($role_name) == "ors_agent") {
            $this->datatables->where('u.role_id', ORS_AGENT_USERS_ROLE_ID);
            $this->datatables->where("u.parent_id", $this->session->userdata('user_id'));
        } elseif (strtolower($role_name) == "distributor") {
            $this->datatables->where('u.role_id', SUBAGENTS_ROLE_ID);
            $this->datatables->where("u.parent_id", $this->session->userdata('user_id'));
        }
        $this->datatables->add_column('email_verify_status', '$1', 'actv_dactv_users(id,email_verify_status,kyc_status)');
        $this->datatables->add_column('action', '$1', 'check_agent_users_status(id,status,kyc_status,email_verify_status,role_name)');
        $data = $this->datatables->generate('json');
        echo $data;
    }

    /*
     * @function    : profile
     * @param       : $id
     * @detail      : view profile Backend.
     *                 
     */

    public function profile($id = "") {
		$this->session->unset_userdata('change_pass_attempts');
        $id = base64_decode($id);
        if ($id != "") {
          $rolename = $this->session->userdata('role_name');
          $roleid = $this->session->userdata('role_id');
          $pofileimg = $this->session->userdata('profile_image');
          $this->users_model->id = $id;
          $udata = $this->users_model->select();
          $data["user_array"] = $udata;
          $data["user_role"] = $rolename;
          $data["profile_image"] = $pofileimg;
		  if($roleid != SUPERADMIN_ROLE_ID ){
          $data["MDCount"] = $this->users_model->getCountUsers($id,MASTER_DISTRIBUTOR_LEVEL);
		  $data["ADCount"] = $this->users_model->getCountUsers($id,AREA_DISTRIBUTOR_LEVEL);
		  $data["DistCount"] = $this->users_model->getCountUsers($id,POS_DISTRIBUTOR_LEVEL);
          $data["RtCount"] = $this->users_model->getCountUsers($id,RETAILOR_LEVEL);
		  $data['KycDoc'] = $this->users_model->getKycDocument($id);
		  }
		  $data['city'] = $this->city_master_model->get_all_city();
          $data['states'] = $this->state_master_model->get_all_state();
          
		  $data['back'] = 'Y';	  
		  clear_otp_attempts();
		  load_back_view(USER_PROFILE, $data);
          } else {
          redirect(base_url());
          } 
    }
	
	/*
     * @function    : update_personal_info(BACKEND EDIT PROFILE)
     * @param       : $id
     * @detail      : update personal infromation for backend users.
     */
    public function	update_personal_info($id = ""){
		
		if(is_null($id) || $id ==''){
			
		}else{
	    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[45]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[45]');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('dob', 'Dob', 'trim|required');
        $this->form_validation->set_rules('firm_name', 'Firm Name', 'trim|required|max_length[45]');
        if ($this->form_validation->run() == FALSE) {
            $this->profile(base64_encode($id));
        } else {
			
		    $now = date('Y-m-d H:i:s', time());
			$Dob = date('Y-m-d',strtotime($this->input->post('dob')));
			$EncryptDob = rokad_encrypt($Dob, $this->config->item('pos_encryption_key'));
			$DisplayName = $this->input->post('first_name').' '.$this->input->post('last_name');
			$data = array(
			'first_name'  => $this->input->post('first_name'),
			'last_name'   => $this->input->post('last_name'),
			'display_name'=> $DisplayName,
			'gender'      => $this->input->post('gender'),
			'dob'	      => $EncryptDob,
			'firm_name'	  => $this->input->post('firm_name'),
			'updated_by'  => $this->session->userdata('user_id'),
			'updated_date'=> $now);
			$Updateid = $this->common_model->update('users','id',$id, $data);
			if ($Updateid > 0) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Personal Information Successfully.');
				redirect(base_url().'admin/users/profile/'.base64_encode($id));
			} 
		}	
		}	
	}

	/*
     * @function    : update_contact_info(BACKEND EDIT PROFILE)
     * @param       : $id
     * @detail      : update Contact infromation for backend users.
     */
    public function	update_contact_info($id = ""){
		if(is_null($id) || $id ==''){
			
		}else{
	    $this->form_validation->set_rules('address1', 'Address Line 1', 'trim|required|max_length[45]');
        $this->form_validation->set_rules('state_id', 'State', 'trim|required|max_length[45]');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->profile(base64_encode($id));
        } else {
		    $now = date('Y-m-d H:i:s', time());
			$Pincode = rokad_encrypt($this->input->post('pincode'), $this->config->item('pos_encryption_key'));
			$data = array(
			'address1'    => $this->input->post('address1'),
			'address2'    => $this->input->post('address2'),
			'state'	      => $this->input->post('state_id'),
			'city'	      => $this->input->post('city'),
			'updated_by'  => $this->session->userdata('user_id'),
			'updated_date'=> $now);
			$Updateid = $this->common_model->update('users','id',$id, $data);
			if ($Updateid > 0) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Contact Information Successfully.');
				redirect(base_url().'admin/users/profile/'.base64_encode($id));
			} 
		}	
	  }	
	}	

    /*
     * @function    : edit_profile_view
     * @param       : $id
     * @detail      : view edit profile.
     */
    public function edit_profile_view($id) {

        //check if user is authorized to perform edit action
        if ($id == $this->session->userdata('id') or $this->session->userdata('role_name') == "admin") {
            $actual_id = $id;
        } else {
            $actual_id = $this->session->userdata('id');
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'your are not authorized to modify URL.');
        }

        $this->load->model('users_model');
        $user_array = $this->users_model->get_user_by_id($actual_id);
        $data["user_array"] = $user_array;
        $data["id"] = $actual_id;

        load_back_view(EDIT_USER_PROFILE_VIEW, $data);
    }

    /*
     * @function    : update_profile
     * @param       : $id
     * @detail      : to update profile.
     *                 
     */

    public function update_profile($id) {

        //check if user is authorized to perform update action
        if ($id == $this->session->userdata('id') or $this->session->userdata('role_name') == "admin") {

            $actual_id = $id;
            if (trim($_POST['fullname']) != "")
                $user_array['display_name'] = trim($_POST['fullname']);

            if (trim($_POST['password']) != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $salt = getRandomId();
                    $user_array['salt'] = $salt;
                    $user_array['password'] = md5($salt . trim($_POST['password']));
                } else {
                    $this->session->set_flashdata('invalid', 'password missmatched');
                    redirect('admin/users/profile/' . $actual_id);
                }
            }

            $data["user_array"] = $user_array;
            $data["id"] = $actual_id;
            $this->load->model('users_model');
            if ($this->users_model->update_user_by_id($data)) {
                $this->session->set_flashdata('invalid', 'profile updated successfully');
            } else {
                $this->session->set_flashdata('invalid', 'profile not updated');
            }

            redirect('admin/users/profile/' . $actual_id);
        } else {
            $actual_id = $this->session->userdata('id');
            $this->load->model('users_model');
            $user_array = $this->users_model->get_user_by_id($actual_id);
            $data["user_array"] = $user_array;
            $data["id"] = $this->session->userdata('id');

            $this->session->set_flashdata('invalid', 'your are not authorized to modify URL.');
            redirect('admin/users/profile/' . $actual_id);
        }
    }

    /*
     * @function    : saveuser
     * @param       : 
     * @detail      : to save user.               
     */

    public function saveuser() {

        $input = $this->input->post(NULL,TRUE);
        $state_no = '';
        $is_true = true;
       
        if (!empty($input['office_select'])) {
            $office_select = trim($input['office_select']);
        } else {
            $office_select = "";
        }
        if (!empty($input['firm_name'])) {
            $firm_name = trim($input['firm_name']);
        } else {
            $firm_name = "";
        }
        $first_name = trim($input['first_name']);
        $last_name = trim($input['last_name']);
        if (!empty($input['fullname'])) {
            $display_name = trim($input['fullname']);
        } else {
            $display_name = $first_name . " " . $last_name;
        }
        $address1 = isset($input['add1']) ? trim($input['add1']) : "";
        $address2 = isset($input['add2']) ? trim($input['add2']) : "";
        $state = isset($input['intStateId']) ? trim($state_no[0]['intStateId']) : "";
        $city = isset($input['agent_city']) ? trim($input['agent_city']) : "";
        $state = isset($input['agent_state']) ? trim($input['agent_state']) : "";
        $country_id = 1;
        $pincode = isset($input['agent_pincode']) ? trim($input['agent_pincode']) : "";
        $pancard = isset($input['agent_pancard']) ? trim($input['agent_pancard']) : "";
        $alternate_no = isset($input['alternate_no']) ? trim($input['alternate_no']) : "";
        $commission_template = isset($input['commission_template']) ? trim($input['commission_template']) : "";

        $mobile_number = trim($input['mobile_number']);
        $email = trim($input['email']);
        $username = trim($input['username']);
        $password = trim($input['password']);
        $confirm_password = trim($input['confirm_password']);
        $salt = getRandomId();
        if ($this->session->userdata('role_name') == ORS_AGENT_ROLE_NAME) {
            $role_name = ORS_AGENT_USERS_ROLE_NAME;
        } else {
            $role_name = strtolower($this->session->userdata('role_name'));
        }

        $user_id = $this->session->userdata('user_id');
        $allowed_users = array();
        $allowed_users_id = array();

        /*
         * This condition is to fecth all particular agent/operators users
         * and store in to two array and check if agent/operator has authority 
         * to view and edit user.
         */
        if ($input["id"] != "" && ($role_name == "operator" || $role_name == "booky agent")) {
            if ($role_name == "operator") {
                $this->users_model->op_id = $user_id;
            } else if ($role_name == "booky agent") {
                $this->users_model->a_id = $user_id;
            }

            $this->users_model->cols = "id,user_id";
            $users = $this->users_model->select();
            foreach ($users as $user) {
                $allowed_users[] = $user->user_id;
                $allowed_users_id[$user->id] = $user->user_id;
            }

            // check agent/operatos has authorization to edit user

            if (array_key_exists($input["id"], $allowed_users_id)) {
                if (!in_array($allowed_users_id[$input["id"]], $allowed_users)) {
                    $is_true = false;
                }
            } else {
                $is_true = false;
            }
        }

        if (isset($input['id']) and $input['id'] != "") {
            // try to reduce this if afterwards
            if ($role_name == "admin" || $role_name == "super admin") {
                $append_msg = "updated";
                $this->users_model->id = $input['id'];

                if (isset($input['password']) && $input['password'] != "" and $password == $confirm_password) {
                    $pass = md5($salt . $password);
                    $this->users_model->salt = $salt;
                    $this->users_model->password = $pass;
                }
            }


            /*             * *****ORS AGENT WITH MULTIPLE OFFICES CHANGES STARTS HERE ******** */
            if ($this->session->userdata('role_name') == ORS_AGENT_ROLE_NAME || $this->session->userdata('role_name') == DISTRIBUTOR_ROLE_NAME) {
                $append_msg = "updated";
                $this->users_model->id = $input['id'];

                //CHANGING ORS AGENT USERS DETAILS.
                if (isset($input['office_select']) && !empty($input['office_select'])) {
                    $this->users_model->office_code = $input['office_select'];
                }
                if (isset($input['password']) && $input['password'] != "" and $password == $confirm_password) {
                    $pass = md5($salt . $password);
                    $this->users_model->salt = $salt;
                    $this->users_model->password = $pass;
                }
                $this->users_model->parent_id = $user_id;

                if ($first_name != "") {
                    $this->users_model->first_name = $first_name;
                }
                if ($last_name != "") {
                    $this->users_model->last_name = $last_name;
                }
                if ($mobile_number != "") {
                    $this->users_model->mobile_no = $mobile_number;
                }
            }
            if ($display_name != "") {
                $this->users_model->display_name = $display_name;
            }
            $this->users_model->alternate_no = isset($alternate_no) ? $alternate_no : "";
            $this->users_model->address1 = isset($address1) ? $address1 : "";
            $this->users_model->address2 = isset($address2) ? $address2 : "";
            $this->users_model->pincode = isset($pincode) ? $pincode : "";
            $this->users_model->pancard = isset($pancard) ? $pancard : "";

            $this->users_model->state = isset($state) ? $state : "";
            $this->users_model->city = isset($city) ? $city : "";
            $this->users_model->firm_name = isset($firm_name) ? $firm_name : "";
            if ($state != '') {
                $this->users_model->country = $country_id;
            }
            $this->users_model->commission_id = isset($commission_template) ? $commission_template : "";

            /*
             * This condition is to update password and salt if password changed.
             */
            if ($role_name == "operator" || $role_name == "booky agent") {
                if (array_key_exists($input["id"], $allowed_users_id)) {
                    if (in_array($allowed_users_id[$input["id"]], $allowed_users)) {
                        $append_msg = "updated";
                        $this->users_model->id = $input['id'];

                        if ($password != "" and $password == $confirm_password) {
                            $pass = md5($salt . $password);
                            $this->users_model->salt = $salt;
                            $this->users_model->password = $pass;
                        }
                    } else {
                        $data["flag"] = '@#failed#@';
                        $data["msg_type"] = 'error';
                        $data["msg"] = 'You are not authorized to view and edit this user.';
                        $is_true = false;
                    }
                } else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'You are not authorized to view and edit this user.';
                    $is_true = false;
                }
            }
        } else {
            $append_msg = "added";
            if ($password != "" and $password == $confirm_password) {

                $this->users_model->cols = "";
                if ($username != "" and $email != "" and $display_name != "") {

                    $this->users_model->where("username", $username)->or_where("email", $email);
                    $user_exists = $this->users_model->select();
                    if (count($user_exists) > 0) {
                        $data["flag"] = '@#error#@';
                        $data["msg_type"] = 'success';
                        $data["msg"] = 'Username or email already exists.';
                        $is_true = false;
                    } else {
                        $pass = md5($salt . $password);

                        // to get role id
                        if ($role_name != "admin" || $role_name != "super admin") {
                            if ($role_name != ORS_AGENT_USERS_ROLE_NAME && $role_name != DISTRIBUTOR_ROLE_NAME) {
                                $this->role_model->role_name = USER_ROLE_NAME;
                                $rdata = $this->role_model->select();
                                $user_role_id = $rdata[0]->id;
                            } elseif ($role_name == ORS_AGENT_USERS_ROLE_NAME) {
                                $user_role_id = ORS_AGENT_USERS_ROLE_ID;
                                $this->users_model->agent_type = ORS_AGENT_USERS_ROLE_NAME;
                            } elseif ($role_name == DISTRIBUTOR_ROLE_NAME) {
                                $user_role_id = SUBAGENTS_ROLE_ID;
                                $this->users_model->agent_type = SUBAGENTS_ROLE_NAME;
                            }

                            $this->users_model->role_id = $user_role_id;
                        }

                        // $this->users_model->id = $current_userid;
                        /*                         * *****ORS AGENT WITH MULTIPLE OFFICES CHANGES STARTS HERE ******** */
                        if ($this->session->userdata('role_name') == ORS_AGENT_ROLE_NAME OR $this->session->userdata('role_name') == DISTRIBUTOR_ROLE_NAME) {

                            $this->users_model->parent_id = $user_id;
                        }
                        /*                         * *****ORS AGENT WITH MULTIPLE OFFICES CHANGES ENDS HERE ******** */
                        $this->users_model->first_name = isset($first_name) ? $first_name : "";
                        $this->users_model->last_name = isset($last_name) ? $last_name : "";
                        $this->users_model->mobile_no = isset($mobile_number) ? $mobile_number : "";
                        $this->users_model->alternate_no = isset($alternate_no) ? $alternate_no : "";

                        $this->users_model->address1 = isset($address1) ? $address1 : "";
                        $this->users_model->address2 = isset($address2) ? $address2 : "";
                        $this->users_model->pincode = isset($pincode) ? $pincode : "";
                        $this->users_model->pancard = isset($pancard) ? $pancard : "";

                        $this->users_model->state = isset($state) ? $state : "";
                        $this->users_model->city = isset($city) ? $city : "";
                        $this->users_model->firm_name = isset($firm_name) ? $firm_name : "";
                        if ($state != '') {
                            $this->users_model->country = $country_id;
                        }
                        $this->users_model->commission_id = isset($commission_template) ? $commission_template : "";
                        $this->users_model->office_code = isset($office_select) ? $office_select : "";


                        $this->users_model->username = $username;
                        $this->users_model->password = $pass;
                        $this->users_model->salt = $salt;
                        $this->users_model->email = $email;
                    }
                } else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'success';
                    $data["msg"] = 'Some fields are empty. Please try again.';
                    $is_true = false;
                }
            } else {
                $data["flag"] = '@#failed#@';
                $data["msg_type"] = 'success';
                $data["msg"] = 'Password you entered is missmatched.';
                $is_true = false;
            }
        }

        /*
         * This condition is to set role id if role is admin/super admin
         */
        if ($role_name == "admin" || $role_name == "super admin") {
            $this->users_model->role_id = $input["role"];
        }

        if ($is_true) {

            if ($role_name == "operator") {
                $this->users_model->op_id = $user_id;
            } else if ($role_name == "booky agent") {
                $this->users_model->a_id = $user_id;
            }
            $this->users_model->kyc_verify_status = "Y";
            $this->users_model->email_verify_status = "Y";
            $this->users_model->verify_status = "Y";
            $this->users_model->display_name = $display_name;
            $this->users_model->inserted_by = $this->session->userdata("user_id");
            $this->users_model->ip_address = $this->session->userdata("ip_address");
            $this->users_model->user_agent = $this->session->userdata("user_agent");
            $last_insert_id = $this->users_model->save();

            if ($last_insert_id > 0) {

                $wallet_array = ['user_id' => $last_insert_id,
                    'amt' => 0.00,
                    'status' => "Y",
                    'comment' => "Created Account",
                    'added_by' => $this->session->userdata("user_id")];

                $wallet_id = $this->wallet_model->insert($wallet_array);


                $custom_data = array();
                $custom_data["verification_type"] = "account";
                $custom_data["mail_title"] = "Agent User Registration";
                $userdata = $this->users_model->where('id', $last_insert_id)->find_all();
                $reg_username = ($userdata[0]->display_name != "") ? $userdata[0]->display_name : $userdata[0]->first_name . " " . $userdata[0]->last_name;
                if ($this->session->userdata('role_name') == ORS_AGENT_ROLE_NAME) {
                    $username = $userdata[0]->username;
                } else {
                    $username = ucwords(strtolower($userdata[0]->display_name));
                }


                if (isset($custom_data["mail_title"])) {
                    $mailbody_array = array("mail_title" => $custom_data["mail_title"],
                        "mail_client" => $userdata[0]->email,
                        "mail_replacement_array" =>
                        array("{{username}}" => $username, '{{password}}' => $password));
                }
                //show($mailbody_array,1);
                $mail_result = setup_mail($mailbody_array);
                if ($mail_result) {
                    $data["flag"] = '@#success#@';
                    $data["msg_type"] = 'success';
                    $data["msg"] = 'User ' . $append_msg . ' successfully' . '.';
                } else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Mail could not be sent.';
                }
            } else {
                $data["flag"] = '@#failed#@';
                $data["msg_type"] = 'error';
                $data["msg"] = 'User could not be' . $append_msg . '.';
            }
        }

        data2json($data);
    }

    /*
     * @function    : edit_user
     * @param       : $id
     * @detail      : view edit user.
     *                 
     */
    public function edit_user() {
        $input = $this->input->post(NULL,TRUE);

        $is_true = true;
        if ($is_true && $input["id"] != "") {
            $this->users_model->cols = "";
            $this->users_model->id = $input["id"];
            $rdata = $this->users_model->select();
            $data["flag"] = '@#success#@';
            $data["user_data"] = $rdata;
        } else {
            $data["flag"] = '@#error#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'You are not authorized to view or edit other users.';
        }
        data2json($data);
    }

    /*
     * @function    : delete_user
     * @param       : 
     * @detail      : delete user.
     *                 
     */
    public function delete_user() {
        $input = $this->input->post(NULL,TRUE);
        $this->users_model->id = $input["id"];
        $rdata = $this->users_model->select();
        $agent_user_array = array();
        $agent_user_id = array();
        /***When ors_agent is disabled, disable all the users under him ****/
       if ($rdata->agent_type == ORS_AGENT_ROLE_NAME && $rdata->role_id == ORS_AGENT_ROLE_ID && $input["postyn"] == "N"){
            $where_condition = array('parent_id' => $input["id"]);
            $agent_users = $this->users_model->where($where_condition)->find_all();
            foreach ($agent_users as $au) {
                $agent_user_id[] = $au->id;
            }
            $agent_user_array = $agent_user_id;
        }
        
        $is_true = true;
        $role_name = strtolower($this->session->userdata('role_name'));
        $user_id = strtolower($this->session->userdata('user_id'));
        /*
         * This condition is to fecth all particular agent/operators users
         * and store in to two array and check if agent/operator has authority 
         * to delete user.
         */
        if ($role_name == "operator" || $role_name == "booky agent") {
            if ($role_name == "operator") {
                $this->users_model->op_id = $user_id;
            } else if ($role_name == "booky agent") {
                $this->users_model->a_id = $user_id;
            }

            $this->users_model->cols = "id,user_id";
            $users = $this->users_model->select();
            foreach ($users as $user) {
                $allowed_users[] = $user->user_id;
                $allowed_users_id[$user->id] = $user->user_id;
            }
            $this->users_model->cols = "";
            // check agent/operatos has authorization to edit user
            if ($input["id"] != "") {
                if (array_key_exists($input["id"], $allowed_users_id)) {
                    if (!in_array($allowed_users_id[$input["id"]], $allowed_users)) {
                        $is_true = false;
                    }
                } else {
                    $is_true = false;
                }
            }
        }

        if ($is_true) {

            if (!empty($agent_user_array)) {
                foreach ($agent_user_array as $disable_user) {
                    $this->users_model->id = $disable_user;
                    $this->users_model->status = "N";
                    $id = $this->users_model->save();
                }
            }

            $this->users_model->id = $input["id"];
            $this->users_model->status = $input["postyn"];
            $id = $this->users_model->save();

            if ($input["postyn"] == "Y") {
                $append = "enabled";
            } else if ($input["postyn"] == "N") {
                $append = "disabled";
            }

            if ($id > 0) {
                $data["flag"] = '@#success#@';
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg', 'User ' . $append . ' successfully.');
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'You are not authorized to view or edit other users.');
                $data["flag"] = '@#error#@';
            }
        } else {
            $data["flag"] = '@#error#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'You dont have autthority to delete this user.';
        }

        data2json($data);
    }

    function get_profile() {
        $id = $this->session->userdata('id');
        $user_id = $this->session->userdata('user_id');
        $user_data = $this->users_model->get_user_detail($id);
        $currentDate = date('Y-m-d');
        $data['udata'] = $user_data[0];
        $data['udata']['email'] = rokad_decrypt($data['udata']['email'], $this->config->item("pos_encryption_key"));
        $data['udata']['username'] = rokad_decrypt($data['udata']['username'], $this->config->item("pos_encryption_key"));
        $data['udata']['mobile_no'] = rokad_decrypt($data['udata']['mobile_no'], $this->config->item("pos_encryption_key"));
        $data['udata']['pincode'] = rokad_decrypt($data['udata']['pincode'], $this->config->item("pos_encryption_key"));
        $data['udata']['pancard'] = rokad_decrypt($data['udata']['pancard'], $this->config->item("pos_encryption_key"));
        $data['udata']['dob'] = rokad_decrypt($data['udata']['dob'], $this->config->item("pos_encryption_key"));
        $country_id = $data['udata']['country'];
        $state_id = $data['udata']['state'];
       
		$RollID = $this->session->userdata('role_id');
        $data['country'] = $this->state_model->getcountry();
        $data['states'] = $this->state_model->getstate($country_id);
        $data['city'] = $this->state_model->getcity($state_id);		
		

        $data['balance_request'] = $balance_request;
        $wallet_flag = 0;

        /* user wallet data */

        $data['wallet'] = $this->wallet_model->where(array('user_id' => $user_id))->find_all();

        if (!$data['wallet']) {
            $wallet_flag = 1;
        }

        $data['wallet_flag'] = $wallet_flag;
        $data['wallet_detail'] = $this->wallet_trans_model->wallet_trans_detail();
        $this->db->order_by("timestamp", "DESC");
        $this->users_action_log_model->user_id = $this->session->userdata('user_id');
        $data['user_action_log'] = $this->users_action_log_model->select();

        $data['wallet_bs'] = $this->wallet_model->where(array('user_id' => $this->session->userdata('user_id')))->find_all();
        
        $actual_wallet_balance = (!empty($data['wallet_bs'])) ? $data['wallet_bs'][0]->amt : 0;
        $data['wallet_agent_balance_show'] = $actual_wallet_balance;
        
        $balance_request= $this->wallet_topup_model->balance_request_info(/*$config["per_page"],$page*/);

        $data['balance_request']     = $balance_request;
       
		$LvlId  = 1;
	    $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	    $data['CompanyOwner'] = $this->common_model->get_value('users','id,first_name,last_name','level='.$LvlId);
		if($RollID == RETAILER_ROLE_ID){
		$Level =  $this->common_model->get_value('users','id,level_5','id='.$user_id);
   
		$data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id='.$Level->level_5);  
	    }
		$data['back'] = 'N';
		$data['email_hidden'] = $user_data[0]['email'];
        load_front_view(FRONT_USER_PROFILE, $data);
    }

    /*
     * @function    : update_user_profile(FRONTED EDIT PROFILE)
     * @param       : 
     * @detail      : this function update user profile data & send verification link to user email id.
     * @author      : pankaj durve               
     */

    function update_user_profile() {
        $uid = $this->session->userdata('id');
        $email = $this->session->userdata('email');

        $this->users_model->id = $uid;
        $user_data = $this->users_model->select();
        $img1_wid = 200;
        $img1_hei = 160;
        $img2_wid = 60;
        $img2_hei = 40;

        $small = 'iconic';
        $large = 'large';
        $medium = 'thumb';

        $input = $this->input->post(NULL,TRUE);


        if (isset($_FILES) && $_FILES['userfile']['name'] !== "") {
            $nm = 'userfile';
        } else {
            $nm = '';
        }

        $config = array(
            array('field' => 'add1', 'label' => 'Address1', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => ' Address1 should not be empty.')),
             array('field' => 'fnm', 'label' => 'First Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'First Name should not be empty.')),
            array('field' => 'lnm', 'label' => 'Last Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Last Name should not be empty.')),
            array('field' => 'gen', 'label' => 'Gender', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please select Gender.')),
            array('field' => 'country', 'label' => 'country', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please select country name.')),
            array('field' => 'city', 'label' => 'city', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please Input City Name.')),
            array('field' => 'state', 'label' => 'State Name', 'rules' => 'trim|xss_clean|required', 'errors' => array("required" => 'Please select state name')),
            array('field' => 'pcode', 'label' => 'pcode', 'rules' => 'trim|xss_clean|required|numeric|callback_valid_pin|min_length[6]|max_length[6]', 'errors' => array('required' => 'Please Enter Pincode Number.', 'valid_pin' => 'Please Enter valid pin number', 'min_length' => 'Pin code number should be 6 digit')),
            array('field' => 'dob', 'label' => 'Date of birth', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Date of birth should not be empty.')),
            array('field' => $nm, 'label' => 'Userfile', 'rules' => 'xss_clean|callback_check_extension|callback_check_image', 'errors' => array('check_extension' => 'Please upload Jpg/jpeg/gif/png image', 'check_image' => 'Invalid image.'))
        );
        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            $data['flag'] = "@#error@";
        } else {

            //Image uplaod  code
            //define array of subfolder
            $pathname = "";
            if (isset($_FILES) && $_FILES['userfile']['name'] !== "") {
                $info = explode('.', $_FILES['userfile']['name']);
                $file_ext = $info[1];
                $file_name = $uid . "." . $file_ext;
                $pathname = $file_name;
                $oldfile = $user_data->profile_image;
                if ($oldfile != "") {
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $large . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $large . '/' . $oldfile);
                    }
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $medium . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $medium . '/' . $oldfile);
                    }
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $small . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $small . '/' . $oldfile);
                    }
                }

                /*
                 * Create user folder to store the images 
                 */

                $folder = "profile_img";
                $subfolder = array("large", "thumb", "iconic");

                create_main_folder('uploads');
                create_user_folder($uid, $folder, $subfolder);
                $config = array(
                    'upload_path' => UPLOAD_FOLDER . $uid . '/profile_img/' . $large,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'file_name' => $file_name
                );
                $this->load->library('upload', $config);

                if ($this->upload->do_upload()) {
                    $img = $this->upload->data();
                    $this->resize($img, $uid, $img1_wid, $img1_hei, $medium);
                    $this->resize($img, $uid, $img2_wid, $img2_hei, $small);
                }
            } else {
                $pathname = $user_data->profile_image;
            }
            $dob = date('Y-m-d', strtotime($input['dob']));
            $new_pincode = rokad_encrypt($input['pcode'], $this->config->item("pos_encryption_key"));
            $new_dob = rokad_encrypt($dob, $this->config->item("pos_encryption_key"));
            
            $this->users_model->id = $uid;
            $this->users_model->address1 = $input['add1'];
            $this->users_model->address2 = $input['add2'];
            $this->users_model->pincode = $new_pincode;
            $this->users_model->country = $input['country'];
            $this->users_model->city = $input['city'];
            $this->users_model->state = $input['state'];
            $this->users_model->first_name = $input['fnm'];
            $this->users_model->last_name = $input['lnm'];
            $this->users_model->profile_image = $pathname;
            $this->users_model->display_name = $input['fnm'].' '.$input['lnm'];
            $this->users_model->gender = $input['gen'];
            $this->users_model->dob = $new_dob;
            $is_updated = $this->users_model->save();

            if ($is_updated) {
                $data['msg_type'] = "success";
                $data['flag'] = "@success";
                $data['msg'] = "Profile updated successfully.";
            } else {
                $data['msg_type'] = "error";
                $data['flag'] = "@#error@";
                $data['msg'] = "Profile could not updated.";
            }
        }
        data2json($data);
    }

    //image resize  fucntion

    public function resize($image_data, $uid, $wid, $hei, $type) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "100";

        $config['new_image'] = './uploads/' . $uid . '/profile_img/' . $type;
        $config['width'] = $wid;
        $config['height'] = $hei;

        $this->image_lib->initialize($config);
        // Call resize function in image library.

        $this->image_lib->resize();
    }

    /**
     * @
     * @param varchar $email 
     * @return boolean
     */
    function email_check($email) {		
        $id = $this->session->userdata('id');
		$email_encrypted = rokad_encrypt($email, $this->config->item('pos_encryption_key'));
        $em = $this->users_model->check_mail($email_encrypted, $id);
        if (count($em) > 0) {			 
            return false;
        } else {
            return true;
        }
    }

    function valid_pin($pin) {
        if ($pin == 0) {
            return false;
        } else {
            return true;
        }
    }

    function alpha_dash_space($str) {

        if (!preg_match("/^([a-z ])+$/i", $str)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function change_pass() {
        $input = $this->input->post(NULL,TRUE);

        $userid = $this->session->userdata('id');

        $config = array(
            array('field' => 'oldpass', 'label' => 'Old Password', 'rules' => 'xss_clean|trim|required', 'errors' => array('required' => 'Please old Password.')),
            array('field' => 'newpass', 'label' => 'New Pass', 'rules' => 'xss_clean|trim|required|min_length[8]|matches[connewpass]', 'errors' => array('required' => 'Please Enter new password.', 'min_length' => 'Please enter 10 character New Password ')),
            array('field' => 'connewpass', 'label' => 'Confirm New Pass', 'rules' => 'xss_clean|trim|required', 'errors' => array("required" => 'Please Enter Confirm new pass')),
        );
        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            $data['flag'] = "@#error@";
        } else {

            $oldpass = $input['oldpass'];
            $newpass = $input['newpass'];
            $connewpass = $input['connewpass'];

            $this->users_model->id = $userid;
            $userinfo = $this->users_model->select();

            $salt = $userinfo->salt;
            $pass = $userinfo->password;
            $password = md5($salt . $oldpass);

            if ($password == $pass) {

                $check_new_pass = md5($salt . $newpass);

                if ($check_new_pass != $password) {
                    $newsalt = getRandomId(); //generate new salt
                    $new_pass = md5($newsalt . $newpass); //concat salt & password
                    $this->users_model->id = $userid;
                    $this->users_model->password = $new_pass;
                    $this->users_model->salt = $newsalt;
                    $this->users_model->save();

                    $data['msg_type'] = "success";
                    $data['flag'] = "@#success@";
                    $data['msg'] = "Password updated successfully.";
                } else {
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Old and new password must be different.';
                    $data['error'] = array("newpass" => "Old and new password must be different.");
                    $data['flag'] = "@#error@";
                }
            } else {
                $data["msg_type"] = 'error';
                $data["msg"] = 'Old Password is wrong.';
                $data['error'] = array("oldpass" => "Old Password is wrong");
                $data['flag'] = "@#error@";
            }
        }
        data2json($data);
    }

    function generate_password() {
        $input = $this->input->post(NULL,TRUE);
		$user_data = $this->session->userdata();
        $userid = $input['id'];
        $email = $input['email'];
        $where_array = array("email" => $email);
        $udata = $this->users_model->where($where_array)->or_where("username", $email)->find_all();
		
		$input_data = array(		
		"user_id" => $userid,
		"email" => $email
		);
		
        if ($udata) {
            $user_array = $udata[0];
            if ($user_array->status == "Y") {
                $password = getRandomId(10);
                $salt = getRandomId();
                $pass = md5($salt . $password);
                $this->users_model->id = $user_array->id;
                $this->users_model->salt = $salt;
                $this->users_model->password = $pass;
                $last_insert_id = $this->users_model->save();
                $display_name = $user_array->display_name;
                $fullname = ucwords(strtolower($user_array->first_name . " " . $user_array->last_name));
                if ($last_insert_id > 0) {
                    $mailbody_array = array("mail_title" => "forgot_password",
                        "mail_client" => $email,
                        "mail_replacement_array" => array('{{fullname}}' => $fullname, '{{username}}' => $email, '{{password}}' => $password)
                    );

                    $action_log_msg = "";
					$mail_result = setup_mail($mailbody_array);
					if ($mail_result) {
                        //insert into user action log                        						
						$action_log_msg = "Password generated";

                        $data['msg_type'] = "success";
                        $data['flag'] = "@#success#@";
                        $data['msg'] = "Password updated successfully.";
                    } else {
                        //insert into user action log
                        $action_log_msg = "Password generate failed";    
						
						$data["msg_type"] = 'error';
                        $data["msg"] = 'Tried to reset the password.';
                        $data['error'] = array("newpass" => "Tried to reset the password.");
                        $data['flag'] = "@#error@";
                    }
					
					$users_action_log = array(
					"user_id" => $user_data['user_id'],
					"name" => $user_data['display_name'],
					"rmn" => $user_data['username'],
					"rollname" => $user_data['role_name'],
					"url" => current_url(),
					"db_function" => "update",
					"db_table_name" => "users",
					"function_name" => "generate password",
					"input_data" => json_encode($input_data),
					"message" => $action_log_msg
					);
					users_action_log_new($users_action_log);
				}
            } else {
                $data["msg_type"] = 'error';
                $data["msg"] = 'Tried to reset the password.';
                $data['error'] = array("newpass" => "Tried to reset the password.");
                $data['flag'] = "@#error@";
            }
        } else {
            $data["msg_type"] = 'error';
            $data["msg"] = 'Tried to reset the password.';
            $data['error'] = array("newpass" => "Tried to reset the password.");
            $data['flag'] = "@#error@";
        }



        data2json($data);
    }

    function change_email_old() {
        $input = $this->input->post(NULL,TRUE);
        $pemail = trim($input['pemail']);        
        $hid = trim($input['hddpemail']);


        $userid = $this->session->userdata('id');
        $config = array(
            array('field' => 'pemail', 'label' => 'Primary Email', 'rules' => 'xss_clean|trim|required|valid_email|callback_email_check', 'errors' => array('required' => 'Please Enter Primary email', 'email_check' => 'This Email ID is already exist'))
        );
        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $data['flag'] = "@#error@";

            $data['msg_type'] = "error";
            $data['msg'] = "Email Address is already registered.";
        } else {

            $this->users_model->id = $userid;
            $this->users_model->sec_email = $secemail;
            $this->users_model->save();
            if ($hid == $pemail) {
                $this->users_model->id = $userid;
                $this->users_model->verify_status = "Y";
                $this->users_model->email = $input['pemail'];
                $this->users_model->save();
            } else {
                $activation_key = getRandomId(8);

                $this->users_model->id = $userid;
                $this->users_model->verify_status = "N";
                $this->users_model->activate_key = $activation_key;
                $this->users_model->email = $input['pemail'];
                $this->users_model->save();

                // $mailbody_array = array("mail_title" => "email_verification",
                //     "mail_client" => $input['pemail'],
                //     "mail_replacement_array" => array('{{fullname}}' => $this->session->userdata('display_name'), '{{verification_mail_url}}' => "<a href=" . base_url() . "admin/login/user_verify/" . $activation_key . "/" . sha1($input['pemail']) . ">Click here to verify </a>", "{{header}}" => "<img src='http://www.bookonspot.com/img/logo.png' width='300px;' />")
                // );
                // $mail_result = setup_mail($mailbody_array);
                // to send mail on registered mail id
                $mailbody_array = array("mail_title" => "email_verification",
                    "mail_client" => $input['pemail'],
                    "mail_replacement_array" => array('{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key . "/email",
                        '{{fullname}}' => ucwords($this->session->userdata('display_name'))
                    )
                );

                $mail_result = setup_mail($mailbody_array);
            }
            $data['priemail'] = $pemail;
            $this->session->set_userdata('email', $pemail);
            $query = $this->users_model->column('verify_status')
                    ->where('id', $userid)
                    ->as_array()
                    ->find_all();

            if ($query) {
                $verify_status = $query[0]['verify_status'];
            }

            $data['secemail'] = $secemail;
            $data['msg_type'] = "success";
            $data['msg'] = "Email ID details are updated successfully.";
            $data['verify_st'] = $verify_status;
            $data['flag'] = "@success#";
        }
        data2json($data);
    }

    /*
     * @function    : resend_link
     * @param       : 
     * @detail      : resend email verification link to user
     *                 
     */

    function resend_link() {

        $userid = $this->session->userdata('id');
        $userinfo = $this->users_model->column("activate_key,email", false)
                ->where('id', $userid)
                ->as_array()
                ->find_all();

        $mailbody_array = array("mail_title" => "email_verification",
            "mail_client" => $userinfo[0]['email'],
            "mail_replacement_array" => array('{{fullname}}' => $this->session->userdata('display_name'), '{{verification_mail_url}}' => "<a href=" . base_url() . "admin/login/user_verify/" . $userinfo[0]['activate_key'] . "/" . sha1($userinfo[0]['email']) . ">Click here to verify </a>", "{{header}}" => "<img src='http://www.bookonspot.com/img/logo.png' width='300px;' />")
        );
        $mail_result = setup_mail($mailbody_array);

        $data['msg_type'] = "success";
        $data['msg'] = "Email verification link send succssfully.";
        $data['flag'] = "@success#";
        data2json($data);
    }

    /*     * email_check_ajax
     * 
     * this funtion checks email on ajax call
     */

    function email_check_ajax() {

        $id = $this->session->userdata('id');
        $email = $this->input->post('email');
        $em = $this->users_model->check_mail($email, $id);

        if (count($em) == 0) {
            $data['status'] = "success";
        } else {
            $data['status'] = "failed";
        }
        data2json($data);
    }

    function checkmat($str, $pemail) {
        if ($str == "") {
            return true;
        }

        if ($str === $pemail) {
            return false;
        } else {
            return true;
        }
    }

    function check_image() {

        show($_FILES);
        $a = $_FILES['userfile']['size'];
        if ($a > 9000) {
            return false;
        } else {
            return true;
        }
    }

    function check_extension() {
        $a = array("image/jpeg", "image/jpg", "image/png", "image/gif");
        $info = $_FILES['userfile']['type'];
        if (in_array($info, $a)) {
            return true;
        } else {
            return false;
        }
    }

    function wallet_trans_topup() {
        $config = array(
		    array('field' => 'trans_type', 'lable' => 'Transaction', 'rules' => 'xss_clean|trim|required', 'errors' => array('required' => 'Please select the Transaction Type')),
            array('field' => 'enter_amount', 'lable' => 'Enter Amount', 'rules' => 'xss_clean|trim|required|numeric|greater_than[0]', 'errors' => array('required' => 'Please enter the Topup Amount', 'greater_than' => 'Please enter greater than zero value.')),
            array('field' => 'bank_name', 'lable' => 'Bank Name', 'rules' => 'xss_clean|trim', 'errors' => array()),
            array('field' => 'account_number', 'lable' => 'Account Number', 'rules' => 'xss_clean|trim|integer', 'errors' => array('integer' => 'Please enter only Number')),
            array('field' => 'tran_ref_no', 'lable' => 'Transaction Reference Number', 'rules' => 'xss_clean|trim', 'errors' => array()),          
        );
        if (form_validate_rules($config)) {
			
            $input = $this->input->post();
			$amount = intval($input['enter_amount']);
			    if($amount <= 0){
					$data["flag"] = '@#failed#@';
					$data["msg_type"] = 'error';
					$data["msg"] = 'Amount Should Be Greater Than 0';  
			}else{
			$OwnAvaiableBalance = $this->common_model->get_value('wallet','id,amt','user_id='.$this->session->userdata('user_id'));
			$NowTotalBalance = $OwnAvaiableBalance->amt + $input['enter_amount'];
			
			if($NowTotalBalance > WALLET_LIMIT){
			 $data1["flag"] = '@#failed#@';
			 $data1["msg"] = ' Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';
			} 
            else {
        		$now = date('Y-m-d H:i:s', time());
        		$data = [];
                $data['user_id'] = $this->session->userdata('user_id');
                $data['request_touserid'] = $input['request_to'];
                $data['topup_by'] = $input['trans_type'];
                $data['amount'] = $input['enter_amount'];
                $data['bank_name'] = $input['bank_name'];
                $data['bank_acc_no'] = $input['account_number'];
                $data['transaction_no'] = $input['tran_ref_no'];
                $data['transaction_remark'] = $input['remarks'];
                $data['user_id'] = $this->session->userdata('user_id');
                $data['transaction_date'] = $now;
                $data['created_by'] = $this->session->userdata('user_id');
                $data['created_date'] = $now;
                $data['updated_by'] = $this->session->userdata('user_id');
                $data['updated_date'] = $now;
                $data['is_status'] = 'Y';
                $data['is_approvel'] = 'NA';
                $topup_request = $this->wallet_topup_model->agent_topup_request($data);
                logging_data("topup/topup_request_log.log", $input, "User Enters data");
                logging_data("topup/topup_request_log.log", $data, "Data Added In to Wallet Table");
                if ($topup_request) {
                    /* send $referrer_email starts */
                    $referrer_email = $referrer_mobile = "";
                    if ($this->config->item('refferrer_email_enable')) {
                        if ($this->session->userdata('user_id')) {
                            $usrid = $this->session->userdata('user_id');
                            $user_referrer_details = $this->users_model->user_referrer_details($usrid);
                            if (is_array($user_referrer_details) && count($user_referrer_details) > 0) {
                                $referrer_email = $user_referrer_details[0]['referrer_email'];
                                $referrer_mobile = $user_referrer_details[0]['referrer_mobile'];
                                $flag_referrer = true;
                            }
                        }
                        /* send $referrer_email ends  */
                    }
                    $firstname = $this->session->userdata('first_name');
                    $lastname = $this->session->userdata('last_name');
    				$ToEmail =  $this->common_model->get_value('users','email','id='.$input['request_to']);
                    $ToMobile =  $this->common_model->get_value('users','mobile_no','id='.$input['request_to']);
                    $ToName =  $this->common_model->get_value('users','first_name,last_name','id='.$input['request_to']);
                    $mail_replacement_array = array(
                        "{{topup_by}}" => $input['trans_type'],
                        "{{amount}}" => round($input['enter_amount'],2),
                        "{{bank_name}}" => $input['bank_name'],
                        "{{bank_acc_no}}" => $input['account_number'],
                        "{{transaction_no}}" => $input['tran_ref_no'],
                        "{{transaction_remark}}" => $input['remarks'],
                        "{{username}}" => $firstname . ' ' . $lastname,
                        "{{useremail}}" => $this->session->userdata('email'),
                        "{{transaction_date}}" => date('d-m-Y H:i:s', strtotime($input['created_date'])),
                        "{{is_approvel}}" => 'NA'
                    );
                    //for admin and not for Retailer
                    $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), WALLET_TOPUP_REQUEST);

                    $mailbody_array = array("subject" => "Wallet Topup Request of Rs " . $input['enter_amount'] . " by Retailer " . $firstname . ' ' . $lastname . " ",
                        "message" => $data['mail_content'],
                        "to" => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
                    );
                    $mail_result = send_mail($mailbody_array);
                    if ($mail_result) {
                        $data1['flag'] = "@#success#@";
                    }

                    //ADMIN sms
                    $flag = true;
                    if ($flag == true) {
                        $sms_array = array (
                                            "{{request_to_username}}" => $ToName->first_name.' '.$ToName->last_name,
                                            "{{request_by_role_name}}" => $this->session->userdata('role_name'),
                                            "{{request_by_username}}" => $firstname . ' ' . $lastname,
                                            "{{amount}}" => round($input['enter_amount'],2)

                                            );
                        $msg_to_sent = str_replace(array_keys($sms_array), array_values($sms_array), WALLET_TOPUP_REQUEST_TO);
                        $is_send = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent);
                        if ($is_send) {
                            $data1['sms'] = "@#success#@";
                        }
                    }
                } 
                else {
                    $data1['flag'] = "@#failed#@";
				    $data1['msg'] = "Please fill the appropriate data";
                }
		    }
		    }
            data2json($data1);
        } else {
            
            $input = $this->input->post();
            $data['error'] = $this->form_validation->error_array();
            logging_data("topup/topup_request_log.log", $input, "User Enters data");
            logging_data("topup/topup_request_log.log", $data['error'], "form error");
            //$this->session->set_flashdata('msg_type', 'error');
            //$this->session->set_flashdata('msg', 'Please enter proper data .');

            $data1['flag'] = "@#failed#@";
            $data1['msg'] = "Please fill the appropriate data";
            data2json($data1);
        }




//      data2json($input);
    }

    public function add_money() {

        $input = $this->input->post();

        $pg_amt = $input['pg_amt'];
        $config = array(
            array('field' => 'pg_amt', 'label' => 'Amount', 'rules' => 'xss_clean|trim|required|numeric|greater_than[0]', 'errors' => array('required' => 'Please enter valid Topup amount.')),
        );
        $users_data = $this->users_model
                        ->where("id", $this->session->userdata('user_id'))
                        ->as_array()->find_all();

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data['error'], "form error");

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', $data['error']['pg_amt']);
            redirect(base_url() . 'admin/users/get_profile');
        } else {
            $RollId = COMPANY_ROLE_ID;
			$RokadData = $this->common_model->get_value('users','id','role_id='.$RollId);
			$RokadWallet = $this->common_model->get_value('wallet','id,amt,threshold_limit','user_id='.$RokadData->id);
			$RokadWalletAmount = round($RokadWallet->amt,2);
			if($pg_amt > $RokadWalletAmount){ 
			    $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Company Balance is less than requested Amount.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/users/get_profile');
			}else{
            $OwnAvaiableBalance = $this->common_model->get_value('wallet','id,amt','user_id='.$this->session->userdata('user_id'));
            $NowTotalBalance = $OwnAvaiableBalance->amt + $input['pg_amt'];
            if($NowTotalBalance > WALLET_LIMIT){
                $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/users/get_profile');
            }
            else {
                $php_uniqid = "agw" . uniqid() . getRandomId(4);
                $track_id = $php_uniqid;

                $top_up_pg_details = [
                    'track_id' => $track_id,
                    'payment_by' => 'ccavenue',
                    'user_id' => $this->session->userdata('user_id'),
                    'total_amount' => $pg_amt,
                    'status' => "initialize",
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->session->userdata("user_agent")
                ];

                $payment_tran_fss_id = $this->payment_transaction_fss_model->insert($top_up_pg_details);

                $wallet_topup = [
                    'user_id' => $this->session->userdata('user_id'),
                    'amount' => $pg_amt,
                    'payment_transaction_id' => $payment_tran_fss_id,
                    'transaction_status' => "pfailed",
                    'payment_confirmation' => "not_done",
                    'topup_by' => 'PG',
                    'transaction_remark' => 'payment_by_online',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_date' => date('Y-m-d H:i:s'),
                    'transaction_date' => date('Y-m-d H:i:s'),
                    'is_status' => 'N',
                    'is_approvel' => 'NA',
                ];

                $wallet_topup_id = $this->wallet_topup_model->insert($wallet_topup);

                logging_data("topup/topup_request_log.log", $wallet_topup, "Wallet Topup  Pg Request data");

                if ($wallet_topup_id) {
                    $ReqTid = "tid=" . time();
                    $ReqorderId = $track_id;
                    $ReqTotalAmount = $pg_amt;
                    $merchant_param1 = $payment_tran_fss_id;
                    $merchant_param2 = $wallet_topup_id;
                    $merchant_param3 = '';
                    $merchant_param4 = "";

                    $ReqMerchantOrderId = "order_id=" . $ReqorderId;
                    $ReqMerchantId = "merchant_id=" . $this->config->item("cca_merchant_id_agent");
                    $ReqAmount = "amount=" . $pg_amt;
                    $ReqCurrency = "currency=INR";
                    $ReqRedirectUrl = "redirect_url=" . base_url() . "admin/users/get_cca_response";
                    $ReqCancelUrl = "cancel_url=" . base_url() . "admin/users/get_cca_response";
                    $ReqLanguage = "language=EN";
                    $ReqMerchantPara1 = "merchant_param1=" . $merchant_param1;
                    $ReqMerchantPara2 = "merchant_param2=" . $merchant_param2;
                    $ReqMerchantPara3 = "merchant_param3=" . $merchant_param3;
                    $ReqMerchantPara4 = "merchant_param4=" . $merchant_param4;

                    $Strhashs = trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);

                    if (isset($users_data) && count($users_data) > 0) {
                        if (isset($users_data[0]['first_name'])) {
                            $ReqBillingName = "billing_name=" . trim($users_data[0]['first_name']);
                            $Strhashs = $Strhashs . "&" . trim($ReqBillingName);
                        }

                        if (isset($users_data[0]['email'])) {
                            $ReqBillingEmail = "billing_email=" . rokad_decrypt(trim($users_data[0]['email']),$this->config->item('pos_encryption_key'));
                            $Strhashs = $Strhashs . "&" . trim($ReqBillingEmail);
                        }

                        if (isset($users_data[0]['mobile_no']) && $users_data[0]['mobile_no'] != '0') {
                            $ReqBillingTelNo = "billing_tel=" . rokad_decrypt(trim($users_data[0]['mobile_no']),$this->config->item('pos_encryption_key'));
                            $Strhashs = $Strhashs . "&" . trim($ReqBillingTelNo);
                        }
                    }

                    //For geo location
                    //UNCOMMENT BELOW LINE ON LIVE
                    // $ip_address = $this->session->userdata("ip_address");
                    //COMMENT BELOW LINE ON LIVE
                    $locationdata = array();
                    $ip_address = $this->input->ip_address();

                    $ReqBillingCountry = "billing_country=India";
                    $Strhashs = $Strhashs . "&" . trim($ReqBillingCountry);
                    //AFTERWARDS DO THIS OR CHANGE LOGIC FOR merchant_param5
                    //Use hash method which is defined below for Hashing ,It will return Hashed valued of above string
                    $hashedstring = hash('sha256', $Strhashs);
                    $ReqMerchantPara5 = "merchant_param5=" . $hashedstring;
                    // $Reqparam = trim($ReqTid)."&".trim($ReqMerchantId)."&".trim($ReqMerchantOrderId)."&".trim($ReqAmount)."&".trim($ReqCurrency)."&".trim($ReqRedirectUrl)."&".trim($ReqCancelUrl)."&".trim($ReqLanguage)."&".trim($ReqMerchantPara1)."&".trim($ReqMerchantPara2)."&".trim($ReqMerchantPara3)."&".trim($ReqMerchantPara4)."&".trim($ReqMerchantPara5)."&";
                    $Reqparam = trim($ReqTid) . "&" . trim($ReqMerchantId) . "&" . trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqRedirectUrl) . "&" . trim($ReqCancelUrl) . "&" . trim($ReqLanguage) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);
                    if (isset($ReqBillingName)) {
                        $Reqparam .= "&" . $ReqBillingName;
                    }

                    if (isset($ReqBillingEmail)) {
                        $Reqparam .= "&" . $ReqBillingEmail;
                    }

                    if (isset($ReqBillingTelNo)) {
                        $Reqparam .= "&" . $ReqBillingTelNo;
                    }

                    $Reqparam .= "&" . $ReqBillingCountry;
                    $Reqparam = $Reqparam . "&" . trim($ReqMerchantPara5) . "&";

                    log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $Reqparam, "Data For Payment");

                    $encrypted_data = encrypt($Reqparam, $this->config->item("cca_working_key_agent"));
                    $ccprocess_data["encrypted_data"] = $encrypted_data;

                    log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $encrypted_data, "Encrypted Data For Payment");

                    load_front_view(AGENT_PGPROCESSING_VIEW, $ccprocess_data);
                } else {
                    $custom_error_mail = array(
                        "custom_error_subject" => "Data Not Add into Wallet TopUp.",
                        "custom_error_message" => "Data Not added into wallet topup while topup from pg - " . json_encode($wallet_topup)
                    );
                    $this->common_model->developer_custom_error_mail($custom_error_mail);

                    log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $wallet_topup, "Pg Request Time Data");

                    load_front_view(UNEXPECTED_ERROR);
                }
            }
          }
        }
    }

    public function get_cca_response() {
        $front_redirect = true;
        $data = $this->cca_payment_gateway->GetTopupResponse($front_redirect);

        $this->session->set_flashdata('msg_type', $data["page_msg_type"]);
        $this->session->set_flashdata('msg', $data["page_msg"]);

        redirect($data["page_redirect"]);
    }

    public function kyc_show_popup() {
        $input = $this->input->post();
        $is_true = true;
        if ($input["id"] != "") {
            $role_name = strtolower($this->session->userdata('role_name'));
            $user_id = $this->session->userdata('user_id');
        } else {
            $is_true = false;
        }

        if ($is_true) {
            $this->users_model->cols = "";
            $this->users_model->id = $input["id"];
            $rdata = $this->users_model->select();
            $data["flag"] = '@#success#@';
            $data["user_data"] = $rdata;
        } else {
            $data["flag"] = '@#error#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'You are not authorized to view or edit other users.';
        }

        data2json($data);
    }

    public function kyc_save() {
        $input = $this->input->post();
        $pan_card = trim($input['pan_card']);
        $addr_proof = trim($input['addr_proof']);
        $approver_name = trim($input['approver_name']);
        $remarks = trim($input['remarks']);
        $agent_id = trim($input['id']);

        if ($pan_card != "") {
            $this->agent_docs_master_model->agent_id = $agent_id;
            $this->agent_docs_master_model->docs_name = "1";
            $this->agent_docs_master_model->comment = $remarks;
            $this->agent_docs_master_model->approver_name = $approver_name;
            $this->agent_docs_master_model->is_status = "1";
            $this->agent_docs_master_model->is_deleted = "0";
            $this->agent_docs_master_model->created_by = "1";
            $this->agent_docs_master_model->created_date = date('Y-m-d H:i:s');
            $last_insert_id = $this->agent_docs_master_model->save();
        }

        if ($addr_proof != "") {
            $this->agent_docs_master_model->agent_id = $agent_id;
            $this->agent_docs_master_model->docs_name = "11";
            $this->agent_docs_master_model->comment = $remarks;
            $this->agent_docs_master_model->approver_name = $approver_name;
            $this->agent_docs_master_model->is_status = "1";
            $this->agent_docs_master_model->is_deleted = "0";
            $this->agent_docs_master_model->created_by = "1";
            $this->agent_docs_master_model->created_date = date('Y-m-d H:i:s');
            $last_insert_id = $this->agent_docs_master_model->save();
        }

        if ($pan_card == "" && $addr_proof == "") {
            $this->agent_docs_master_model->agent_id = $agent_id;
            $this->agent_docs_master_model->comment = $remarks;
            $this->agent_docs_master_model->approver_name = $approver_name;
            $this->agent_docs_master_model->is_status = "1";
            $this->agent_docs_master_model->is_deleted = "0";
            $this->agent_docs_master_model->created_by = "1";
            $this->agent_docs_master_model->created_date = date('Y-m-d H:i:s');
            $last_insert_id = $this->agent_docs_master_model->save();
        }

        if ($last_insert_id) {
            $this->users_model->id = $agent_id;
            $this->users_model->kyc_verify_status = "Y";
            $this->users_model->verify_status = "Y";
            $this->users_model->status = "Y";
            $updated_verified_user = $this->users_model->save();
            if ($updated_verified_user) {
                $userdata = $this->users_model->where('id', $agent_id)->find_all();
                $custom_data = array();
                $custom_data["verification_type"] = "account";
                $custom_data["mail_title"] = "email_verification_success";

                //Send sms
                $reg_username = ($userdata[0]->display_name != "") ? $userdata[0]->display_name : $userdata[0]->first_name . " " . $userdata[0]->last_name;
                $reg_sms_array = array("{{username}}" => ucwords($reg_username));
                $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_ACCOUNT_ACTIVATION);
                $is_send = sendSMS($userdata[0]->mobile_no, $temp_sms_msg);

                if (isset($custom_data["mail_title"])) {
                    $mailbody_array = array("mail_title" => $custom_data["mail_title"],
                        "mail_client" => $userdata[0]->email,
                        "mail_replacement_array" =>
                        array("{{username}}" => ucwords(strtolower($userdata[0]->first_name . " " . $userdata[0]->last_name))
                        )
                    );

                    $mail_result = setup_mail($mailbody_array);

                    if ($is_send || $mail_result) {
                        if (trim($custom_data["verification_type"]) == "account") {
                            $data["flag"] = '@#success#@';
                            $data["msg"] = "KYC done successfully.User activated successfully!!";
                        } else if (trim($custom_data["verification_type"]) == "email") {
                            $data["flag"] = '@#error#@';
                            $data["msg_type"] = 'error';
                            $data["msg"] = 'Failed to send email or sms.';
                        }
                    }
                }
            } else {
                $data["flag"] = '@#error#@';
                $data["msg_type"] = 'error';
                $data["msg"] = 'Failed to update the users table';
            }
        } else {
            $data["flag"] = '@#error#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Some error occurred.Please try again after some time.';
        }

        data2json($data);
    }


	public function send_verification_email() {	
		$input = $this->input->post();		
		$encrypted_user_id = trim($input['id']);
		$user_id = base64_decode($encrypted_user_id);
					
		$where_array = array("id" => $user_id);
		$user_data = $this->users_model->where($where_array)->find_all();
		$allow_to_send_flag = true;
		$mail_verification_limit = 2; // in minutes
		if(count($user_data) > 0)
		{
			$user_data = $user_data[0];			
			$email_verify_status = $user_data->email_verify_status;			
			if($email_verify_status == "N")
			{				
				$user_email_encrypted = $user_data->email;
				$user_email_plain = rokad_decrypt($user_email_encrypted, $this->config->item('pos_encryption_key'));
				$display_name = $user_data->display_name;							
				$last_sent = $this->user_email_model->get_last_sent($user_id, $user_email_encrypted);		
				if(count($last_sent) > 0)
				{				
					$curr_date = date('Y-m-d H:i:s');
					$last_sent_date = $last_sent['sent_date'];			
					$diff = get_difference_in_time($last_sent_date, $curr_date, 60);			
					if($diff < $mail_verification_limit)
					{					
						// not allow to send
						//show("not allow to send");					
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = 'Email verification link not allow to send again within '. $mail_verification_limit. ' minutes.';					
						$allow_to_send_flag = false;						
					}				
				}					
			
				if($allow_to_send_flag)
				{
					// allow to send
					$response = save_n_send_activation_key($user_id, $user_email_plain, $display_name, 'email_verification');
					if($response['msg_type'] == 'success')
					{
						$data['flag'] = "@success#";	
					}
					else
					{
						$data["flag"] = '@#failed#@';
					}
					$data["msg_type"] = 'success';
					$data["msg"] = 'Email verification link sent successfully';				
				}
			}
			else
			{
				// User email is already verified
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'User email is already verified';	
			}
		}
		else
		{
			// Invalid user
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Invalid user';	
		}
		data2json($data);
	}
	
	function resend_otp() {				
		$response['msg_type'] = "";
		$response['msg'] = "";
		
		$input = $this->input->post();
		$resend_key = $input['resend_key'] ? $input['resend_key'] : '';
				
		$user_id = $this->session->userdata('user_id');
		$mobile_no = $this->session->userdata('username');
		$display_name = $this->session->userdata('display_name');
		
		$otp_response = check_otp_attempts();			
		if($otp_response['msg_type'] == 'success')
		{
			$response = save_n_send_otp($user_id, $mobile_no, $display_name, RESET_USERNAME_OTP, $resend_key);
		}
		else
		{
			$response['msg_type'] = "error";
			$response['msg'] = $otp_response['msg'];
		}		
		data2json($response);
	}
		
	public function	change_password(){
		$input = $this->input->post(NULL, TRUE);	
		
		$old_password = trim(base64_decode($input['old_password_hidden']));
		$new_password = trim(base64_decode($input['new_password_hidden']));
		$cnf_password = trim(base64_decode($input['cnf_password_hidden']));
				
		$max_old_password = $this->config->item('max_old_password');
		$change_pass_attempts = intval($this->session->userdata('change_pass_attempts'));		
		$user_id = $this->session->userdata('user_id');
		$mobile_no = $this->session->userdata('username');
		$display_name = $this->session->userdata('display_name');
		
		if($old_password != "" && $new_password != "" && $cnf_password != "")
		{
			if($new_password === $cnf_password)
			{
				$response = is_strong_password($new_password);
				if($response["msg_type"] == "error")
				{
					$data['msg_type']='error';
					$data['msg']='Password should be of minimum 8 character with atlest 1 capital, 1 small, 1 special character [!@#%&._$*], and 1 numeric character.';
				}
				else
				{					
					$user_data = $this->users_model->where('id', $user_id)->find_all();
					if(count($user_data) > 0)
					{
						$user_data = $user_data[0];
						$salt = $user_data->salt;
						$user_password = $user_data->password;
						
						$old_password_hashed = md5($salt . $old_password);					
						if($user_password == $old_password_hashed)
						{
							$last_pass = $this->user_password_model->get_last_passwords($user_id);			
							$samepass = 'N';				
							if(sizeof($last_pass) > 0)
							{
								foreach($last_pass  as $key => $value)
								{
									$new_password_hashed = md5($value['salt'] . $new_password);
									if($value['password'] == $new_password_hashed)
									{
										$samepass = 'Y';							 
									}
								}				
							}			
							
							if($samepass == 'N')
							{							
								$newsalt = getRandomId();
								$passnew = md5($newsalt . $new_password);						
								$this->users_model->save_password($user_id, $passnew, $newsalt);
								
								$pass_data = array("user_id" => $user_id,
								"salt" => $newsalt,			
								"password" => $passnew,
								"created_date" => date('Y-m-d H:i:s')
								);	

								$this->user_password_model->save_password_log($pass_data);	
								
								$data['msg_type']='success';
								$data['msg']='Password Changed Successfully';
								$this->session->unset_userdata('change_pass_attempts');
							}
							else
							{
								$data['msg_type']='error';
								$data['msg']='New password cannot be same as last 5 passwords';
								$change_pass_attempts++;
							}
						}
						else
						{							
							$data['msg_type']='error';
							$data['msg']='Invalid old password';
							$change_pass_attempts++;
						}
					}
					else
					{						
						$data['msg_type']='error';
						$data['msg']='Invalid user';
						$change_pass_attempts++;
					}					
				}
			}
			else
			{
				$data['msg_type']='error';
				$data['msg']='Confirm password should match new password';							
			}
		}
		else
		{
			$data['msg_type']='error';
			$data['msg']='All fields are required';							
		}
	
		if($change_pass_attempts > $max_old_password)
		{			
			// disable account and send sms
			$this->users_model->id = $user_id;
			$this->users_model->status = 'N';
			$id = $this->users_model->save();								
			if($id > 0)
			{
				$reg_sms_array = array("{{username}}" => ucwords($display_name));
				$temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), ACCOUNT_DISABLED);
				$is_send = sendSMS($mobile_no, $temp_sms_msg);	
			}
			
			$data['msg_type']='error';
			$data['msg']='Your account is disabled please contact support.';			
		}		
		
		$this->session->set_userdata('change_pass_attempts', $change_pass_attempts);
		data2json($data);
	}
	
	public function is_strong_password() {
		$input = $this->input->post();
		$response = is_strong_password($input['password_inp']);
		if($response["msg_type"] == "error")
		{
			echo "false";
		}
		else
		{
			echo "true";
		}	
	}
	
	public function	change_tpin(){
		
		clear_otp_attempts();
		$user_id = $this->session->userdata('user_id');
		$this->session->unset_userdata('change_tpin');
		
		$input = $this->input->post(NULL, TRUE);	
		
		$old_tpin = trim(base64_decode($input['old_tpin_hidden']));
		$new_tpin = trim(base64_decode($input['new_tpin_hidden']));
		$cnf_tpin = trim(base64_decode($input['cnf_tpin_hidden']));		
		
		if(validate_tpin($old_tpin) && validate_tpin($new_tpin) && validate_tpin($cnf_tpin))
		{
			if($new_tpin === $cnf_tpin)
			{				
				$user_data = $this->users_model->where('id', $user_id)->find_all();
				if(count($user_data) > 0)
				{
					$user_data = $user_data[0];
					$tpin = $user_data->tpin;
					$encrypted_old_tpin = rokad_encrypt($old_tpin, $this->config->item('pos_encryption_key'));
					$username = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));
					if($tpin == $encrypted_old_tpin)
					{					
						$new_tpin_encrypted = rokad_encrypt($new_tpin, $this->config->item('pos_encryption_key'));
						$last_tpins = $this->user_tpin_model->get_last_tpins($user_id);			
						
						$sametpin = 'N';				
						if(sizeof($last_tpins) > 0)
						{
							foreach($last_tpins  as $key => $value)
							{
								 if($value['tpin'] == $new_tpin_encrypted)
								 {
									 $sametpin = 'Y';							 
								 }
							}				
						}
						
						if($sametpin == 'N')
						{
							$response = save_n_send_otp($user_id, $username, $user_data->display_name, RESET_USERNAME_OTP, 'reset_tpin');	
							if($response['msg_type'] == 'success')
							{
								$data['msg_type'] = $response['msg_type'];
								$data['msg'] = $response['msg'];								

								$submitted_data = array(
								'new_tpin' => $new_tpin								
								);							
								$this->session->set_userdata('change_tpin', serialize($submitted_data));
							}
							else
							{
								$data['msg_type'] = $response['msg_type'];
								$data['msg'] = $response['msg'];										
							}							
						}
						else
						{
							$data['msg_type']='error';
							$data['msg']='New T-PIN cannot be same as last 5 T-PIN';					
						}
					}
					else
					{
						$data['msg_type']='error';
						$data['msg']='Invalid old T-PIN';	
					}
				}
				else
				{
					$data['msg_type']='error';
					$data['msg']='Invalid user';
				}				
			}
			else
			{
				$data['msg_type']='error';
				$data['msg']='Confirm T-PIN should match new T-PIN';
			}
		}
		else
		{
			$data['msg_type']='error';
			$data['msg']='All fields are required';
		}
		data2json($data);	
	}
	
	function validate_tpin_otp() {			
		$otp_verification_expiry = 5;
		$user_id = $this->session->userdata('user_id');
		$username = $this->session->userdata('username');
		
		$input = $this->input->post(NULL, TRUE);		
		$otp = trim(base64_decode($input['new_otp_hidden']));	
		
		if($otp != "")
		{
			if(validate_otp($otp))
			{
				$response = verify_otp($user_id, $username, $otp);	
				if($response['msg_type'] == 'success')
				{
					$change_tpin = unserialize($this->session->userdata('change_tpin'));					 
					$new_tpin = $change_tpin['new_tpin'];
					
					$encrypted_new_tpin = rokad_encrypt($new_tpin, $this->config->item('pos_encryption_key'));		
					$this->users_model->save_tpin($user_id, $encrypted_new_tpin);							
					
					$tpin_data = array("user_id" => $user_id,
					"tpin" => $encrypted_new_tpin,			
					"created_date" => date('Y-m-d H:i:s')
					);	

					$this->user_tpin_model->save_tpin_log($tpin_data);			
												
					// log otp saved
					$tpin_log_data = array(
					"user_id" => $user_id,
					"tpin" => $new_tpin,
					"encrypted_new_tpin" => $encrypted_new_tpin,
					"created_date" => $tpin_data['created_date']
					);		
					
					log_data("tpin/tpin_".date("d-m-Y")."_log.log",$tpin_log_data, 'tpin_log_data array');	
					
					$data['msg_type']='success';
					$data['msg']='T-PIN Changed Successfully';
					
					$this->session->sess_destroy();
					unset($_SESSION);
				}
				else
				{				
					$data["msg_type"] = 'error';
					$data["msg"] = $response['msg'];				 
				}
			}			
			else
			{ // invalid user or mobile no
				$data['msg_type'] = "error";
				$data['msg'] = "Enter valid 6 digit otp number";			
			}			
		}
		else
		{
			$data['msg_type']='error';
			$data['msg']='All fields are required';							
		}
		data2json($data);
	}
		
	public function	change_email(){
		$input = $this->input->post(NULL, TRUE);		
		
		$inp_email = trim(base64_decode($input['inp_email_hidden']));		
		if($inp_email != "")
		{
			$this->load->library('email');
			if($this->email->valid_email($inp_email))
			{
				if($this->email_check($inp_email))
				{
					$user_id = $this->session->userdata('user_id');
					$email = $this->session->userdata('email');
					
					if($inp_email != $email)
					{				
						$user_data = $this->users_model->where('id', $user_id)->find_all();
						if(count($user_data) > 0)
						{
							$user_data = $user_data[0];	
							//$this->users_model->inactivate_user_email($user_id);
							$response = save_n_send_activation_key($user_id, $inp_email, $user_data->display_name, 'change_email_verification');
							if($response['msg_type'] == 'success')
							{
								$data['msg_type']='success';
								$data['msg']='Verification link sent successfully on you new email id';			
							}
							else
							{
								$data['msg_type']='error';
								$data['msg']='Faild to send verification mail';		
							}
						}
						else
						{						
							$data['msg_type']='error';
							$data['msg']='Invalid user';					
						}
					}
					else
					{
						$data['msg_type']='error';
						$data['msg']='No changes found';	
					}		
				}
				else
				{
					$data['msg_type']='error';
					$data['msg']='This Email ID already exists';
				}
			}
			else
			{
				$data['msg_type']='error';
				$data['msg']='Please enter valid email';
			}
		}
		else
		{
			$data['msg_type']='error';
			$data['msg']='All fields are required';							
		}
		data2json($data);
	}	
	
	public function send_reset_rmn_link() {	
		$input = $this->input->post();		
		$user_id = trim($input['id']);		
		$where_array = array("id" => $user_id);
		$user_data = $this->users_model->where($where_array)->find_all();
		if(count($user_data) > 0)
		{
			$user_data = $user_data[0];
			if($user_data->status == 'Y' && $user_data->otp_verify_status == 'Y')
			{				
				$user_email_encrypted = $user_data->email;
				$user_email_plain = rokad_decrypt($user_email_encrypted, $this->config->item('pos_encryption_key'));
				$display_name = $user_data->display_name;							
						
				// allow to send
				$response = save_n_send_activation_key($user_id, $user_email_plain, $display_name, 'reset_username');			
				if($response['msg_type'] == 'success')
				{
					$data['flag'] = "@success#";	
				}
				else
				{
					$data["flag"] = '@#failed#@';
				}
				$data["msg_type"] = 'success';
				$data["msg"] = 'Reset RMN link sent succesfully on user email id';			
			}
			else
			{
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'User account is disabled or RMN not verified';	
			}
		}
		else
		{
			// Invalid user
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Invalid user';	
		}
		data2json($data);
	}
}
