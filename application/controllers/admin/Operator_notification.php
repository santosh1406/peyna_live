<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Permissions
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
class Operator_notification extends CI_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();
        $rname = $this->session->userdata("role_name");

        if (strtolower($rname) != "admin" and strtolower($rname) != "super admin") {
            die("change this"); //later change die to redirect url
        }
        $lib = [
                    'api_provider/etravelsmart_api'
         ];

        $this->load->library($lib);
        $this->load->model(array('op_master_model','api_balance_log_model'));
    }

    public function index() {
         $opname = $this->op_master_model->select();
         $data['opname'] = $opname;
         $data['min_msrtc'] = $this->api_balance_log_model->min_msrtc();
         $data['user_email'] = $data['min_msrtc'][0]['user_email'];
         $data['min_upsrtc'] = $this->api_balance_log_model->min_upsrtc();
         $data['min_rsrtc'] = $this->api_balance_log_model->min_rsrtc();
         $data['min_hrtc'] = $this->api_balance_log_model->min_hrtc();
         $data['min_ets'] = $this->api_balance_log_model->min_ets();
         load_back_view(OPERATOR_NOTIFICATION_VIEW,$data);
    }
    public function send_mail_operator()
    {
      $input = $this->input->post();
      $user_email = $input['user_email'];
      $rsrtc_min  = $input['rsrtc'];
      $hrtc_min   = $input['hrtc'];
      $msrtc_min  = $input['msrtc'];
      $upsrtc_min = $input['upsrtc'];
      $ets_smin    = $input['ets'];
      $config = array 
      (
        array('field' => 'rsrtc','rules' => 'required', 'errors' => array('required' => 'Please Provide rsrtc balance')),
        array('field' => 'hrtc', 'rules' => 'required', 'errors' => array('required' => 'Please Provide hrtc balance')),
        array('field' => 'msrtc','rules' => 'required', 'errors' => array('required' => 'Please msrtc balance')),
        array('field' => 'upsrtc','rules' => 'required', 'errors' => array('required' => 'Please Provide upsrtc balance')),
        array('field' => 'ets','rules' => 'required', 'errors' => array('required' => 'Please Provide Etravelsmart balance'))
      );
     if (form_validate_rules($config, $input)) 
     {
        $msrtc_bal  = $this->api_balance_log_model->msrtc_balance();
        $msrtc_current = $msrtc_bal[0]['api_balance'];
        $upsrtc_bal   = $this->api_balance_log_model->upsrtc_balance();
        $upsrtc_current = $upsrtc_bal[0]['api_balance'];
        $hrtc_bal  = $this->api_balance_log_model->hrtc_balance();
        $hrtc_current = $hrtc_bal[0]['api_balance'];
        $rsrtc_bal = $this->api_balance_log_model->rsrtc_balance();
        $rsrtc_current = $rsrtc_bal[0]['api_balance'];
        $ets_balance = $this->etravelsmart_api->getMyPlanAndBalance();
        $ets_current = $ets_balance->balanceAmount; 

        $up_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 2,"op_name" =>"upsrtc","api_balance" =>$upsrtc_current ,"min_balance" =>$upsrtc_min,"user_email" => $user_email ];
        $this->api_balance_log_model->insert($up_balance);

        $ms_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 3,"op_name" =>"msrtc","api_balance" =>$msrtc_current ,"min_balance" =>$msrtc_min,"user_email" => $user_email];
        $this->api_balance_log_model->insert($ms_balance);
         
        $hr_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 4,"op_name" =>"hrtc","api_balance" =>$hrtc_current ,"min_balance" =>$hrtc_min,"user_email" => $user_email] ;
        $this->api_balance_log_model->insert($hr_balance); 

        $rs_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 1,"op_name" =>"rsrtc","api_balance" =>$rsrtc_current ,"min_balance" =>$rsrtc_min,"user_email" => $user_email] ;
        $this->api_balance_log_model->insert($rs_balance);

        $es_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => "","op_name" =>"Etravelsmart","api_balance" =>$ets_current ,"min_balance" =>$ets_smin,"user_email" => $user_email] ;
        $this->api_balance_log_model->insert($es_balance);

        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'SuccessFully Submitted');
        redirect(base_url().'admin/operator_notification');  

     } 
     else
     {
              $this->session->set_flashdata('msg_type', 'success');
              $this->session->set_flashdata('msg', 'Please Provide Valid Input');
              redirect(base_url().'admin/operator_notification');   
     }  
   }

}

//end of Role_menu_rel class
