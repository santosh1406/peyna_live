<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seller_amount extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('seller_amount_model','common_model'));
                $this->load->library('form_validation');
		//echo "hi";exit;
		
	}
    
    public function index()
    {	   
        $data = array();
        $data['seller_amt_info'] =  $this->seller_amount_model->getAllSellerAmtInfo();
        load_back_view(SELLER_AMOUNT_VIEW, $data);             
    }
    
    public function add_seller_amount_view()
    {
        $data = array();
        $data['stop_cd'] =  $this->seller_amount_model->getBusStopCode();
        load_back_view(SELLER_AMOUNT_ADD, $data);
    }
    
    public function add_seller_amount(){
        $input = $this->input->post(NULL,TRUE);
            $config = array(
		array('field' => 'seller_min_amt', 'label' => 'Seller Min amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Minimum Amount.')),
                array('field' => 'seller_code', 'label' => 'Seller Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Seller Code.'))
            );
            if (form_validate_rules($config)) {
                $amount = intval($input['seller_min_amt']);
                if($amount <= 0) {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Amount Should Be Greater Than 0';
                } else {
                    $UserID = $this->session->userdata("user_id");
                    $now = date('Y-m-d H:i:s', time());
                    $seller_amount_data = array(
                        'BUS_STOP_CD' => $input['seller_code'],
                        'min_amount' => $input['seller_min_amt'],
                        'inserted_by' => $UserID,
                        'inserted_date' => $now,
                        'is_active' => 'Y',
                        'is_deleted' => 'N'
                    );
                    $id = $this->seller_amount_model->insert_seller_data('cb_seller_amount', $seller_amount_data);
                    if ($id > 0) {
                        $data["flag"] = '@#success#@';
			$data["msg_type"] = 'success';
			$data["msg"] = 'Seller Minimum Balance Added successfully.';
                    }else {
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Seller Minimum Balance could not be added';
                    }		                               
                } 
            } else {
                $data["flag"] = '@#failed#@';
		$data["msg_type"] = 'error';
		$data["msg"] = 'Seller Minimum Balance could not be added without All * Neccessary Data Not Fill.';
            }		
	echo json_encode($data);
    }
    
        
    public function edit_seller_amount_view($seller_amount_id)
    {
        $data['seller_amt_info'] =  $this->seller_amount_model->getSellerAmtInfo($seller_amount_id);
        load_back_view(SELLER_AMOUNT_EDIT, $data);

    }
    
    public function edit_seller_amount($seller_amount_id){
        $input = $this->input->post(NULL,TRUE);
            $config = array(
		array('field' => 'seller_min_amt', 'label' => 'Seller Min amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Minimum Amount.')),
            );
//            show($seller_amount_id,1);
            if (form_validate_rules($config)) {
                $amount = intval($input['seller_min_amt']);
                if($amount <= 0) {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Amount Should Be Greater Than 0';
                } else {
                    $UserID = $this->session->userdata("user_id");
                    $now = date('Y-m-d H:i:s', time());
                    $seller_amount_data = array(
                        'min_amount' => $input['seller_min_amt'],
                        'updated_by' => $UserID,
                        'updated_date' => $now,
                        'is_active' => 'Y',
                        'is_deleted' => 'N'
                    );
                    $id = $this->seller_amount_model->update_seller_data('cb_seller_amount',$seller_amount_id, $seller_amount_data);
                    if ($id > 0) {
                        $data["flag"] = '@#success#@';
			$data["msg_type"] = 'success';
			$data["msg"] = 'Seller Minimum Balance Edited Successfully.';
                    }else {
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Seller Minimum Balance Could Not Be Edited ';
                    }		                               
                } 
            } else {
                $data["flag"] = '@#failed#@';
		$data["msg_type"] = 'error';
		$data["msg"] = 'Seller Minimum Balance could not be Edited without All * Neccessary Data Not Fill.';
            }		
	echo json_encode($data);
    }
    
    public function remove_seller_amount($seller_amount_id)
    {   
        $UserID = $this->session->userdata("user_id");
        $now = date('Y-m-d H:i:s', time());
        $seller_amount_data = array(
            'updated_by' => $UserID,
            'updated_date' => $now,
            'is_active' => 'N',
            'is_deleted' => 'Y'
        );
        $id = $this->seller_amount_model->update_seller_data('cb_seller_amount',$seller_amount_id, $seller_amount_data);
    
        if ($id > 0) {
            $this->session->set_flashdata('msg', 'Data Delete sucessfully');
        }else {
            $this->session->set_flashdata('msg', 'Data Could Not Be Deleted');
        }
        redirect(site_url('admin/seller_amount'));
    }


}