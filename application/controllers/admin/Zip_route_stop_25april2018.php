<?php

class Zip_route_stop extends MY_Controller {

     public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('zip_route_stop_model','common_model'));
                $this->load->library('form_validation');
                $this->load->helper('download');
		
    }
    
    public function index()
    {	   
        $data = array();
        $data['seller_point'] =  $this->zip_route_stop_model->getBusStopCode();
        load_back_view(ZIP_ROUTE_STOP, $data);             
    }
    
    public function list_route()
    {
        $input = $this->input->get();
        $route_data = $this->zip_route_stop_model->get_route($input);
        echo json_encode($route_data);
    }
    
    public function generate_route_stop_zip()
    {
        $input = $this->input->post();
        $data="";
        $dir_path = 'seller_point_zip/';
        if (is_dir($dir_path)) {
            $files = glob($dir_path . '*', GLOB_MARK);
                foreach ($files as $file) {
                    if (is_dir($file)) {
                        self::deleteDir($file);
                    } else {
                        unlink($file);
                    }
                }
            rmdir($dir_path);
        }
        mkdir($dir_path, 0777, true);
        $data .= $this->zip_route_stop_model->header_of_data('msrtc_replication.routes','####Route Detail####','route_name');
        foreach($input['route'] as $val) {
            $data .= $this->zip_route_stop_model->create_route($input['seller_code'],$val);
        }
        
        $data .= $this->zip_route_stop_model->header_of_data('msrtc_replication.route_stops','####Route Stop Details####','DEVNAGIRI_NM');
        
        foreach($input['route'] as $val) {
            $data .= $this->zip_route_stop_model->create_route_stop_csv_file($input['seller_code'],$val);
        }
        $route_file = $input['seller_code'].".csv.gz";
        $path = 'seller_point_zip/';
        
        $gzdata = gzencode($data, 9);   
        $fp = fopen($path . $route_file, "w");
        fwrite($fp, $gzdata);
        fclose($fp);
       
        if (is_file($path . $route_file)) {  
            header('Content-type: text/plain; charset=utf-8');
        }
        chmod($path . $route_file, 0740, true);
        force_download('seller_point_zip/'.$input['seller_code'].".csv.gz", NULL);
        
        exit;
    }
}    

