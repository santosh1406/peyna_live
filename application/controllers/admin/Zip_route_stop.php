<?php

class Zip_route_stop extends MY_Controller {

     public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('zip_route_stop_model','common_model'));
                $this->load->library('form_validation');
                $this->load->helper('download');
                $this->load->library('zip');
                $this->zip->compression_level = 0;
		
    }
    
    public function index()
    {	   
        $data = array();
        $data['seller_point'] =  $this->zip_route_stop_model->getBusStopCode();
        load_back_view(ZIP_ROUTE_STOP, $data);             
    }
    
    public function zip_fare_stop()
    {	   
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $data = array();
        $data['seller_point'] =  $this->zip_route_stop_model->getBusStopCode();
        load_back_view(ZIP_FARE_STOP, $data);             
    }
    
    public function list_route()
    {
        $input = $this->input->get();
        $route_data = $this->zip_route_stop_model->get_route($input);
        echo json_encode($route_data);
    }
    
    public function generate_route_stop_zip()
    {
        $input = $this->input->post();
        $data="";
        $dir_path = 'seller_point_zip/';
        if (is_dir($dir_path)) {
            $files = glob($dir_path . '*', GLOB_MARK);
                foreach ($files as $file) {
                    if (is_dir($file)) {
                        self::deleteDir($file);
                    } else {
                        unlink($file);
                    }
                }
            rmdir($dir_path);
        }
        mkdir($dir_path, 0777, true);
        $data .= $this->zip_route_stop_model->header_of_data('msrtc_replication.routes','####Route Detail####','route_name');
        foreach($input['route'] as $val) {
            $data .= $this->zip_route_stop_model->create_route($input['seller_code'],$val);
        }
        
        $data .= $this->zip_route_stop_model->header_of_data('msrtc_replication.route_stops','####Route Stop Details####','DEVNAGIRI_NM');
        
        foreach($input['route'] as $val) {
            $data .= $this->zip_route_stop_model->create_route_stop_csv_file($input['seller_code'],$val);
        }
        $route_file = $input['seller_code'].".csv.gz";
        $path = 'seller_point_zip/';
        
        $gzdata = gzencode($data, 9);   
        $fp = fopen($path . $route_file, "w");
        fwrite($fp, $gzdata);
        fclose($fp);
       
        if (is_file($path . $route_file)) {  
            header('Content-type: text/plain; charset=utf-8');
        }
        chmod($path . $route_file, 0740, true);
        force_download('seller_point_zip/'.$input['seller_code'].".csv.gz", NULL);
        
        exit;
    }
    
    public function generate_fare_zip()
    {
        $input = $this->input->post();
//        show($input,1);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $data="";
        $dir_path = 'fare_zip/';
        if (is_dir($dir_path)) {
            $files = glob($dir_path . '*', GLOB_MARK);
                foreach ($files as $file) {
                    if (is_dir($file)) {
                        self::deleteDir($file);
                    } else {
                        unlink($file);
                    }
                }
            rmdir($dir_path);
        }
        mkdir($dir_path, 0777);
        foreach($input['route'] as $route){
            $data = " ";
            $data .= $this->zip_route_stop_model->header_of_data(MSRTC_DB_NAME.'.route_header_details_prepare','####Header Details####','');

            $data .= $this->zip_route_stop_model->create_header_fare($route,$input['seller_code']);


            $data .= $this->zip_route_stop_model->header_of_data(MSRTC_DB_NAME.'.route_season_details_prepare','####Season Details####','');

            $data .= $this->zip_route_stop_model->create_season_fare($route,$input['seller_code']);

            $data .= $this->zip_route_stop_model->header_of_data(MSRTC_DB_NAME.'.route_details_prepare','####Route Details####','');

            $data .= $this->zip_route_stop_model->create_RouteDetails_fare($route,$input['seller_code']);

            $route_file = $input['seller_code']."_".$route.".csv.gz";
            $path = 'fare_zip/';

            $gzdata = gzencode($data, 9);   
            $fp = fopen($path . $route_file, "w");
            fwrite($fp, $gzdata);
            fclose($fp);

            if (is_file($path . $route_file)) {  
                header('Content-type: text/plain; charset=utf-8');
            }
            
            $this->zip->add_data($route_file, $gzdata);
           
        }
        $this->zip->archive('fare.zip');
        $this->zip->download('fare.zip');
    
//        foreach($input['route'] as $routes){
//             force_download('fare_zip/'.$input['seller_code']."_".$routes.".csv.gz", NULL);
//        }
   
//exit;
    }
}    
