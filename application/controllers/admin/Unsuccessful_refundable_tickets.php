<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unsuccessful_refundable_tickets extends CI_Controller {

    public function __construct() {
        parent::__construct();

       is_logged_in();
       set_time_limit(0);
       
       ini_set('max_execution_time', 0);
       ini_set('memory_limit', -1);

        $this->load->model(array('tickets_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'waybill_no_model', 'hand_held_machine_model', 'api_provider_model','city_master_model','wallet_trans_model','cancel_failure_transaction_log_model','payment_gateway_master_model','payment_gateway_wallet_model','payment_gateway_wallet_trans_model','return_ticket_mapping_model'));
        $this->load->library("Datatables");
        $this->load->library("cca_payment_gateway");
    
    }
    
    public function index() {
        $data = array();
        load_back_view(UNSUCCESSFUL_BOOKED_TKT_VIEW, $data);
    }

   /*
    * Author    : Rajkumar Shukla
    * Function  : Unsuccessful_refundable_tickets()
    * Detail    : Unsuccessfull refundable ticket fetch here
    */
    public function refund_unsuccessfull_ticket($ticket_id) 
    {
      $data   = array();
      $input  = $this->input->post();
      $tinfo  = $this->tickets_model->get_unsuccess_booked_tkt_detail($ticket_id);
    
      if($tinfo)
      {           
//        $refund_amt = $tinfo['payment_by'] == 'wallet' ? $tinfo['tot_fare_amt_with_tax'] : $tinfo['merchant_amount'];
        $reqest = $this->cancel_failure_transaction_log_model->column('id,ticket_id')->where(array("ticket_id" => $tinfo["ticket_id"]))->find_all();     
        
        if(empty($reqest))
        {
          if(in_array($tinfo["payment_by"], array("wallet","ccavenue","csc")))
          {
            if($tinfo["payment_by"] == "wallet")
            {   
              $refund_amt = $tinfo['tot_fare_amt_with_tax'];
              //here we fetching wallet detail against user_id
              $walletdata = $this->wallet_model->where(array('user_id' => $tinfo["inserted_by"]))->find_all() ;
              $wallet_id = $walletdata[0]->id;
              $wallet_current_amount = $walletdata[0]->amt;
              
              // here we fetching wallet_trans detail against the ticket_id transaction
              $wallet_tkt_trans_data = $this->wallet_trans_model->get_wallet_booked_ticket($tinfo['ticket_id']);

              if(!$wallet_tkt_trans_data)
              {
                log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/refund_unsuccess_tkt_request_log.log", $this->db->last_query(),"Transaction_against_wallet");
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Transaction against wallet error. Please contact technical team.');
                redirect(base_url()."admin/unsuccessful_refundable_tickets");
              }

              $wallet_amt_after_trans = $wallet_current_amount+$refund_amt;
              $wallet_trans_id_db =  $wallet_tkt_trans_data[0]->id;
              $transaction_no = substr(hexdec(uniqid()), 4, 12);
              $wallet_trans_refund =  array (   "w_id" => $wallet_id,
                                                "amt" => $refund_amt,
                                                "comment" => "Unsuccessful booked ticket refund",
                                                "status" => "Refund Credit",
                                                "user_id" => $tinfo["inserted_by"],
                                                "added_by" => $this->session->userdata('user_id'),
                                                "added_on" => date("Y-m-d H:i:s"),
                                                "amt_before_trans" => $wallet_current_amount,
                                                "amt_after_trans" => $wallet_amt_after_trans,
                                                "ticket_id" => $tinfo['ticket_id'],
                                                "transaction_type_id" => '8',
                                                "wallet_type" => 'actual_wallet',
                                                "transaction_type" => 'Credited',
                                                "is_status" => 'Y',
                                                "transaction_no" => $transaction_no
                                            );
              $insert_wallet_trans = $this->wallet_trans_model->insert($wallet_trans_refund);
              if($insert_wallet_trans > 0)
              {

                $update_array = array(
                                            'amt' => $wallet_amt_after_trans,
                                            'updated_by'=> $this->session->userdata('user_id'),
                                            'updated_date'=> date('Y-m-d h:m:s'),
                                            'last_transction_id' => $insert_wallet_trans,
                                         );
                $update_wallet_amt = $this->wallet_model->update_where('id', $wallet_id ,$update_array);
              }
              
              if($update_wallet_amt > 0)
              {
                $array_to_insert =  array(
                                          "ticket_id" => isset($tinfo['ticket_id']) ? $tinfo['ticket_id'] : '',
                                          "pg_tracking_id" => isset($tinfo['pg_tracking_id']) ? $tinfo['pg_tracking_id'] : '',
                                          "ticket_transaction_status" => isset($tinfo['transaction_status']) ? $tinfo['transaction_status'] : '',
                                          "ticket_issuing_date" => $tinfo['issue_time'],
                                          "ticket_fare" => isset($tinfo['tot_fare_amt_with_tax']) ? $tinfo['tot_fare_amt_with_tax'] : '',
                                          "merchant_amount" => isset($tinfo['merchant_amount']) ? $tinfo['merchant_amount'] : '',
                                          "refund_amount" => isset($refund_amt) ? $refund_amt : '',
                                          "refund_date" => date('Y-m-d H:i:s'),
                                          "refund_reason" => "Unsuccessful ticket booked from wallet against successful transaction" ,
                                          "type_id" => '8',
                                          "wallet_id" => isset($wallet_id) ? $wallet_id : '' ,
                                          "wallet_trans_id_db" => isset($wallet_trans_id_db) ? $wallet_trans_id_db : '' ,
                                          "wallet_trans_id_cr" => isset($insert_wallet_trans) ? $insert_wallet_trans : ''
                                          );

                $table_entry = $this->cancel_failure_transaction_log_model->insert($array_to_insert); 
                
                log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/agent_topup_request_log.log", $walletdata,"Wallet Data (Refunded tkt)");
                log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/agent_topup_request_log.log", $array_to_insert,"Wallet Data to insert in CFTL table (Refunded tkt)");

                $title = array('User Id','Amount','Amount Befor','Amount After','Ticket Id','Ticket Id','Date','Detail');
                $val = array ($tinfo['inserted_by'],$wallet_amt_after_trans,$wallet_current_amount,$wallet_amt_after_trans,$tinfo['ticket_id'],'',date("Y-m-d H:i:s"),'Function Detail : user_wallet/get_reason, Detail : Refund tkt amount against unsuccess tkt');

                $filename = "wallet/user_wallet_refund_unsuccessfull_tkt/wallet_".$wallet_id.".csv";

                make_csv($filename,$title,$val);
                //send mail
                $mail_replacement_array = array("{{ticket_id}}" => $tinfo['ticket_id'],
                                                "{{ticket_amount}}" => $refund_amt,
                                                "{{pg_tracking_id}}" => isset($tinfo["pg_tracking_id"]) ? $tinfo["pg_tracking_id"] : "",
                                                "{{wallet_id}}" => isset($wallet_id) ? $wallet_id : "",
                                                "{{journey_from_to}}" =>$tinfo['from_stop_name']." - ". $tinfo['till_stop_name'],
                                                "{{journey_date}}" => $tinfo['boarding_time'],
                                                "{{booking_date}}" => $tinfo['inserted_date'],
                                                "{{transaction_status}}" => $tinfo['transaction_status'],
                                                "{{site_url}}" => base_url(),
                                                );

                $data['mail_content']   = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),UNSUCCESSFUL_TICKET_BOOKED_REFUND);       
                // for admin
                eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
                $mailbody_array_admin = array(
                                              "subject"  => "(".ENVIRONMENT.") "."Unsuccessful Ticket Booked Against Successfull Payment Refund Rs ".$refund_amt." for Ticket id ".$tinfo['ticket_id']." ",
                                              "message"  => $data['mail_content'],
                                              "to"       => $admin_array_for_agent
                                              );
                $mail_result_admin = send_mail($mailbody_array_admin);
                $this->session->set_flashdata('msg_type','success');
                $this->session->set_flashdata('msg','Refund is done in your wallet Successfully');
                redirect(base_url()."admin/unsuccessful_refundable_tickets");
              } //update_wallet_amt
            }// payment by wallet

            if($tinfo["payment_by"] == "ccavenue")
            {
              $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_is_return_ticket_refund($ticket_id);
              $check_is_return_ticket = $this->return_ticket_mapping_model->as_array()->get_parent_ticketid_mapping($ticket_id);
              if ($check_is_return_ticket_refund || $check_is_return_ticket) {
               $refund_amt = $tinfo["tot_fare_amt_with_tax"];
            } else {
                $refund_amt = $tinfo["merchant_amount"];
            }
              $check_ticket_in_table = $this->cancel_failure_transaction_log_model->column('id,ticket_id')->where(array("ticket_id" => $tinfo["ticket_id"]))->find_all();
              if(empty($check_ticket_in_table))
              {
                
                $extra_data['ticket_id'] = $ticket_id;
                $extra_data['transaction_status'] = $tinfo['transaction_status'];
                $extra_data['pg_tracking_id'] = $tinfo['pg_tracking_id'];
                $confirmation_object = new stdClass();
                $confirmation_object->reference_no=  $tinfo['pg_tracking_id'];
                $confirmation_object->amount= $refund_amt;
                $merchant_array_data ['order_List'][] = $confirmation_object;

                $refund_status = $this->cca_payment_gateway->cancelOrder($merchant_array_data,$extra_data);
                if($refund_status->success_count == 1)
                {
                  $array_to_insert =  array(
                                            "ticket_id" => $ticket_id,
                                            "pg_tracking_id" => isset($tinfo['pg_tracking_id']) ? $tinfo['pg_tracking_id'] : '',
                                            "ticket_transaction_status" => isset($tinfo['transaction_status']) ? $tinfo['transaction_status'] : '',
                                            "ticket_issuing_date" => $tinfo['issue_time'],
                                            "ticket_fare" => isset($tinfo['tot_fare_amt_with_tax']) ? $tinfo['tot_fare_amt_with_tax'] : '',
                                            "merchant_amount" => isset($tinfo['merchant_amount']) ? $tinfo['merchant_amount'] : '',
                                            "refund_amount" => isset($tinfo['merchant_amount']) ? $tinfo['merchant_amount'] : $tinfo['tot_fare_amt_with_tax'],
                                            "refund_date" => date('Y-m-d H:i:s'),
                                            "refund_reason" => "Unsuccessful ticket booked against successful transaction" ,
                                            "type_id" => '8' ,
                                            "wallet_id" => isset($tinfo['wallet_trans_id_db']) ? $tinfo['wallet_trans_id_db'] : '' ,
                                            "wallet_trans_id_db" => isset($tinfo['wallet_trans_id_db']) ? $tinfo['wallet_trans_id_db'] : '' ,
                                            "wallet_trans_id_cr" => isset($tinfo['wallet_trans_id_cr']) ? $tinfo['wallet_trans_id_cr'] : '' 
                                            );

                  $table_entry = $this->cancel_failure_transaction_log_model->insert($array_to_insert); 
                  log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/refund_unsuccess_tkt_request_log.log", $array_to_insert,"Wallet Data to insert in CFTL table (Refunded tkt)");

                  $mail_replacement_array = array("{{ticket_id}}" => $tinfo['ticket_id'],
                                                   "{{ticket_amount}}" => $refund_amt,
                                                   "{{pg_tracking_id}}" => isset($tinfo["pg_tracking_id"]) ? $tinfo["pg_tracking_id"] : "",
                                                   "{{wallet_id}}" => isset($wallet_id) ? $wallet_id : "",
                                                   "{{journey_from_to}}" =>$tinfo['from_stop_name']." - ". $tinfo['till_stop_name'],
                                                   "{{journey_date}}" => $tinfo['boarding_time'],
                                                   "{{booking_date}}" => $tinfo['inserted_date'],
                                                   "{{transaction_status}}" => $tinfo['transaction_status'],
                                                   "{{site_url}}" => base_url(),
                                                   );

                  $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),UNSUCCESSFUL_TICKET_BOOKED_REFUND);

                  eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
                  $mailbody_array_admin       =   array(
                                                        "subject"  => "(".ENVIRONMENT.") "."Unsuccessful Ticket Booked Against Successfull Payment Refund Rs ".$refund_amt." for Ticket id ".$tinfo['ticket_id']." ",
                                                        "message"  => $data['mail_content'],
                                                        "to"       => $admin_array_for_agent
                                                        );
                  $mail_result_admin = send_mail($mailbody_array_admin);
                  $this->session->set_flashdata('msg_type', 'success');
                  $this->session->set_flashdata('msg','Ticket Refund Request Raise Successfully.');
                  redirect(base_url()."admin/unsuccessful_refundable_tickets");
                }
                else
                {
                  log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/refund_unsuccess_tkt_request_log.log", $walletdata,"Wallet Data (Refunded tkt)");
                  log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/refund_unsuccess_tkt_request_log.log", $array_to_insert,"Wallet Data to insert in CFTL table (Refunded tkt)");
                  $this->session->set_flashdata('msg_type', 'success');
                  $this->session->set_flashdata('msg', 'This order is not confirmed from ccavenue');
                  redirect(base_url()."admin/unsuccessful_refundable_tickets");
                }
              }//check_ticket_in_table
              else
              {                                            
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg','Already refund request is done');
                redirect(base_url()."admin/unsuccessful_refundable_tickets");
              }
            }//payment by ccavenue
            if($tinfo["payment_by"] == "csc") {
                $acual_refund_paid = $tinfo["tot_fare_amt_with_tax"];
                $pg_com_excluding_vle = $this->ticket_details_model->column("sum(pg_commission_after_tds) as pg_com")->where(["ticket_id"=>$ticket_id])->find_all();
                $pg_wallet_amount = $this->payment_gateway_master_model->pg_wallet_balance('csc');
                $chck_in_pg_wallet_trnas = $this->payment_gateway_wallet_trans_model->get_csc_booked_ticket($ticket_id);
                $csc_trans_id_db =  $chck_in_pg_wallet_trnas[0]->id;
                if(!$chck_in_pg_wallet_trnas)
                {
                  log_data("Refund_unsuccess_tkt/".date("M")."/".date("d")."/refund_unsuccess_tkt_request_log.log", $this->db->last_query(),"Transaction_against_csc");
                  $this->session->set_flashdata('msg_type', 'error');
                  $this->session->set_flashdata('msg', 'Transaction against CSC error. Please contact technical team.');
                  redirect(base_url()."admin/unsuccessful_refundable_tickets");
                }
                if(!empty($pg_wallet_amount)) {
                        $update_pg_wallet_amount = array(
                                                            "amount" => $pg_wallet_amount['amount'] + ($acual_refund_paid),
                                                        ); 
                        $array_where          = array("id"    => $pg_wallet_amount["id"]);
                        $update_pg_wallet = $this->payment_gateway_wallet_model->update_where($array_where,'', $update_pg_wallet_amount);
                if($update_pg_wallet) {
                        $pg_wallet_data = array(
                                                "pg_wallet_id" => $pg_wallet_amount["id"],
                                                "amount" => ($acual_refund_paid),
                                                "amount_paid_by_vle" => $tinfo['total_fare_without_discount'],
                                                "comment" => "Unsuccessful booked ticket refund",
                                                "amt_before_trans" => $pg_wallet_amount['amount'],
                                                "amt_after_trans" => $pg_wallet_amount['amount'] + ($acual_refund_paid),
                                                "ticket_id" => $ticket_id,
                                                "return_ticket_id" => '',
                                                "transaction_type_id" => '8',
                                                "transaction_type" => 'Credited',
                                                "added_on" => date("Y-m-d H:i:s")
                                                );
                        $pg_wallet_trans_data = $this->payment_gateway_wallet_trans_model->insert($pg_wallet_data);
                    }
                    else
                    {
                        $custom_error_mail  = array(
                                                        "custom_error_subject" => "Pg Wallet  table not updated.",
                                                        "custom_error_message" => "Unsuccessful booked ticket refund  not updated for ticket_id - ".$ticket_id." Pg Wallet Amount -".$pg_wallet_amount['amount']." Refund Amount - ".$acual_refund_paid
                                                    );
                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                    }
                if($pg_wallet_trans_data) {
                    $array_to_insert =  array(
                                          "ticket_id" => isset($ticket_id) ? $ticket_id : '',
                                          "pg_tracking_id" => isset($tinfo['pg_tracking_id']) ? $tinfo['pg_tracking_id'] : '',
                                          "ticket_transaction_status" => isset($tinfo['transaction_status']) ? $tinfo['transaction_status'] : '',
                                          "ticket_issuing_date" => $tinfo['issue_time'],
                                          "ticket_fare" => isset($chck_in_pg_wallet_trnas[0]->amount_paid_by_vle) ? $chck_in_pg_wallet_trnas[0]->amount_paid_by_vle : '',
                                          "merchant_amount" => isset($tinfo['merchant_amount']) ? $tinfo['merchant_amount'] : '',
                                          "refund_amount" => ($acual_refund_paid),
                                          "refund_date" => date('Y-m-d H:i:s'),
                                          "refund_reason" => "Unsuccessful ticket booked from wallet against successful transaction" ,
                                          "type_id" => '8',
                                          "wallet_id" => isset($pg_wallet_amount["id"]) ? $pg_wallet_amount["id"] : '' ,
                                          "wallet_trans_id_db" => isset($csc_trans_id_db) ? $csc_trans_id_db : '' ,
                                          "wallet_trans_id_cr" => isset($pg_wallet_trans_data) ? $pg_wallet_trans_data : ''
                                          );

                    $table_entry = $this->cancel_failure_transaction_log_model->insert($array_to_insert); 
                    
                }
                  else
                  {
                      $custom_error_mail  = array(
                                                      "custom_error_subject" => "Pg Wallet  table not updated.",
                                                      "custom_error_message" => "Unsuccessful booked ticket refund  not updated for cacnel failure trasn log ticket_id - ".$ticket_id." Pg Wallet Amount -".$pg_wallet_amount['amount']." Refund Amount - ".$acual_refund_paid - $pg_com_excluding_vle[0]->pg_com
                                                  );
                      $this->common_model->developer_custom_error_mail($custom_error_mail);
                  }
                $mail_replacement_array = array("{{ticket_id}}" => $tinfo['ticket_id'],
                                                "{{ticket_amount}}" => ($acual_refund_paid - $pg_com_excluding_vle[0]->pg_com),
                                                "{{pg_tracking_id}}" => isset($tinfo["pg_tracking_id"]) ? $tinfo["pg_tracking_id"] : "",
                                                "{{wallet_id}}" => isset($pg_wallet_amount["id"]) ? $pg_wallet_amount["id"] : "",
                                                "{{journey_from_to}}" =>$tinfo['from_stop_name']." - ". $tinfo['till_stop_name'],
                                                "{{journey_date}}" => $tinfo['boarding_time'],
                                                "{{booking_date}}" => $tinfo['inserted_date'],
                                                "{{transaction_status}}" => $tinfo['transaction_status'],
                                                "{{site_url}}" => base_url(),
                                                );

                $data['mail_content']   = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),UNSUCCESSFUL_TICKET_BOOKED_REFUND);       
                // for admin
                eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
                $mailbody_array_admin = array(
                                              "subject"  => "(".ENVIRONMENT.") "."Unsuccessful Ticket Booked Against Successfull Payment Refund Rs ".$acual_refund_paid - $pg_com_excluding_vle[0]->pg_com." for Ticket id ".$tinfo['ticket_id']." ",
                                              "message"  => $data['mail_content'],
                                              "to"       => $admin_array_for_agent
                                              );
//                $mail_result_admin = send_mail($mailbody_array_admin);
                  $this->session->set_flashdata('msg_type','success');
                  $this->session->set_flashdata('msg','Refund is done in your CSC wallet Successfully');
                  redirect(base_url()."admin/unsuccessful_refundable_tickets");
                }
            }
          }//payment by not in check
          else
          {
          $this->session->set_flashdata('msg_type', 'error');
          $this->session->set_flashdata('msg', 'Your payment is not done by  Wallet or PG');
          redirect(base_url()."admin/unsuccessful_refundable_tickets");
          }
        }//request empty
        else
        {
        $this->session->set_flashdata('msg_type', 'error');
        $this->session->set_flashdata('msg','Already refund request is done');
        redirect(base_url()."admin/unsuccessful_refundable_tickets");
        }
      } 
      else
      {
        $this->session->set_flashdata('msg_type','error');
        $this->session->flashdata('msg','Please contact customer support');
        redirect(base_url()."admin/unsuccessful_refundable_tickets");
      }
      redirect(base_url()."admin/unsuccessful_refundable_tickets");
}
  
  /** list of Unsuccessed tickets ****/
	function unsuccessful_tkt_details()
  {
      $this->datatables->select("'1',t.ticket_id,transaction_status,ticket_ref_no,t.pg_tracking_id,user_email_id,inserted_date,dept_time as doj,IFNULL(t.`tot_fare_amt_with_tax`,0) AS 'Ticket Fare',(case WHEN t.payment_by='wallet' THEN `t`.`tot_fare_amt_with_tax` ELSE `t`.`merchant_amount` END) as 'refund_amount'");
      $this->datatables->from('tickets t');
      $this->datatables->join('cancel_failure_transaction_log cftl', 'cftl.ticket_id = t.ticket_id', 'left');
      $this->datatables->where("cftl.ticket_id", NULL);
      if ($this->input->post('from_date'))
      {
        $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
      }

      if ($this->input->post('till_date'))
      {
        $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('till_date'))));
      }
      $this->datatables->where('payment_by !=', NULL);
//      $this->datatables->where('payment_mode !=', NULL);

      $status=array('psuccess','failed');
      $this->datatables->where_in('t.transaction_status', $status);
      $this->datatables->group_by('t.ticket_id');
      $this->db->order_by("t.inserted_date", "desc");
      $action .= '<a href="' . base_url('admin/unsuccessful_refundable_tickets/refund_unsuccessfull_ticket') . '/$1" class="btn btn-primary btn-xs edit_btn" onClick="return doconfirm();" data-ref="$1" title="Refund Ticket" > <i class="fa fa-edit"></i> </a>&nbsp;&nbsp;';
      $this->datatables->add_column('action', $action, 'ticket_id');
      $data = $this->datatables->generate('json');
      echo $data;
    }
}
?>
