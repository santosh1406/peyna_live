<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Lock extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('datatables');		
	}

	public function index() {
	
		load_back_view(LOCK_VIEW);		
	}
	public function get_lock_list() {		
		$session_level = $this->session->userdata('level');
        $user_id = $this->session->userdata('user_id'); 
        $msrtc_db_name = MSRTC_DB_NAME;
		$this->datatables->select('u.id,CONCAT(u.first_name," ",u.last_name) as agent_name,u.agent_code,u.lock_status,u.lock_reason,(select CONCAT(first_name," ",last_name) from users where id=u.lock_user_id) as lock_agent_name,(select CONCAT(first_name," ",last_name) from users where id=u.unlock_user_id) as unlock_agent_name,mu.machine_status as machine_status');
		$this->datatables->from('users u');
		$this->datatables->where('u.kyc_verify_status','Y');
		$this->datatables->where('u.status','Y');
		$this->datatables->where("u.agent_code like '%AGT%'");
		$this->datatables->join("$msrtc_db_name.users mu","u.agent_code = mu.agent_code");
		if($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID)
        {
            $level    = MASTER_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }
        if($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
        {
            $level    = AREA_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }
		if($this->session->userdata('role_id') == DISTRIBUTOR)
        {
            $level    = POS_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }

        $this->datatables->add_column('action', '<a href="javascript:void" class="del_btn" ref="$1" data-ref="$2" data-status="$3"><i class="fa fa-square-o"></i> </a>', 'id,lock_status,machine_status');
        $data = $this->datatables->generate('json');
        echo $data;		
	}

	public function change_status () {
		$input = $this->input->post();
		
		$this->users_model->id = $input['id'];
		$this->users_model->lock_status = $input['postyn'];
		

		if($input['postyn'] == "Y") {
			$this->users_model->lock_user_id = $input['lock_user_id'];
			$this->users_model->lock_reason = $input['reason'];
			$msg = "Locked";
		} else if($input['postyn'] == "N") {
			$this->users_model->unlock_user_id = $input['unlock_user_id'];
			$msg = "Unlocked";
		}
		$id = $this->users_model->save();
		if($id>0) {
			 $data["flag"] = '@#success#@';
			$this->session->set_flashdata('msg_type','success');
			$this->session->set_flashdata('msg','User '.$msg);

		} else {
            $data["flag"] = '@#failed#@';
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'User could not be locked.');
        }
        data2json($data);
	}

}