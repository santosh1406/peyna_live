<?php

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array('api_provider/etravelsmart_api','api_provider/its_api','api_provider/hrtc_api.php','rediscache'));
        $this->load->model(array('bus_source_destination_model','state_model', 'bus_stop_master_model', 'bus_type_model','travel_destinations_model', 'operator_commission_model', 'agent_commission_model','boarding_stop_model','banner_model','cancellation_charges_model','wallet_model','credit_account_detail_model','agent_commission_template_log_model','search_trans_model'));
        check_front_access();
    }

    public function index() {
        $this->search_new();
    }

    public function search_new() {
        $data = array();
        $is_true = TRUE;
        $currentDate = date('Y-m-d');
        $from = '';
        $to = '';        
        $last_search_result = $this->search_trans_model->column('from,to')->where(array('userid' => $this->session->userdata('user_id')))->order_by('datetime','desc')->limit('1')->as_array()->find_all();
        if(!empty($last_search_result)) {
            $from = $last_search_result[0]['from'];
            $to = $last_search_result[0]['to'];
        }
        $data['filter_data'] = array(
                                    "triptype" => 'single',
                                    "from" => $from,
                                    "to" => $to,
                                    "date_of_jour" => date('d-m-Y'),
                                    "date_of_arr" => '',
                                    "provider" => 'all',
                                    "searchbustype" => 'all'
                                );
        
        if($is_true) {
            
            $data['enable_discount'] = $DiscountData['enable_discount'];
            $data['wallet_bs']       = $this->wallet_model->where(array('user_id' => $this->session->userdata('user_id')))->find_all();
            $actual_wallet_balance   = (!empty($data['wallet_bs'])) ? $data['wallet_bs'][0]->amt : 0;
            $data['wallet_agent_balance_show']  = $actual_wallet_balance;
            
            load_front_view(HOME_SEARCH_VIEW, $data);
        }
        else {
            $this->session->set_flashdata("msg_type","error");
            $this->session->set_flashdata("msg","Onward journey must be before return journey.");
            redirect(base_url());
        }
    }

    public function bus_services() {
        $data = array();
        $comm_id_data = array();
        $data["bus_detail"] = array();
        $data["time_fare"]["bus_detail"] = array("min_fare" => 0,
            "max_fare" => 1,
            "min_dep_tm" => 0,
            "max_dep_tm" => 0
        );

        $min_max_fare   = [0];
        $min_max_dep_tm = [0];
        $merged_bus_services = array();

        $time_range = array(array('start' => '04:00:00', 'end' => '11:59:00', "shift" => "morning"),
            array('start' => '12:00:00', 'end' => '15:59:00', "shift" => "afternoon"),
            array('start' => '16:00:00', 'end' => '18:59:00', "shift" => "evening"),
            array('start' => '19:00:00', 'end' => '23:59:00', "shift" => "night"),
            array('start' => '00:00:00', 'end' => '03:59:00', "shift" => "mid_night")
        );
        $input = $this->input->post();

        $config = array(
            // array( 'field'   => 'from',  'label'   => 'From Stop', 'rules'   => 'trim|required'),
            array('field' => 'from', 'label' => 'From Stop', 'rules' => 'xss_clean|required', 'errors' => array('required' => 'Provide From Location')),
            array('field' => 'to', 'label' => 'To Stop', 'rules' => 'xss_clean|required', 'errors' => array('required' => 'Provide Destination Location')),
            array('field' => 'date_of_jour', 'label' => 'Date', 'rules' => 'xss_clean|trim|required'),
        );

        $user_data['from'] = $input['from'];
        $user_data['to'] = $input['to'];
        $user_data['date_of_jour'] = $input['date_of_jour'];
        $user_data['date'] = date("Y-m-d", strtotime($input['date_of_jour']));
        
        $insert_search = $this->last_search_insert($user_data);
        
        /* DONT DELETE BELOW COMMENTED LINE
         * Below line is used at the time 
         * when you want to fetch onwards and return journey 
         * at the same time
         */

        // $user_data['date_of_arr'] = isset($input['date_of_arr']) ? $input['date_of_arr'] : "";
        // $user_data['date_arrival'] = (isset($input['date_of_arr']) && ($input['date_of_arr'] != "")) ? date("Y-m-d", strtotime($input['date_of_arr'])) : "";
        $data['user_data'] = $user_data;
        if (form_validate_rules($config, $input) == FALSE) {
            $merged_bus_services['error'] = $this->form_validation->error_array();
            $merged_bus_services['status'] = "error";
        } 
        else {
      
            // ------Operator Condition Start--------- 
            if ($input['provider'] == "all" || $input['provider'] == "operator") {
                $tdata['journey']['bus_detail'] = $this->travel_destinations_model->get_search($user_data);
                if(count($tdata['journey']['bus_detail'])) {
                    $data['user_data']["from_stop_code"] = $tdata['journey']['bus_detail'][0]["from_stop_code"];
                    $data['user_data']["to_stop_code"] = $tdata['journey']['bus_detail'][0]["to_stop_code"];

                    //get upsrtc cancellation charges
                    $rsrtc_cacellation_charges = $this->cancellation_charges_model->cancellation_charges("1");
                    $upsrtc_cacellation_charges= $this->cancellation_charges_model->cancellation_charges("2");
                    $msrtc_cacellation_charges = $this->cancellation_charges_model->cancellation_charges("3");
                    //get msrtc cancellation charges
                }

                /* DONT DELETE BELOW COMMENTED LINE
                 * Below line if($user_data['date_arrival'] != "") condition is used at the time 
                 * when you want to fetch onwards and return journey 
                 * at the same time
                 */
                // if($user_data['date_arrival'] != "")
                // {
                //     $return_journey_data['from'] = $input['to'];
                //     $return_journey_data['to'] = $input['from'];
                //     $return_journey_data['date'] = $user_data['date_arrival'];
                //     $tdata['journey']['return_bus_detail'] = $this->travel_destinations_model->get_search($return_journey_data);
                // }

                $travel = array();
                $bus_type = array();
                $dept_tm = array();
                $diff_in_sec = array();

                $stu_total_seats = 0;
                $min_fare = 0;
                $max_fare = 1;
                $min_max_fare = [];
                $min_dep_tm = 0;
                $max_dep_tm = 0;
                $min_max_dep_tm = [];
                $op_name_array = [];
                $op_trip_compare = [];
                $is_bus_type_filter_on = in_array($input['searchbustype'], ["ac", "non_ac"]) ? true : false;

                foreach ($tdata['journey'] as $jkey => $jvalue) {
                    foreach ($jvalue as $key => $value) {
                        if(!in_array($value['op_trip_no'],$op_trip_compare)) {
                            $op_trip_compare [] = $value['op_trip_no'];
                            $to_proceed = true;
                            if ($is_bus_type_filter_on) {
                                /*
                                 * NOTICE : Extra array manipulated here. To Matched with ETS & global function "check_search_bus_type"
                                 */

                                $extra_array["mapped_bus_types"] = [trim($value["bus_type_name"]) => trim($value["bus_type_name"])];
                                $extra_array["busType"] = trim($value["bus_type_name"]);
                                $to_proceed = check_search_bus_type($input, $extra_array);
                            }

                            if ($to_proceed) {
                                $value['op_name'] = ucwords(str_replace("'", "", $value['op_name']));

                                if (!in_array($value['op_name'], $travel)) {
                                    $travel[] = $value['op_name'];
                                }

                                if (!in_array($value['dept_tm'], $dept_tm)) {
                                    $dept_tm[] = $value['sch_departure_tm'];
                                }
                                //retrive trip wise  boarding stop detail
                                $dept_tm_his = date("H:i:s", strtotime($value['dept_tm']));
                                
                                //MSRTC NOT PROVIDING BOARDING AND ALIGHTING STOPS
                                $op_name_array[] = strtolower($value['op_name']);
                                $bord_data = array();
                                if (strtolower($value['op_name']) != "msrtc") {
                                    $bord_data = $this->boarding_stop_model->getstops($value['trip_no'], $value['from_stop_code'], $value['op_bus_type_cd']);
                                }

                                $data[$jkey][$key] = $value;
                                $data[$jkey][$key]["from_stop"] = ucwords($value["from_stop"]);
                                $data[$jkey][$key]["to_stop"] = ucwords($value["to_stop"]);

                                $data[$jkey][$key]['barr'] = $bord_data;
                                $data[$jkey][$key]['str_dep_tm'] = strtotime($value['dept_tm']);
                                $data[$jkey][$key]['str_dep_tm_his'] = date('h:i A', strtotime($value['sch_departure_tm']));
                                $data[$jkey][$key]['arrival_time'] = date('h:i A', strtotime($value['arrival_time']));
                                $data[$jkey][$key]['arrival_date'] = date('d M Y', strtotime($value['arrival_time']));
                                $data[$jkey][$key]['arrival_date_time'] = $value['arrival_time'];
                                // $data[$jkey][$key]['str_dep_tm_hm'] = trim(hoursToMinutes($value['sch_departure_tm']));
                                $data[$jkey][$key]['str_dep_tm_hm'] = strtotime($user_data['date'] . " " . $value['sch_departure_tm']);

                                $data[$jkey][$key]['provider_id'] = $value['provider_id'];
                                $data[$jkey][$key]['provider_type'] = $value['provider_type'];
                                $data[$jkey][$key]['op_bus_type_cd'] = $value['op_bus_type_cd'];
                                /*
                                 * NOTICE: MOST IMP
                                 *
                                 *
                                 * BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
                                 * THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
                                 * CHANGE THIS AFTER DISCUSSION
                                 */
                                // * ************CONVEY CHARGE STATIC LOGIC START **************** 
                                $extra_value = [
                                    "op_bus_type_cd" => $value['op_bus_type_cd'],
                                ];
                                $actual_fare = $fare_with_conveycharge = getConveyCharges($value['seat_fare'], strtolower($value['op_name']), $extra_value);
                                // * ************CONVEY CHARGE STATIC LOGIC END **************** 

                                // * *****temp discount logic start***** 
                                $data[$jkey][$key]['service_discount'] = false;
                                
                                // * *****temp discount logic end*****
                                
                                $data[$jkey][$key]['seat_fare'] = $fare_with_conveycharge;
                                $data[$jkey][$key]['actual_seat_fare'] = $actual_fare;
                                $data[$jkey][$key]['mticket_allowed'] = false;
                                $data[$jkey][$key]['cancellation_policy'] = "";
                                if (strtolower($value['op_name']) == "upsrtc") {
                                    $data[$jkey][$key]['cancellation_policy'] = $upsrtc_cacellation_charges;
                                }
                                else if (strtolower($value['op_name']) == "msrtc") {
                                    $data[$jkey][$key]['cancellation_policy'] = $msrtc_cacellation_charges;
                                }
                                else if (strtolower($value['op_name']) == "rsrtc") {
                                    $data[$jkey][$key]['cancellation_policy'] = $rsrtc_cacellation_charges;
                                }
                                $data[$jkey][$key]['inventory_type'] = "";

                                
                                $tmdiff = calculate_date_diff($value["dept_tm"], $value["arrival_time"]);
                                $days = $tmdiff->format('%d');
                                $hours = $tmdiff->format('%h');
                                $minutes = $tmdiff->format('%i');

                                $travel_time = (($days > 0) ? $days . " day " : "") . (($hours > 0) ? $hours . " hours " : "") . (($minutes > 0) ? $minutes . " minutes" : "");
                                $short_travel_time = (($days > 0) ? $days . " D " : "") . (($hours > 0) ? $hours . " H " : "") . (($minutes > 0) ? $minutes . " M" : "");
                                $new_time_to_show = calculate_travel_duration($tmdiff);

                                $data[$jkey][$key]["travel_time"] = $new_time_to_show;
                                $data[$jkey][$key]["short_travel_time"] = $new_time_to_show;
                                $data[$jkey][$key]["bus_travel_duration"] = round(abs(strtotime($value["arrival_time"]) - strtotime($value["dept_tm"])), 2);
                                $diff_in_sec[] = round(abs(strtotime($value["arrival_time"]) - strtotime($value["dept_tm"])), 2);
                                // * *********** total time required to travel calculation end************* 
    			   
                                // * *********** For Travel Duration Filter Start************* 
                                $travel_duration = "";
                                // $travel_duration = ($hours <= 6) ? "six" : ($hours > 6 && $hours <= 12) ? "twelve" : ($hours > 12 && $hours <= 24) ? "twentyfour" : ($hours > 24) ? "more" : "";
                                $travel_duration = "";
                                if ($days == 0 && $hours <= 6) {
                                    $travel_duration = "six";
                                }
                                else if ($days == 0 && $hours > 6 && $hours <= 12) {
                                    $travel_duration = "twelve";
                                }
                                else if ($days == 0 && $hours > 12 && $hours <= 24) {
                                    $travel_duration = "twentyfour";
                                }
                                else if ($days > 0) {
                                    $travel_duration = "more";
                                }

                                $data[$jkey][$key]["travel_duration"] = $travel_duration;
                                // * *********** For Travel Duration Filter End*************** 

                                $min_max_fare[] = $fare_with_conveycharge;
                                $min_max_dep_tm[] = $data[$jkey][$key]['str_dep_tm_hm'];
                                $stu_total_seats += $value["tot_seat_available"];
                                //                             * ***** to claculate shift ****** 
                                foreach ($time_range as $i => $shift_time) {
                                    if (strtotime($time_range[$i]['start']) <= strtotime($dept_tm_his) && strtotime($time_range[$i]['end']) >= strtotime($dept_tm_his)) {
                                        $shift = $shift_time["shift"];
                                    }
                                }

                                $data[$jkey][$key]['shift'] = $shift;
                                // * ******** end ****************** 
                            }
                        }
                    }
                }

                $data["filter_data"] = [];
                $data["filter_data"]["operator"] = $travel;
                $data["filter_data"]["bus_type"] = $bus_type;

                $min_max_fare = (!empty($min_max_fare)) ? makeArrayUnique($min_max_fare) : [0];
                $min_max_dep_tm = (!empty($min_max_dep_tm)) ? makeArrayUnique($min_max_dep_tm) : [0];

                $data["time_fare"]["bus_detail"]['min_fare'] = (float) min($min_max_fare);
                $data["time_fare"]["bus_detail"]['max_fare'] = (float) max($min_max_fare);

                $data["time_fare"]["bus_detail"]['min_dep_tm'] = (float) min($min_max_dep_tm);
                $data["time_fare"]["bus_detail"]['max_dep_tm'] = (float) max($min_max_dep_tm);
            }

            $stu_diff_in_sec = makeArrayUnique($diff_in_sec);
            $op_name_array = makeArrayUnique($op_name_array);

            $merged_bus_services["time_fare"]["stu_range"] = [
                "min_duration" => (!empty($stu_diff_in_sec)) ? sec2hms(min($stu_diff_in_sec)) : 0,
                "max_duration" => (!empty($stu_diff_in_sec)) ? sec2hms(max($stu_diff_in_sec)) : 0,
                "min_fare" => (float) min($min_max_fare),
                "max_fare" => (float) max($min_max_fare),
                "min_dep_tm" => (float) min($min_max_dep_tm),
                "start_at" => date("h:i A", (float) min($min_max_dep_tm)),
                "mticket_allowed" => false,
                "max_dep_tm" => (float) max($min_max_dep_tm),
                "stu_total_seats" => $stu_total_seats,
                "stu_total_buses" => count($data["bus_detail"]),
                "op_name_array" => $op_name_array,
            ];

            // ------Operator Condition End--------- 

            //calling etravelsmart
            $etravels_data = array();
            if ($this->config->item("enable_etravelsmart") && ($input['provider'] == "all" || $input['provider'] == "api")) {
                $etravels_data = $this->etravelsmart_api->getAvailableBuses($input,$comm_id_data);
                $etravels_data["filter_data"] = [];
                $etravels_data["filter_data"]["operator"] = [];
                $etravels_data["filter_data"]["bus_type"] = [];
            } 
    	    else {

                    $etravels_data["filter_data"] = [];
                    $etravels_data["filter_data"]["operator"] = [];
                    $etravels_data["filter_data"]["bus_type"] = [];

                    $etravels_data["bus_detail"] = array();
                    $etravels_data["time_fare"]["bus_detail"] = array(
                        "min_fare" => 0,
                        "max_fare" => 0,
                        "min_dep_tm" => 0,
                        "max_dep_tm" => 0
                    );
                }
            
	        // * ***********Calling HRTC Start Here************************ 
            $hrtc_bus_services["bus_detail"] = [];
            $hrtc_bus_services["time_fare"]["bus_detail"] = [];
            $hrtc_bus_services["filter_data"] = [];
            $hrtc_bus_services["filter_data"]["operator"] = [];
            $hrtc_bus_services["filter_data"]["bus_type"] = [];

            if ($this->config->item("enable_hrtc_services")) {
                // $hrtc_availability = $this->bus_source_destination_model->where(["source_name" => $input["from"], "destination_name" => $input["to"]])->find_all(); 
                $hrtc_availability = $this->bus_source_destination_model->like("source_name", $input["from"], "both")->like("destination_name", $input["to"], "both")->find_all();
                if ($hrtc_availability) {
                    $hrtc_bus_services = $this->hrtc_api->getAvailableBuses($input,$comm_id_data); // input data and commission data
                    if (!empty($hrtc_bus_services["bus_detail"])) {
                        if (isset($hrtc_bus_services["extra_detail"]) && isset($hrtc_bus_services["extra_detail"]["stu_details"])) {
                            $merged_bus_services["time_fare"]["stu_range"] = $hrtc_bus_services["extra_detail"]["stu_details"];
                        }
                    }
                }
            }
            // * ***********Calling HRTC End   Here************************ 
            
            // * ***********ITS Start Here************************ 
            if (($input['provider'] == "all" || $input['provider'] == "api") && $this->config->item("enable_its_services")) { 
                $its_availability = $this->bus_source_destination_model->where("api_provider_id",ITS_PROVIDER_ID)->where("source_name", $input["from"])->where("destination_name", $input["to"])->as_array()->find_all();
                $input["source_destination_data"] = $its_availability[0];
                if (!empty($its_availability)) { 
                    $input["etravels_data"] = !empty($etravels_data['bus_detail']) ? $etravels_data :array();
                    $itsRes = $this->its_api->getAvailableBuses($input,$comm_id_data);
                    $its_data = $itsRes['its_data'];
                    if(!empty($itsRes['ets_data'])) {
                        $etravels_data = $itsRes['ets_data'];
                    }
                    unset($input['etravels_data']);
                }
                else {
                    $its_data["bus_detail"] = array();
                    $its_data["time_fare"]["bus_detail"] = array(
                        "min_fare" => 0,
                        "max_fare" => 0,
                        "min_dep_tm" => 0,
                        "max_dep_tm" => 0
                    );

                }
                unset($input["source_destination_data"]);
            }
            else {
                $its_data["bus_detail"] = array();
                $its_data["time_fare"]["bus_detail"] = array(
                    "min_fare" => 0,
                    "max_fare" => 0,
                    "min_dep_tm" => 0,
                    "max_dep_tm" => 0
                );
            }


            if(!empty($etravels_data['compare_commission_data'])) {
                unset($etravels_data['compare_commission_data']);
            }
            
            // * ***********Calling ITS End   Here************************ 
            
            //* *******Merging Bus Services Start********************** 
            $merged_bus_services["user_data"] = $data['user_data'];
            
            $merged_bus_services["bus_detail"] = array_merge($data["bus_detail"], $etravels_data["bus_detail"], $its_data["bus_detail"], $hrtc_bus_services["bus_detail"]);
            $merged_bus_services["time_fare"]["bus_detail"] = array_merge_recursive($etravels_data["time_fare"]["bus_detail"],$its_data["time_fare"]["bus_detail"], $data["time_fare"]["bus_detail"], $hrtc_bus_services["time_fare"]["bus_detail"]);

            $merged_bus_services["time_fare"]["bus_detail"] = array(
                "min_fare"   => min($merged_bus_services["time_fare"]["bus_detail"]["min_fare"]),
                "max_fare"   => max($merged_bus_services["time_fare"]["bus_detail"]["max_fare"]),
                "min_dep_tm" => min($merged_bus_services["time_fare"]["bus_detail"]["min_dep_tm"]),
                "max_dep_tm" => max($merged_bus_services["time_fare"]["bus_detail"]["max_dep_tm"])
            );
            //* *******Merging Bus Services End************************* 

            //$merged_bus_services["filter_data"]["operator"] = array_merge($etravels_data["filter_data"]["operator"], $data["filter_data"]["operator"], $hrtc_bus_services["filter_data"]["operator"]);
            //$merged_bus_services["filter_data"]["bus_type"] = array_merge($etravels_data["filter_data"]["bus_type"], $data["filter_data"]["bus_type"], $hrtc_bus_services["filter_data"]["bus_type"]);
            $merged_bus_services["filter_data"]["operator"] = array();
            $merged_bus_services["filter_data"]["bus_type"] = array();
            //insert into user action log
            $users_action_log = array("user_id"  => $this->session->userdata("user_id"),
                                        "name"   => $this->session->userdata("display_name"),
                                        "url"    => "",
                                        "action" => "searched bus service",
                                        "message"=> "Searched bus from <span class='navy_blue location'>" . ucfirst($input['from']) . "</span> to <span class='navy_blue location'>" . ucfirst($input['to']) . "</span> for the day <span class='navy_blue date'>" . date("jS F Y", strtotime($input['date_of_jour'])) . "</span>"
                                    );

            // users_action_log($users_action_log);

            /* $data['status'] =  "success";
              data2json($data); */

            $merged_bus_services['status'] = "success";

            // enable_profiler();
            data2json($merged_bus_services);
        }
    }
    
    public function filter() {
        $input = $this->input->post();
        $data['bus_detail'] = $this->travel_destinations_model->get_search($input);
        // show($data['bus_detail'],1);
        $response['flag'] = '@#success#@';
        $response['view'] = $this->load->view(HS_BUS_DETAIL_AJAX, $data, TRUE);

        data2json($response);
    }

    public function autocomplete_bus_stop($q = '')
    {
        $input = $this->input->get();

        // $this->db->select('bus_stop_id as value,bus_stop_name as label');
        $this->db->select('bus_stop_name as value,bus_stop_name as label');
        // $this->db->like('bus_stop_name', $input['q'], 'both');
        $this->db->like('bus_stop_name', $input['q'], 'after');
        $this->db->from('bus_stop_master');

        $query = $this->db->get();
        $data = $query->result_array();

        if (count($data) > 0) {
            data2json($data);
        }
    }

    public function __destruct() {
        $this->db->cache_off();
    }

    function page($page = '')
    {
        if($page == '')
        {
            redirect(base_url());
        }

        $data = array();

        $data['page'] = $page;
        load_front_view(COMING_SOON, $data);
    }
    
    function last_search_insert($searchdata) {
            $search_detail = array("from" => $searchdata['from'],
                "to" => $searchdata['to'],
                "datetime" => date("Y-m-d H:i:s"),
                "dateofsearch" => $searchdata['date'],
                "userid" => $this->session->userdata("user_id"),
                "updated_datetime" => '0'
            );
            $search_trans_id = $this->search_trans_model->insert($search_detail);

            if ($search_trans_id > 0) {
                return $search_trans_id;
            }
    }

}
