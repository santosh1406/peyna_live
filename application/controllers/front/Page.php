<?php

class Page extends CI_Controller {

    public function __construct() {
    	parent::__construct();
        $this->load->model(array('newsletter_model','contactus_model'));
    }

    function contact_us()
    {
    	$data = array();
        $data["metadata_title"] = $this->config->item('title', 'contact_us_metadata');
        $data["metadata_description"] = $this->config->item('description', 'contact_us_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'contact_us_metadata');
    	load_front_view(CONTACT_US, $data);
    }

    function services()
    {
    	$data = array();
    	load_front_view(SERVICES);
    }

    function about_us()
    {
    	$data = array();

        $data["metadata_title"] = $this->config->item('title', 'about_metadata');
        $data["metadata_description"] = $this->config->item('description', 'about_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'about_metadata');
        
    	load_front_view(ABOUT_US, $data);
    }

    function faq()
    {
        $data = array();

        $data["metadata_title"] = $this->config->item('title', 'faq_metadata');
        $data["metadata_description"] = $this->config->item('description', 'faq_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'faq_metadata');

        load_front_view(FAQ, $data);
    }	
     function faq_static()
    {
        $data = array();

        $data["metadata_title"] = $this->config->item('title', 'faq_metadata');
        $data["metadata_description"] = $this->config->item('description', 'faq_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'faq_metadata');

        $CI = & get_instance();
        $CI->load->view(FAQ_STATIC, $data);
    }	

    function testimonials()
    {
        $data = array();
        load_front_view(TESTIMONIALS);
    }

    function privacy_policy()
    {
        $data = array();
        $data["metadata_title"] = $this->config->item('title', 'privacy_policy_metadata');
        $data["metadata_description"] = $this->config->item('description', 'privacy_policy_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'privacy_policy_metadata');
        load_front_view(PRIVACY_POLICY, $data);
    }
     function privacy_policy_static()
    {
        $data = array();
        $data["metadata_title"] = $this->config->item('title', 'privacy_policy_metadata');
        $data["metadata_description"] = $this->config->item('description', 'privacy_policy_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'privacy_policy_metadata');
          $CI = & get_instance();
        $CI->load->view(PRIVACY_POLICY_STATIC, $data);
    }

    function terms_and_condition()
    {
        $data = array();

        $data["metadata_title"] = $this->config->item('title', 'tc_metadata');
        $data["metadata_description"] = $this->config->item('description', 'tc_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'tc_metadata');

        load_front_view(TERMS_AND_CONDITION, $data);
    }
    
    function terms_and_condition_static()
    {
        $data = array();

        $data["metadata_title"] = $this->config->item('title', 'tc_metadata');
        $data["metadata_description"] = $this->config->item('description', 'tc_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'tc_metadata');

        $CI = & get_instance();
        $CI->load->view(TERMS_AND_CONDITION_STATIC, $data);
    }


    function newsLetter()
    {
        $response = array();
        $input = $this->input->post();
        $email = $input["newsletteremail"];
        $is_email_exists = $this->newsletter_model->where("email",$email)->find_all();

        if($email != "")
        {
            if(!$is_email_exists)
            {
                $this->newsletter_model->email = $email;
                $this->newsletter_model->ip_address = $this->session->userdata("ip_address");
                $this->newsletter_model->user_agent = $this->session->userdata("user_agent");
                $id = $this->newsletter_model->save();

                if($id)
                {
                    $response['flag'] = '@#success#@';
                    $response['msg'] = 'Newsletter activated successfully.';
                }
                else
                {
                    $response['flag'] = '@#error#@';
                    $response['msg'] = 'Some problem occured. Please try again.';
                }
            }
            else
            {
                $response['flag'] = '@#error#@';
                $response['msg'] = 'Newsletter already activated.';
            }
        }
        else
        {
            $response['flag'] = '@#error#@';
            $response['msg'] = 'Please provide email Id.'; 
        }

        data2json($response);
    }



    function contactus_message()
    {
		$input = $this->input->post(NULL, TRUE);
		if(!empty($input))
        {            
            $uname    = trim($input["uname"]);
			$uemail   = trim($input["uemail"]);
            $umessage = trim($input["umessage"]);			
			$g_recaptcha_response = trim($input['g-recaptcha-response']);
			
			if($uname != "" && $uemail != "" && $umessage != "" && $g_recaptcha_response != "")
			{
				$g_captcha_data = google_captcha($g_recaptcha_response);
				if ($g_captcha_data->success) 
				{					
					$this->load->library('email');
					if($this->email->valid_email($uemail))
					{
						if(strlen($umessage) <= 300)
						{							
							$this->contactus_model->name = $uname;
							$this->contactus_model->email = $uemail;
							$this->contactus_model->message = $umessage;
							$this->contactus_model->ip_address = $this->session->userdata("ip_address");
							$this->contactus_model->user_agent = $this->session->userdata("user_agent");
							$id = $this->contactus_model->save();							
							if($id)
							{
								eval(CONTACT_US_EMAIL);
								$mail_array = array( "subject" => "Query From : ".$uemail,
													 "to" => $contactus_email_array,
													 "message" => $umessage
													);
								$result = send_mail($mail_array);							
								$this->session->set_flashdata('msg_type', 'success');
								$this->session->set_flashdata('msg', 'Thank you for contacting us. We will respond to you as soon as possible.');
								redirect(base_url()."contact_us");		
							}
							else
							{
								$this->session->set_flashdata('msg_type', 'error');
								$this->session->set_flashdata('msg', 'Some problem occured. Please try again');
								redirect(base_url()."contact_us");
							}
						}
						else
						{
							$this->session->set_flashdata('msg_type', 'error');
							$this->session->set_flashdata('msg', 'Message cannot exceed 300 characters');
							redirect(base_url()."contact_us");
						}
					}
					else
					{
						$this->session->set_flashdata('msg_type', 'error');
						$this->session->set_flashdata('msg', 'Please enter valid email');
						redirect(base_url()."contact_us");
					}
				}
				else 
				{
					$this->session->set_flashdata('msg_type', 'error');
					$this->session->set_flashdata('msg', 'Please verify captcha');	
					redirect(base_url()."contact_us");
				}				
			}
			else 
			{
				$this->session->set_flashdata('msg_type', 'error');
				$this->session->set_flashdata('msg', 'All fields are required');
				redirect(base_url()."contact_us");
			}
		}
        else
        {
			$this->session->set_flashdata('msg_type', 'error');
			$this->session->set_flashdata('msg', 'Some problem occured. Please try again');
			redirect(base_url()."contact_us");
        }       
    }
}