<?php
class Booking extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->library(array('nusoap_lib','cca_payment_gateway','api_provider/etravelsmart_api','api_provider/its_api','api_provider/hrtc_api',"soap_api/upsrtc_api","soap_api/msrtc_api","soap_api/rsrtc_api"));
        $this->load->helper('soap_data');
        $this->load->model(array('op_seat_layout_model','wallet_model', 'wallet_trans_model', 'soap_client_model', 'booking_model', 'bus_stop_master_model', 'travel_destinations_model', 'tickets_model', 'ticket_details_model', 'users_model','boarding_stop_model', 'op_config_model','mail_template_model', 'ticket_dump_model', 'cancellation_charges_model', 'bus_type_convey_charge_master_model', 'ticket_tax_trans_model', 'convey_charge_type_master_model', 'payment_transaction_fss_model','favourite_list_model','return_process_ticket_mapping_model','return_ticket_mapping_model', 'common_model', 'users_model', 'etravels_bus_service_model', 'ticket_id_proof_model', 'agent_tickets_model','tracking_request_response_model','bus_service_stops_model','coupon_applyed_to_ticket','ticket_metadata_model','agent_commission_template_log_model','wallet_trans_log_model','tickets_temp_model','commission_calculation_model'));
        check_front_access();
    }

    /*Author : Suraj Rathod
    *Function : get_seat_layout()
    *Detail : To get seat layout and all details
    */
    public function get_seat_layout() {
        
        $input = $this->input->post();
        $data = array();
        $stop_det = array();
        $available_seats_response = array();
        
        if($input["provider_id"] != "" && $input["provider_type"] != "") {
            $details = $this->common_model->get_provider_lib($input["provider_id"],$input["provider_type"],$input["op_id"]);
            if($details) {
                $library_name =  strtolower(trim($details['library'])).'_api';
                $data = $this->{$library_name}->get_seat_layout($input);
            }            
        }
        
        data2json($data);
    }


    /*
    *Author : Suraj Rathod
    *Function : temp_booking()
    *Detail : To temporary book the ticket.
    */
    public function temp_booking() 
    {
        $this->session->unset_userdata('available_detail');
        $this->session->unset_userdata('available_detail_return');
        
        $data = array();
        $onwardJourney = array();
        $returnJourney = array();
        $bus_type = array();
        $rbus_type = array();
        $data['userid'] = '';
        $input = $this->input->get();
        $currentDate = date('Y-m-d');
        
        if(isset($input["seats"]) && !empty($input["seats"])) {
            $details = $this->common_model->get_provider_lib($input["provider_id"], $input["provider_type"],$input["op_id"]);
            $input["library"] = ($details) ? strtolower(trim($details['library'])) : "";
           
            if(isset($input["library"]) && $input["library"] != "") {
                $library_name =  strtolower(trim($details['library'])).'_api';
                $onward_journey_data = $this->{$library_name}->getSeatAvailability($input); 
				
                if(!empty($onward_journey_data) && isset($onward_journey_data["flag"]) && $onward_journey_data["flag"] == "success") {
                    if(strtolower(trim($details['role_name'])) == "operator") {
                        if(strtolower($input["op_name"]) != "hrtc") {
                            $bus_type = $this->travel_destinations_model->get_bus_type_by_trip_no($input["trip_no"]);
                        }
                        else {
                            $bus_type = $onward_journey_data["operator_bus_type_detail"];
                        }
                    }
                    else if(strtolower(trim($details['role_name'])) == "api provider") {
                        $etravels_data = $this->session->userdata("provider_available_bus");
                        $bus_type[0]["op_bus_type_name"] = $etravels_data[$input["trip_no"]]["bus_type"];
                    }
                    
                    if(count($bus_type) > 0)
                        $data['bus_type'] = $bus_type[0];
                    else
                        $data['bus_type'] = "NA";

                    $onwards_seat_availability  =   $onward_journey_data['SeatAvailabilityResponse'];

                    $onwardJourney = array(
                        'trip_no' => $input['trip_no'],
                        'from_stop_id' => (isset($onwards_seat_availability['fromStopId'])) ? $onwards_seat_availability['fromStopId']: "",//$onwards_seat_availability['fromStopId'],
                        'to_stop_id' => (isset($onwards_seat_availability['toStopId'])) ? $onwards_seat_availability['toStopId'] : "",//$onwards_seat_availability['toStopId'],
                        'sch_dept_time' => $onward_journey_data["trip_data"]["sch_departure_tm"],
                        'arrival_time' => $onward_journey_data["trip_data"]["arrival_time"],
                        'from' => $input['from'],
                        'to' => $input['to'],
                        'boarding_stop_name' => $input['boarding_stop_name'],
                        'destination_stop_name' => $input['destination_stop_name'],
                        'boarding_stop_cd' => $input['boarding_stop_cd'],
                        'destination_stop_cd' => $input['destination_stop_cd'],
                        'date' => $input['date'],
                        'fare' => $onwards_seat_availability['fare'],
                        'senior_citizen_fare' => (isset($onwards_seat_availability['seniorcitizenfare'])) ? $onwards_seat_availability['seniorcitizenfare'] :  $onwards_seat_availability['fare'],
                        'is_child_concession' => $onward_journey_data["trip_data"]["is_child_concession"],
                        'child_seat_fare' => $onwards_seat_availability['childFare'],
                        'sleeperFare' => (isset($onwards_seat_availability['sleeperFare'])) ? $onwards_seat_availability['sleeperFare'] : 0.00,
                        'op_name' => (isset($details['op_name']) && $details['op_name'] != "") ? $details['op_name'] : $input['op_name'], 
                        'library_name' => strtolower(trim($details['library'])),
                        'bsd' => $input["bsd"],
                        'dsd' => $input["dsd"],
                        'user_type' => $onward_journey_data["trip_data"]["op_id"],
                        'op_id' => $onward_journey_data["trip_data"]["op_id"],//IMP
			            'bus_type_id' => $input["bus_type_id"],// IMP for Discount calculation
                        'service_id' => $onward_journey_data["trip_data"]['op_trip_no'],
                        'inventory_type' => isset($onward_journey_data["trip_data"]['inventory_type']) ? $onward_journey_data["trip_data"]['inventory_type'] : "",
                        'seats_details' => isset($onwards_seat_availability['seats_details']) ? $onwards_seat_availability['seats_details'] : "",
                        'id_proof_required' => (isset($onward_journey_data["trip_data"]['id_proof_required'])) ? $onward_journey_data["trip_data"]['id_proof_required'] : false,     
                        'bus_type' => $data['bus_type'],
                        'provider_id' => $input["provider_id"],
                        'provider_type' => $input["provider_type"],
                        'available_seat' => $onwards_seat_availability['availableSeats']['availableSeat'],
                        'available_seat_fare' => (isset($onwards_seat_availability['available_seat_fare'])) ? $onwards_seat_availability['available_seat_fare'] : "",
                        'its_reference_number'=> (!empty($input["its_reference_number"]) ? $input["its_reference_number"] : '' )
                        
                    );

                    $this->session->set_userdata('available_detail', $onwardJourney); //session set to used on next pages(Till booking end);
                    // $available_detail = $this->session->userdata('available_detail'); 
                    
                    //Check if return journey start.
                    if(isset($input["rfrom"]) && $input["rfrom"] != "" && isset($input["rto"]) && $input["rto"] != "" && isset($input["rdate"]) && $input["rdate"] != "" && isset($input["rseats"]) && $input["rseats"] != "" && isset($input["rtrip_no"]) && $input["rtrip_no"] != "" && isset($input["rop_name"]) && $input["rop_name"] != "") {
                        $return_input = array("from" => $input["rfrom"],
                                               "to" => $input["rto"],
                                               "date" => $input["rdate"],
                                               "seats" => $input["rseats"],
                                               "trip_no" => $input["rtrip_no"],
                                               "op_name" => $input["rop_name"],
                                               "op_id" => $input["rop_id"],
					                           "bus_type_id" => $input["rbus_type_id"],// IMP for Discount calculation
                                               "destination_stop_name" => $input["rdestination_stop_name"],
                                               "destination_stop_cd" => $input["rdestination_stop_cd"],
                                               "boarding_stop_name" => $input["rboarding_stop_name"],
                                               "boarding_stop_cd" => $input["rboarding_stop_cd"],
                                               "is_return_journey" => true,
                                               "its_reference_number" => !empty($input["r_its_reference_number"]) ? $input["r_its_reference_number"] : ''
                                             );

                        $rdetails = $this->common_model->get_provider_lib($input["rprovider_id"],$input["rprovider_type"],$input["rop_id"]);
                        // $input["rlibrary"] = strtolower(trim($details['library']));
                        $rlibrary_name =  strtolower(trim($rdetails['library'])).'_api';
                        $return_journey_data = $this->{$rlibrary_name}->getSeatAvailability($return_input); 
                        
                        if(isset($return_journey_data["flag"]) && $return_journey_data["flag"] == "success") {
                            if(strtolower(trim($details['role_name'])) == "operator") {
                                $rbus_type = $this->travel_destinations_model->get_bus_type_by_trip_no($input["rtrip_no"]);
                            }
                            else if(strtolower(trim($details['role_name'])) == "api provider") {
                                $etravels_data_return = $this->session->userdata("provider_available_bus_return");
                                $rbus_type[0]["op_bus_type_name"] = $etravels_data_return[$input["rtrip_no"]]["bus_type"];
                            }

                            if(count($rbus_type) > 0)
                                $data['rbus_type'] = $rbus_type[0];
                            else
                                $data['rbus_type'] = "NA";


                            $returns_seat_availability  =   $return_journey_data['SeatAvailabilityResponse'];

                            $returnJourney = array(
                                                    'trip_no' => $input['rtrip_no'],
                                                    'from_stop_id' => (isset($returns_seat_availability['fromStopId'])) ? $returns_seat_availability['fromStopId'] : "",
                                                    'to_stop_id' => (isset($returns_seat_availability['toStopId'])) ? $returns_seat_availability['toStopId'] : "",
                                                    'sch_dept_time' => $return_journey_data["trip_data"]["sch_departure_tm"],
                                                    'arrival_time' => $return_journey_data["trip_data"]["arrival_time"],
                                                    'from' => $input['rfrom'],
                                                    'to' => $input['rto'],
                                                    'boarding_stop_name' => $input['rboarding_stop_name'],
                                                    'destination_stop_name' => $input['rdestination_stop_name'],
                                                    'boarding_stop_cd' => $input['rboarding_stop_cd'],
                                                    'destination_stop_cd' => $input['rdestination_stop_cd'],
                                                    'date' => $input['rdate'],
                                                    'fare' => $returns_seat_availability['fare'],
                                                    'senior_citizen_fare' => (isset($returns_seat_availability['seniorcitizenfare'])) ? $returns_seat_availability['seniorcitizenfare'] :  $returns_seat_availability['fare'],
                                                    'is_child_concession' => $return_journey_data["trip_data"]["is_child_concession"],
                                                    'child_seat_fare' => $returns_seat_availability['childFare'],
                                                    /*'op_name' => $input['rop_name'], */
                                                    'op_name' => (isset($rdetails['op_name']) && $rdetails['op_name'] != "") ? $rdetails['op_name'] : $input['rop_name'], 
                                                    'bsd' => $input["rbsd"],
                                                    'dsd' => $input["rdsd"],
                                                    'user_type' => $return_journey_data["trip_data"]["op_id"],
                                                    'library_name' => strtolower(trim($rdetails['library'])),
                                                    'op_id' => $return_journey_data["trip_data"]["op_id"],//Do not  delete this. Because this must be used later.
						                            'bus_type_id' => $input["rbus_type_id"],// IMP for Discount calculation
                                                    'service_id' => $return_journey_data["trip_data"]['op_trip_no'],
                                                    'inventory_type' => isset($return_journey_data["trip_data"]['inventory_type']) ? $return_journey_data["trip_data"]['inventory_type'] : "",
                                                    'seats_details' => isset($returns_seat_availability['seats_details']) ? $returns_seat_availability['seats_details'] : "",
                                                    'id_proof_required' => (isset($return_journey_data["trip_data"]['id_proof_required'])) ? $return_journey_data["trip_data"]['id_proof_required'] : false,

                                                    'bus_type' => $data['rbus_type'],
                                                    
                                                    'provider_id' => $input["rprovider_id"],
                                                    'provider_type' => $input["rprovider_type"],
                                                    'service_id' => $return_journey_data["trip_data"]['op_trip_no'],
                                                    'available_seat' => $returns_seat_availability['availableSeats']['availableSeat'],
                                                    'available_seat_fare' => (isset($returns_seat_availability['available_seat_fare'])) ? $returns_seat_availability['available_seat_fare'] : "",
                                                    'its_reference_number'=> (!empty($input["r_its_reference_number"]) ? $input["r_its_reference_number"] : '')
                                                );
                            
                            $this->session->set_userdata('available_detail_return', $returnJourney);
                        }
                    }
                    //Check if return journey end.

                    $data['journey_date_time'] = $onwardJourney["date"]." ".$onwardJourney["sch_dept_time"];
                    $data['journey_date'] = date("d M Y", strtotime($onwardJourney["date"]));
                    $data['arrival_time'] = ($onwardJourney["arrival_time"] != "") ? date("d M Y", strtotime($onwardJourney["arrival_time"])) : "NA";
                    $data['journey_date_hs'] = date("h:i A", strtotime($data['journey_date_time']));
                    $data['arrival_time_hs'] = ($onwardJourney["arrival_time"] != "") ? date("h:i A", strtotime($onwardJourney["arrival_time"])) : "NA";
                    
                    $is_seat_available = true;
                    $data['seats'] = $input['seats'];
                    // $bus_type = array();
                    $seat_booked = array();
                    $seat_already_booked = array();

                    $onwardJourney_available_seat = (!is_array($onwardJourney["available_seat"])) ? array($onwardJourney["available_seat"]) : $onwardJourney["available_seat"];
                
                    if(count($onwardJourney_available_seat) > 0) {
                        foreach ($input['seats'] as $berthno => $seatvalue) {
                            $seat_booked[$berthno] = $seatvalue;
                            if(!empty($onwardJourney_available_seat[$berthno]) && !(count(array_intersect($seatvalue, $onwardJourney_available_seat[$berthno])) == count($seatvalue)) ) {
                                $is_seat_available = false;
                                $seat_already_booked[$berthno] = $seatvalue;
                            } 
                        }
                    }
                    else {
                        $is_seat_available = false;
                    }
                    
                    //do process for return journey
                    if(!empty($returnJourney)) {
                        $data['rjourney_date_time'] = $returnJourney["date"]." ".$returnJourney["sch_dept_time"];
                        $data['rjourney_date'] = date("d M Y", strtotime($returnJourney["date"]));
                        $data['rarrival_time'] = date("d M Y", strtotime($returnJourney["arrival_time"]));
                        $data['rjourney_date_hs'] = date("h:i A", strtotime($data['rjourney_date_time']));
                        $data['rarrival_time_hs'] = date("h:i A", strtotime($returnJourney["arrival_time"]));
                        
                        $is_rseat_available = true;
                        $data['rseats'] = $input['rseats'];
                        // $rseat_booked = explode(',', $input['rseats']);
                        // $rbus_type = array();
                        $rseat_booked = array();
                        $rseat_already_booked = array();

                        $returnJourney_available_seat = (!is_array($returnJourney["available_seat"])) ? array($returnJourney["available_seat"]) : $returnJourney["available_seat"];

                        if(count($returnJourney_available_seat) > 0) {
                            foreach ($input['rseats'] as $rberthno => $rseatvalue) {
                                $rseat_booked[$rberthno] = $rseatvalue;
                                // if(!empty($returnJourney_available_seat) && !in_array($rsvalue, $returnJourney_available_seat))
                                if(!empty($returnJourney_available_seat[$rberthno]) && !(count(array_intersect($rseatvalue, $returnJourney_available_seat[$rberthno])) == count($rseatvalue))) {
                                    $is_rseat_available = false;
                                    $rseat_already_booked[$rberthno] = $rseatvalue;
                                    $data['is_rseat_available'] = 0;
                                } 
                            }

                            if($is_rseat_available) {
                                $data['rtrip_no'] = $returnJourney['trip_no'];
                                $data['rseat_booked'] = $rseat_booked;
                                $data['rfare'] = $returnJourney['fare'];
                                $data['available_detail_return'] = $returnJourney; 
                            }
                        }
                        else {
                            $is_rseat_available = false;
                        }
                    }
                    else {
                        $is_rseat_available = false;
                    }

                    $data['is_rseat_available'] = $is_rseat_available;

                    if($is_seat_available) {
                        $user_id = $this->session->userdata('user_id');
                        if(isset($user_id) && $user_id != "")
                        {   
                            $data['wallet_bs'] = $this->wallet_model->where(array('user_id' => $user_id))->find_all();
                            $wallet_balance = (!empty($data['wallet_bs'])) ? $data['wallet_bs'][0]->amt : 0;
                            $data['wallet_agent_balance_show'] = $wallet_balance;
                            
                        }
                        
                        $data['trip_no'] = $onwardJourney['trip_no'];
                        $data['seat_booked'] = $seat_booked;
                        $data['fare'] = $onwardJourney['fare'];
                        $data['available_detail'] = $onwardJourney;
                        load_front_view(PASSENGER_INFORMATION_VIEW, $data);
                        
                    }
                    else
                    {
                        $data['seat_already_booked'] = $seat_already_booked;
                        load_front_view(SEAT_UNAVAILABLE, $data);
                    }
                }
                else {
                    load_front_view(UNAVAILABLE);
                }
            }
            else {
                load_front_view(UNEXPECTED_ERROR);
            }
        }
        else {
            load_front_view(SEAT_UNAVAILABLE, $data);
        }
    }



    /* After name and email Form*/
    public function process_payment_confirm_booking()
    {  
        $input = $this->input->post(); 
        
        $data["is_agent_markup_onward"] = false;
        $data["is_agent_markup_return"] = false;
        $currentDate = date('Y-m-d');
        $final_total_amount = 0;
        $array_to_process = array();
        $data = array();
        
        if(!empty($input)) {
            /********************************* User exists or not start *************/
            if ($this->session->userdata('user_id') == '') {
                $email = $input['email_id'];
                $mobile_no = $input['mobile_no'];
                // Check user email in users table
                $this->users_model->email = $email;
                $user_exists = $this->users_model->select();
                // if user email id is not exists then add users
                if(empty($user_exists)) {
                    $this->users_model->email = $input['email_id'];
                    $this->users_model->mobile_no = $input['mobile_no'];
                    $this->users_model->role_id = USER_ROLE_ID;
                    $user_id = $this->users_model->save();
                    if($user_id > 0) {
                       $this->session->set_userdata("booking_user_id",$user_id); 
                    }
                    else {
                        $this->session->set_userdata("booking_user_id",""); 
                    }
                    $this->session->set_userdata("is_user_registered",false);
                }
                // if exists then use register user id
                else {
                    $user_id = $user_exists[0]->id;
                    $this->session->set_userdata("booking_user_id",$user_id);
                    if($user_exists[0]->password != "" && $user_exists[0]->salt != "") {
                        $this->session->set_userdata("is_user_registered",true);
                    }
                    else {
                        $this->session->set_userdata("is_user_registered",false);
                    }
                }
            }
            else {
                $user_id = $this->session->userdata("user_id");
                
                $this->wallet_model->user_id = $user_id;
                $walletdata = $this->wallet_model->select();
                    
                $user_account_balance = 0;
                
                $actual_wallet_balance = (!empty($walletdata)) ? $walletdata[0]->amt : 0;
                $data['wallet_balance'] = $actual_wallet_balance;
                $data['actual_wallet_balance'] = $actual_wallet_balance;
                
                $email = $input['email_id'];
                $mobile_no = $input['mobile_no'];
                $this->session->set_userdata("booking_user_id",$this->session->userdata("user_id"));
            }
            /********************************* User exists or not end ***************/
            eval(FME_AGENT_ROLE_ID);
            if((in_array($this->session->userdata("role_id"), $fme_agent_role_id)) || $this->session->userdata('role_id') == ORS_AGENT_USERS_ROLE_ID) {
                if(empty($input["email_id"])   && !empty($input["agent_email_id"])) {
                    $input["email_id"] = $input["agent_email_id"];
                }
            }
            if(isset($input["email_id"]) && isset($input["mobile_no"]) && $input["email_id"] != "" && $input["mobile_no"] != "") {
                if(isset($input["seat"]) && count($input["seat"]) > 0) {                    
                    $seats = array();
                    foreach ($input["seat"] as $ps_berth_no => $ps_value_array) {
                        foreach ($ps_value_array as $ps_array_key => $value_seat) {
                            $seats[$ps_berth_no][$value_seat["seat_no"]] = $value_seat;
                        }
                    }
 
                    $array_to_process["input"] = array("seat" => $seats,
                                                        "agent_mobile_no" => isset($input['agent_mobile_no']) ? $input['agent_mobile_no'] : '',
                                                        "agent_email_id" => isset($input['agent_email_id']) ? $input['agent_email_id'] : '' ,
                                                        "email_id" => $input["email_id"],
                                                        "mobile_no" => $input["mobile_no"],
                                                        "user_id" => $user_id,
                                                        "id_type" => isset($input["id_type"]) ? $input["id_type"] : "",
                                                        "name_on_id" => isset($input["name_on_id"]) ? $input["name_on_id"] : "",
                                                        "id_card_number" => isset($input["id_card_number"]) ? $input["id_card_number"] : ""
                                                       );

                    $array_to_process["available_detail"] = $this->session->userdata('available_detail');

                    $library_name = strtolower($array_to_process["available_detail"]['library_name']) . '_api';

                    if(is_ors_agent_ticket($this->session->userdata('available_detail'))) {
                        $data["is_agent_markup_onward"] = true;
                    }

                    if($library_name != "" && $library_name != "_api") {
                        $data["onwardJourney"] = $this->{$library_name}->do_temp_booking($array_to_process);
                    }
                    else {
                        $custom_error_mail = array(
                                                "custom_error_subject" => "Library name not found.",
                                                "custom_error_message" => "Library name not found from available detail - ".json_encode($array_to_process["available_detail"])
                                            );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                        load_front_view(UNEXPECTED_ERROR);
                    }
                }
                if($this->session->userdata('role_id') == ORS_AGENT_USERS_ROLE_ID) {
                    $userids = $this->session->userdata("parent_id");
                }
                else {
                    $userids            = $this->session->userdata('user_id');
                }                
                if(trim($data["onwardJourney"]["payment_confirm_booking_status"]) == "success")
                {
                    $pt_data = $this->tickets_model->ticket_detail_by_refno($data["onwardJourney"]["block_key"]);
                    // Calculate Commission
                    if($this->config->item('calculate_commission')) {
                        $update_data = false;
                        $commission_data = $this->calculate_commission($pt_data,$update_data,$array_to_process["available_detail"]["bus_type_id"]);           
                    }
                    
                    if(!empty($commission_data)) {
                        $data["onwardJourney"]["final_retailer_commission"] =  $commission_data['final_retailer_commission'];
                        $data["onwardJourney"]["retailer_commission"] =  $commission_data['retailer_commission'];
                        $data["onwardJourney"]["tds_on_retailer_commission"] =  $commission_data['tds_on_retailer_commission'];
                    }
                    $final_total_amount = $data["onwardJourney"]["total_fare"];    
                    
                    // Return ticket  
                    if(isset($input["rseat"]) && count($input["rseat"]) > 0) {
                        $rseats = array();
                        foreach ($input["rseat"] as $rps_berth_no => $rps_value_array) {
                            foreach ($rps_value_array as $rps_array_key => $rvalue_seat) {
                                $rseats[$rps_berth_no][$rvalue_seat["seat_no"]] = $rvalue_seat;
                            }
                        }

                        $array_to_process["input"] = array("seat" => $rseats,
                                                            "agent_mobile_no" => isset($input['agent_mobile_no']) ? $input['agent_mobile_no'] : '',
                                                            "agent_email_id" => isset($input['agent_email_id']) ? $input['agent_email_id'] : '' ,
                                                            "email_id" => $input["email_id"],
                                                            "mobile_no" => $input["mobile_no"],
                                                            "user_id" => $user_id,
                                                            "is_return_journey" => true,
                                                            "id_type" => isset($input["rid_type"]) ? $input["rid_type"] : "",
                                                            "name_on_id" => isset($input["rname_on_id"]) ? $input["rname_on_id"] : "",
                                                            "id_card_number" => isset($input["rid_card_number"]) ? $input["rid_card_number"] : "",
                                                           );

                        $array_to_process["available_detail"] = $this->session->userdata('available_detail_return');
                        $library_name = strtolower($array_to_process["available_detail"]['library_name']) . '_api';
                        if(is_ors_agent_ticket($this->session->userdata('available_detail_return'))) {
                            $data["is_agent_markup_return"] = true;
                        }

                        //library call
                        if($library_name != "" && $library_name != "_api") {
                            $data["returnJourney"] = $this->{$library_name}->do_temp_booking($array_to_process);
                        }
                        else {
                            $custom_error_mail = array(
                                                    "custom_error_subject" => "Library name not found return journey.",
                                                    "custom_error_message" => "Library name not found from available detail return journeyreturn journey - ".json_encode($array_to_process["available_detail"])
                                                );

                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                        }

                        if(!empty($data["returnJourney"])) {
                            if($data["returnJourney"]["payment_confirm_booking_status"] == "success") {
                                
                                $rpt_data = $this->tickets_model->ticket_detail_by_refno($data["returnJourney"]["block_key"]);
                                
                                // Calculate Commission
                                if($this->config->item('calculate_commission')) {
                                    $update_data = false;
                                    $commission_data = $this->calculate_commission($rpt_data,$update_data,$array_to_process["available_detail"]["bus_type_id"]);           
                                }

                                if(!empty($commission_data)) {
                                    $data["returnJourney"]["final_retailer_commission"]     =  $commission_data['final_retailer_commission'];
                                    $data["returnJourney"]["retailer_commission"]           =  $commission_data['retailer_commission'];
                                    $data["returnJourney"]["tds_on_retailer_commission"]    =  $commission_data['tds_on_retailer_commission'];
                                }
                                
                                $final_total_amount += $data["returnJourney"]["total_fare"];
                                $this->return_ticket_mapping_model->ticket_id = $data["onwardJourney"]["ticket_id"];
                                $this->return_ticket_mapping_model->return_ticket_id = $data["returnJourney"]["ticket_id"];
                                $this->return_ticket_mapping_model->save();
                            }
                            else {
                                $custom_error_mail = array(
                                                              "custom_error_subject" => "Return journey temp booking failed for - ".$library_name,
                                                              "custom_error_message" => "Error from API is <b>".$data["returnJourney"]["error"]."</b>. Function name process_payment_confirm_booking. Customer Email - ".$input["email_id"]." and Mobile Number - ".$input["mobile_no"]
                                                            );

                                $this->common_model->management_custom_error_mail($custom_error_mail, $library_name);
                                $this->session->set_flashdata('msg_type', 'error');
                                $this->session->set_flashdata('msg', 'Some problem occured while booking retrun ticket.');
                                load_front_view(UNEXPECTED_ERROR);
                            }
                        }
                        else {
                            $custom_error_mail = array(
                                                      "custom_error_subject" => "Return journey temp booking failed for - ".$library_name,
                                                      "custom_error_message" => "<b>Response not came</b>.function name process_payment_confirm_booking. Customer Email - ".$input["email_id"]." and Mobile Number - ".$input["mobile_no"]
                                                    );

                            $this->common_model->management_custom_error_mail($custom_error_mail, $library_name);
                            $this->session->set_flashdata('msg_type', 'error');
                            $this->session->set_flashdata('msg', 'Some problem occured while booking retrun ticket.');
                            load_front_view(UNEXPECTED_ERROR);
                        }
                    }
                    
                    
                    $data["total_fare"] = $final_total_amount;
                    $data["fb_event_code"] = "AddToCart"; // IMP : for facebook pixel. Don't Remove this line.
                    
                    load_front_view(PAYEMENT_GATEWAY_CONFIRM_BOOKING_VIEW, $data); 
                      
                }   
                else {

                    $message_to_send = "Error from API is <b>".$data["onwardJourney"]["error"]."</b>. Function name process_payment_confirm_booking. Customer Email - ".$input["email_id"]." and Mobile Number - ".$input["mobile_no"];
                    if(isset($data["onwardJourney"]["ticket_id"])) {
                        $request_response_log = $this->tracking_request_response_model->where(["ticket_id" => $data["onwardJourney"]["ticket_id"]])->find_all();
                        
                        if($request_response_log) {
                            $request_response_log = $request_response_log[0];
                            $message_to_send .= "<br/><br/>Request - ";
                            $message_to_send .= htmlentities($request_response_log->request);
                            $message_to_send .= "<br/><br/>response - ".$request_response_log->response;
                        }
                    }
                    if(true && in_array($library_name, ["msrtc","msrtc_api","upsrtc","upsrtc_api"])) {
                        $layout_confirmation['from_stop']           = $array_to_process["available_detail"]["boarding_stop_cd"];
                        $layout_confirmation['to_stop']             = $array_to_process["available_detail"]["destination_stop_cd"];
                        $layout_confirmation['bus_from_stop_id']    = "";
                        $layout_confirmation['bus_to_stop_id']      = "";
                        $layout_confirmation['user_type']           = "1";
                        $layout_confirmation['service_id']          = $array_to_process["available_detail"]["service_id"];
                        $layout_confirmation['date'] = date('d/m/Y', strtotime($array_to_process["available_detail"]["date"]));
                        $soap_name = strtolower($array_to_process["available_detail"]["library_name"].'_api');
                        $data = $this->{$soap_name}->seatAvailability($layout_confirmation);

                        $message_to_send .= "<br/><br/>available seats - ".json_encode($data);
                    }

                    $custom_error_mail = array(
                                                  "custom_error_subject" => "Temp booking failed for - ".$library_name,
                                                  "custom_error_message" => $message_to_send
                                                );
                    $this->common_model->management_custom_error_mail($custom_error_mail, $library_name);
                    load_front_view(UNEXPECTED_ERROR);
                }
            }
            else {
                load_front_view(UNEXPECTED_ERROR);
            }
        }
        else {
            load_front_view(UNEXPECTED_ERROR);
        }
    }

    /*
     * @function    : wallet_payment
     * @param       : 
     * @detail      : transaction process.
     *                 
     */

    public function payment_wallet_gateway($payment_type, $total_fare, $pg_array,$input) {
      
        $user_id = $this->session->userdata("booking_user_id");
        $payment_type = trim($payment_type);
        $currentDate = date('Y-m-d');
        // if ($total_fare != "" && $user_id != "")
        if($total_fare > 0) {
			//Checknig payment type is wallet 
            if($payment_type == "wallet" && $this->session->userdata("user_id") != "") {
                $api_data = array();
                $final_data = array();
                $rpt_data = array();
                $fb_event_value = 0;
                $notify_seats = "";
                
                // Get Wallet Data
                $this->wallet_model->user_id = $this->session->userdata("user_id");
                $walletdata = $this->wallet_model->select();            
                if (count($walletdata) > 0) {
					//get Tikcet Detail
                   $pt_data =  $this->tickets_model
                   ->where('ticket_ref_no', $pg_array['ticket_ref_no'])
                   ->find_all();	
                    //Check ticket transaction status is temp_booked then only process  
                   if($pt_data[0]->transaction_status == "temp_booked") {
                      $pg_array["payment_by"] = "wallet";
                      $ccaid = $this->transaction_initiate($pg_array);
                      if($ccaid) {
                            //Check return ticket 
                        $rpt_data        = $this->return_ticket_mapping_model->get_rpt_mapping($pt_data[0]->ticket_id);
                        
                        $current_balance = $walletdata[0]->amt;
                        
                            //Check wallet balance is greater then total fare
                        if ($current_balance >= $total_fare)  {
                            $update_actual_wallet = $current_balance - $total_fare;
                            
                                //IN RETURN JOURNEY CASE IF RETURN JOURNEY NOT BOOKED ADD AMOUNT IN WALLET
                            $balance_to_update          = $current_balance - $total_fare;
                            
                            $UserId = array('user_id' => $this->session->userdata('user_id'));
                            $AgentUserId = $this->session->userdata('user_id');
                            $FileName = "wallet/user_wallet_detail/wallet_".$AgentUserId.".csv";
                            
                            $wallet_to_update = array(
                              'amt' => $balance_to_update,
                              'updated_by'=> $this->session->userdata('user_id'),
                              'updated_date'=> date('Y-m-d h:i:s')
                          );
                            
                            $wallet_id = $this->wallet_model->update_where($UserId, '', $wallet_to_update);

                            $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                            $val = array ($this->session->userdata('user_id'),$total_fare,$current_balance,$balance_to_update,'',$pt_data[0]->ticket_id,date("Y-m-d H:i:s"),'Function Detail : booking/payment_wallet_gateway, Detail : Deduct Amount from wallet against ticket booking');
                            
                            make_csv($FileName,$title,$val);

                            if($wallet_id > 0) 
                            {
                                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                                $wallet_trans_detail =  array("w_id" => $walletdata[0]->id,
                                  "amt" => $total_fare,
                                  "comment" => "ticket booking transaction",
                                  "status" => "deduction",
                                  "user_id" => $AgentUserId,
                                  "added_by" => $this->session->userdata("user_id"),
                                  "added_on" => date("Y-m-d H:i:s"),
                                  "amt_before_trans" => $current_balance,
                                  "amt_after_trans" => $balance_to_update,
                                  "ticket_id" => $pt_data[0]->ticket_id,
                                  "transaction_type_id" => '5',
                                  "transaction_type" => 'Debited',
                                  "is_status" => 'Y',
                                  "wallet_type" => 'actual_wallet',
                                  "return_ticket_id" => isset($rpt_data[0]->ticket_id) ? trim($rpt_data[0]->ticket_id) : "",
                                  "transaction_no" => $transaction_no
                              );

                                $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
                                
                                if($wallet_trans_id > 0)
                                {
                                  $transaction_status = array('transaction_status' => "psuccess",
                                   'payment_by' => 'wallet',
                                   'payment_confirmation' => "full"
                               );

                                  $this->tickets_model->update($pt_data[0]->ticket_id,$transaction_status);

										//to check if return journey
                                  if(count($rpt_data) > 0) {
                                     $transaction_status = array('transaction_status' => "psuccess",
                                      'payment_by' => 'wallet',
                                      'payment_confirmation' => "full"
                                  );

                                     $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction_status);

                                     $fb_event_value += $rpt_data[0]->tot_fare_amt_with_tax;
                                     $notify_seats   .= $rpt_data[0]->num_passgr."+";
                                 }
                                 
                                 $fb_event_value += $pt_data[0]->tot_fare_amt_with_tax;
                                 $notify_seats   .= $pt_data[0]->num_passgr;
                                 
                                 
                                 $array_where = array('track_id' => $pg_array['track_id']);
                                 $fields_update = array('status' => 'success');

                                 $pay_trans_true_false = $this->payment_transaction_fss_model->update_where($array_where,'', $fields_update);
                                 
                                 /************* Actual ticket booking Start Here ***********/
                                 $api_data['ticket_id']              = str_pad($pt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
                                 $api_data['ticket_ref_no']          = $pg_array['ticket_ref_no'];
                                 $api_data['seat_count']             = $pt_data[0]->num_passgr;
                                 $api_data['payment_by']             = "wallet";
                                 $api_data['rpt_data']               = $rpt_data;
                                 $api_data['pt_data']                = $pt_data[0];
                                 
                                 $final_data['ticket_ref_no'] = $pg_array['ticket_ref_no'];
                                 $final_data['ticket_pnr'] = '';
                                 $final_data["pg_response"]["payment_mode"] = 'wallet';
                                 $final_data['ticket_id'] = $pt_data[0]->ticket_id;
                                 $final_data["pg_response"]["result"] = 'CAPTURED';
                                 $final_data["is_updated"] = true;
                                 

                                 if(isset($pt_data) && $pt_data[0]->transaction_status != 'success') {                                    
                                     $available_detail = $this->session->userdata('available_detail');
                                     $library_name = strtolower($available_detail["library_name"]).'_api';
                                     $final_tbdata = $this->$library_name->final_booking($api_data,$final_data);
                                     logging_data("hrtc_checking/1.log",$final_tbdata,"final_tbdata in booking");
                                     if(trim($final_tbdata["status"]) == "success") {
                                        /**********BOS WALLET TRANSACTION START*******/
                                        $rokad_data = $this->bos_wallet_deduction($pt_data[0]->ticket_id);
                                        if(!empty($rokad_data)){
                                           $rokad_key =  json_decode($rokad_data)->bos_key;
                                           $this->tickets_model->update($pt_data[0]->ticket_id,['rokad_ref_no' => $rokad_key]);
                                       }
                                       /**********BOS WALLET TRANSACTION END**********/
                                       
                                                // add commission amount into wallet 
                                       $comission_add_response = $this->add_commission_for_ticket($pt_data);
                                       logging_data("data/com.log",$comission_add_response,"comission_add_response");
                                       
                                       if(false && $this->config->item('enable_discount') && strtotime(date('Y-m-d')) <= strtotime($this->config->item('discount_till')) ) {
                                           $notify_bos_ref_no  =   $final_tbdata["transaction_process_response"]["bos_ref_no"];
                                           
                                           if(count($rpt_data) > 0) {
                                              $notify_bos_ref_no  .= " & ".$final_tbdata["transaction_process_response_return"]["bos_ref_no"];
                                          }

                                          $discounted_psg_cnt = 0;
                                          $discounted_tkt_cnt = 0;

                                          $discount_file_path = LOG_FOLDER."offer/count.log";
                                          $discounted_psg_cnt = file_read($discount_file_path);

                                          $ticket_discount_file_path = LOG_FOLDER."offer/ticket_count.log";
                                          $discounted_tkt_cnt = file_read($ticket_discount_file_path);

                                          $discounted_psg_cnt = $discounted_psg_cnt + $pt_data[0]->num_passgr;
                                          $discounted_tkt_cnt += 1;
                                          
                                          if(count($rpt_data) > 0) {
                                              $discounted_psg_cnt = $discounted_psg_cnt + $rpt_data[0]->num_passgr;
                                              $discounted_tkt_cnt += 1;
                                          }

                                          write_file($discount_file_path, $discounted_psg_cnt, "w+");
                                          write_file($ticket_discount_file_path, $discounted_tkt_cnt, "w+");
                                          
                                          $ticket_notification_detail = array(
                                           "{{bos_ref_no}}" => $notify_bos_ref_no,
                                           "{{total_amount}}" => $fb_event_value,
                                           "{{total_seats}}" => $notify_seats,
                                           "{{cust_name}}" => $pg_array["user_detail"]->psgr_name,
                                           "{{mobile_no}}" => $pt_data[0]->mobile_no,
                                           "{{discount_psgr_count}}" =>  $discounted_psg_cnt,
                                           "{{discount_tkt_count}}" => $discounted_tkt_cnt
                                       );

                                          $msg_to_sent = str_replace(array_keys($ticket_notification_detail),array_values($ticket_notification_detail),INTERNAL_TICKET_NOTIFICATION);

                                          sendSMS($this->config->item("developer_mobile_no"), $msg_to_sent);
                                      }


												/*              NOTICE
												 *  If return ticket booking failed send mail.
												 */

												if(count($rpt_data) > 0)
												{
                                                    if(trim($final_tbdata["return_ticket_status"]) == "success") {
                                                        // add commission amount into wallet 
                                                        $comission_add_response = $this->add_commission_for_ticket($rpt_data);
                                                        logging_data("data/com.log",$comission_add_response,"comission_add_response");
                                                    }
                                                    if(isset($final_tbdata["return_ticket_status"]) && $final_tbdata["return_ticket_status"] == "error")
                                                    {
                                                      /***************/
                                                      $this->error_in_do_confirm_booking($rpt_data);
                                                      /***************/

                                                      $tfs_mail_data = [];
                                                      $tfs_mail_data["journey_ticket_data"] = $rpt_data[0];
                                                      $tfs_mail_data["api_failed_reason"] = isset($final_tbdata["api_failed_reason"]) ? $final_tbdata["api_failed_reason"] : "confirm ticket booking failed.";
                                                      $tfs_ticket_amount = $rpt_data[0]->tot_fare_amt_with_tax;
                                                      $tfs_ticket_id = $rpt_data[0]->ticket_id;

                                                      $tfs_mail_data["ticket_amount"] = $tfs_ticket_amount;
                                                      $tfs_mail_data["ticket_id"] = $tfs_ticket_id;
                                                      $tfs_mail_data["ticket_refund_amount"] = "Please check as it is return journey";
                                                      $tfs_mail_data["total_ticket_amount"] = $total_fare;
                                                      $tfs_mail_data["ccavenue_charges"] = "Please check as it is return journey";
                                                      
                                                      $tfs_mail_data["extra"] = "WALLET : Please note that. Onward journey ticket booked but return journey failed.";

                                                      /*$this->common_model->payment_success_ticket_failed_mail($tfs_mail_data);*/
                                                      $this->common_model->management_payment_success_ticket_failed_mail($tfs_mail_data);
                                                      $this->common_model->payment_gateway_failed($final_tbdata["data"]);
                                                  }
                                              }

                                              $this->session->unset_userdata('provider_available_bus');
                                              $this->session->unset_userdata('provider_available_bus_return');
                                              $this->session->unset_userdata('seatlayout_boarding_points');
                                              
                                              logging_data("hrtc_checking/1.log","last","last in booking");
                                              $final_tbdata["fb_event_code"] = "Purchase";
                                              $final_tbdata["fb_event_value"] = $fb_event_value;
                                              
                                              load_front_view(PAYMENT_SUCCESS_PAGE,$final_tbdata);
                                              
                                          }
                                          else if(trim($final_tbdata["status"]) == "error" && isset($final_tbdata["return_transaction_process_api"]) && $final_tbdata["return_transaction_process_api"] == "error")
                                          {
                                            $custom_error_mail = array(
                                             "custom_error_subject" => "Payment not confirm for ".$pt_data[0]->ticket_id,
                                             "custom_error_message" => "Return journey transction process api for ticket id".$pt_data[0]->ticket_id." Some error in transaction_process method in library."
                                         );

                                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                                            load_front_view(UNEXPECTED_ERROR);
                                        }
                                        else if(trim($final_tbdata["status"]) == "error")
                                        {

                                            /***************/
                                            $this->error_in_do_confirm_booking($pt_data);
                                            /***************/

                                            $tfs_mail_data = [];
                                            $tfs_mail_data["journey_ticket_data"] = $pt_data[0];
                                            $tfs_mail_data["api_failed_reason"] = isset($final_tbdata["api_failed_reason"]) ? $final_tbdata["api_failed_reason"] : "confirm ticket booking failed.";
                                            $tfs_ticket_amount = $pt_data[0]->tot_fare_amt_with_tax;
                                            $tfs_ticket_id = $pt_data[0]->ticket_id;
                                            if(count($rpt_data) > 0)
                                            {
                                               $tfs_mail_data["return_ticket_data"] = $rpt_data[0];
                                               $tfs_ticket_amount .= " , ".$rpt_data[0]->tot_fare_amt_with_tax;
                                               $tfs_ticket_id .= " , ".$rpt_data[0]->ticket_id;
                                           }

                                           $tfs_mail_data["ticket_amount"] = $tfs_ticket_amount;
                                           $tfs_mail_data["ticket_id"] = $tfs_ticket_id;
                                           $tfs_mail_data["ticket_refund_amount"] = $total_fare;
                                           $tfs_mail_data["total_ticket_amount"] = $total_fare;
                                           
                                           /*$this->common_model->payment_success_ticket_failed_mail($tfs_mail_data);*/
                                           $this->common_model->management_payment_success_ticket_failed_mail($tfs_mail_data);
                                           $this->common_model->payment_gateway_failed($final_tbdata["data"]);
                                       }
                                       else
                                       {
                                        $custom_error_mail = array(
                                         "custom_error_subject" => "Confirm booking not done for ticket id ".$pt_data[0]->ticket_id,
                                         "custom_error_message" => "Status not found at the time of confirmbooking. Payment done & ticket not booked."
                                     );

                                        $this->common_model->developer_custom_error_mail($custom_error_mail);

                                        load_front_view(UNEXPECTED_ERROR);
                                    }
                                }
                                else
                                {
                                 $custom_error_mail = array(
                                   "custom_error_subject" => "Double confirmbooking hit issue ".$pt_data[0]->ticket_id,
                                   "custom_error_message" => "Please track this. Hit double confirmbooking. Ticket Data -> ".json_encode($pt_data)
                               );

                                 $this->common_model->developer_custom_error_mail($custom_error_mail);

                                 redirect(base_url()."front/booking/view_ticket/".$pt_data[0]->ticket_ref_no);
                             }
                         }
                         else
                         {
                          $custom_error_mail = array(
                              "custom_error_subject" => "Critical Error - Data not inserted into wallet transaction table",
                              "custom_error_message" => "Function Name - payment_wallet_gateway. Data not inserted into wallet transaction table but amount deducted from wallet . Current Balance - " .$current_balance. " Ticket Fare -" .$total_fare. " User id - ".$this->session->userdata("user_id") ." and Ticket id - " . $pg_array['ticket_id']. " wallet id - " .$wallet_id
                          );

                          $this->common_model->developer_custom_error_mail($custom_error_mail);
                          
                          $err_data['error'] = " Data not inserted into wallet transaction table";
                          
                          log_data("booking/".date("M")."/bos_booking_from_wallet.log", $err_data,"Data not inserted into wallet transaction table");
                          log_data("booking/".date("M")."/bos_booking_from_wallet.log", $wallet_trans_detail,"Data not inserted into wallet transaction table");

                          return false;
                      }
                  } 
                  else 
                  {
                   /* Used Before Changes By Mihir */ 
									/* $pg_array["payment_by"] = "fss";
										$this->transaction_initiate($pg_array); //Make Entry in payment_transaction_fss table
										$this->fss_payment_gateway->SendPerformRequest($pg_array);//check here condition that if entry is made in payment_transaction_fss table
									*/
                                       
                                       $custom_error_mail = array(
                                          "custom_error_subject" => "Wallet Error - Wallet amount not deducted while processing booking from wallet payment",
                                          "custom_error_message" => "Function Name - payment_wallet_gateway. Wallet amount not deducted while processing booking from wallet payment. User id - ".$this->session->userdata("user_id") ." and Ticket id - " . $pg_array['ticket_id']
                                      );

                                       $this->common_model->developer_custom_error_mail($custom_error_mail);
                                       
                                       $err_data['error'] = " Wallet amount not deducted while processing booking from wallet payment";
                                       
                                       log_data("booking/".date("M")."/bos_booking_from_wallet.log", $err_data,"Wallet not updated");

                                       return false;
                                   }
                               }
                               else  {
                                
                                $custom_error_mail = array(
                                  "custom_error_subject" => "Wallet Error - Current Balance is less than Ticket Fare",
                                  "custom_error_message" => "Function Name - payment_wallet_gateway. Current Balancd less then ticket fare. Current Balance - " .$current_balance. " Ticket Fare -" .$total_fare. " User id - ".$this->session->userdata("user_id") ." and Ticket id - " . $pg_array['ticket_id']
                              );

                                $this->common_model->management_custom_error_mail($custom_error_mail, "management");
                                
                                $err_data['error'] = " Current Balance is less then ticket fare";
                                
                                logging_data("booking/".date("M")."/bos_booking_from_wallet.log", $err_data,"Current Balance is less than Ticket Fare");

                                return false;
                            }
                        }
                        else {
                         $custom_error_mail = array(
                           "custom_error_subject" => "Critical Error - Data not inserted in payment_transaction_fss table",
                           "custom_error_message" => "Function Name - payment_wallet_gateway. data not inserted in payment_transaction_fss table for ticket id - ".$pg_array["ticket_id"]
                       );

                         $this->common_model->developer_custom_error_mail($custom_error_mail);
                         
                         $err_data['error'] = " Data not inserted in payment_transaction_fss table";
                         $err_data['error_detail'] = " Function Name - payment_wallet_gateway. data not inserted in payment_transaction_fss table for ticket id - ".$pg_array["ticket_id"];
                         
                         logging_data("booking/".date("M")."/bos_booking_from_wallet.log", $err_data,"Data not inserted in payment_transaction_fss table");

                         return false;
							// load_front_view(UNEXPECTED_ERROR);
                     }
                 }
                 else
                 {
                  $redirected_to = "home";
                  $redirect_to_home = true;
                  $redirect_url = base_url();
                  if($pt_data[0]->transaction_status == "success" && $pt_data[0]->pnr_no != "")
                  {
                    $redirected_to = "view ticket";
                    $redirect_to_home = false;
                    $redirect_url = base_url()."front/booking/view_ticket/".$pt_data[0]->ticket_ref_no;
                }
                
                if($pt_data[0]->transaction_status == "psuccess")
                {
                 $custom_error_mail = array(
                    "custom_error_subject" => "Confirm booking not done for ticket id ".$pt_data[0]->ticket_id,
                    "custom_error_message" => "Status not found at the time of confirmbooking. Payment done & ticket not booked."
                );

                 $this->common_model->developer_custom_error_mail($custom_error_mail);

                 load_front_view(UNEXPECTED_ERROR);
             }
             
             $custom_error_mail = array(
                "custom_error_subject" => "Double Hit (Page Refresh) & redirected to ".$redirected_to,
                "custom_error_message" => "Page refreshed or Made double hit in the process of Wallet booking. So we have redirected this user to the <b>".$redirected_to."</b> page. please find the use details for ticket id ".$pt_data[0]->ticket_id."<br/><br/>".json_encode($pt_data[0])
            );

             $this->common_model->developer_custom_error_mail($custom_error_mail);

             redirect($redirect_url);
         }
     } 
     else  {
       $custom_error_mail = array(
          "custom_error_subject" => "Error - Wallet not created",
          "custom_error_message" => "Function Name - payment_wallet_gateway. wallet not created for user id - ".$user_id ." and ticket id - " . $pg_array['ticket_id']
      );

       $this->common_model->developer_custom_error_mail($custom_error_mail);
       
       $err_data['error'] = "Wallet not created";
       $err_data['user_id'] = $user_id;

       logging_data("booking/".date("M")."/bos_booking_from_wallet.log", $err_data,"Wallet not created error");
       redirect(base_url());
   }
} 
else if (trim($payment_type) == "payment_gateway" && isset($pg_array['type_of_payment']) && trim($pg_array['type_of_payment']) == "cca") 
{
    $pg_array['payment_by'] = "cca";
    $total_fare             = isset($pg_array["total_amount"]) ? $pg_array["total_amount"] : '';
                $ccaid                  = $this->transaction_initiate($pg_array); //Make Entry in payment_transaction_fss table
                if($ccaid)
                {
                    /******************* Process Data Start*******/
                    $ReqTid = "tid=".time();
                    $ReqorderId = isset($pg_array["track_id"]) ? $pg_array["track_id"] : '';
                    //$ReqTotalAmount = isset($pg_array["total_amount"]) ? $pg_array["total_amount"] : '';
                    $ReqTotalAmount = $total_fare;
                    $merchant_param1 = isset($pg_array["ticket_ref_no"]) ? $pg_array["ticket_ref_no"] : "";
                    $merchant_param2 = isset($pg_array["ticket_id"]) ? $pg_array["ticket_id"] : "";
                    $merchant_param3 = isset($pg_array["user_id"]) ? $pg_array["user_id"] : "";
                    $merchant_param4 = "";

                    $ReqMerchantOrderId = "order_id=".$ReqorderId;
                    $ReqMerchantId = "merchant_id=".$this->config->item("cca_merchant_id");
                    $ReqAmount = "amount=".$ReqTotalAmount;
                    $ReqCurrency = "currency=INR";
                    $ReqRedirectUrl = "redirect_url=".base_url()."front/booking/get_handle_response_cca";
                    $ReqCancelUrl = "cancel_url=".base_url()."front/booking/get_handle_response_cca";
                    $ReqLanguage = "language=EN";
                    $ReqMerchantPara1="merchant_param1=".$merchant_param1;  
                    $ReqMerchantPara2="merchant_param2=".$merchant_param2; 
                    $ReqMerchantPara3="merchant_param3=".$merchant_param3;
                    $ReqMerchantPara4="merchant_param4=".$merchant_param4;


                    $Strhashs=trim($ReqMerchantOrderId)."&".trim($ReqAmount)."&".trim($ReqCurrency)."&".trim($ReqMerchantPara1)."&".trim($ReqMerchantPara2)."&".trim($ReqMerchantPara3)."&".trim($ReqMerchantPara4);    
                    
                    // Below parameter is set to reduce user effort to enter detail on PG Page
                    if(isset($pg_array["user_detail"]) && !empty($pg_array["user_detail"]))
                    {
                        if(isset($pg_array["user_detail"]->psgr_name))
                        {
                            $ReqBillingName="billing_name=".trim($pg_array["user_detail"]->psgr_name);
                            $Strhashs = $Strhashs."&".trim($ReqBillingName);
                        }

                        if(isset($pg_array["user_detail"]->user_email_id))
                        {
                            $ReqBillingEmail="billing_email=".trim($pg_array["user_detail"]->user_email_id);
                            $Strhashs = $Strhashs."&".trim($ReqBillingEmail);
                        }


                        if(isset($pg_array["user_detail"]->mobile_no))
                        {
                            $ReqBillingTelNo="billing_tel=".trim($pg_array["user_detail"]->mobile_no);
                            $Strhashs = $Strhashs."&".trim($ReqBillingTelNo);
                        }
                    }

                    //For geo location

                    //UNCOMMENT BELOW LINE ON LIVE
                    // $ip_address = $this->session->userdata("ip_address");
                    //COMMENT BELOW LINE ON LIVE
                    $locationdata = array();
                    $ip_address = $this->input->ip_address();
                    // $locationdata = getGeoLocation($ip_address);

                    if(count($locationdata) && trim($locationdata["status"]) == "success")
                    {
                        if(isset($locationdata["city"]) && $locationdata["city"] != "")
                        {
                            $ReqBillingCity="billing_city=".trim($locationdata["city"]);
                            $Strhashs = $Strhashs."&".trim($ReqBillingCity);
                        }

                        if(isset($locationdata["regionName"]) && $locationdata["regionName"] != "")
                        {
                            $ReqBillingState="billing_state=".trim($locationdata["regionName"]);
                            $Strhashs = $Strhashs."&".trim($ReqBillingState);
                        }

                        /*if(isset($locationdata["country"]) && $locationdata["country"] != "")
                        {
                            $ReqBillingCountry="billing_country=".trim($locationdata["country"]);
                            $Strhashs = $Strhashs."&".trim($ReqBillingCountry);
                        }*/

                        if(isset($locationdata["zip"]) && $locationdata["zip"] != "")
                        {
                            $ReqBillingZip="billing_zip=".trim($locationdata["zip"]);
                            $Strhashs = $Strhashs."&".trim($ReqBillingZip);
                        }
                    }

                    $ReqBillingCountry="billing_country=India";
                    $Strhashs = $Strhashs."&".trim($ReqBillingCountry);


                    //AFTERWARDS DO THIS OR CHANGE LOGIC FOR merchant_param5
                    //Use hash method which is defined below for Hashing ,It will return Hashed valued of above string
                    $hashedstring= hash('sha256', $Strhashs); 
                    $ReqMerchantPara5="merchant_param5=".$hashedstring;  

                    // $Reqparam = trim($ReqTid)."&".trim($ReqMerchantId)."&".trim($ReqMerchantOrderId)."&".trim($ReqAmount)."&".trim($ReqCurrency)."&".trim($ReqRedirectUrl)."&".trim($ReqCancelUrl)."&".trim($ReqLanguage)."&".trim($ReqMerchantPara1)."&".trim($ReqMerchantPara2)."&".trim($ReqMerchantPara3)."&".trim($ReqMerchantPara4)."&".trim($ReqMerchantPara5)."&";
                    $Reqparam = trim($ReqTid)."&".trim($ReqMerchantId)."&".trim($ReqMerchantOrderId)."&".trim($ReqAmount)."&".trim($ReqCurrency)."&".trim($ReqRedirectUrl)."&".trim($ReqCancelUrl)."&".trim($ReqLanguage)."&".trim($ReqMerchantPara1)."&".trim($ReqMerchantPara2)."&".trim($ReqMerchantPara3)."&".trim($ReqMerchantPara4);
                    
                    // Below parameter is set to reduce user effort to enter detail on PG Page
                    if(isset($pg_array["user_detail"]) && !empty($pg_array["user_detail"]))
                    {
                        if(isset($ReqBillingName))
                        {
                            $Reqparam .= "&".$ReqBillingName;
                        }

                        if(isset($ReqBillingEmail))
                        {
                            $Reqparam .= "&".$ReqBillingEmail;
                        }

                        if(isset($ReqBillingTelNo))
                        {
                            $Reqparam .= "&".$ReqBillingTelNo;
                        }
                    }

                    if(count($locationdata) && trim($locationdata["status"]) == "success")
                    {
                        if(isset($ReqBillingCity))
                        {
                            $Reqparam .= "&".$ReqBillingCity;
                        }

                        if(isset($ReqBillingState))
                        {
                            $Reqparam .= "&".$ReqBillingState;
                        }

                        /*if(isset($ReqBillingCountry))
                        {
                            $Reqparam .= "&".$ReqBillingCountry;
                        }*/

                        if(isset($ReqBillingZip))
                        {
                            $Reqparam .= "&".$ReqBillingZip;
                        }

                    }

                    $Reqparam .= "&".$ReqBillingCountry;
                    $Reqparam = $Reqparam ."&".trim($ReqMerchantPara5)."&";

                    //for log request
                    log_data("pg/ccavenue_pg_".date("d-m-Y")."_log.log",$Reqparam, 'Parameter sent to CCAvenue PG');
                    log_data("pg/ccavenue_pg_".date("d-m-Y")."_log.log",explode("&",$Reqparam), 'Parameter sent to CCAvenue PG (Readable Parameter)');
                    $encrypted_data = encrypt($Reqparam,$this->config->item("cca_working_key"));
                    
                    $ccprocess_data["encrypted_data"] = $encrypted_data;
                    
                    log_data("pg/ccavenue_pg_".date("d-m-Y")."_log.log",$ccprocess_data, 'Parameter sent to CCAvenue PG (encrypted_data Parameter)');

                    $ccprocess_data["fb_event_code"] = "InitiateCheckout"; // IMP : for facebook pixel. Don't Remove this line.
                    $ccprocess_data["status"] = "success";
                    $ccprocess_data["type"]   = "cca";
                    return $ccprocess_data;
                    // load_front_view(CCA_PROCESSING_PAGE,$ccprocess_data);
                    // $this->cca_payment_gateway->SendPerformRequest($pg_array);//check here condition that if entry is made in payment_transaction_fss table
                }
                else
                {
                    $custom_error_mail = array(
                      "custom_error_subject" => "Critical Error - Data not inserted in payment_transaction_fss table",
                      "custom_error_message" => "Function Name - payment_wallet_gateway. data not inserted in payment_transaction_fss table for ticket id - ".$pg_array["ticket_id"]
                  );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);

                    return false;
                    // load_front_view(UNEXPECTED_ERROR);
                }
            } 
            else 
            {
                $transaction = array('transaction_status' => "failed",
                  'failed_reason' => 'unexpected payment type'
              );
                $id = $this->tickets_model->update($pg_array['ticket_id'],$transaction);

                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Transaction error occured. Please try again.');

                $custom_error_mail = array(
                    "custom_error_subject" => "VVVVVVVIMP : Tried to change payment type",
                    "custom_error_message" => "Please track this. Row Data -> ".json_encode($pg_array)
                );

                /*$this->common_model->developer_custom_error_mail($custom_error_mail);*/
                $this->common_model->management_custom_error_mail($custom_error_mail, "management");

                redirect(base_url());
                // return false;
            }
        }
        else
        {

            $transaction = array('transaction_status' => "failed",
               'failed_reason' => 'user id or total fare is null'
           );
            $id = $this->tickets_model->update($pg_array['ticket_id'],$transaction);

            $custom_error_mail = array(
              "custom_error_subject" => "Error - user id or total fare is null",
              "custom_error_message" => "Function Name - payment_wallet_gateway. User id or total fare is null - total fare is - ".$total_fare
          );

            $this->common_model->developer_custom_error_mail($custom_error_mail);

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Transaction error occured. Please try again.');
            return false;
        }
    }


    //Payment gateway Initiate
    public function transaction_initiate($pg_array)
    {
        //return true; //IMP CHANGE THIS TO PAYMENT GATEWAY AFTER DISCUSSION // redirect to payment gateway
        //Make Entry in payment_transaction_fss table

        $transaction = array('transaction_status' => "pfailed");
        $this->tickets_model->update($pg_array["ticket_id"],$transaction);

        if(isset($pg_array["rticket_id"]) && $pg_array["rticket_id"] > 0)
        {
            $transaction = array('transaction_status' => "pfailed");
            $this->tickets_model->update($pg_array["rticket_id"],$transaction);
        }

        $this->payment_transaction_fss_model->track_id = $pg_array["track_id"];
        $this->payment_transaction_fss_model->payment_by = $pg_array["payment_by"];
        $this->payment_transaction_fss_model->user_id = $pg_array["user_id"];
        $this->payment_transaction_fss_model->ticket_id = $pg_array["ticket_id"];
        if(isset($pg_array["rticket_id"]) && $pg_array["rticket_id"] > 0)
        {
            $this->payment_transaction_fss_model->return_ticket_id = $pg_array["rticket_id"];
        }
        $this->payment_transaction_fss_model->total_amount = $pg_array["total_amount"];
        $this->payment_transaction_fss_model->ip_address = $this->input->ip_address();
        $this->payment_transaction_fss_model->user_agent = $this->session->userdata("user_agent");
        $id = $this->payment_transaction_fss_model->save();
        return $id;
    }


    public function wallet_payment_trans($wallet)
    {
        $this->wallet_trans_model->w_id             = $wallet["id"];
        $this->wallet_trans_model->amt              = $wallet["amt"];
        $this->wallet_trans_model->comment          = $wallet["comment"];
        $this->wallet_trans_model->status           = $wallet["status"];
        $this->wallet_trans_model->user_id          = $wallet["user_id"];
        $this->wallet_trans_model->added_by         = $wallet["user_id"];
        $this->wallet_trans_model->amt_before_trans = $wallet["amt_before_trans"];
        $this->wallet_trans_model->amt_after_trans  = $wallet["amt_after_trans"];
        $this->wallet_trans_model->extra            = isset($wallet["extra"]) ? $wallet["extra"] : "";
        $wallet_trans_id = $this->wallet_trans_model->save();

        return $wallet_trans_id;
    }

    /* PG FORM */
    public function do_confirm_booking($blockKey) {
      
        $data = array();
        $data['onwardJourney']['error'] = '';
        $input = $this->input->post();
		$input["agent_markup"] = (isset($input["agent_markup"]) && $input["agent_markup"] != "") ? $input["agent_markup"] : 0;
        if(isset($input["terms_and_conditions"]) && $input["terms_and_conditions"] == 1) {
            if(isset($input["t_pin"]) && $input["t_pin"] != '') {
                if(rokad_encrypt($input["t_pin"],$this->config->item('pos_encryption_key')) == $this->session->userdata('t_pin')) {
                    if($blockKey != 'failure') {
                        $pt_data = $this->tickets_model->ticket_detail_by_refno($blockKey);
                        if($pt_data) {
                            if($this->session->userdata("available_detail") != null && ors_agent_ticket_entry($this->session->userdata("available_detail"))) {   
                                if(!($input["agent_markup"] <= ORS_AGENT_MARKUP_AMOUNT)) {
                                    $this->session->set_flashdata('msg_type', 'error');
                                    $this->session->set_flashdata('msg', 'You have entered markup value greater than specified.');

                                    $custom_error_mail = array(
                                                          "custom_error_subject" => "Agent entered higher markup value.",
                                                          "custom_error_message" => "Detail - ".json_encode($this->session->userdata())
                                                         );

                                    /*$this->common_model->developer_custom_error_mail($custom_error_mail);*/
                                    $this->common_model->management_custom_error_mail($custom_error_mail, "management");
                                    redirect(base_url());
                                }

                                $agent_ticket_array = array(
                                                            "ticket_id"=> $pt_data[0]->ticket_id,
                                                            "ticket_ref_no"=> $pt_data[0]->ticket_ref_no,
                                                            "total_without_discount"=> $pt_data[0]->total_fare_without_discount,
                                                            "total_paid"=> $pt_data[0]->tot_fare_amt_with_tax,
                                                            "markup_value" => $pt_data[0]->num_passgr * $input["agent_markup"],
                                                            "agent_total" => $pt_data[0]->total_fare_without_discount+($pt_data[0]->num_passgr * $input["agent_markup"]),
                                                            "markup_type" => ORS_AGENT_MARKUP_TYPE,
                                                            "markup_price" => ORS_AGENT_MARKUP_AMOUNT
                                                            );

                                $check_if_agent_ticket_exits = $this->agent_tickets_model->where(["ticket_id"=> $pt_data[0]->ticket_id,"ticket_ref_no"=> $pt_data[0]->ticket_ref_no])->find_all();
                                if(!$check_if_agent_ticket_exits) {
                                    $ata_id = $this->agent_tickets_model->insert($agent_ticket_array);
                                    if(!$ata_id) {
                                        $custom_error_mail = array(
                                                                    "custom_error_subject" => "ORS Agent ticket data not inserted for ticket_id : ".$pt_data[0]->ticket_id,
                                                                    "custom_error_message" => "Data not inserted in agent_tickets table & ticket not booked."
                                                                    );

                                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                                        redirect(base_url());
                                    }
                                }
                                else {
                                    $custom_error_mail = array(
                                                                "custom_error_subject" => "VIMP ISSUE : ORS Agent ticket(Agent Ticket Already Inserted)",
                                                                "custom_error_message" => "Please track this agent issue.".json_encode($pt_data)
                                                                );
                                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                                }
                            }
                            // check return ticket detail
                            if(isset($input["block_key_return"] )&& $input["block_key_return"] != "") {
                                $this->tickets_model->ticket_ref_no = $input["block_key_return"];
                                $rpt_data = $this->tickets_model->select();
                            }
                            // Total Payment with return  ticket.
                            $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
                            if(isset($input["block_key_return"]) && $input["block_key_return"] != "" && isset($rpt_data[0]->tot_fare_amt_with_tax) && $rpt_data[0]->tot_fare_amt_with_tax != "") {
                                $payment_total_fare += $rpt_data[0]->tot_fare_amt_with_tax;
                            }

                            $php_uniqid = uniqid();
                            $track_id = $php_uniqid.$pt_data[0]->ticket_id;
            				
                            $pg_array = array('track_id' => $track_id, 
                                              'user_id' => $pt_data[0]->inserted_by,
                                              'ticket_id' => $pt_data[0]->ticket_id,
                                              'ticket_ref_no' => $blockKey, 
                                              'total_amount' => $payment_total_fare,
                                              'type_of_payment' => $input["type_of_payment"],
                                              'user_detail' => $pt_data[0]
                                             );

                            if(isset($input["block_key_return"]) && $input["block_key_return"] != "" && isset($rpt_data) && count($rpt_data) > 0) {
                                $pg_array['rticket_id'] = $rpt_data[0]->ticket_id;
                                $pg_array['rop_name'] = $rpt_data[0]->op_name;
                                $pg_array['rpsngr_no'] = $rpt_data[0]->num_passgr;
                                $pg_array['rticket_ref_no'] = $input["block_key_return"];

                                if($this->session->userdata("available_detail_return") != null && ors_agent_ticket_entry($this->session->userdata("available_detail_return"))) {
                                    $agent_return_ticket_array = array(
                                                                        "ticket_id"=> $rpt_data[0]->ticket_id,
                                                                        "ticket_ref_no"=> $rpt_data[0]->ticket_ref_no,
                                                                        "total_without_discount"=> $rpt_data[0]->total_fare_without_discount,
                                                                        "total_paid"=> $rpt_data[0]->tot_fare_amt_with_tax,
                                                                        "markup_value" => $rpt_data[0]->num_passgr * $input["agent_markup"],
                                                                        "agent_total" => $rpt_data[0]->total_fare_without_discount+($rpt_data[0]->num_passgr * $input["agent_markup"]),
                                                                        "markup_type" => ORS_AGENT_MARKUP_TYPE,
                                                                        "markup_price" => ORS_AGENT_MARKUP_AMOUNT
                                                                        );
                                    
                                    $return_check_if_agent_ticket_exits = $this->agent_tickets_model->where(["ticket_id"=> $rpt_data[0]->ticket_id,"ticket_ref_no"=> $rpt_data[0]->ticket_ref_no])->find_all();
                                    if(!$return_check_if_agent_ticket_exits) {
                                        $arta_id = $this->agent_tickets_model->insert($agent_return_ticket_array);
                                        if(!$arta_id) {
                                            $custom_error_mail = array(
                                                                        "custom_error_subject" => "ORS Agent return ticket data not inserted for ticket_id : ".$rpt_data[0]->ticket_id,
                                                                        "custom_error_message" => "Data not inserted in agent_tickets table & ticket not booked."
                                                                        );
                                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                                            redirect(base_url());
                                        }
                                    }
                                    else {
                                        $custom_error_mail = array(
                                                                    "custom_error_subject" => "VIMP ISSUE : ORS Agent ticket",
                                                                    "custom_error_message" => "Please track this agent issue.".json_encode($rpt_data)
                                                                    );
                                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                                    }
                                }
                            }
                            
                            if($this->config->item('calculate_commission')) {
                                $available_detail = $this->session->userdata("available_detail");
                                $update_data = true;
                                $commission_data = $this->calculate_commission($pt_data,$update_data,$available_detail['bus_type_id']);
                                
                                // Return ticket commission 
                                if(isset($rpt_data) && count($rpt_data) > 0 && (empty($rpt_data[0]->discount_id) || $rpt_data[0]->discount_id == 0 ) && isset($input["block_key_return"] )&& $input["block_key_return"] != "") {
                                    $available_detail_return = $this->session->userdata("available_detail_return");
                                    
                                    $update_data = true;
                                    $commission_data = $this->calculate_commission($rpt_data,$update_data,$available_detail_return['bus_type_id']);
                                }

                                if(!empty($onwardJourney_agent_commission_data) || !empty($return_journey_agent_commission_data)) {
                                    
                                    $total_fare_onward_journey = isset($total_fare_onward_journey) ? $total_fare_onward_journey : $pt_data[0]->tot_fare_amt_with_tax;
                                    $total_fare_return_journey = isset($total_fare_return_journey) ? $total_fare_return_journey : $rpt_data[0]->tot_fare_amt_with_tax;
                                    $total_fare   = $total_fare_onward_journey + $total_fare_return_journey;
                                    $pg_array['total_amount']   = $total_fare;
                                    $payment_total_fare = $total_fare;
                                }
                                elseif(!empty($onwardJourney_agent_commission_data)) {
                                    $total_fare   = $total_fare_onward_journey;
                                    $pg_array['total_amount']   = $total_fare;
                                    $payment_total_fare = $total_fare;
                                }
                            }

                            $payment_status = $this->payment_wallet_gateway($input["payment_type"], $payment_total_fare, $pg_array,$input);
            				
                            if(!empty($payment_status) && isset($payment_status["status"]) && $payment_status["status"] == "success"  && $payment_status["type"] == "cca") {
                                load_front_view(CCA_PROCESSING_PAGE,$payment_status);
                            }
                            else {
                               load_front_view(UNEXPECTED_ERROR); 
                            }
                        }
                        else {
                            $custom_error_mail = array(
                                                      "custom_error_subject" => "Ticket data not found",
                                                      "custom_error_message" => "Data not found for block key - ".$blockKey
                                                      );
                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                            load_front_view(UNEXPECTED_ERROR);
                        }
                    }
                }
                else {
                    $custom_error_mail = array(
                                      "custom_error_subject" => "User entered wrong T-PIN.",
                                      "custom_error_message" => "User entered wrong T-PIN. Block key - ".$blockKey
                                      );

                    $this->common_model->management_custom_error_mail($custom_error_mail, "management");
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'You entered wrong T-PIN number.');
                    redirect(base_url() . 'front/home/search_new');
                }
            }
            else {
                $custom_error_mail = array(
                                      "custom_error_subject" => "User Not entered T-PIN.",
                                      "custom_error_message" => "User Not entered T-PIN. Block key - ".$blockKey
                                      );

                $this->common_model->management_custom_error_mail($custom_error_mail, "management");
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'You not entered T-PIN number.');
                redirect(base_url() . 'front/home/search_new');
            }
        }
        else {
            $custom_error_mail = array(
                                      "custom_error_subject" => "Not accepted terms and conditions.",
                                      "custom_error_message" => "User not accepted terms and conditions. Block key - ".$blockKey
                                      );

            $this->common_model->management_custom_error_mail($custom_error_mail, "management");
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'You are not accepted terms and conditions.');
            redirect(base_url());
        }
    }


    //to view ticket after ticket booking
    public function view_ticket($block_key,$return_block_key = "")
    {
        if(!empty($block_key))
        {
            $this->ticket_dump_model->ticket_ref_no    = $block_key;
            $ticket_layout = $this->ticket_dump_model->select();
            // To get ticket booked detail
            $ticket_details = $this->tickets_model->get_ticket_detail_by_refno($block_key);
            if($return_block_key != "" && $block_key != $return_block_key)
            {
                $this->ticket_dump_model->ticket_ref_no    = $return_block_key;
                $rticket_layout = $this->ticket_dump_model->select();
                // To get ticket booked detail
                $rticket_details = $this->tickets_model->get_ticket_detail_by_refno($return_block_key); 
            }
            
            if(count($ticket_layout) > 0)
            {
                // To Check User Exists
                $this->users_model->email = $ticket_layout[0]->email;

                $data["ticket_view"][0]['user_exists'] = $this->users_model->select();
                $data["ticket_view"][0]["ticket_layout"] = $ticket_layout;
                $data["ticket_view"][0]["ticket_details"] = $ticket_details[0];
                $data["ticket_view"][0]["email"] = $ticket_layout[0]->email;
                $data["ticket_view"][0]["journey_type"] = "towards";

                if(isset($rticket_layout) && count($rticket_layout) > 0)
                {
                    $this->users_model->email = $rticket_layout[0]->email;
                    $data["ticket_view"][1]['user_exists'] = $this->users_model->select();
                    $data["ticket_view"][1]["ticket_layout"] = $rticket_layout;
                    $data["ticket_view"][1]["ticket_details"] = $rticket_details[0];
                    $data["ticket_view"][1]["email"] = $rticket_layout[0]->email;
                    $data["ticket_view"][1]["journey_type"] = "return";
                }

                load_front_view(FINAL_BOOKING_VIEW, $data);
            }
            else
            {
                load_front_view(UNAVAILABLE);
            }
        }
        else
        {
            load_front_view(UNAVAILABLE);
        }
    }

    public function get_seat_fare() {

        $input      = $this->input->post();
        $data       = array();
        $stop_det   = array();
        $soap_name  = strtolower($input['op_name'].'_api');

        $trip_data        = $this->trip_master_model->get_trip_detail($input['trip_no']);
        // $from_to_data     = $this->bus_stop_master_model->get_stops_detail($input['from'], $input['to'], $input['op_name']);


        $from_data   = $this->bus_stop_master_model->get_stops_detail($input['from']);
        $to_data   = $this->bus_stop_master_model->get_stops_detail($input['to']);

        $all_det['from_stop']           = $from_data[0]['op_stop_cd'];
        $all_det['bus_from_stop_id']    = $from_data[0]['bus_stop_id'];

        $all_det['bus_to_stop_id']      = $to_data[0]['bus_stop_id'];
        $all_det['to_stop']             = $to_data[0]['op_stop_cd'];
        //dont delete below commented line. we will use it later.
        $all_det['user_type']           = $from_data[0]['op_id'];
        $all_det['service_id']         = $trip_data['op_trip_no'];
        $all_det['date']               = date('d/m/Y', strtotime($input['date']));
        
        $data = $this->$soap_name->seatAvailability($all_det);

        $data['error'] = array();
        //if (isset($data['SeatAvailabilityResponse']) && !isset($data['SeatAvailabilityResponse']['serviceError']) ) {
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
            if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
                $op_trip_no                             = $data['SeatAvailabilityResponse']['serviceId'];
                $tot_fare                               = $data['SeatAvailabilityResponse']['fare'];
                $tot_data_seat_layout                   = $this->booking_model->get_search($op_trip_no, $input['trip_no']);
                $tot_seats                              = count($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
                $update_travel_data                     = array();
                $update_travel_data['trip_no']          = $input['trip_no'];
                $update_travel_data['bus_from_stop_id'] = $all_det['bus_from_stop_id'];
                $update_travel_data['bus_to_stop_id']   = $all_det['bus_to_stop_id'];
                $update_travel_data['fare']             = $tot_fare;
                $update_travel_data['child_seat_fare']  = $data['SeatAvailabilityResponse']['childFare'];;
                $update_travel_data['tot_seats']        = $tot_seats;
                $update_travel_data['search_date']      = $input['date'];
                $update_travel_data['from']             = $input['from'];
                $update_travel_data['to']               = $input['to'];
               
                $update_travel_destn    = $this->travel_destinations_model->update_fare_totseats($update_travel_data);
               
                $data_update['fare'] = $update_travel_data['fare'];
                $data_update['tot_seats'] = $update_travel_data['tot_seats'];
                data2json($data_update);
            }
        } else {
            $data['error']                          = $data['SeatAvailabilityResponse']['serviceError'];
            $update_travel_data                     = array();
            $update_travel_data['trip_no']          = $input['trip_no'];
            $update_travel_data['bus_from_stop_id'] = $all_det['bus_from_stop_id'];
            $update_travel_data['bus_to_stop_id']   = $all_det['bus_to_stop_id'];
            
            $update_travel_data['fare']             = 0; //$tot_fare;
            $update_travel_data['tot_seats']        = 0; //$tot_seats;
            $update_travel_data['child_seat_fare']  = 0; //child fare
            
            $update_travel_data['search_date']      = $input['date'];
            $update_travel_data['from']             = $input['from'];
            $update_travel_data['to']               = $input['to'];

            $update_travel_destn = $this->travel_destinations_model->update_fare_totseats($update_travel_data);
            $data_update['fare'] = $update_travel_data['fare'];
            $data_update['tot_seats'] = $update_travel_data['tot_seats'];
                     
            data2json($data_update);
        }
    }

    public function booking_history()
    {
        $user_id = $this->session->userdata("user_id");

        if($user_id > 0)
        {
            $data["ticket_details"] = $this->tickets_model->get_booking_history_by_userid($user_id);
            //$data["ticket_details"] = $ticket_details;
            load_front_view(BOOKING_HISTORY_VIEW, $data);
        }
        else
        {
            redirect(base_url());
        }
    }

    public function booking_history_json()
    {
        is_ajax_request();

        $user_id = $this->session->userdata("user_id");

        $td = array();
        if($user_id > 0)
        {
            $ticket_details = $this->tickets_model->get_booking_history_by_userid($user_id);
            // $data["ticket_details"] = $ticket_details;
            foreach ($ticket_details as $key => $value) {
                $value['bus_dept_time'] = date("d M Y h:i A",strtotime($value['dept_time']));
                //$value['inserted_date'] = date("d M Y h:i A",strtotime($value['inserted_date']));
                $td[$value["inserted_date"]][] = $value;
               
            }
          
            $data['tickets'] = $td;
            $data["flag"] = '@#success#@';
            data2json($data);
        }
    
    }


    /*
    * @Author      : Suraj Rathod
    * @function    : print_sms_ticket()
    * @param       : 
    * @detail      : view of print/sms/email ticket by pnr number.
    *                 
    */
    public function print_sms_ticket()
    {
        $data = [];
        $data["metadata_title"] = $this->config->item('title', 'print_sms_ticket_metadata');
        $data["metadata_description"] = $this->config->item('description', 'print_sms_ticket_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'print_sms_ticket_metadata');
        load_front_view(PRINT_SMS_TICKET,$data);
    }



    /*
    * @Author      : Suraj Rathod
    * @function    : ticket_by_sms_email()
    * @param       : 
    * @detail      : get ticket by sms and email or view and print ticket.
    *                 
    */
    public function ticket_by_sms_email ()
    {
        //is_ajax_request();
        $ticket_dump_data = array();
        $input = $this->input->post();

        //same functionality implemented in two pages. is_history_task used to differentiate.
        $is_history_task = $input["is_history_task"];
        $ticket_mode = $input["ticket_mode"];

        if($is_history_task)
        {
            $ticket_ref_no = $input["ticket_ref_no"];
        }
        else
        {
            $bos_ref_no = $input["bos_ref_no"];
            $email = $input["email"];
        }
       
        if($ticket_mode == "print" || $ticket_mode == "sms" || $ticket_mode == "mail")
        {
            if($is_history_task)
            {
                if($this->session->userdata("user_id") > 0)
                {
                    $ticket_dump_data = $this->tickets_model->get_history_ticket_sms_email($ticket_ref_no,$this->session->userdata("user_id"));
                    $bos_ref_no = $ticket_dump_data[0]->boss_ref_no;
                }
            }
            else
            {
                $ticket_dump_data = $this->tickets_model->get_ticket_sms_email($bos_ref_no,$email);
            }

            if(count($ticket_dump_data) > 0)
            {
                if($ticket_dump_data[0]->status == "Y")
                {
                    if($ticket_mode == "print")
                    {
                        $data['ticket_details'] = $ticket_dump_data[0];
                        $data["flag"] = '@#success#@';
                        $data['type'] = "print"; 
                    }
                    else if ($ticket_mode == "sms")
                    {
                        //Send ticket on Message. 
                        /*
                        *This is for testing Purpose onle.
                        *After discussion change this.
                        */
                        if($ticket_dump_data[0]->eticket_count <= SMS_LIMIT-1)
                        {
                            $ticket_display_fare = $ticket_dump_data[0]->tot_fare_amt_with_tax;
                            eval(FME_AGENT_ROLE_NAME);
                            if(in_array($ticket_dump_data[0]->booked_by, $fme_agent_role_name))
                            {
                                $agent_tickets_data = $this->agent_tickets_model->where("ticket_id",$ticket_dump_data[0]->ticket_id)->find_all();
                                $ticket_display_fare = $agent_tickets_data[0]->agent_total;
                            }
                            //here dept date is boarding date
                            $dept_date = date("D, d M Y, h:i A",strtotime($ticket_dump_data[0]->boarding_time)); 
                            
                            //THIS SEAT NO QUERY CAN BE REDUCE LATER
                            $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_dump_data[0]->ticket_id);

                            if(count($seat_no) > 1)
                            {
                                $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
                            }
                            else
                            {
                                $seat_with_berth = $seat_no[0]->seat_no;
                            }

                            /*$ticket_sms_detail = array(
                                                        "{{pnr_no}}" => $ticket_dump_data[0]->pnr_no,
                                                        "{{bos_ref_no}}" => $ticket_dump_data[0]->boss_ref_no,
                                                        "{{amount_to_pay}}" => $ticket_display_fare,
                                                        "{{from_stop}}" => $ticket_dump_data[0]->from_stop_name,
                                                        "{{to_stop}}" => $ticket_dump_data[0]->till_stop_name,
                                                        "{{seat_no}}" => $seat_with_berth,
                                                        "{{doj}}" => $dept_date
                                                      );

                            $temp_sms_msg = getSMS("ticket_booking");
                            $temp_sms_msg = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);
                            $is_send = sendSMS($ticket_dump_data[0]->mobile_no, $temp_sms_msg, array("ticket_id" => $ticket_dump_data[0]->ticket_id));*/
                            
                            $sms_ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_dump_data[0]->ticket_id)->find_all();
                            $smsData = [];
                            $smsData["data"] = ["passenger_ticket" => $ticket_dump_data[0], "passenger_ticket_details" => $sms_ptd_data];
                            $smsData["seat_with_berth"] = $seat_with_berth;
                            $is_send = sendTicketBookedSms($smsData, $ticket_dump_data[0]->mobile_no);
                            $eticket_count = ($is_send) ? 1 : 0;

                            if($is_send)
                            {
                                $this->tickets_model->ticket_id = $ticket_dump_data[0]->ticket_id;
                                $this->tickets_model->bos_ref_no = $bos_ref_no;
                                $this->tickets_model->eticket_count = $ticket_dump_data[0]->eticket_count + 1; 
                                $this->tickets_model->save();

                                $data["flag"] = '@#success#@';
                                $data['type'] = "sms";
                                $data['msg_type'] = "success"; 
                                $data['msg'] = "Your ticket is sent to ".$ticket_dump_data[0]->mobile_no;
                            }
                            else
                            {
                                $data["flag"] = '@#error#@';
                                $data['type'] = "sms";
                                $data['msg_type'] = "error"; 
                                $data['msg'] = "Problem occured while sending message on ".$ticket_dump_data[0]->mobile_no;
                            }
                        }
                        else
                        {
                            $data["flag"] = '@#error#@';
                            $data['type'] = "sms";
                            $data['msg_type'] = "error"; 
                            $data['msg'] = "Your E ticket limit is ended."; 
                        }
                        /*
                        * The Above SMS Code is temp and For testing purpose only
                        */
                    }
                    else if ($ticket_mode == "mail") {
                        
                        $mail_array = array("subject" => "Bookonspot Ticket", 
                                            "message" => html_entity_decode($ticket_dump_data[0]->ticket),
                                            "to" => $ticket_dump_data[0]->user_email_id
                                            );

                        $mail_result = send_mail($mail_array);

                        // if($mail_result > 0)
                        // {
                           $data['flag'] = "@#success#@";
                           $data['type'] = "mail";  
                           $data['msg_type'] = "success"; 
                           $data['msg'] = "Mail Sent successfully to ".$ticket_dump_data[0]->user_email_id;  
                        // }
                    }
                }
                else
                {
                    $data["flag"] = '@#error#@';
                    $data['type'] = "sms";
                    $data['msg_type'] = "error"; 
                    $data['msg'] = "Ticket is cancelled. Could not perform opration."; 
                }
            }
            else
            {
                $data["flag"] = '@#error#@';
                $data['msg_type'] = "error"; 
                $data['msg'] = "Please provide appropriate details.";
            }
        }
        else
        {
            $data["flag"] = '@#error#@';
            $data['msg_type'] = "error"; 
            $data['msg'] = "Please select ticket method";
        }
        
        data2json($data);
    } // End of ticket_by_sms_email





     /*
    * @Author      : Suraj Rathod
    * @function    : cancellation_charges_by_op_id()
    * @param       : 
    * @detail      : get cancellation charges by operator id.
    *                 
    */
    public function cancellation_charges_by_op_id ()
    {
        is_ajax_request();
        $input = $this->input->post();
        if(!empty($input) && $input["op_id"] != "")
        {
            $data["cancellation_charges"] = $this->cancellation_charges_model->cancellation_charges($input["op_id"]);
            $data["flag"] = '@#success#@';
            $data['msg_type'] = "success"; 
        }
        else
        {
            $data["flag"] = '@#error#@';
            $data['msg_type'] = "error"; 
            $data['msg'] = "Could not fouund cancellation charges";
        }

        data2json($data);
    } // End of cancellation_charges_by_op_id

    /*
    * @Author      : Suraj Rathod
    * @function    : get_handle_response_cca()
    * @param       : 
    * @detail      : Response from ccavenue payment gateway.
    *                 
    */
    public function get_handle_response_cca()
    {
       $data = $this->cca_payment_gateway->GetHandleResponse();
       $this->process_handled_response($data);
    }// End of get_handle_response_cca

	
    /*
    * @Author      : Suraj Rathod
    * @function    : process_handled_response()
    * @param       : 
    * @detail      : process payment gateway response.
    *                 
    */
    public function process_handled_response($data)
    {
        $pg_success_response = array();
        $rpg_success_response = array();
        $api_data = array();
        $rpt_data = array();
        $fb_event_value = 0;
        $notify_seats = "";
        $notify_bos_ref_no = "";

       //this if condition is for payment success
       if($data["is_updated"])
       {
            $transaction_status = array('transaction_status' => "psuccess",
                                        'payment_by' => $data["payment_by"],
                                        'pg_tracking_id' => $data["pg_response"]["tracking_id"],
                                        'merchant_amount' => $data["pg_response"]["amount"],
                                        'pg_charges' => $data["pg_response"]["amount"] - $data["pg_response"]["mer_amount"]
                                        );

            $this->tickets_model->update($data["ticket_id"],$transaction_status);

            //to check if return journey
            $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["ticket_id"]);
            if(count($rpt_data) > 0)
            {
                $transaction_status = array('transaction_status' => "psuccess",
                                            'payment_by' => $data["payment_by"],
                                            'pg_tracking_id' => $data["pg_response"]["tracking_id"]
                                            );

                $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction_status);

                $fb_event_value += $rpt_data[0]->tot_fare_amt_with_tax;
                $notify_seats   .= $rpt_data[0]->num_passgr."+";
            }

            $pt_data =  $this->tickets_model
                        ->where('ticket_ref_no', $data["ticket_ref_no"])
                        ->find_all();

            $fb_event_value += $pt_data[0]->tot_fare_amt_with_tax;
            $notify_seats   .= $pt_data[0]->num_passgr;
            /************* Actual ticket booking Start Here ***********/
            $api_data['ticket_id']              = str_pad($pt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
            $api_data['ticket_ref_no']          = $data["ticket_ref_no"];
            $api_data['seat_count']             = $pt_data[0]->num_passgr;
            $api_data['payment_by']             = $data["payment_by"];
            $api_data['rpt_data']               = $rpt_data;
            $api_data['pt_data']                = $pt_data[0];

            if($pt_data[0]->transaction_status != "success")
            {
               /* $soap_name = strtolower($pt_data[0]->op_name) . '_api';
                $final_tbdata = $this->$soap_name->final_booking($api_data,$data);*/
                $available_detail = $this->session->userdata('available_detail');
                $library_name = strtolower($available_detail["library_name"]).'_api';
                $final_tbdata = $this->$library_name->final_booking($api_data,$data);
                logging_data("data/final_data.log",$final_tbdata,"Check");
                if(trim($final_tbdata["status"]) == "success")
                {
                    if(trim($data["payment_by"]) == "csc")
					{
                        // call BridgePGUtil.php library
                        $bconn            = new BridgePGUtil();
                        //$call_recon  = $bconn->recon_log($data['merchant_txn'],$data['csc_txn'],$data['csc_id'],$data['product_id'],$data['txn_amount'],$data['merchant_txn_date_time'],$data['status_message'],$data['merchant_receipt_no']);
                        //logging_data("pg/csc".date("d-m-Y")."_log.log",$call_recon, 'Response From Recon log API');
                
						$transaction_csc = array('payment_confirmation' => "full");
                        $this->tickets_model->update($data["ticket_id"],$transaction_csc);
						
						if(count($rpt_data) > 0)
                        {
							$this->tickets_model->update($rpt_data[0]->ticket_id,$transaction_csc);
						}
					}
                    if(false && $this->config->item('enable_discount') && strtotime(date('Y-m-d')) <= strtotime($this->config->item('discount_till')) )
                    {
                        $notify_bos_ref_no  =   $final_tbdata["transaction_process_response"]["bos_ref_no"];
                        if(count($rpt_data) > 0)
                        {
                            $notify_bos_ref_no  .= " & ".$final_tbdata["transaction_process_response_return"]["bos_ref_no"];
                        }

                        $discounted_psg_cnt = 0;
                        $discounted_tkt_cnt = 0;

                        $discount_file_path = LOG_FOLDER."offer/count.log";
                        $discounted_psg_cnt = file_read($discount_file_path);

                        $ticket_discount_file_path = LOG_FOLDER."offer/ticket_count.log";
                        $discounted_tkt_cnt = file_read($ticket_discount_file_path);

                        $discounted_psg_cnt = $discounted_psg_cnt + $pt_data[0]->num_passgr;
                        $discounted_tkt_cnt += 1;
                        if(count($rpt_data) > 0)
                        {
                            $discounted_psg_cnt = $discounted_psg_cnt + $rpt_data[0]->num_passgr;
                            $discounted_tkt_cnt += 1;
                        }

                        write_file($discount_file_path, $discounted_psg_cnt, "w+");
                        write_file($ticket_discount_file_path, $discounted_tkt_cnt, "w+");
      
                        $ticket_notification_detail = array(
                                                            "{{bos_ref_no}}" => $notify_bos_ref_no,
                                                            "{{total_amount}}" => $fb_event_value,
                                                            "{{total_seats}}" => $notify_seats,
                                                            "{{cust_name}}" => $data["pg_response"]["billing_name"],
                                                            "{{mobile_no}}" => $pt_data[0]->mobile_no,
                                                            "{{discount_psgr_count}}" =>  $discounted_psg_cnt,
                                                            "{{discount_tkt_count}}" => $discounted_tkt_cnt
                                                          );

                        $msg_to_sent = str_replace(array_keys($ticket_notification_detail),array_values($ticket_notification_detail),INTERNAL_TICKET_NOTIFICATION);

                        sendSMS($this->config->item("developer_mobile_no"), $msg_to_sent);
                    }


                    /*              NOTICE
                     *  If return ticket booking failed send mail.
                     */

                    if(count($rpt_data) > 0)
                    {
                        if(isset($final_tbdata["return_ticket_status"]) && $final_tbdata["return_ticket_status"] == "error")
                        {
                            /***************/
                            $this->error_in_do_confirm_booking($rpt_data);
                            /***************/
                            
                            $tfs_mail_data = [];
                            $tfs_mail_data["payment_gateway_response"] = $data["pg_response"];
                            $tfs_mail_data["journey_ticket_data"] = $rpt_data[0];
                            $tfs_mail_data["api_failed_reason"] = isset($final_tbdata["api_failed_reason"]) ? $final_tbdata["api_failed_reason"] : "confirm ticket booking failed.";
                            $tfs_ticket_amount = $rpt_data[0]->tot_fare_amt_with_tax;
                            $tfs_ticket_id = $rpt_data[0]->ticket_id;

                            $tfs_mail_data["ticket_amount"] = $tfs_ticket_amount;
                            $tfs_mail_data["ticket_id"] = $tfs_ticket_id;
                            $tfs_mail_data["ticket_refund_amount"] = "Please check as it is return journey";
                            $tfs_mail_data["total_ticket_amount"] = $data["pg_response"]["amount"];
                            $tfs_mail_data["ccavenue_charges"] = "Please check as it is return journey";
                            
                            $tfs_mail_data["extra"] = "Please note that. Onward journey ticket booked but return journey failed.";

                            /*$this->common_model->payment_success_ticket_failed_mail($tfs_mail_data);*/
                            $this->common_model->management_payment_success_ticket_failed_mail($tfs_mail_data);
                            $this->common_model->payment_gateway_failed($final_tbdata["data"]);
                        }
                        else if(isset($final_tbdata["return_ticket_status"]) && $final_tbdata["return_ticket_status"] == "success")
                        {
                            /*              NOTICE
                             *  Case I : Onward & Return Journey
                             *  Tickey booked successfully.
                             *  So we called confirm payment API
                             */ 

                            /*$confirmation_array = array();
                            $pg_confirmation_array = array();
                            $confirmation_object = new stdClass();
                            $confirmation_object->reference_no = $data["pg_response"]["tracking_id"];
                            $confirmation_object->amount = $pt_data[0]->tot_fare_amt_with_tax+$rpt_data[0]->tot_fare_amt_with_tax;
                            $confirmation_array[] = $confirmation_object;

                            $pg_confirmation_response = $this->cca_payment_gateway->paymentConfirmation($confirmation_array,array("ticket_id" => $data["ticket_id"]));
                            $pg_confirmation_array["pg_confirmation_response"] = $pg_confirmation_response;
                            $pg_confirmation_array["ticket_id"] = $pt_data[0]->ticket_id;
                            $pg_confirmation_array["return_ticket_id"] = $rpt_data[0]->ticket_id;
                            $pg_confirmation_array["pg_confirmation_response"] = $pg_confirmation_response;
                            
                            $this->common_model->pg_confirmation($pg_confirmation_array);*/
                        }
                    }
                    else
                    {
                        /*              NOTICE
                         *  Case II : Onward Journey
                         *  Only onward journey Here.
                         *  So we called confirm payment API
                         */
                        //call confirm payment api here
                        /*$confirmation_array = array();
                        $pg_confirmation_array = array();
                        $confirmation_object = new stdClass();
                        $confirmation_object->reference_no = $data["pg_response"]["tracking_id"];
                        $confirmation_object->amount = $pt_data[0]->tot_fare_amt_with_tax;
                        $confirmation_array[] = $confirmation_object;

                        $pg_confirmation_response = $this->cca_payment_gateway->paymentConfirmation($confirmation_array,array("ticket_id" => $data["ticket_id"]));
                        $pg_confirmation_array["pg_confirmation_response"] = $pg_confirmation_response;
                        $pg_confirmation_array["ticket_id"] = $pt_data[0]->ticket_id;

                        $this->common_model->pg_confirmation($pg_confirmation_array);*/
                    }

                    /*              NOTICE
                     *  Here we force to register user.
                     *  Sending registration mail to user.
                     */
                    if(!$this->session->userdata("is_user_registered") && $this->session->userdata("booking_user_id"))
                    { 
                        $activation_key = getRandomId(20);
                        $registration_mail_array = array("mail_title" => "bos_registration",
                                                         "mail_client" => $pt_data[0]->user_email_id,
                                                         "mail_replacement_array" => array('{{verification_mail_url}}' => base_url().'admin/login/activate_user_view/'.$activation_key,
                                                                                           '{{fullname}}' => "User"
                                                                                          )
                                                    );

                        $registration_mail_result = setup_mail($registration_mail_array);

                        $update_user_where  = array(
                                                "id" => $this->session->userdata("booking_user_id"),
                                                "email" => $pt_data[0]->user_email_id
                                               );

                        $this->users_model->update($update_user_where,array('activate_key'  => $activation_key));
                    }
                    /********************* User Registration end ************/
                    
                    $this->session->unset_userdata('provider_available_bus');
                    $this->session->unset_userdata('provider_available_bus_return');
                    $this->session->unset_userdata('seatlayout_boarding_points');
                    
                    $final_tbdata["fb_event_code"] = "Purchase";
                    $final_tbdata["fb_event_value"] = $fb_event_value;
                    load_front_view(PAYMENT_SUCCESS_PAGE,$final_tbdata);

                }
                else if(trim($final_tbdata["status"]) == "error" && isset($final_tbdata["return_transaction_process_api"]) && $final_tbdata["return_transaction_process_api"] == "error")
                {
                    $custom_error_mail = array(
                                                "custom_error_subject" => "Payment not confirm for ".$pt_data[0]->ticket_id,
                                                "custom_error_message" => "Return journey transction process api for ticket id".$pt_data[0]->ticket_id." Some error in transaction_process method in library."
                                              );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                    load_front_view(UNEXPECTED_ERROR);
                }
                else if(trim($final_tbdata["status"]) == "error")
                {
                    /***************/
                    $this->error_in_do_confirm_booking($pt_data);
                    /***************/
                    $tfs_mail_data = [];
                    $tfs_mail_data["payment_gateway_response"] = $data["pg_response"];
                    $tfs_mail_data["journey_ticket_data"] = $pt_data[0];
                    $tfs_mail_data["api_failed_reason"] = isset($final_tbdata["api_failed_reason"]) ? $final_tbdata["api_failed_reason"] : "confirm ticket booking failed.";
                    $tfs_ticket_amount = $pt_data[0]->tot_fare_amt_with_tax;
                    $tfs_ticket_id = $pt_data[0]->ticket_id;
                    if(count($rpt_data) > 0)
                    {
                        $tfs_mail_data["return_ticket_data"] = $rpt_data[0];
                        $tfs_ticket_amount .= " , ".$rpt_data[0]->tot_fare_amt_with_tax;
                        $tfs_ticket_id .= " , ".$rpt_data[0]->ticket_id;
                    }

                    $tfs_mail_data["ticket_amount"] = $tfs_ticket_amount;
                    $tfs_mail_data["ticket_id"] = $tfs_ticket_id;
                    $tfs_mail_data["ticket_refund_amount"] = $data["pg_response"]["amount"];
                    $tfs_mail_data["total_ticket_amount"] = $data["pg_response"]["amount"];
                    $tfs_mail_data["ccavenue_charges"] = $data["pg_response"]["amount"] - $data["pg_response"]["mer_amount"];

                    /*$this->common_model->payment_success_ticket_failed_mail($tfs_mail_data);*/
                    $this->common_model->management_payment_success_ticket_failed_mail($tfs_mail_data);
                    $this->common_model->payment_gateway_failed($final_tbdata["data"]);
                }
                else
                {
                    $custom_error_mail = array(
                                                "custom_error_subject" => "Confirm booking not done for ticket id ".$pt_data[0]->ticket_id,
                                                "custom_error_message" => "Status not found at the time of confirmbooking. Payment done & ticket not booked."
                                              );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);

                    load_front_view(UNEXPECTED_ERROR);
                }
            }
            else
            {
                $custom_error_mail = array(
                                            "custom_error_subject" => "Double confirmbooking hit issue ".$pt_data[0]->ticket_id,
                                            "custom_error_message" => "Please track this. Hit double confirmbooking. Ticket Data -> ".json_encode($pt_data)
                                            );

                $this->common_model->developer_custom_error_mail($custom_error_mail);

                redirect(base_url()."front/booking/view_ticket/".$pt_data[0]->ticket_ref_no);
            }
            /************* Actual ticket booking End Here ***********/
       }
       else
       {
            $this->common_model->payment_gateway_failed($data);
       }
    }
    /*
    * @Author      : Suraj Rathod
    * @function    : get_cancel_response_cca()
    * @param       : 
    * @detail      : Cancel Response from ccavenue payment gateway.
    *                 
    */


    public function get_all_bus_service_stops()
    {
        $return = [];
        $input = $this->input->post();
        $data = $this->bus_service_stops_model->get_bus_service_stops_by_op_id($input, false);
        if($data)
        {
           $return["service_stops"] = $data;
           $return["status"] = "success";
        }
        else
        {
            $return["status"] = "error";
        }
        data2json($return);
    }

    public function get_bus_service_stop_details()
    {
        $return = [];
        $input = $this->input->post();
        eval(AGENT_ID_ANGULAR);
        if(in_array($this->session->userdata("user_id"), $agent_id_angular) || ($this->session->userdata("role_id") == ORS_AGENT_USERS_ROLE_ID))
        {
            $input =  (array)json_decode(file_get_contents('php://input'));
        }
        $from_data  = $this->bus_service_stops_model->get_from_bus_service_stops_by_op_id($input, false);
        $to_data    = $this->bus_service_stops_model->get_to_bus_service_stops_by_op_id($input, false);
        if($from_data && $to_data)
        {
           $return["from_service_stops"] = $from_data;
           $return["to_service_stops"] = $to_data;
           $return["status"] = "success";
        }
        else
        {
            $return["status"] = "error";
        }
        data2json($return);
    }

    public function error_in_do_confirm_booking($pt_data)
    {
        $cmessage_to_send = "";

        if(!empty($pt_data))
        {
            if(isset($pt_data[0]->ticket_id))
            {
                $request_response_log = $this->tracking_request_response_model
                                        ->where(["ticket_id" => $pt_data[0]->ticket_id, "type" => "confirm_booking"])
                                        ->find_all();
                if($request_response_log)
                {
                    $request_response_log = $request_response_log[0];
                    $cmessage_to_send .= "<br/><br/>Request - ";
                    $cmessage_to_send .= htmlentities($request_response_log->request);
                    $cmessage_to_send .= "<br/><br/>Response - ".$request_response_log->response;

                    $custom_error_mail = array(
                                                "custom_error_subject" => "Confirm booking Error for ".$pt_data[0]->booked_from." for ticket id ".$pt_data[0]->ticket_id,
                                                "custom_error_message" => $cmessage_to_send
                                              );

                    $this->common_model->management_custom_error_mail($custom_error_mail, $pt_data[0]->booked_from);
                }
            }
        }
    }

    private function calculate_commission($ticket_data,$update_data,$bus_type_id) {
        $data = array();
        // Get Master distributor id
        $master_distributor_id = $this->session->userdata('master_distributor_id');
        
        // Get Currently Active Commission Id
        $comm_id_data       = $this->agent_commission_template_log_model->column('agent_commission_template_log.commission_id')->join("commission c","c.id = agent_commission_template_log.commission_id","left")->where(array("user_id" => $master_distributor_id,"agent_commission_template_log.status" => '1',"c.status" => '1'))->where('("'.date("Y-m-d").'" between `commission_from` and commission_to )')->order_by("agent_commission_template_log.id","desc")->limit('1')->find_all();
        // check 
        if(!empty($comm_id_data[0]->commission_id) && $comm_id_data[0]->commission_id > 0 && (empty($ticket_data[0]->discount_id) || $ticket_data[0]->discount_id == 0 ) ) {
            
            $ticket_detail = $this->ticket_details_model->column('id,final_commission')->where(array("ticket_id" => $ticket_data[0]->ticket_id))->as_array()->find_all(); 
            logging_data("commission/calculate_commission.log",$ticket_detail,"Ticket Detail(Function: process_payment_confirm_booking),ticket id: ".$ticket_data[0]->ticket_id);
            
            $commission_cal_response    = calculate_commission_new($ticket_detail,$ticket_data[0]->provider_id, $ticket_data[0]->op_id,$ticket_data[0]->provider_type,$ticket_data[0]->ticket_id,$comm_id_data[0]->commission_id,$bus_type_id,$update_data);
            
            if(!empty($commission_cal_response)) {
                $data["final_retailer_commission"]  = $commission_cal_response['final_retailer_commission'];
                $data["retailer_commission"]        = $commission_cal_response["retailer_commission"];
                $data["tds_on_retailer_commission"] = $commission_cal_response["tds_on_retailer_commission"];
            }
        }
        logging_data("commission/calculate_commission.log",$data,"calculated data (function : calculate_commission in booking.php),ticket id: ".$ticket_data[0]->ticket_id);
        return $data;
    }
    

    public function bos_wallet_deduction($ticket_id){
        $data['ticket_data'] = json_encode($this->tickets_model->as_array()->find_by(['ticket_id'=>$ticket_id]));
        if(!empty($data['ticket_data'])){
            $data['ticket_detail_data'][] = $this->ticket_details_model->as_array()->find_all_by(['ticket_id'=>$ticket_id]);
        }
       
        $data['operator_commission'] = 0;
        $data['tds'] = 0;
        $data['net_commission']= 0;
        foreach ($data['ticket_detail_data'][0] as $key => $value) {
            $data['operator_commission'] += $value['op_commission'];
            $data['tds'] += $value['tds_on_commission'];
            $data['net_commission'] += $value['final_commission'];
        }
        if(!empty($data['ticket_data'])){
            $data['ticket_detail_data'] = json_encode($data['ticket_detail_data'][0]);
        }
        log_data("bos_wallet_debit/bos_wallet_debit".date("d-m-Y")."_log.log",$data,'Final confirmation Parameter Sent To BOS');

        $response = BosSendDataOverPost($this->config->item("bos_transaction_api_url"),$data,1);

        log_data("bos_wallet_debit/bos_wallet_debit".date("d-m-Y")."_log.log",$response,'Confirmation Response From bos');
        return $response;
    }

    private function add_commission_for_ticket($ticket_detail) {

        $master_distributor_id = $this->session->userdata('master_distributor_id');
        
        // Get Currently Active Commission Id
        $comm_id_data       = $this->agent_commission_template_log_model->column('agent_commission_template_log.commission_id')->join("commission c","c.id = agent_commission_template_log.commission_id","left")->where(array("user_id" => $master_distributor_id,"agent_commission_template_log.status" => '1',"c.status" => '1'))->where('("'.date("Y-m-d").'" between `commission_from` and commission_to )')->order_by("agent_commission_template_log.id","desc")->limit('1')->find_all();
        if(!empty($comm_id_data[0]->commission_id) && $comm_id_data[0]->commission_id > 0 ) {
            $retailer_commission = $this->commission_calculation_model->retailer_commission_calculation($ticket_detail[0]->ticket_id,$this->session->userdata('user_id'));
            if(is_array($retailer_commission) && $retailer_commission[0]->total_commission > 0 ) {
                $this->db->trans_begin();

                $commission_amount = $retailer_commission[0]->total_commission;
                /******* Add Amount in to user wallet *******/ 
                // Get user wallet data 
                $user_wallet_data = $this->wallet_model->get_wallet_detail($this->session->userdata('user_id'));
                if(!empty($user_wallet_data)) {
                    $user_actual_wallet_amount = $user_wallet_data[0]->amt;
                    $user_updated_wallet_amount = $user_actual_wallet_amount + $commission_amount;
                    $transaction_no = substr(hexdec(uniqid()), 4, 12);
                    // insert into wallet trans for user
                    $wallet_trans_arr = array(
                                        'w_id'                => $user_wallet_data[0]->id,
                                        'amt'                 => $commission_amount,
                                        'user_id'             => $this->session->userdata('user_id'),
                                        'amt_before_trans'    => $user_actual_wallet_amount,    
                                        'amt_after_trans'     => $user_updated_wallet_amount,
                                        'status'              => 'added',
                                        'transaction_type'    => 'Credited',
                                        'comment'             => 'Commission for ticket booking',
                                        'wallet_type'         => 'actual_wallet',
                                        'ticket_id'           => $ticket_detail[0]->ticket_id,
                                        'is_status'           => 'Y',
                                        'transaction_type_id' => 15,
                                        'added_on'            => date('Y-m-d H:i:s'), 
                                        'added_by'            => $this->session->userdata('user_id'),
                                        'transaction_no'      => $transaction_no
                                    );
                    $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                    
                    if($wallet_trans_id){
                        $data = array(
                                    'amt'          => $user_updated_wallet_amount,
                                    'updated_by'   => $this->session->userdata('user_id'),
                                    'updated_date' => date('Y-m-d H:i:s'),
                                    'last_transction_id' => $wallet_trans_id
                                );
                        $UpdateId = $this->common_model->update('wallet','user_id',$this->session->userdata('user_id'),$data);

                        // Get rokad wallet data
                        if($UpdateId) {
                            /*$RollID = COMPANY_ROLE_ID;
                            $RokadUserData = $this->common_model->get_value('users','id','role_id='.$RollID); 
                            $rokad_wallet_data = $this->wallet_model->get_wallet_detail($RokadUserData->id);    
                            if(!empty($rokad_wallet_data)) {
                                $rokad_actual_wallet_amount = $rokad_wallet_data[0]->amt;
                                $rokad_updated_wallet_amount = $rokad_actual_wallet_amount - $commission_amount;
                                
                                // insert into wallet trans for user
                                $wallet_trans_arr = array(
                                                    'w_id'                => $rokad_wallet_data[0]->id,
                                                    'amt'                 => $commission_amount,
                                                    'user_id'             => $RokadUserData->id,
                                                    'amt_before_trans'    => $rokad_actual_wallet_amount,    
                                                    'amt_after_trans'     => $rokad_updated_wallet_amount,
                                                    'status'              => 'deducted',
                                                    'transaction_type'    => 'Debited',
                                                    'comment'             => 'Commission for ticket booking',
                                                    'is_status'           => 'Y',
                                                    'wallet_type'         => 'actual_wallet',
                                                    'ticket_id'           => $ticket_detail[0]->ticket_id,
                                                    'transaction_type_id' => 16,
                                                    'added_on'            => date('Y-m-d H:i:s'), 
                                                    'added_by'            => $this->session->userdata('user_id')
                                                );
                                $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                                
                                if($wallet_trans_id){
                                    $data = array(
                                                'amt'          => $rokad_updated_wallet_amount,
                                                'updated_by'   => $this->session->userdata('user_id'),
                                                'updated_date' => date('Y-m-d H:i:s'),
                                                'last_transction_id' => $wallet_trans_id
                                            );
                                    $UpdateId = $this->common_model->update('wallet','user_id',$RokadUserData->id,$data);
                                    if($UpdateId) {
                                    }
                                    else {
                                        $custom_error_mail = array(
                                                                "custom_error_subject" => " VVVVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                                "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                                              );

                                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                                        
                                        $data['msg'] = "error";
                                    }
                                }
                                else {
                                    $custom_error_mail = array(
                                                            "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                            "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                                          );

                                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                                    
                                    $data['msg'] = "error";
                                }
                            }
                            else {
                                $custom_error_mail = array(
                                                            "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                            "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                                          );

                                $this->common_model->developer_custom_error_mail($custom_error_mail);
                                
                                $data['msg'] = "error";
                            } */ 

                            // update commission calculation table 
                            $update_arr = array('booking_flag' => "Y",
                                                'booking_flag_date' => date('Y-m-d H:i:s')
                                            );

                            $this->commission_calculation_model->update(array("user_id" => $this->session->userdata('user_id'), "ticket_id" => $ticket_detail[0]->ticket_id),$update_arr);
                            $data['msg'] = "success";  
                        }
                        else {
                            $custom_error_mail = array(
                                                    "custom_error_subject" => " VVVVVIMP commission for retailer not added for ".$ticket_detail[0]->ticket_id,
                                                    "custom_error_message" => "commission for retailer not added for ".$ticket_detail[0]->ticket_id." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                                  );

                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                            
                            $data['msg'] = "error";
                        }
                    }
                    else {
                        $custom_error_mail = array(
                                                "custom_error_subject" => " VVVIMP commission for retailer not added for ".$ticket_detail[0]->ticket_id,
                                                "custom_error_message" => "commission for retailer not added for ".$ticket_detail[0]->ticket_id." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                              );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                        
                        $data['msg'] = "error";
                    }
                }
                else {
                    $custom_error_mail = array(
                                                "custom_error_subject" => " VVVIMP commission for retailer not added for ".$ticket_detail[0]->ticket_id,
                                                "custom_error_message" => "commission for retailer not added for ".$ticket_detail[0]->ticket_id." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                              );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                    
                    $data['msg'] = "error";
                }

                if ($this->db->trans_status() === FALSE) {
                    show(error_get_last());
                    $this->db->trans_rollback();
                }
                else {
                    $this->db->trans_commit();
                } 
            }
            else {
                $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission detail not found  for ".$ticket_detail[0]->ticket_id,
                                            "custom_error_message" => "commission detail not found for ".$ticket_detail[0]->ticket_id." Query for search commission calculation detail :".$this->db->last_query()
                                          );

                $this->common_model->developer_custom_error_mail($custom_error_mail);
                
                $data['msg'] = "error";
            }
        }
        else {
            $data['msg'] = "sucess";
        }
        return $data;
    }
}
?>
