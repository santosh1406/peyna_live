<?php

class Its extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('tickets_model', 'ticket_details_model', 'users_model', 'op_config_model', 'mail_template_model', 'ticket_dump_model', 'ticket_tax_trans_model', 'processed_ticket_tax_trans_model', 'etravels_bus_service_model', 'api_bus_types_model', 'temp_bus_types_model', 'return_ticket_mapping_model', 'api_bus_services_model'));
        $this->load->library("Excel", "excel_reader");
        $this->load->library(array('api_provider/its_api', 'api_provider/etravelsmart_api', 'api_provider/hrtc_api', 'cca_payment_gateway'));

        $this->load->model(array('bus_source_destination_model', 'op_bus_types_model', 'state_model', 'bus_stop_master_model', 'bus_type_model',
            'travel_destinations_model', 'operator_commission_model', 'agent_commission_model', 'boarding_stop_model', 'op_seat_layout_model', 'op_layout_details_model'
                )
        );
    }

    public function index() {
        show("Welcome to ITS API", 1);
    }

    public function getSource() {
        $response = $this->its_api->getSource();
        show($response, 1);
    }

    public function getDestinationsBasedOnSource() {
        //703 - Mumbai
        $response = $this->its_api->getDestinationsBasedOnSource("703");
        show($response, 1);
    }

    public function getAvailableRoutes() {
        $response = $this->its_api->getAvailableRoutes("703", "718", "30-11-2016");
        show($response, 1);
    }

    public function getSeatArrangementDetails() {
        $reference_number = "1#703#718#4097#10098#101#30112016#10:00 PM#9:30 AM";
        $response = $this->its_api->getSeatArrangementDetails($reference_number);
        show($response, 1);
    }

    public function getBoardingDropLocationsByCity() {
        $company_id = "1";
        $city_id = "703";
        $response = $this->its_api->getBoardingDropLocationsByCity($company_id, $city_id);
        show($response, 1);
    }

    public function blockTicket() {
        $data = [
            "reference_number" => "1#703#718#4097#10098#101#30112016#10:00 PM#9:30 AM",
            "passenger_name" => "Suraj Rathod",
            "seat_number_gender" => "1,M|10,F",
            "email" => "suraj.trimax@gmail.com",
            "phone" => "7738222015",
            "pickup_id" => "11915",
            "payable_amount" => 1000,
            "total_passengers" => 2
        ];
        $response = $this->its_api->blockSeatV2($data);
        show($response, 1);
    }

    public function confirmBlockTicket() {
        $data = [
            "reference_number" => "1#703#718#4097#10098#101#30112016#10:00 PM#9:30 AM",
            "passenger_name" => "Suraj Rathod",
            "seat_number_gender" => "1,M|10,F",
            "email" => "suraj.trimax@gmail.com",
            "phone" => "7738222015",
            "pickup_id" => "11915",
            "payable_amount" => 1000,
            "total_passengers" => 2
        ];
        $response = $this->its_api->confirmBlockSeat($data);
        show($response, 1);
    }

    public function cancel_details() {
        $data = [
            "pnr_no" => "51228437"
        ];
        $response = $this->its_api->cancelDetails($data);
        show($response, 1);
    }

    public function confirm_cancel() {
        $data = [
            "pnr_no" => "51228437"
        ];
        $response = $this->its_api->confirmCancellation($data);
        show($response, 1);
    }

    public function insertAllSDPairs() {
        $source = $this->its_api->insertAllSDPairs();
        show("Done", 1);
    }
    public function getBalance()
    {
        $balance = $this->its_api->GetCurrentAccountBalance();
        show($balance,1);
    }

}
