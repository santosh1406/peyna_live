<?php

class Cancel_booking extends CI_Controller {

    var $soap_name;
    var $payment_by_array;
    
    public function __construct() {
        parent::__construct();
        $this->load->library(array('cca_payment_gateway','pagination','Nusoap_lib','api_provider/etravelsmart_api','api_provider/its_api','api_provider/hrtc_api','soap_api/upsrtc_api','soap_api/msrtc_api','soap_api/rsrtc_api'));
        /*,'agent/BridgePGUtil'*/
        $this->load->helper('soap_data');
        $this->load->model(array('soap_client_model', 'tickets_model', 'ticket_details_model', 'cancel_booking_model','cancel_ticket_model', 'users_model', 'common_model', 'return_ticket_mapping_model','wallet_model','wallet_trans_model','wallet_trans_log_model','credit_account_detail_model','payment_gateway_master_model','payment_gateway_wallet_trans_model','payment_gateway_wallet_model','payment_gateway_cancel_order_model','payment_transaction_fss_model','commission_calculation_model'));
        $this->payment_by_array = array("wallet");
        check_front_access(); 
       // $this->bconn = new BridgePGUtil();
    }
    

    function index()
    {
        $data = array();
        $input = $this->input->post();

        $data['email_id']       = isset($input['email']) ? $input['email'] : '';
        $data['pnr_no']         = isset($input['pnr_no']) ? $input['pnr_no'] : '';
        $data['bos_ref_no']     = isset($input['bos_ref_no']) ? $input['bos_ref_no'] : '';
        $data['is_support']     = ((strtolower($this->session->userdata("role_name")) == SUPPORT_ROLE_NAME) && ($data['email_id'] != "") && ($data['pnr_no'] != "" || $data['bos_ref_no'] != "")) ? 0 : 1;
        $user_id                = $this->session->userdata("user_id");
        $data['user_id']        = $user_id;
        $currentDate = date('Y-m-d');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $per_page               = 5;
        $ticket_data            = $this->tickets_model->get_search($data, $page, $per_page);
        $data['ticket_detail']  = $ticket_data['data'];
        $total_rows             = $ticket_data['count'];

        $config["base_url"] = base_url() . "cancel_booking";
    
        $config["total_rows"]        = $total_rows ;
        // $config['use_page_numbers']  = false;
        $config["per_page"]          = $per_page;
        $config['num_links']         = $total_rows;
        $config['next_link']         = 'Next';
        $config['prev_link']         = 'Previous';

        $this->pagination->initialize($config);                        

        $data['links']          =   $this->pagination->create_links();
        
        $data["metadata_title"] = $this->config->item('title', 'cancel_booking_metadata');
        $data["metadata_description"] = $this->config->item('description', 'cancel_booking_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'cancel_booking_metadata');

        if(isset($this->session->userdata['user_id'])) {
            $data['wallet_bs'] = $this->wallet_model->where(array('user_id' => $this->session->userdata('user_id')))->find_all();
            
            $actual_wallet_balance = (!empty($data['wallet_bs'])) ? $data['wallet_bs'][0]->amt : 0;
            $data['wallet_agent_balance_show'] = $actual_wallet_balance; 
        }
        load_front_view(CANCEL_BOOKING_VIEW, $data);
    }

    function cancel_tickets1() {
        $data                   = array();
        $seat_no                = array();
        $request                = array();
        $request['seat_no']     = array();
        $is_true                = true;
        $input                  = $this->input->post();   
        $request['pnr_no']      = $input['pnr_no'];
        $provider_id            = $input['pro_id'];
        $provider_type          = $input['proty'];
        $op_id                  = $input['opi'];
        // Get Ticket Info
        $tinfo = $this->tickets_model->get_ticket_and_detail($input['email'], $input['pnr_no']);
        if($tinfo) {
            $to_allow_cancel = true;
            if(!$this->session->userdata('role_id')) {
                $to_allow_cancel = false;
            }

            if($to_allow_cancel) {
                if(in_array($tinfo["payment_confirmation"], array("full","partial","auto_confirmed"))) {
                    if((strtolower($tinfo["payment_by"]) == "wallet") || (time() > strtotime("+60 minutes", strtotime($tinfo["inserted_date"])))) {
                        $partial_cancellation = $tinfo["partial_cancellation"];
                        $ticket_id            = $tinfo["ticket_id"];

                        if($ticket_id != $input['ticket_id']) {
                            $hack_string = "User With user id = ".$tinfo["inserted_by"]." & email id ".$tinfo["user_email_id"]." tried to crack the system or chnaged parameter(Ticket Id - ".$input['ticket_id'].") while cancelling ticket.";
                            logging_data("hack/hack_".date("d-m-Y")."_log.log",$hack_string, 'Ticket Cancel');
                        }

                        if($partial_cancellation == 0) {
                            //This is partial cancellation false condition.
                            $seat_no = $request['seat_no']  =  explode(",", $tinfo["seat_no"]);
                        }
                        else if($partial_cancellation == 1 && !empty($input['seat_no'])) {
                            //This is partial cancellation true condition.
                            $seat_no = $request['seat_no']  =  explode(",", $input["seat_no"]);
                        }
                        else {
                            $is_true = false;
                        }

                        if($is_true) {
                            if((strtolower($tinfo["payment_by"]) == "wallet") || (strtolower($tinfo["payment_by"]) == "csc")) {
                                $is_true = true;
                            }
                            else {
                                $is_true = true;
                                /*      VIMP : DO NOT REMOVE CODE (check_is_return_ticket_refund)
                                 *  This is code is useful at the time of only one journey cancellation
                                 */
                                $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_return_ticket_refund_condition($tinfo["ticket_id"]);
                            
                                if($check_is_return_ticket_refund) {
                                    if($check_is_return_ticket_refund[0]->onward_transaction_status == "success" && $check_is_return_ticket_refund[0]->return_transaction_status == "success" ) {
                                        $is_true = true;
                                    }
                                    else {
                                        $is_true = false;
                                        if(!empty($check_is_return_ticket_refund[0]->onward_cancel_date) && time() > strtotime("+6 minutes", strtotime($check_is_return_ticket_refund[0]->onward_cancel_date))) {
                                            $is_true = true;
                                        }
                                        else if(!empty($check_is_return_ticket_refund[0]->return_cancel_date) && time() > strtotime("+6 minutes", strtotime($check_is_return_ticket_refund[0]->return_cancel_date))) {
                                            $is_true = true;
                                        }

                                        $try_after_minutes = 0;
                                        if(!$is_true && !empty($check_is_return_ticket_refund[0]->onward_cancel_date)) {
                                            $try_after_minutes = round(abs(time() - strtotime($check_is_return_ticket_refund[0]->onward_cancel_date)) / 60,2);
                                        }
                                        else if(!$is_true && !empty($check_is_return_ticket_refund[0]->return_cancel_date)) {
                                            $try_after_minutes = round(abs(time() - strtotime($check_is_return_ticket_refund[0]->return_cancel_date)) / 60,2);
                                        }

                                        if($try_after_minutes > 0) {
                                            $try_after_minutes = 6 - $try_after_minutes;
                                            $try_after_minutes .= " minutes";
                                        }
                                        else {
                                            $try_after_minutes = "sometime";
                                        }
                                    }
                                }
                            }
                            
                            if($is_true) {
                                if(isset($input['pnr_no']) && $input['pnr_no'] != "" && isset($provider_id) && $provider_id && isset($input['email']) && $input['email'] != "") {
                                    $ldetails = $this->common_model->get_provider_lib($provider_id, $provider_type, $op_id);
                                    $library_name =  strtolower(trim($ldetails['library'])).'_api';

                                    //for Etravelsmart api we need api_ticket_no
                                    $request["api_ticket_no"]                   = $tinfo["api_ticket_no"];
                                    $request["current_ticket_information"]      = $tinfo;
                                    if($partial_cancellation == 0) {
                                        $cancel_data = $this->{$library_name}->cancel_ticket($request);
                                    }
                                    else if($partial_cancellation == 1) {
                                        $cancel_data = $this->{$library_name}->cancel_ticket($request);
                                       // $cancel_data = $this->{$library_name}->cancel_ticket_fullpartial($request);
                                    }
                                
                                    if(strtolower($cancel_data['status']) == 'success') {
                                        if($partial_cancellation == 1) {
                                            // Use for incentive
                                            $inactive_ptd_data = $this->ticket_details_model->column("SUM(CASE WHEN `status` = 'Y' THEN 1 ELSE 0 END) AS total_active,SUM(CASE WHEN `status` = 'N' THEN 1 ELSE 0 END) AS total_inactive")->where(["ticket_id"=>$ticket_id])->find_all();
                                            $inactive_data = is_array($inactive_ptd_data) ? $inactive_ptd_data[0]->total_inactive : 0;
                                            // Use for the cancellation type(Full or partial)                                           
                                            $total_cancel_seat = $inactive_data + count($request['seat_no']);
                                            $total_passgr = $inactive_ptd_data[0]->total_inactive + $inactive_ptd_data[0]->total_active;
                                            
                                            if($total_passgr == $total_cancel_seat) {
                                                $seats_to_update = $inactive_ptd_data[0]->total_inactive + $inactive_ptd_data[0]->total_active;
                                                $ticket_update_array = array("transaction_status" => "cancel",
                                                                              "num_passgr" => $seats_to_update,
                                                                              "status" => 'N'
                                                                             );
                                            }
                                            else {
                                                $seats_to_update = $inactive_ptd_data[0]->total_active-count($request['seat_no']);
                                                $ticket_update_array = array("transaction_status" => "success",
                                                                              "status" => 'Y',
                                                                              "num_passgr" => $seats_to_update,
                                                                              "is_partially_cancelled" => "Y",
                                                                              "pnr_no" => $cancel_data['newTicketNO']
                                                                             );
                                            }
                                        }
                                        else {
                                            $ticket_update_array = array("transaction_status" => "cancel",
                                                                          "status" => 'N'
                                                                         );
                                        }
                                        // update tickets table
                                        $ticket_update_where = array('pnr_no' => $input['pnr_no'], "user_email_id" => $input['email'], "ticket_id" => $ticket_id);

                                        $is_ticket_updated = $this->tickets_model->update_where($ticket_update_where,"",$ticket_update_array);

                                        if($is_ticket_updated) {
                                            /*                  NOTICE
                                             *After Discussion Handle PARTIAL CANCELLATION HERE
                                             *i.e. Handle seatwise cancellation
                                             */
                                            if($partial_cancellation == 1) {
                                                $is_ticket_detail_updated = $this->ticket_details_model->where_in("seat_no",$request['seat_no'])->update_where(array("ticket_id" => $ticket_id),"",array("status" => "N"));
                                                if($is_ticket_detail_updated) {
                                                 //   $this->{$library_name}->regenerateTicket($tinfo);
                                                }
                                            }
                                            else {
                                                $is_ticket_detail_updated = $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id),"",array("status" => "N"));
                                            }

                                            $cancel_pnr_to_update = "";
                                            if(isset($cancel_data['newTicketNO'])) {
                                                $cancel_pnr_to_update = $cancel_data['newTicketNO'];
                                            }
                                            else if(isset($cancel_data['newPNR'])) {
                                                $cancel_pnr_to_update = $cancel_data['newPNR'];
                                            }
                                            //Save data in cancel ticket table
                                            $this->cancel_ticket_model->ticket_id                   = $ticket_id;
                                            $this->cancel_ticket_model->pnr_no                      = $input['pnr_no'];
                                            $this->cancel_ticket_model->new_pnr                     = $cancel_pnr_to_update;
                                            $this->cancel_ticket_model->seat_no                     = implode(",", $seat_no);
                                            $this->cancel_ticket_model->refund_amt                  = $cancel_data['refundAmount'];
                                            $this->cancel_ticket_model->cancel_charge_operator      = $cancel_data["cancellationCharge"];
                                            $this->cancel_ticket_model->cancel_percentage           = isset($cancel_data["cancellationChargePer"]) ? str_replace("%", "", $cancel_data["cancellationChargePer"]) : "";
                                            $this->cancel_ticket_model->raw                         = json_encode($cancel_data);
                                            $id = $this->cancel_ticket_model->save();
                                            if($id) {
                                                if(($tinfo['payment_by'] != 'wallet')  && (strtolower(trim($tinfo['payment_by'])) != 'csc') ) {
                                                    $paymentgateway_ref_no = "";
                                                    $paymentgateway_ref_no = $tinfo["pg_tracking_id"];
                                                        
                                                    if($partial_cancellation == 0) {
                                                        $calculated_cancellation_charges = $tinfo["total_fare_without_discount"]-$tinfo["fare_convey_charge"]-$cancel_data["refundAmount"];
                                                        $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                        $acual_refund_paid = getRefundAmountToPay($tinfo["tot_fare_amt_with_tax"],$cancel_data["cancellationCharge"]);
                                                        $acual_refund_paid_to_customer = getRefundAmountToPay($tinfo["total_fare_without_discount"],$cancel_data["cancellationCharge"]);
                                                    }
                                                    else if($partial_cancellation == 1) {
                                                        $seat_sum = $this->ticket_details_model->column("sum(fare_amt) as total_fare_amt, sum(op_commission) as commission_amount, sum(tds_on_commission) as tds_amount,sum(final_commission) as pg_commission_after_tds")->where_in("seat_no" , $request['seat_no'])->where(["ticket_id"=>$ticket_id])->find_all();
                                                        if($seat_sum) {
                                                            $calculated_cancellation_charges = $seat_sum[0]->total_fare_amt-$cancel_data["refundAmount"];
                                                            $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                            $total_fare_amt = (($seat_sum[0]->total_fare_amt - $seat_sum[0]->commission_amount - $seat_sum[0]->discount_value) + $seat_sum[0]->tds_amount);
                                                            $acual_refund_paid = getRefundAmountToPay($total_fare_amt,$cancel_data["cancellationCharge"]);
                                                            $acual_refund_paid_to_customer = getRefundAmountToPay($tinfo["total_fare_without_discount"],$cancel_data["cancellationCharge"]);
                                                        }
                                                    }

                                                    $merchant_array_data  = array(
                                                                                "reference_no" => $paymentgateway_ref_no,
                                                                                "refund_amount" => $acual_refund_paid,
                                                                                "refund_ref_no" => uniqid()
                                                                                );

                                                    $refund_status = $this->cca_payment_gateway->refundOrder($merchant_array_data);
                                                        
                                                    if($refund_status->reason == "" && !$refund_status->refund_status) {
                                                        $update_refund_status = array('is_refund' => "1",
                                                                                    "actual_refund_paid" => $acual_refund_paid,
                                                                                    "cancel_charge" => $calculated_cancellation_charges,
                                                                                    "pg_refund_amount" => $acual_refund_paid
                                                                                );
                                                        // Update cancel ticket table                        
                                                        $update_where_cancel = array("id" => $id);
                                                        $refund_id = $this->cancel_ticket_model->update($update_where_cancel,$update_refund_status);
                                                        
                                                        if(!$refund_id) {
                                                            log_data("refund/success_but_not_updated_ccavenue_refund_".date("d-m-Y")."_log.log","Refund is success but status is not updated for 'cancel_ticket_data table' for id ".$id,'cancel_ticket_data table id');
                                                        }

                                                        $cancel_detail = array("{{bos_ref_no}}" => $tinfo["boss_ref_no"],
                                                                                "{{amount_to_pay}}" => (isset($acual_refund_paid)) ? $acual_refund_paid : "NA",
                                                                         );
                                                        $temp_sms_msg = str_replace(array_keys($cancel_detail),array_values($cancel_detail),BOS_CANCEL_TICKET);
                                                        log_data("trash/temp_sms_msg_".date("d-m-Y")."_log.log",$temp_sms_msg, 'cancel temp_sms_msg');
                                                        // Send Sms
                                                        $is_send = sendSMS($tinfo["mobile_no"], $temp_sms_msg, array("ticket_id" => $ticket_id));
                                                        //TEMP success flag. WHEN SMS ACTIVE REMOVE THIS LINE
                                                        $data['flag']   =   '@#success#@'; 
                                                        //KEEP THIS INSIDE SUCCESS FLAG
                                                        $data['refund_amt']            = in_array($this->session->userdata('role_name'),$fme_agent_role_name) ? $acual_refund_paid_to_customer : $acual_refund_paid ;
                                                        // $data['refund_amt_to_customer'] = $acual_refund_paid_to_customer; 
                                                        $data['cancellation_cahrge']    = $cancel_data["cancellationCharge"];

                                                        if(in_array($tinfo['booked_from'],array('rsrtc','etravelsmart'))) {
                                                            // cancelled_ticket
                                                            if($partial_cancellation == 1) {
                                                                if($total_passgr == $total_cancel_seat) {
                                                                    $ptd_data = $this->ticket_details_model->where('ticket_id', $tinfo['ticket_id'])->find_all();
                                                                    $cancel_mail_content = $this->{$library_name}->mail_cancel_ticket($tinfo,$data,$ptd_data);
                                                                }
                                                            }
                                                            else {
                                                                $ptd_data = $this->ticket_details_model->where('ticket_id', $tinfo['ticket_id'])->find_all();
                                                                $cancel_mail_content = $this->{$library_name}->mail_cancel_ticket($tinfo,$data,$ptd_data);
                                                            }
                                                                
                                                            $this->ticket_dump_model->ticket_ref_no = $tinfo['ticket_ref_no'];
                                                            $ticket_layout = $this->ticket_dump_model->select();
                                                        }
                                                        // To get ticket booked detail
                                                        $ticket_details = $this->tickets_model->get_ticket_detail_by_refno($tinfo['ticket_ref_no']);
                                                        // To Send Cancelled ticket Mail Start here //
                                                        /************ Mail Start Here ********/
                                                        
                                                        $mail_replacement_array = array(
                                                                                     "{{username}}" => "User",
                                                                                     "{{from_stop}}" => ucwords($tinfo["from_stop_name"]),
                                                                                     "{{to_stop}}" => ucwords($tinfo["till_stop_name"]),
                                                                                     "{{bos_ref_no}}" =>  $tinfo["boss_ref_no"],
                                                                                     "{{amount_to_pay}}" => $acual_refund_paid,
                                                                                     "{{pnr_no}}" => $tinfo["pnr_no"],
                                                                                );
                                                                                        
                                                        $mailbody_array = array(
                                                                            "mail_title" => "ticket_refund",
                                                                            "mail_client" => $tinfo["user_email_id"],
                                                                            "mail_replacement_array" => $mail_replacement_array 
                                                                            );

                                                        $mail_result = setup_mail($mailbody_array);
                                                            
                                                        /************ Mail End Here ********/

                                                        if($is_send) {
                                                            $data['flag']   =   '@#success#@';
                                                            if(in_array($tinfo['booked_from'],array('rsrtc','etravelsmart'))) {
                                                                $data['ticket_layout'] = $ticket_layout[0]->cancel_ticket;
                                                            }
                                                        }
                                                        else {
                                                            $data['flag']   =   '@#failed#@';
                                                            $data['error']  =   "Error while sending message.";
                                                        }
                                                    }
                                                    else {
                                                        $data['flag']   =   '@#failed#@';
                                                        $data['error']  =   "Error in processing request. Please contact customer care.";
                                                    }
                                                }
                                                elseif($tinfo['payment_by'] == 'wallet') {
                                                    $is_send = true;
                                                    if($partial_cancellation == 0) {
                                                        $calculated_cancellation_charges = $tinfo["total_fare_without_discount"]-$tinfo["fare_convey_charge"]-$cancel_data["refundAmount"];
                                                        $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                        $acual_refund_paid = getRefundAmountToPay($tinfo["tot_fare_amt_with_tax"],$cancel_data["cancellationCharge"]);
                                                        $acual_refund_paid_to_customer = getRefundAmountToPay($tinfo["total_fare_without_discount"],$cancel_data["cancellationCharge"]);
                                                    }
                                                    else if($partial_cancellation == 1) {
                                                        $seat_sum = $this->ticket_details_model->column("sum(fare_amt) as total_fare_amt, sum(op_commission) as commission_amount, sum(tds_on_commission) as tds_amount,sum(final_commission) as pg_commission_after_tds")->where_in("seat_no" , $request['seat_no'])->where(["ticket_id"=>$ticket_id])->find_all();
                                                        if($seat_sum) {
                                                            $calculated_cancellation_charges = $seat_sum[0]->total_fare_amt-$cancel_data["refundAmount"];
                                                            $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                            $total_fare_amt = (($seat_sum[0]->total_fare_amt - $seat_sum[0]->commission_amount - 0) + $seat_sum[0]->tds_amount);
                                                            $acual_refund_paid = getRefundAmountToPay($total_fare_amt,$cancel_data["cancellationCharge"]);
                                                            $acual_refund_paid_to_customer = getRefundAmountToPay($tinfo["total_fare_without_discount"],$cancel_data["cancellationCharge"]);
                                                        }
                                                    }  
                                                    /* ------ Process for cancel ticket when pay from wallet ------- */
                                                        
                                                    // get wallet Detail
                                                    $user_id = $this->session->userdata("user_id");
                                                    $this->wallet_model->user_id = $user_id;
                                                    $walletdata = $this->wallet_model->select();

                                                    if(!empty($walletdata)) {
                                                        $current_balance = $walletdata[0]->amt;
                                                    
                                                        //Check Refund value is greater then zero
                                                        if($acual_refund_paid > 0) {
                                                            $balance_to_update      = $current_balance + $acual_refund_paid;
                                                        }
                                                        // Check refund is given or not
                                                        $is_refund = '0';
                                                        if($balance_to_update < WALLET_LIMIT) {
                                                            $UserWalletId = $walletdata[0]->id;
                                                            $wallet_to_update = array(
                                                                                  'amt' => $balance_to_update,
                                                                                  'updated_by'=> $this->session->userdata('user_id'),
                                                                                  'updated_date'=> date('Y-m-d h:m:s')
                                                                                  );
                                                            $is_cancle_wallet_updated = $this->wallet_model->update($UserWalletId, $wallet_to_update);
                                                        
                                                            $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                                                            $val = array ($user_id,$acual_refund_paid,$current_balance,$balance_to_update,'',$ticket_id,date("Y-m-d H:i:s"),'Function Detail : cancel_booking/cancel_booking, Detail :Add  Amount into wallet against ticket cancel process');
                                                            $FileName = "wallet/user_wallet_detail/wallet_".$user_id.".csv";
                                                            make_csv($FileName,$title,$val);
                                                            $transaction_no = substr(hexdec(uniqid()), 4, 12);
                                                            if($is_cancle_wallet_updated) {
                                                                $wallet_trans_detail['w_id'] = $walletdata[0]->id;
                                                                $wallet_trans_detail['amt'] = $acual_refund_paid;
                                                                $wallet_trans_detail['comment'] = 'ticket cancel Process';
                                                                $wallet_trans_detail['status'] = 'added';
                                                                $wallet_trans_detail['user_id'] = $user_id;
                                                                $wallet_trans_detail['added_by'] = $user_id;
                                                                $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                                $wallet_trans_detail['amt_before_trans'] = $current_balance;
                                                                $wallet_trans_detail['amt_after_trans'] = $balance_to_update;
                                                                $wallet_trans_detail['ticket_id'] = $ticket_id;
                                                                $wallet_trans_detail['transaction_type_id'] = '3';
                                                                $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                                                $wallet_trans_detail['transaction_type'] = 'Credited';
                                                                $wallet_trans_detail['is_status'] = 'Y';
                                                                $wallet_trans_detail['transaction_no'] = $transaction_no;
                                                                
                                                                $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
                                                                
                                                                /**********BOS WALLET TRANSACTION END**********/
                                                                if($wallet_trans_id) {
                                                                    $this->deduct_commission_for_ticket($tinfo);
                                                                }

                                                                $is_refund = '1';
                                                                
                                                                
                                                            }
                                                            else {
                                                                $custom_error_mail = array(
                                                                                        "custom_error_subject" => "Wallet Error - Wallet amount not added while processing cancel booking ",
                                                                                        "custom_error_message" => "Function Name - cancel ticket. Wallet amount not added while processing cancel booking. User id - ".$user_id ." and Ticket id - " . $ticket_id
                                                                                    );

                                                                $this->common_model->developer_custom_error_mail($custom_error_mail);
                                                                $err_data['error'] = " Wallet amount not added while processing cancel booking";
                                                                logging_data("booking/".date("M")."/bos_cancel_booking_from_wallet.log", $err_data,"Wallet not updated");
                                                                $data['flag']   =   '@#failed#@';
                                                                $data['error']  =   "Error in processing request. Please contact customer care.";
                                                            }
                                                        } 

                                                        /**********BOS WALLET TRANSACTION START*******/
                                                        $bos_wallet_data=[];
                                                        if(!empty($tinfo["rokad_ref_no"])){
                                                            $bos_wallet_data["bos_key"] = $tinfo["rokad_ref_no"];
                                                            $bos_wallet_data["cancellation_charge"] = $cancel_data["cancellationCharge"];
                                                            $bos_wallet_data["new_pnr"] = $cancel_pnr_to_update;
                                                            $bos_wallet_data["api_refund_amount"] = $cancel_data['refundAmount'];
                                                            $bos_wallet_data["cancel_percentage"] = isset($cancel_data["cancellationChargePer"]) ? str_replace("%", "", $cancel_data["cancellationChargePer"]) : "";
                                                            $rokad_data = $this->bos_wallet_refund($bos_wallet_data);
                                                            if(!empty($rokad_data)){
                                                                $cancel_ticket_id =  json_decode($rokad_data)->cancel_ticket_id;
                                                                $update_where_cancel = array("id" => $id);
                                                                $refund_id = $this->cancel_ticket_model->update($update_where_cancel,['cancel_ticket_id' => $cancel_ticket_id]);
                                                            }
                                                        }

                                                        $update_refund_status = array('is_refund' => $is_refund,
                                                                                       "actual_refund_paid" => $acual_refund_paid,
                                                                                       "cancel_charge" => $calculated_cancellation_charges,
                                                                                       "pg_refund_amount" => $acual_refund_paid
                                                                                     );
                                                                                
                                                        $update_where_cancel = array("id" => $id);
                                                        $refund_id = $this->cancel_ticket_model->update($update_where_cancel,$update_refund_status);

                                                        if(!$refund_id) {
                                                            log_data("refund/success_but_not_updated_ccavenue_refund_".date("d-m-Y")."_log.log","Refund is success but status is not updated for 'cancel_ticket_data table' for id ".$id,'cancel_ticket_data table id');
                                                        }

                                                        //KEEP THIS INSIDE SUCCESS FLAG
                                                        $data['refund_amt']            = in_array($this->session->userdata('role_name'),$fme_agent_role_name) ? $acual_refund_paid_to_customer : $acual_refund_paid ;
                                                        // $data['refund_amt_to_customer'] = $acual_refund_paid_to_customer; 
                                                        $data['cancellation_cahrge']    = $cancel_data["cancellationCharge"];
                                                    
                                                        if(in_array($tinfo['booked_from'],array('rsrtc','etravelsmart'))) {
                                                            // cancelled_ticket
                                                            if($partial_cancellation == 1) {
                                                                if($total_passgr == $total_cancel_seat) {
                                                                    $ptd_data = $this->ticket_details_model->where('ticket_id', $tinfo['ticket_id'])->find_all();
                                                                    $cancel_mail_content = $this->{$library_name}->mail_cancel_ticket($tinfo,$data,$ptd_data);
                                                                }
                                                            }
                                                            else {
                                                                $ptd_data = $this->ticket_details_model->where('ticket_id', $tinfo['ticket_id'])->find_all();
                                                                $cancel_mail_content = $this->{$library_name}->mail_cancel_ticket($tinfo,$data,$ptd_data);
                                                            }
                                                                
                                                            $this->ticket_dump_model->ticket_ref_no = $tinfo['ticket_ref_no'];
                                                            $ticket_layout = $this->ticket_dump_model->select();
                                                        }
                                                        // To get ticket booked detail
                                                        $ticket_details = $this->tickets_model->get_ticket_detail_by_refno($tinfo['ticket_ref_no']);
                                                    
                                                        // For Cancel Ticket Mail send from here end//
                                                        // cancelled_ticket
                                                        
                                                        /************ Mail Start Here ********/
                                                                
                                                        $mail_replacement_array = array(
                                                                                     "{{username}}" => "User",
                                                                                     "{{from_stop}}" => ucwords($tinfo["from_stop_name"]),
                                                                                     "{{to_stop}}" => ucwords($tinfo["till_stop_name"]),
                                                                                     "{{bos_ref_no}}" =>  $tinfo["boss_ref_no"],
                                                                                     "{{amount_to_pay}}" => $acual_refund_paid,
                                                                                     "{{pnr_no}}" => $tinfo["pnr_no"],
                                                                                    );
                                                                                        
                                                        $mailbody_array = array(
                                                                            "mail_title" => "ticket_refund",
                                                                            "mail_client" => $tinfo["user_email_id"],
                                                                            "mail_replacement_array" => $mail_replacement_array 
                                                                            );
                                                        $mail_result = setup_mail($mailbody_array);
                                                        /************ Mail End Here ********/
														// send sms to user
														$cancel_detail = array("{{bos_ref_no}}" => $tinfo["boss_ref_no"],
																				"{{amount_to_pay}}" => (isset($acual_refund_paid)) ? $acual_refund_paid : "NA",
																			);
														$temp_sms_msg = str_replace(array_keys($cancel_detail),array_values($cancel_detail),BOS_CANCEL_TICKET);
														log_data("trash/temp_sms_msg_".date("d-m-Y")."_log.log",$temp_sms_msg, 'cancel temp_sms_msg');
														$is_send = sendSMS($tinfo["mobile_no"], $temp_sms_msg, array("ticket_id" => $ticket_id));
														
                                                        if($is_send) {
                                                            $data['flag']   =   '@#success#@';
                                                            if(in_array($tinfo['booked_from'],array('rsrtc','etravelsmart'))) {
                                                                $data['ticket_layout'] = $ticket_layout[0]->cancel_ticket;
                                                            }
                                                        }
                                                        else {
                                                            $data['flag']   =   '@#failed#@';
                                                            $data['error']  =   "Error while sending message.";
                                                        }

                                                    }
                                                    else {
                                                        $custom_error_mail = array(
                                                                                    "custom_error_subject" => " VVVVIMP Wallet Error - Wallet not created  ",
                                                                                    "custom_error_message" => "Function Name - cancel ticket. Wallet not created. User id - ".$user_id ." and Ticket id - " . $ticket_id
                                                                                );

                                                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                                                        $err_data['error'] = " Wallet amount not added while processing cancel booking";
                                                        logging_data("booking/".date("M")."/bos_cancel_booking_from_wallet.log", $err_data,"Wallet not updated");
                                                        $data['flag']   =   '@#failed#@';
                                                        $data['error']  =   "Error in processing request. Please contact customer care.";
                                                    } 
                                                }
                                            }
                                            else {
                                                $data['flag']           = '@#failed#@';
                                                $data['error']          = "Something went wrong please try again later."; 
                                            }
                                        }
                                        else {
                                            $data['flag']           = '@#failed#@';
                                            $data['error']          = "Something went wrong please try again later."; 
                                        }
                                    }
                                    else if(strtolower($cancel_data['status']) == 'error') {
                                        $error_message = (isset($cancel_data["error_message"])) ? $cancel_data["error_message"] : "Something unexpected happened. Please Try again.";
                                        $data['flag']           = '@#failed#@';
                                        $data['error']          = $error_message;
                                    }
                                    else {
                                        $data['flag']           = '@#failed#@';
                                        $data['error']          = 'Please Try again after sometime'; 
                                    }
                                }
                                else {
                                    $data['flag']           = '@#failed#@';
                                    $data['error']          = 'Ticket Error'; 
                                }
                            }
                            else {
                                $data['flag']               = '@#failed#@';
                                $data['error']              = 'System is processing the previous cancellation request. Please try again after '.$try_after_minutes.'.';
                            }
                        }
                        else {
                            $data['flag']   = '@#failed#@';
                            $data['error']  =  "Please Select Seat.";
                        }
                    }
                    else {
                        $data['flag']   = '@#failed#@';
                        $data['error']  =  "Could not initiate refund till 1 hour after booking."; 
                    }
                }
                else {
                    $data['flag']   = '@#failed#@';
                    $data['error']  =  "Your payment is not confirmed yet, Please try again after sometime."; 
                }
            }
            else {
                $data['flag']   = '@#failed#@';
                $data['error']  =  "You are not allowed to cancel ticket. Please contact customer support."; 
            }
        }
        else {
            $data['flag']   = '@#failed#@';
            $data['error']  =  "Please contact customer support.";
        }

        data2json($data);
    }
    
    function is_ticket_cancellable() {
        $data                   = array();
        $seat_no                = array();
        $request                = array();
        $request['seat_no']     = array();
        
        $is_true                = true;
        $input                  = $this->input->post();   
        $request['pnr_no']      = $input['pnr_no'];
        $provider_id            = $input['pro_id'];
        $provider_type          = $input['proty'];
        $op_id                  = $input['opi'];
        $ticket_id              = $input['ticket_id'];
        $tinfo = $this->tickets_model->get_ticket_and_detail($input['email'], $input['pnr_no']);
        if($tinfo) {
            $to_allow_cancel = true;
            eval(FME_AGENT_ROLE_ID);
            eval(FME_AGENT_ROLE_NAME);
            if((in_array($tinfo["role_id"], $fme_agent_role_id)) && (in_array($tinfo["booked_by"], $fme_agent_role_name))) {
                if(!$this->session->userdata('role_id') && (!in_array($this->session->userdata('role_id'), $fme_agent_role_name)) && !$this->session->userdata('role_name') && (in_array($this->session->userdata('role_name'), $fme_agent_role_name))) {
                    $to_allow_cancel = false;
                }
            }

            if($to_allow_cancel) {
                if(in_array($tinfo["payment_confirmation"], array("full","partial","auto_confirmed"))) {
                    if(time() < strtotime($tinfo["boarding_time"])) {
                        if((in_array(strtolower($tinfo["payment_by"]),$this->payment_by_array)) || (time() > strtotime("+60 minutes", strtotime($tinfo["inserted_date"])))) {
                            $partial_cancellation = $tinfo["partial_cancellation"];
                            $ticket_id            = $tinfo["ticket_id"];

                            if($ticket_id != $input['ticket_id']) {
                                $hack_string = "User With user id = ".$tinfo["inserted_by"]." & email id ".$tinfo["user_email_id"]." tried to crack the system or chnaged parameter(Ticket Id - ".$input['ticket_id'].") while cancelling ticket.";
                                log_data("hack/hack_".date("d-m-Y")."_log.log",$hack_string, 'Ticket Cancel');
                            }

                            if($partial_cancellation == 0) {
                                //This is partial cancellation false condition.
                                $seat_no = $request['seat_no']  =  explode(",", $tinfo["seat_no"]);
                            }
                            else if($partial_cancellation == 1 && !empty($input['seat_no'])) {
                                //This is partial cancellation true condition.
                                $seat_no = $request['seat_no']  = explode(",", $input["seat_no"]);
                            }
                            else {
                                $is_true = false;
                            }

                            if($is_true) {
                                if(in_array(strtolower($tinfo["payment_by"]),$this->payment_by_array)) {
                                    $is_true = true;
                                }
                                else {  
                                    $is_true = true;

                                    /*      VIMP : DO NOT REMOVE CODE (check_is_return_ticket_refund)
                                     *  This is code is useful at the time of only one journey cancellation
                                     */

                                    $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_return_ticket_refund_condition($tinfo["ticket_id"]);
                                    if($check_is_return_ticket_refund) {
                                        if($check_is_return_ticket_refund[0]->onward_transaction_status == "success" && $check_is_return_ticket_refund[0]->return_transaction_status == "success" ) { 
                                            $is_true = true;
                                        }
                                        else {
                                            $is_true = false;

                                            if(!empty($check_is_return_ticket_refund[0]->onward_cancel_date) && time() > strtotime("+6 minutes", strtotime($check_is_return_ticket_refund[0]->onward_cancel_date))) {
                                                $is_true = true;
                                            }
                                            else if(!empty($check_is_return_ticket_refund[0]->return_cancel_date) && time() > strtotime("+6 minutes", strtotime($check_is_return_ticket_refund[0]->return_cancel_date))) {
                                                $is_true = true;
                                            }

                                            $try_after_minutes = 0;
                                            if(!$is_true && !empty($check_is_return_ticket_refund[0]->onward_cancel_date)) {
                                                $try_after_minutes = round(abs(time() - strtotime($check_is_return_ticket_refund[0]->onward_cancel_date)) / 60,2);
                                            }
                                            else if(!$is_true && !empty($check_is_return_ticket_refund[0]->return_cancel_date)) {
                                                $try_after_minutes = round(abs(time() - strtotime($check_is_return_ticket_refund[0]->return_cancel_date)) / 60,2);
                                            }

                                            if($try_after_minutes > 0) {
                                                $try_after_minutes = 6 - $try_after_minutes;
                                                $try_after_minutes .= " minutes";
                                            }
                                            else {
                                                $try_after_minutes = "sometime";
                                            }
                                        }
                                    }
                                }
                        
                                if($is_true) {
                                    if(isset($input['pnr_no']) && $input['pnr_no'] != "" && isset($provider_id) && $provider_id && isset($input['email']) && $input['email'] != "") {
                                        $ldetails = $this->common_model->get_provider_lib($provider_id, $provider_type, $op_id);
                                        $library_name =  strtolower(trim($ldetails['library'])).'_api';

                                        //for Etravelsmart api we need api_ticket_no
                                        $request["api_ticket_no"]                   = $tinfo["api_ticket_no"];
                                        $request["pnr_no"]                          = $input['pnr_no'];
                                        $request["current_ticket_information"]      = $tinfo;
                                        if($partial_cancellation == 0) {
                                            $cancel_data = $this->{$library_name}->is_ticket_cancellable($request);
                                        }
                                        else if($partial_cancellation == 1) {
                                            $cancel_data = $this->{$library_name}->is_ticket_cancellable_fullpartial($request);
                                        }

                                        if(strtolower($cancel_data['status']) == 'success') {
                                            /*                  NOTICE
                                             *After Discussion Handle PARTIAL CANCELLATION HERE
                                             *i.e. Handle seatwise cancellation
                                             */
                                            if($partial_cancellation == 0) {
                                                $calculated_cancellation_charges = $tinfo["total_fare_without_discount"]-$tinfo["fare_convey_charge"]-$cancel_data["refundAmount"];
                                                $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                $acual_refund_paid = getRefundAmountToPay($tinfo["tot_fare_amt_with_tax"],$cancel_data["cancellationCharge"]);  
                                                
                                                $acual_refund_paid_to_customer = getRefundAmountToPay($tinfo["total_fare_without_discount"],$cancel_data["cancellationCharge"]);
                                            
                                            }
                                            else if($partial_cancellation == 1) {
                                                $seat_sum = $this->ticket_details_model->column("sum(fare_amt) as total_fare_amt, sum(op_commission) as commission_amount, sum(tds_on_commission) as tds_amount,sum(final_commission) as pg_commission_after_tds")->where_in("seat_no" , $request['seat_no'])->where(["ticket_id"=>$ticket_id])->find_all();
                                                if($seat_sum) {
                                                    $calculated_cancellation_charges = $seat_sum[0]->total_fare_amt-$cancel_data["refundAmount"];
                                                    $cancel_data["cancellationCharge"] = $calculated_cancellation_charges;
                                                    if($tinfo['payment_by'] == 'csc') {
                                                        $total_fare_amt = (($seat_sum[0]->total_fare_amt - $seat_sum[0]->commission_amount - $seat_sum[0]->discount_value) + $seat_sum[0]->tds_amount);
                                                    }
                                                    else {
                                                      $total_fare_amt = (($seat_sum[0]->total_fare_amt - $seat_sum[0]->commission_amount - $seat_sum[0]->discount_value - $seat_sum[0]->pg_commission_after_tds) + $seat_sum[0]->tds_amount);
                                                    }
                                                    $acual_refund_paid = getRefundAmountToPay($total_fare_amt,$cancel_data["cancellationCharge"]);
                                                    $acual_refund_paid_to_customer = getRefundAmountToPay($seat_sum[0]->total_fare_amt,$cancel_data["cancellationCharge"]);
                                                }
                                            }

                                            if($acual_refund_paid > 0) {
                                                $data['flag']                  =   '@#success#@';
                                                $data['message']               =   'Ticket is cancellable. Please confirm cancellation.';
                                                $data['refund_amt']            =    $acual_refund_paid;
                                                $data['cancellation_cahrge']   =    $cancel_data["cancellationCharge"];
                                                $data['custumer_return']       =    $acual_refund_paid_to_customer;
                                                
                                            }
                                            else {
                                                $data['flag']           = '@#failed#@';
                                                $data['error']          = 'Ticket is non-cancellable.'; 
                                                $data['refund_amt']            =    $acual_refund_paid;
                                            }
                                        }
                                        else if(strtolower($cancel_data['status']) == 'error') {
                                            $error_message          = (isset($cancel_data["error_message"])) ? $cancel_data["error_message"] : "Something unexpected happened. Please Try again.";
                                            $data['flag']           = '@#failed#@';
                                            $data['error']          = $error_message;
                                        }
                                        else {
                                            $data['flag']           = '@#failed#@';
                                            $data['error']          = 'Please Try again after sometime'; 
                                        }
                                    }
                                    else {
                                        $data['flag']           = '@#failed#@';
                                        $data['error']          = 'Please provide valid ticket information.'; 
                                    }
                                }
                                else {
                                    $data['flag']               = '@#failed#@';
                                    $data['error']              = 'System is processing the previous cancellation request. Please try again after '.$try_after_minutes.'.';
                                }
                            }
                            else {
                                $data['flag']   = '@#failed#@';
                                $data['error']  =  "Please Select Seat.";
                            }
                        }
                        else {
                            $data['flag']   = '@#failed#@';
                            $data['error']  =  "Could not initiate refund till 1 hour after booking."; 
                        }
                    }
                    else {
                        $data['flag']   = '@#failed#@';
                        $data['error']  =  "This ticket is outdated."; 
                    }
                }
                else {
                    $data['flag']   = '@#failed#@';
                    $data['error']  =  "Your payment is not confirmed yet, Please try again after sometime."; 
                }
            }
            else {
                $data['flag']   = '@#failed#@';
                $data['error']  =  "You are not allowed to cancel ticket. Please contact customer support."; 
            }
        }
        else {
            $data['flag']   = '@#failed#@';
            $data['error']  =  "Please contact customer support.";
        }
        data2json($data);
    }
    
    function get_cancel_booking() {

        $ticket_id                   = $this->input->post();
        $user_data['msg']            = '';
        $soap_name_api = $this->session->userdata("available_detail");
        $soap_name = strtolower($ticket_id['op_name'].'_api');
        $cancel_data                 = $this->{$soap_name}->cancel_booking($ticket_id );
        //    $cancel_data                 = $this->call_cancel_api($ticket_id );
        // show($cancel_data,1);
        /* $cancel_data['status']          = "SUCCESS";
          $cancel_data["cancellable"]     = true;
          $cancel_data["newPNR"]          = "XDLHE2501231500003";
          $cancel_data["refundAmount"]    = "660.0";
                $cancel_data["cancellationCharge"] = "32.0";*/
      //  if($cancel_data['status']    != 'FAILURE') {

        $user_data['refund_amt'] = $cancel_data['refundAmount'];
        $user_data['refund_amt'] = $cancel_data["cancellationCharge"];
         $config["per_page"]          = 2;
        if ($this->uri->segment(4)) {
        $page    = ($this->uri->segment(4));
        } else {
            $page    = 1;
        }

        $flag                    =  $this->tickets_model->cancel_booking($ticket_id );
        $data['email_id']        = $this->session->userdata('email');
        $user_data['all_data']   = $this->tickets_model->get_search($data,$page,$config["per_page"]);
        $user_data['flag']       = "@#success#@";
     //   $out['view']             = $this->load->view(CANCEL_BOOKING_AJAX, $user_data, true);
        data2json($user_data);
        
    }
    
    function call_cancel_api(){
        $ticket_id                   = $this->input->post();
        $soap_name_api = $this->session->userdata("available_detail");
        $soap_name = strtolower($ticket_id['op_name'].'_api');
        $cancel_data                    = $this->{$soap_name}->cancel_booking( $ticket_id   );
      
        if($cancel_data['status']    != 'FAILURE') {
          $user_data['refund_amt']    =   $cancel_data['refundAmount'];
          $user_data['refund_amt']    =   $cancel_data["cancellationCharge"];
           // show($cancel_data ,1);
     //   $user_data['refund_amt']    =  '200';
       // $user_data['refund_cancellation_charge']    =   '100';
          $user_data['flag']          =   '@#success#@';
        }else{
             $user_data['serviceError']=$cancel_data['serviceError']['errorReason'];
         }
        
       /// $user_data['view']          = $this->load->view(PASSENGER_BOOKING_INFO, $user_data, true);
        data2json($user_data);
   }
   
    function getpas_data() {
        $tkt_no = $_POST['ref_no'];
        $data['pas_data']       = $this->tickets_model->getpass_model($tkt_no);

        $data['flag']          = '@#success#@';
       // $data['view']          = $this->load->view(PASSENGER_BOOKING_INFO, $res, true);
        data2json($data);
   }
   public function bos_wallet_refund($data){
        log_data("bos_wallet_credit/bos_wallet_credit".date("d-m-Y")."_log.log",$data,'Final confirmation Parameter Sent To BOS');
        $response = BosSendDataOverPost($this->config->item("bos_transaction_refund_url"),$data,1);
        log_data("bos_wallet_credit/bos_wallet_credit".date("d-m-Y")."_log.log",$response,'Confirmation Response From bos');
        return $response;
    }

    public function refund(){
    $data['bos_key'] = '6553f7e5';
    $data['cancellation_charge'] = '40';
    $data['new_pnr'] = 'XMAU3404261700010';
    $data['api_refund_amount'] = '884.0';
    $data['cancel_percentage'] = '';
         log_data("bos_wallet_credit/bos_wallet_credit".date("d-m-Y")."_log.log",$data,'Final confirmation Parameter Sent To BOS');
        $response = BosSendDataOverPost($this->config->item("bos_transaction_refund_url"),$data,1);
        log_data("bos_wallet_credit/bos_wallet_credit".date("d-m-Y")."_log.log",$response,'Confirmation Response From bos');
    }

    private function deduct_commission_for_ticket($ticket_detail) {
        $retailer_commission = $this->commission_calculation_model->retailer_commission_calculation_deduction($ticket_detail['ticket_id'],$this->session->userdata('user_id'));
        if(is_array($retailer_commission) && $retailer_commission[0]->total_commission > 0 ) {
            $this->db->trans_begin();

            $commission_amount = $retailer_commission[0]->total_commission;
            /******* Add Amount in to user wallet *******/ 
            // Get user wallet data 
            $user_wallet_data = $this->wallet_model->get_wallet_detail($this->session->userdata('user_id'));
            if(!empty($user_wallet_data)) {
                $user_actual_wallet_amount = $user_wallet_data[0]->amt;
                $user_updated_wallet_amount = $user_actual_wallet_amount - $commission_amount;
                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                // insert into wallet trans for user
                $wallet_trans_arr = array(
                                    'w_id'                => $user_wallet_data[0]->id,
                                    'amt'                 => $commission_amount,
                                    'user_id'             => $this->session->userdata('user_id'),
                                    'amt_before_trans'    => $user_actual_wallet_amount,    
                                    'amt_after_trans'     => $user_updated_wallet_amount,
                                    'status'              => 'deducted',
                                    'transaction_type'    => 'Debited',
                                    'comment'             => 'Commission deducted for  ticket cancellation',
                                    'wallet_type'         => 'actual_wallet',
                                    'ticket_id'           => $ticket_detail['ticket_id'],
                                    'is_status'           => 'Y',
                                    'transaction_type_id' => 17,
                                    'added_on'            => date('Y-m-d H:i:s'), 
                                    'added_by'            => $this->session->userdata('user_id'),
                                    'transaction_no'      => $transaction_no
                                );
                $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                
                if($wallet_trans_id){
                    $data = array(
                                'amt'          => $user_updated_wallet_amount,
                                'updated_by'   => $this->session->userdata('user_id'),
                                'updated_date' => date('Y-m-d H:i:s'),
                                'last_transction_id' => $wallet_trans_id
                            );
                    $UpdateId = $this->common_model->update('wallet','user_id',$this->session->userdata('user_id'),$data);

                    // Get rokad wallet data
                    if($UpdateId) {
                        /*$RollID = COMPANY_ROLE_ID;
                        $RokadUserData = $this->common_model->get_value('users','id','role_id='.$RollID); 
                        $rokad_wallet_data = $this->wallet_model->get_wallet_detail($RokadUserData->id);    
                        if(!empty($rokad_wallet_data)) {
                            $rokad_actual_wallet_amount = $rokad_wallet_data[0]->amt;
                            $rokad_updated_wallet_amount = $rokad_actual_wallet_amount - $commission_amount;
                            
                            // insert into wallet trans for user
                            $wallet_trans_arr = array(
                                                'w_id'                => $rokad_wallet_data[0]->id,
                                                'amt'                 => $commission_amount,
                                                'user_id'             => $RokadUserData->id,
                                                'amt_before_trans'    => $rokad_actual_wallet_amount,    
                                                'amt_after_trans'     => $rokad_updated_wallet_amount,
                                                'status'              => 'deducted',
                                                'transaction_type'    => 'Debited',
                                                'comment'             => 'Commission for ticket booking',
                                                'is_status'           => 'Y',
                                                'wallet_type'         => 'actual_wallet',
                                                'ticket_id'           => $ticket_detail[0]->ticket_id,
                                                'transaction_type_id' => 16,
                                                'added_on'            => date('Y-m-d H:i:s'), 
                                                'added_by'            => $this->session->userdata('user_id')
                                            );
                            $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                            
                            if($wallet_trans_id){
                                $data = array(
                                            'amt'          => $rokad_updated_wallet_amount,
                                            'updated_by'   => $this->session->userdata('user_id'),
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $wallet_trans_id
                                        );
                                $UpdateId = $this->common_model->update('wallet','user_id',$RokadUserData->id,$data);
                                if($UpdateId) {
                                }
                                else {
                                    $custom_error_mail = array(
                                                            "custom_error_subject" => " VVVVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                            "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                                          );

                                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                                    
                                    $data['msg'] = "error";
                                }
                            }
                            else {
                                $custom_error_mail = array(
                                                        "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                        "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                                      );

                                $this->common_model->developer_custom_error_mail($custom_error_mail);
                                
                                $data['msg'] = "error";
                            }
                        }
                        else {
                            $custom_error_mail = array(
                                                        "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                        "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                                      );

                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                            
                            $data['msg'] = "error";
                        } */ 

                        // update commission calculation table 
                        $update_arr = array('cancel_flag' => "Y",
                                            'cancel_flag_date' => date('Y-m-d H:i:s')
                                        );

                        $this->commission_calculation_model->update(array("user_id" => $this->session->userdata('user_id'), "ticket_id" => $ticket_detail['ticket_id']),$update_arr);
                        $data['msg'] = "success";  
                    }
                    else {
                        $custom_error_mail = array(
                                                "custom_error_subject" => " VVVVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                                "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                              );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                        
                        $data['msg'] = "error";
                    }
                }
                else {
                    $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                            "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                          );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                    
                    $data['msg'] = "error";
                }
            }
            else {
                $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                            "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                          );

                $this->common_model->developer_custom_error_mail($custom_error_mail);
                
                $data['msg'] = "error";
            }

            if ($this->db->trans_status() === FALSE) {
                show(error_get_last());
                $this->db->trans_rollback();
            }
            else {
                $this->db->trans_commit();
            } 
        }
        else {
            $custom_error_mail = array(
                                        "custom_error_subject" => " VVVIMP commission detail not found while cancel ticket for ".$ticket_detail['ticket_id'],
                                        "custom_error_message" => "commission detail not found for ".$ticket_detail['ticket_id']." Query for search commission calculation detail :".$this->db->last_query()
                                      );

            $this->common_model->developer_custom_error_mail($custom_error_mail);
            
            $data['msg'] = "error";
        }

        return $data;
    }
}
