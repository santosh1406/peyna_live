<?php

class Hrtc extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array('tickets_model', 'ticket_details_model', 'users_model', 'op_config_model','mail_template_model', 'ticket_dump_model', 'ticket_tax_trans_model', 'processed_ticket_tax_trans_model', 'etravels_bus_service_model', 'api_bus_types_model', 'temp_bus_types_model', 'return_ticket_mapping_model','api_bus_services_model'));
        $this->load->library("Excel","excel_reader");
        $this->load->library(array('api_provider/etravelsmart_api','api_provider/hrtc_api','cca_payment_gateway'));

        $this->load->model(array('bus_source_destination_model','op_bus_types_model','state_model', 'bus_stop_master_model', 'bus_type_model', 
                                 'travel_destinations_model', 'operator_commission_model', 'agent_commission_model','boarding_stop_model','op_seat_layout_model','op_layout_details_model'
                                )
                          );
    }


    public function index()
    {
        $ptd_data = [];
        $temp = new stdClass;



        $temp->id = "22600";
        $temp->ticket_id = "15630";
        $temp->seat_no = "16";
        $temp->berth_no = "1";
        $temp->psgr_name = "Suraj Rathod";
        $temp->psgr_age = "26";
        $temp->psgr_sex = "M";
        $temp->psgr_type = "A";
        $temp->psgr_no = "1";
        $temp->concession_cd = "1";
        $temp->concession_rate = "1.00";
        $temp->discount = "1.00";
        $temp->is_home_state_only = "1";
        $temp->quota_flag = "";
        $temp->adult_basic_fare = "0.00";
        $temp->child_basic_fare = "0.00";
        $temp->fare_acc = "0.00";
        $temp->fare_hr = "0.00";
        $temp->fare_it = "0.00";
        $temp->fare_octroi = "0.00";
        $temp->fare_other = "0.00";
        $temp->fare_reservationCharge = "0.00";
        $temp->fare_sleeperCharge = "0.00";
        $temp->fare_toll = "0.00";
        $temp->fare_convey_charge = "0.00";
        $temp->ac_service_tax = "";
        $temp->asn_fare = "";
        $temp->service_tax_per = "0.00";
        $temp->service_tax = "0.00";
        $temp->op_service_charge_per = "0.00";
        $temp->op_service_charge = "0.00";
        $temp->commission = "";
        $temp->home_state_amt = "";
        $temp->state_amt_1 = "";
        $temp->state_amt_2 = "";
        $temp->state_amt_3 = "";
        $temp->state_amt_4 = "";
        $temp->state_amt_5 = "";
        $temp->convey_name = "Convenience charge";
        $temp->convey_type = "";
        $temp->convey_value = "0";
        $temp->additional_Amt = "";
        $temp->fare_amt = "398.00";
        $temp->refund_amt = "";
        $temp->cancel_charge = "";
        $temp->seat_status = "Y";
        $temp->concession_proff = "1";
        $temp->proff_detail = "1";
        $temp->identify_proof = "";
        $temp->identify_nm = "";
        $temp->identify_no = "";
        $temp->status = "Y";


        $ptd_data[] = $temp;
        $number_of_childs = 0;
        $number_of_adults = 0;
        $passanger_detail = "";
        foreach ($ptd_data as $key => $value)
        {
              if($value->psgr_age <= 12)
              {
                    $number_of_childs++;
              }
              else
              {
                    $number_of_adults++;
              }
              
              $passanger_detail .= "<tr>";
              $passanger_detail .= "<td align='center'>".$value->seat_no."</td>";
              $passanger_detail .= "<td align='center'>".ucwords($value->psgr_name)."</td>";
              $passanger_detail .= "<td align='center'>".$value->psgr_age."</td>";
              $passanger_detail .= "<td align='center'>".(strtolower($value->psgr_sex) == "f") ? "Female" : "Male"."</td>";
              $passanger_detail .= "</tr>";
        }

        echo $passanger_detail;die();


    }

    public function mergecities()
    {
        /*$cities = '';
        $cities = array_unique($cities,SORT_REGULAR);
        array_multisort($cities,SORT_ASC);
        $data = json_encode($cities);

        log_data('hrtc_cities/final_cities_'.date('d-m-y').'.log',$data,'All Stops HRTC');
        show($cities,1);*/
    }

    public function getAllTransactionDetails()
    {
        $input = ["from_date" => "2016-08-02","to_date" => "2016-08-03"];
        $response = $this->hrtc_api->cancel_ticket($input);
       show($response,1);
    }


    public function getDec()
    {
        $str = "vbgEQq2DIxvQAPm+tCII3g==";
        $response_string = mcrypt_decrypt_e($str,"Pas5pr@seairs");
        show($response_string);
         $response_array = xml_to_array($response_string);
         show($response_array,1);
    }
    public function manually_cancel()
    {
        $input = ["pnr_no" => "20160802004290"];
        $response = $this->hrtc_api->cancel_ticket($input);
        show($response,1);
       /* $str = "4udRY3GYCWmh4njJlUla8OpkLgae65q/VKueLie8TxPaQML5eFZbN3DjL29GF6lGTTQr8zTxhOB7BgqaSV8gG7wdH3j0omRbYbDQtnMhewJUo6ScGMqVSPPjxwyspfZ1g1vbfBnB2R3bs9OnVYHWEr4XuPf7qkbchpvaAXhVf5purxXNNkcGmxdYcgo3Kvf2cr8AkQmM8RcY95djXaJR21i0qV+9CmhxUqmJM52v0kHFmXZwKN/hrAR2nm/pyNvxyiI0GfbhQO3glBqnzBtQh2jWvGf8HSIfnwNVHwMuGTAu1UCt+C7nqmRyJ19OfqmRj+x0zlcZu6SauKKd0qXuxpjMESyxZnnKwjmjWFK/Ps8=";
        $response_string = mcrypt_decrypt_e($str,"Pas5pr@seairs");
        show($response_string);
         $response_array = xml_to_array($response_string);
         show($response_array,1);*/

        /* $str = "rmMHSZTwDfXpcOmyf+WHxw==";
        $response_string = mcrypt_decrypt_e($str,"Pas5pr@seairs");
        show($response_string);
         $response_array = xml_to_array($response_string);
         show($response_array,1);*/
    }

    public function getAllCities()
    {
        $response_string = $this->hrtc_api->getAllCities();
        $response = xml_to_array($response_string);

        $hrtc_cities = [];
        if($response)
        {
            foreach ($response["Table"] as $key => $value)
            {
                $hrtc_cities[$key]["value"] = $value["CityName"];
                $hrtc_cities[$key]["label"] = $value["CityName"];
            }
        }

        // $data = json_encode($hrtc_cities);

        log_data('hrtc_cities/cities_'.date('d-m-y').'.log',$data,'All Stops HRTC');
        show($response,1);
    }

    public function getAllBusTypes()
    {
        $response_string = $this->hrtc_api->getAllBusTypes();
        $response = xml_to_array($response_string);
        show($response,1);
    }

    public function getBalance()
    {
        $response = $this->hrtc_api->getBalance();
        show($response,1);
    }

    public function insertAllBusTypes()
    {
        $response_string = $this->hrtc_api->getAllBusTypes();
        $response = xml_to_array($response_string);
        $data_to_insert = [];
        if($response && isset($response["Table"]))
        {
            foreach ($response["Table"] as $key => $value)
            {
                $data_to_insert["op_id"] = HRTC_OPERATOR_ID;
                $data_to_insert["op_bus_type_name"] = $value["BUS_TYPE_NAME"];
                $this->op_bus_types_model->insert($data_to_insert);
            }
        }

        show("Inserted All Bus Types",1);
    }

    public function getAllSDPairs()
    {
        $response_string = $this->hrtc_api->getAllSDPairs();
        $response = xml_to_array($response_string);
        show($response,1);
    }

    public function insertAllSDPairs()
    {
        $response_string = $this->hrtc_api->getAllSDPairs();
        $response = xml_to_array($response_string);
        $sd_pairs = [];
        if($response && isset($response["Table"]))
        {
            foreach ($response["Table"] as $key => $value)
            {
                $sd_pairs = ["op_id" => HRTC_OPERATOR_ID,
                               "source_name" => $value["sourceName"],
                               "source_name_id" => $value["sourceId"],
                               "destination_name" => $value["DestinationName"],
                               "destination_name_id" => $value["destinationid"]
                              ];

                $check_service = $this->bus_source_destination_model->where($sd_pairs)->find_all();

                if(!$check_service)
                {
                    $this->bus_source_destination_model->insert($sd_pairs); 
                }
            }
        }

        show($response,1);
    }

    public function insertBusLayout()
    {
        $files_path = glob(FCPATH . "export/layout/*.csv");
        foreach($files_path as $filepath)
        {
          $csv_file_name = basename($filepath,".csv");
          $layout = $this->op_seat_layout_model->where(["layout_name" => $csv_file_name,"op_id" => HRTC_OPERATOR_ID])->find_all();
          
          if($layout)
          {
            if(isset($layout[0]->id) && $layout[0]->id > 0)
            {
                $handle = fopen($filepath, "r");
                $first_row = true;
                $final_data = array();
                $headers = array();

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
                {           
                    if($first_row)
                    {
                        $headers = $data;
                        $first_row = false;
                    }
                    else
                    {
                        $final_data = array_combine($headers, array_values($data));

                        if($final_data)
                        {
                            $final_data["op_seat_layout_id"]    = $layout[0]->id;
                            $final_data["seat_type"]            = "Se";
                            $final_data["berth_no"]             = 1;
                            
                            $layout_details = $this->op_layout_details_model->where($final_data)->find_all();
                            if(!$layout_details)
                            {
                                $this->op_layout_details_model->insert($final_data);
                            }
                        }
                    }
                }
            }
          }
        }
        
        show("Inserted Seat Layout",1);
    }

    public function getSearchResults()
    {
        $response = $this->hrtc_api->getAvailableBuses();
        show($response,1);
    }


    public function getAllPickDropPointForRouteId()
    {
        $response_string = $this->hrtc_api->getAllPickDropPointForRouteId();
        $response = xml_to_array($response_string);
        show($response,1);
    }

    public function getAvailabilityStatus()
    {
        $response_string = $this->hrtc_api->getAvailabilityStatus();
        $response = xml_to_array($response_string);
        show($response,1);
    }

    public function layout()
    {   
        $seat_service_tax_amt = 0.00;
        $seat_service_tax_perc = 0.00;
        $op_service_charge_amt = 0.00;
        $op_service_charge_perc = 0.00;
        $route_source_destination_array = [];
        $response = array();

        $tot_data_seat_layout       =   [];
        $current_available_seats    =   [];
        $bus_service_where = array("from" => "Palampur","op_name" => "hrtc", "to" => "Ambala", "date" => "2016-07-30");
        $all_bus_services = $this->api_bus_services_model->where($bus_service_where)->find_all();
        if($all_bus_services)
        {
            $decoded_bus_services = json_decode($all_bus_services[0]->bus_service,true);
            $current_bus_services = $decoded_bus_services[61];
        }

        $seat_allowed_from = $current_bus_services["SeatFrom"];
        $seat_allowed_till = $current_bus_services["SeatTo"];
        if($current_bus_services)
        {
          $booked_seats = $this->hrtc_api->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => "30-07-2016"]);
          if(!empty($booked_seats))
          {
            $hrtc_booked_seats          =   array_column($booked_seats, 'SeatNo');
            $hrtc_allowed_seats         =   range($seat_allowed_from,$seat_allowed_till);
            $current_available_seats    =   array_values(array_diff($hrtc_allowed_seats, $hrtc_booked_seats));
            $tot_data_seat_layout       =   $this->op_seat_layout_model->get_seat_layout_for_operator(trim($current_bus_services["Type"]));
          }
        }

        if($tot_data_seat_layout)
        {
            $current_bus_type_id = 0;
            $bus_type_detail = $this->bus_type_model->get_operator_bus_type_detail(["operator_bus_type_name" => trim($current_bus_services["Type"]),"operator_id" => HRTC_OPERATOR_ID]);
            if($bus_type_detail)
            {
                $current_bus_type_id = $bus_type_detail[0]["bus_type_id"];
            }
            $tot_fare   =   $current_bus_services["Fare"];
            $seat_fare_detail = array();
            $total_available_seats = 0;
            $fare_with_conveycharge = getConveyCharges($tot_fare,'hrtc');

            $current_actual_seat_value = $fare_with_conveycharge;
            $current_actual_seat_value_with_tax = $fare_with_conveycharge;

            $basic_fare_array = array($fare_with_conveycharge);
            $fare_array = array($fare_with_conveycharge);

            $response['actual_fare'] = implode("/",array_filter($basic_fare_array));
            $response['actual_fare_array'] = implode("/",array_filter($fare_array));
            /*******temp discount logic start******/
            $discount_array = array();
            $discount_array = $this->discounts->discount_calculation_new($fare_with_conveycharge,
                                                                         $fare_with_conveycharge,
                                                                         $seat_service_tax_amt,
                                                                         $seat_service_tax_perc,
                                                                         $op_service_charge_amt,
                                                                         $op_service_charge_perc,
                                                                         $tot_data_seat_layout[0]->op_id,
                                                                         $current_bus_type_id
                                                                         );
            if(!empty($discount_array))
            {
              $fare_with_conveycharge = $discount_array["current_seat_value_with_tax"];
              $current_seat_service_tax = $discount_array["current_seat_service_tax"];
              $current_seat_operator_service_charge = $discount_array["current_seat_operator_service_charge"];
              $current_seat_discount = $discount_array["current_seat_discount"];

              $basic_fare_array = array($fare_with_conveycharge);
              $fare_array = array($fare_with_conveycharge);
            }

            foreach ($tot_data_seat_layout as $key => $value)
            {
                $current_seat_type = "";
                $horizontal_seat = false;
                $vertical_seat = false;

                if(strtolower($value->quota_type) == "ladies")
                {
                  $current_seat_type = "ladies";
                }

                $pos = $value->row_no . "X" . $value->col_no;
                $birth_no = $value->berth_no;

                $seat_layout[$birth_no][$pos] = array(
                                                        'seat_no' => $value->seat_no,
                                                        'actual_seat_fare' => $current_actual_seat_value_with_tax,
                                                        'actual_seat_basic_fare' => $current_actual_seat_value,
                                                        'seat_fare' => $fare_with_conveycharge,
                                                        'seat_basic_fare' => $fare_with_conveycharge,
                                                        'seat_service_tax_amt' => $seat_service_tax_amt,
                                                        'seat_service_tax_perc' => $seat_service_tax_perc,
                                                        'op_service_charge_amt' => $op_service_charge_amt,
                                                        'op_service_charge_perc' => $op_service_charge_perc,
                                                        'row_no' => $value->row_no,
                                                        'col_no' => $value->col_no,
                                                        'seat_type' => $value->seat_type,
                                                        'berth_no' => $birth_no,
                                                        'quota' => strtolower($value->quota_type),
                                                        'pos' => $pos,
                                                        'current_seat_type' => $current_seat_type,
                                                        'horizontal_seat' => $horizontal_seat,
                                                        'vertical_seat' => $vertical_seat,
                                                        'seat_discount' => $current_seat_discount
                                                    ); 

                if(!empty($current_available_seats) && in_array($value->seat_no, $current_available_seats))
                {
                  $seat_layout[$birth_no][$pos]["available"] = 'Y';
                  $total_available_seats++;
                }
                else
                {
                  $seat_layout[$birth_no][$pos]["available"] = 'N';
                }

                $seat_fare_detail[$birth_no][$value->seat_no] = $seat_layout[$birth_no][$pos];
            }

            $response['fare'] = implode("/",array_filter($basic_fare_array));
            $response['fare_array'] = implode("/",array_filter($fare_array));
            $response['total_seats'] = $tot_data_seat_layout[0]->ttl_seats;
            $response['total_available_seats'] = $total_available_seats;
            $response['available_seats'] = $current_available_seats;
            $response['tot_rows'] = $tot_data_seat_layout[0]->no_rows;
            $response['tot_cols'] = $tot_data_seat_layout[0]->no_cols;
            $response['tot_berth'] = 1; //Only One Berth IN HRTC. Please Discuss this.
            $response['seat_layout'] = $seat_layout;
            $response['seat_fare_detail'] = $seat_fare_detail;
            $response['flag'] = '@#success#@';
        }
        show($response,1);
    }


    public function test()
    {   
        /*      Very Important Notice
         *Please check available services for trip number and route id
         */
        $input  = ["from" => "Palampur", "to" => "Ambala", "date" => "31-07-2016",
                   "date_of_jour" => "31-07-2016", "boarding_stop_name" => "Palampur", "destination_stop_name" => "Ambala",
                   "trip_no"=>"61_164_5_6_30","seats" => ["1" => ["0" => "30"]],
                   "op_name" => "hrtc","op_id" => "4","bsd" => "","dsd" => "","provider_id" => "1","provider_type" => "operator"];
        $data = array();
        $booked_seats   =   [];
        $SeatAvailabilityResponse = [];
        $trip_data = [];
        $rid_array = explode("_", $input["trip_no"]);
        $input["op_trip_no"] = $rid_array[0];
        $current_available_seats    =   [];
        $bus_service_where = array("from" => $input["from"], "to" => $input["to"],"op_name" => "hrtc", "date" => date("Y-m-d",strtotime($input['date'])));
        $all_bus_services = $this->api_bus_services_model->where($bus_service_where)->find_all();
        if($all_bus_services)
        {
            $decoded_bus_services = json_decode($all_bus_services[0]->bus_service,true);
            $current_bus_services = $decoded_bus_services[$input["op_trip_no"]];
        }

        $seat_allowed_from = $current_bus_services["SeatFrom"];
        $seat_allowed_till = $current_bus_services["SeatTo"];
        if($current_bus_services)
        {
            $booked_seats = $this->hrtc_api->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => date("Y-m-d",strtotime($input['date']))]);
            if(!empty($booked_seats))
            {
                $hrtc_booked_seats          =   array_column($booked_seats, 'SeatNo');
                $hrtc_allowed_seats         =   range($seat_allowed_from,$seat_allowed_till);
                $current_available_seats    =   array_values(array_diff($hrtc_allowed_seats, $hrtc_booked_seats));
            

                $current_bus_type_id = 0;
                $bus_type_detail = $this->bus_type_model->get_operator_bus_type_detail(["operator_bus_type_name" => trim($current_bus_services["Type"]),"operator_id" => HRTC_OPERATOR_ID]);
                if($bus_type_detail)
                {
                    $current_bus_type_id = $bus_type_detail[0]["bus_type_id"];
                }

                $seat_format_details = [];
                $berth_no = 1;
                $bus_layout_details = $this->op_seat_layout_model->get_seat_layout_for_operator(trim($current_bus_services["Type"]));
                foreach ($bus_layout_details as $tkey => $tvalue)
                {
                    $is_ladies_seat = (strtolower($tvalue->quota_type) == "ladies") ? true : false;
                    $seat_format_details[$berth_no][$tvalue->seat_no] = array('is_ladies_seat' => $is_ladies_seat);
                }

                $is_child_concession = ($current_bus_services["TypeID"] == 1 || $current_bus_services["TypeID"] == 2) ? true : false; 
                $SeatAvailabilityResponse = [
                                                "serviceId" => $input["op_trip_no"],
                                                "fromStopId" => "",
                                                "toStopId" => "",
                                                "dateOfJourney" => $input["date"],
                                                "fare" => $current_bus_services["Fare"],
                                                "sleeperFare" => "0.00",
                                                "childFare" => getHrtcChildFare($is_child_concession, $current_bus_services),
                                                "availableSeats" => 
                                                                    [
                                                                        "availableSeat" =>  ["1" => $current_available_seats]
                                                                    ],
                                                "seats_details"  => $seat_format_details,
                                            ];

                $trip_data  = [
                                "sch_departure_tm" => date("H:i:s", strtotime($current_bus_services["DepartureTime"])),
                                "arrival_time" => date("Y-m-d",strtotime($input['date']))." ".date("h:i:s", strtotime($current_bus_services["DepartureTime"])),
                                "is_child_concession" => $is_child_concession,
                                "user_type" => $input["op_id"],
                                "op_id" => $input["op_id"],
                                "bus_type_id" => $current_bus_type_id,
                                "service_id" => $input["op_trip_no"],
                                "inventory_type" => "",
                                "id_proof_required" => true,
                                'provider_id' => $input["provider_id"],
                                'provider_type' => $input["provider_type"]
                              ];

                $from_data[] = [
                                "op_id" => HRTC_OPERATOR_ID,
                                "op_stop_name" => ucwords(strtolower($input["from"])),
                                "op_stop_cd" => "",
                                "bus_stop_name" => ucwords(strtolower($input["from"])) 
                                ];


                $to_data[] = [
                                "op_id" => HRTC_OPERATOR_ID,
                                "op_stop_name" => ucwords(strtolower($input["to"])),
                                "op_stop_cd" => "",
                                "bus_stop_name" => ucwords(strtolower($input["to"])) 
                                ];


                $data = [
                            "SeatAvailabilityResponse" => $SeatAvailabilityResponse,
                            "error" => [],
                            "flag" => "success",
                            "trip_data" => $trip_data,
                            "from_data" => $from_data,
                            "to_data" => $to_data
                        ];
            }
        }



        show($data,1);
    }
}

