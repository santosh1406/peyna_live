<?php

class Home_new extends CI_Controller {

    public function __construct() {
        parent::__construct();

      //   $this->load->library(array('api_provider/its_api','api_provider/hrtc_api.php'));
        $this->load->model(array('bus_source_destination_model','state_model', 'bus_stop_master_model', 'bus_type_model','travel_destinations_model', 'operator_commission_model', 'agent_commission_model','boarding_stop_model','banner_model','cancellation_charges_model','wallet_model','credit_account_detail_model','agent_commission_template_log_model','search_trans_model'));
        is_logged_in_home_page();
    }

    public function index() {
        
        $data = array();
        $data["metadata_title"] = $this->config->item('title', 'home_metadata');
        $data["metadata_description"] = $this->config->item('description', 'home_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'home_metadata');
        $currentDate = date('Y-m-d');
        
        $data["to_show_number"] = true;
        eval(AGENT_SITE_ARRAY);
        if(in_array($this->session->userdata("role_id"), $agent_site_array)) {
            redirect(base_url()."front/home/agent_search");
        }
        else {
            load_front_view(HOME_VIEW_NEW, $data);
        }
    }
}
