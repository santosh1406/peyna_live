<?php

class route_sync_model extends Model{
    
    // Header variables
    private $HEADER_ROUTE_NO = 6;
    private $HEADER_BUS_SERVICE_CODE = 3;
    private $HEADER_BUS_SERVICE_NAME = 10;
    private $HEADER_TOTAL_STOP = 3;
    private $HEADER_FROM_CODE = 4;
    private $HEADER_FROM_NAME = 20;
    private $HEADER_TILL_CODE = 4;
    private $HEADER_TILL_NAME = 20;
    private $HEADER_VIA = 3;
    private $HEADER_VIA_NAME = 20;
    private $HEADER_MAX_LUGGAGE = 2;
    private $HEADER_DEPOT_CODE = 3;
    private $HEADER_FAIR_TYPE = 1; 
    
    // Route Stop Variables
    private $RSTOP_ROUTE_NO = 6;
    //private $RSTOP_STOP_CODE = ;
    private $RSTOP_STOP_NAME = 20;
    private $RSTOP_STOP_SEQ = 3;
    private $RSTOP_SUB_STAGE = 0;
    private $RSTOP_KM_STAGE = 5;
    private $RSTOP_INTRA_STATE_DISTANCE = 5;
    private $RSTOP_INTER_STATE_DISTANCE = 5;
    private $RSTOP_STAGE_NO = 3;
    private $RSTOP_CHANGE_POINT = 1;
    private $RSTOP_STATE_CODE = 2;
    private $RSTOP_IS_INTER_STATE = 1;
    private $RSTOP_IS_RESERVATION = 1;
    private $RSTOP_STATUS = 1;
    private $RSTOP_IS_AUDIT_TRIAL = 0;    
    
    // Route Fare Variables
    private $RFARE_ROUTENO = 6;
    private $RFARE_BUS_SERVICE_CODE = 3;
    // private $RFARE_FROM_CODE = 4;
    private $RFARE_FROM_NAME = 20;
    // private $RFARE_TILL_CODE = 4;
    private $RFARE_TILL_NAME = 20;
    private $RFARE_FROM_SEQ = 3;
    private $RFARE_TILL_SEQ = 3;
    private $RFARE_TRAVELLED_KM = 4;
    private $RFARE_ADULT_AMOUNT = 7;
    private $RFARE_CHILD_AMOUNT = 7;
    private $RFARE_LUGGAGE_AMOUNT_PER_UNIT = 7;
    private $RFARE_Adult_BASIC_FAIR = 7;
    private $RFARE_Child_BASIC_FAIR = 7;
    private $RFARE_ASC_AMOUNT = 7;
    private $RFARE_BSC_AMOUNT = 7;
    private $RFARE_TOLL_AMOUNT = 7;
    private $RFARE_IT_AMOUNT = 7;
    private $RFARE_OCTORI_AMOUNT = 7;
    private $RFARE_SLEEPER_AMOUNT = 7;
    private $RFARE_RESERVATION_CHARGES = 7;
    private $RFARE_STATE_CODE1 = 7;
    private $RFARE_STATE_CODE2 = 7;
    private $RFARE_STATE_CODE3 = 7;
    private $RFARE_STATE_CODE4 = 7;
    private $RFARE_STATE_CODE5 = 7;
    private $RFARE_STATE_CODE6 = 7;
    private $RFARE_STATE_CODE7 = 7;
    private $RFARE_STATE_CODE8 = 7;
    private $RFARE_STATE_CODE9 = 7;
    private $RFARE_STATE_CODE10 = 7;
    private $RFARE_FACTOR_TYPE = 1;
    private $RFARE_FAIR_TYPE = 1;
    //added for sbi coins 
    private $RFARE_SBI_ADULT_AMOUNT = 7;
    private $RFARE_SBI_CHILD_AMOUNT = 7;
    private $RFARE_SBI_LUGGAGE_AMOUNT_PER_UNIT = 7;
    
    
    
    
    public function __construct(){
        parent::__construct();
    }
    
    public function makeRouteHeaderString($data){
        $busTypeStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $incr++;
            $busCode = $decodeArr[$i]->BUS_TYPE_ID;
            $busShortCode = $decodeArr[$i]->BUS_TYPE_CD;
            $busServiceName = $decodeArr[$i]->BUS_TYPE_NM;
            $otherLanguage = "";

            if($busTypeStrings != "")
                $busTypeStrings .= "|";

            $serialStr = $this->makeString($serialNo, $this->SERIAL_NO, "0");
            $busCodeStr = $this->makeString($busCode, $this->BUS_CODE, "0");
            $busShortCodeStr = $this->makeString($busShortCode, $this->BUS_SHORT_CODE, "");
            $busServiceNameStr = $this->makeString($busServiceName, $this->BUS_SERVICE_NAME, " ");
            $otherLangStr = $this->makeString($otherLanguage, $this->OTHER_LANGUAGE, " ");
            
            $busTypeStrings.= "'".$serialStr."','".$busCodeStr."','".$busShortCodeStr."','".$busServiceNameStr."','".$otherLangStr."'";
            
        }
        return $busTypeStrings;
    }
    
    public function makeRouteStopString($data){
        $rfidStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $incr++;
            $key_a = $decodeArr[$i]->auth_key_A;
            $key_b = $decodeArr[$i]->auth_key_B;

            if($rfidStrings != "")
                $rfidStrings .= "|";
                    
            $rfidStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "0") . "','"
                . $this->makeString($key_a, $this->KEY_A, "") . "','"
                . $this->makeString($key_b, $this->KEY_B, "") . "'";
          
        }
        return $rfidStrings;        
    }
    
    public function makeRouteFareString($data){
        $gprsStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $decodeArr[$i]->ID;
            $service_provider = $decodeArr[$i]->SERVICE_PROVIDER;
            $sim_check = $decodeArr[$i]->GPRS_FLAG;

            if($gprsStrings != "")
                $gprsStrings .= "|";
                    
            $gprsStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "0") . "','"
                . $this->makeString($service_provider, $this->SERVICE_PROVIDER, "") . "','"
                . $this->makeString($sim_check, $this->SIM_CHECK, "") . "'";
          
        }
        return $gprsStrings;
    }  
    
    public function makeParameterString($data){
        $actStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $decodeArr[$i]->serial_no;
            $activity_name = $decodeArr[$i]->activity_name;
            $activity_code = $decodeArr[$i]->activity_code;
            $count = $decodeArr[$i]->count;            
            $waybill_no = $decodeArr[$i]->waybill_no;
            $print_flag = $decodeArr[$i]->print_flag;
            $update_datetime = $decodeArr[$i]->update_datetime;
    
            if($actStrings != "")
                $actStrings .= "|";
            
            $actStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "") . "','"
                . $this->makeString($activity_name, $this->ACTIVITY_NAME, "") . "','"
                . $this->makeString($activity_code, $this->ACTIVITY_CODE, "") . "','" . $this->makeString($count, $this->COUNT, "") . "','"
                . $this->makeString($waybill_no, $this->WAYBILL_NO, "") . "','" . $this->makeString($print_flag, $this->PRINT_FLAG, "") . "','"
                . $this->makeString($update_datetime, $this->UPDATE_DATETIME, "") . "'";            
          
        }
        return $actStrings;        
    }
}
?>