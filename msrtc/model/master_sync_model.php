<?php

class master_sync_model extends Model{
    
    private $SERIAL_NO = 2;
    private $BUS_CODE = 2;
    private $BUS_SHORT_CODE = 3;
    private $BUS_SERVICE_NAME = 8;
    private $OTHER_LANGUAGE = 16;
    
    private $KEY_A = 16;
    private $KEY_B = 16;    
    
    private $SERVICE_PROVIDER = 16;
    private $SIM_CHECK = 1;    
    
    private $ACTIVITY_NAME = 32;
    private $ACTIVITY_CODE = 16;
    private $COUNT = 2;
    private $WAYBILL_NO = 10;
    private $PRINT_FLAG = 1;
    private $UPDATE_DATETIME = 20;    
    
    private $ID = 6;
    private $NEW_NAME = 20;
    private $OLD_NAME = 20;
    private $NEW_CODE = 5;
    private $OLD_CODE = 5;
    private $STATUS = 1;   
    
    private $DEPOT_ID = 3;
    private $DEPOT_CODE = 5;
    private $DEPOT_NAME = 20;  
    
    private $OPERATOR_ID = 2;
    private $OPERATOR_NAME = 35;
    private $HEIGHT = 1;
    private $WIDTH = 1;  
  
    private $CON_NO = 2;
    private $CON_CODE = 4;
    private $CON_NAME = 7;
    private $CON_PER = 2;    
    public function __construct(){
        parent::__construct();
    }
    
    public function makeBusTypeString($data){
        $busTypeStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $incr++;
            $busCode = $decodeArr[$i]->BUS_TYPE_ID;
            $busShortCode = $decodeArr[$i]->BUS_TYPE_CD;
            $busServiceName = $decodeArr[$i]->BUS_TYPE_NM;
            $otherLanguage = "";

            if($busTypeStrings != "")
                $busTypeStrings .= "|";

            $serialStr = $this->makeString($serialNo, $this->SERIAL_NO, "0");
            $busCodeStr = $this->makeString($busCode, $this->BUS_CODE, "0");
            $busShortCodeStr = $this->makeString($busShortCode, $this->BUS_SHORT_CODE, "");
            $busServiceNameStr = $this->makeString($busServiceName, $this->BUS_SERVICE_NAME, " ");
            $otherLangStr = $this->makeString($otherLanguage, $this->OTHER_LANGUAGE, " ");
            
            $busTypeStrings.= "'".$serialStr."','".$busCodeStr."','".$busShortCodeStr."','".$busServiceNameStr."','".$otherLangStr."'";
            
        }
        return $busTypeStrings;
    }
    
    public function makeRfidMasterString($data){
        $rfidStrings = "";
        $decodeArr = json_decode($data);

        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $incr++;
            $key_a = $decodeArr[$i]->auth_key_A;
            $key_b = $decodeArr[$i]->auth_key_B;

            if($rfidStrings != "")
                $rfidStrings .= "|";
                    
            $rfidStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "0") . "','"
                . $this->makeString($key_a, $this->KEY_A, "") . "','"
                . $this->makeString($key_b, $this->KEY_B, "") . "'";
          
        }
        return $rfidStrings;        
    }
    
    public function makeGprsMasterString($data){
        $gprsStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $decodeArr[$i]->ID;
            $service_provider = $decodeArr[$i]->SERVICE_PROVIDER;
            $sim_check = $decodeArr[$i]->GPRS_FLAG;

            if($gprsStrings != "")
                $gprsStrings .= "|";
                    
            $gprsStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "0") . "','"
                . $this->makeString($service_provider, $this->SERVICE_PROVIDER, "") . "','"
                . $this->makeString($sim_check, $this->SIM_CHECK, "") . "'";
          
        }
        return $gprsStrings;
    }  
    
    public function makeActivityLogMasterString($data){
        $actStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $serialNo = $decodeArr[$i]->serial_no;
            $activity_name = $decodeArr[$i]->activity_name;
            $activity_code = $decodeArr[$i]->activity_code;
            $count = $decodeArr[$i]->count;            
            $waybill_no = $decodeArr[$i]->waybill_no;
            $print_flag = $decodeArr[$i]->print_flag;
            $update_datetime = $decodeArr[$i]->update_datetime;
    
            if($actStrings != "")
                $actStrings .= "|";
            
            $actStrings .= "'" . $this->makeString($serialNo, $this->SERIAL_NO, "") . "','"
                . $this->makeString($activity_name, $this->ACTIVITY_NAME, "") . "','"
                . $this->makeString($activity_code, $this->ACTIVITY_CODE, "") . "','" . $this->makeString($count, $this->COUNT, "") . "','"
                . $this->makeString($waybill_no, $this->WAYBILL_NO, "") . "','" . $this->makeString($print_flag, $this->PRINT_FLAG, "") . "','"
                . $this->makeString($update_datetime, $this->UPDATE_DATETIME, "") . "'";            
          
        }
        return $actStrings;        
    } 
    
    public function makeStationChangeLogMasterString($data){
        $stationChangeStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
                   
            $old_name = $decodeArr[$i]->oldname;
            $new_name = $decodeArr[$i]->newname;
            $old_code = $decodeArr[$i]->oldcode;
            $new_code = $decodeArr[$i]->newcode;            
            $status = $decodeArr[$i]->status;
            $update_datetime = $decodeArr[$i]->Date;
    
            if($stationChangeStrings != "")
                $stationChangeStrings .= "|";

            $stationChangeStrings .= "'" . $this->makeString($old_name, $this->OLD_NAME, "") . "','"
                . $this->makeString($new_name, $this->NEW_NAME, "") . "','"
                . $this->makeString($old_code, $this->OLD_CODE, "") . "','" 
                . $this->makeString($new_code, $this->NEW_CODE, "") . "','"
                . $this->makeString($status, $this->STATUS, "") . "','" 
                . $this->makeString($update_datetime, $this->UPDATE_DATETIME, "") . "'";            
          
        }
        return $stationChangeStrings;
    }
    
    public function makeDepotMasterString($data){
        $depotListStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $depoId = $incr++;
            $depotCode = $decodeArr[$i]->DEPOT_CD;
            $depotName = $decodeArr[$i]->DEPOT_NM;

            if($depotListStrings != "")
                $depotListStrings .= "|";
                    
            $depotListStrings .= "'" . $this->makeString($depoId, $this->DEPOT_ID, "0") . "','"
                . $this->makeString($depotCode, $this->DEPOT_CODE, "") . "','"
                . $this->makeString($depotName, $this->DEPOT_NAME, "") . "'";
          
        }
        return $depotListStrings;   
    }
    
    public function makeBusOperationString($data){
        $busOpStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $optId = $decodeArr[$i]->paraid;
            $optName = $decodeArr[$i]->para_eng_value;
            $printFlag = $decodeArr[$i]->para_enable;
            $height = $decodeArr[$i]->para_height;            
            $width = $decodeArr[$i]->para_width;
    
            if($busOpStrings != "")
                $busOpStrings .= "|";

            $busOpStrings .= "'" . $this->makeString($optId, $this->OPERATOR_ID, "") . "','"
                . $this->makeString($optName, $this->OPERATOR_NAME, "") . "','"
                . $this->makeString($printFlag, $this->PRINT_FLAG, "") . "','" 
                . $this->makeString($height, $this->HEIGHT, "0") . "','"
                . $this->makeString($width, $this->WIDTH, ""). "'"; 
        }
        return $busOpStrings;  
    }    


 public function makeBusConcessionString($data){
        $busOpStrings = "";
        $decodeArr = json_decode($data);
        $incr = 1;
        for($i=0;$i<count($decodeArr);$i++){
            $concession_number = $decodeArr[$i]->concession_number;
            $concession_type_code = $decodeArr[$i]->concession_type_code;
            $concession_type_name = $decodeArr[$i]->concession_type_name;
            $concession_percentage = $decodeArr[$i]->concession_percentage;            
           // $width = $decodeArr[$i]->para_width;
    
           if($busOpStrings != "")
                $busOpStrings .= "|"; 

            $busOpStrings .= "'" . $this->makeString($concession_number, $this->CON_NO, "") . "','"
                . $this->makeString($concession_type_code, $this->CON_CODE, "") . "','"
                . $this->makeString($concession_type_name, $this->CON_NAME, "") . "','" 
                . $this->makeString($concession_percentage, $this->CON_PER, ""). "'"; 
              //  . $this->makeString($width, $this->WIDTH, ""). "'"; 
   }
        return $busOpStrings;  
    }    
}
?>
