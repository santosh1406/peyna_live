<?php

class mcollection_model extends Model{
    
    public function __construct(){
        parent::__construct();
    }
//    91912577,SMP00003,APP_ID,01.03,060313063945,33,33";
//    serialno,machineno,,machineversion,machinebuild date,waybillno
    public function machineVerification($depot_cd, $info,$action)
    {
//        ini_set('display_errors',1);
        $ajaxData = "<root>";
        $verification = 1;
        // $info = "91912577,SMP00003,APP_ID,01.03,060313063945,33,33";
        if ($info != "") {
            $imieInfo = explode(",",$info);
            if (count($imieInfo) == 3) {
                $SIM_IMSI = $imieInfo[1];
                $SIM_CCID = $imieInfo[2];
                $etim_no = $imieInfo[1];
                $ret = "";
                if ($SIM_IMSI == "0") {
                    $fields = array('SIM_CCID'=>$SIM_CCID,'SIM_IMSI'=>'','etim_no'=>$etim_no);
                    $ret = $this->nusoapClient(WS_URL,'updateEtmMaster',$fields);
                } else {
                    $fields = array('SIM_CCID'=>$SIM_CCID,'SIM_IMSI'=>$SIM_IMSI,'etim_no'=>$etim_no);
                    $ret = $this->nusoapClient(WS_URL,'updateEtmMaster',$fields);
                }
            }
        }        
        if ($info != "") {    
            $arrInfo = explode(",",$info);

            if (count($arrInfo) > 5) {
                $machineNo = $arrInfo[1];
                $handleNo = $arrInfo[0];
//                $register_dt = date_format(strtotime($arrInfo[4]),"dMyhms");
                $register_dt = date_parse_from_format('dmyhis', $arrInfo[4]);
                $version_dt = $register_dt['year']."-".$register_dt['month']."-".$register_dt['day'];
//                date("y-M-d",$arrInfo[4]);
                $machine_app_id = (isset($arrInfo[2])?$arrInfo[2]:"");
                $machine_version = (isset($arrInfo[3])?$arrInfo[3]:"");
                $waybill_no = (isset($arrInfo[5])?$arrInfo[5]:"");
                $transactionNo = (isset($arrInfo[6])?$arrInfo[6]:"");
                $simcheck = (isset($arrInfo[7])?$arrInfo[7]:"");
                $m_no = $this->getMachineNoPrefix($machineNo);
                $machine_version_id = "";
                $machine_version_dt = "";

                if ($m_no!="") {
                    if ($m_no!=$depot_cd) {
                        //displayMessage("This machine is not valid for " + depot_cd + " depot");
                        $ajaxData .= "<Sub id=\"MESSAGE\">NOT VALID</Sub>";
                        $ajaxData .= "</root>";
                        return $ajaxData;
                    }
                }

                $machinStatusArr['machineNo'] = $machineNo;
                $machinStatusArr['version_dt'] = $version_dt;
                $machinStatusArr['handleNo'] = $handleNo;
                
                $mach_isRegister = $this->nusoapClient(WS_URL,'checkMachineStatus',$machinStatusArr);
                
                $machinVerArr['version_dt'] = $version_dt;
                $machinVerArr['machineNo'] = $machineNo;
                $machinVerArr['machine_version_id'] = $machine_version_id;
                $machinVerArr['machine_version_dt'] = $machine_version_dt;
                $machinVerArr['action'] = $action;
                
                $isVersion = $this->nusoapClient(WS_URL,'checkVersion',$machinVerArr);

                $ajaxData .= "<Sub id=\"HANDLENO\">" .$handleNo + "</Sub>";
                $ajaxData .= "<Sub id=\"MACHINENO\">" .$machineNo + "</Sub>";
                $ajaxData .= "<Sub id=\"APPID\">" .$machine_app_id + "</Sub>";
                $ajaxData .= "<Sub id=\"VERSION\">" .$machine_version + "</Sub>";
                $ajaxData .= "<Sub id=\"VERSIONDT\">" .$version_dt + "</Sub>";
                $ajaxData .= "<Sub id=\"WAYBILLNO\">" .$waybill_no + "</Sub>";
                $ajaxData .= "<Sub id=\"TRANSACTION\">" .$transactionNo + "</Sub>";
                $ajaxData .= "<Sub id=\"MACHINEVERSIONID\">" .$machine_version_id + "</Sub>";
                $ajaxData .= "<Sub id=\"SIMCHECK\">" .$simcheck + "</Sub>";

                if ($mach_isRegister=="OK" && $isVersion=="Y") {
                    $ajaxData .= "<Sub id=\"MACHINESTATUS\">OK</Sub>";
                } else {
                    $ajaxData .= "<Sub id=\"MACHINESTATUS\">NO</Sub>";
                }

                if ($mach_isRegister=="NOTWORKING") {
                    $ajaxData .= "<Sub id=\"MESSAGE\">NOTWORKING</Sub>";
                    $verification = 0;
                    //displayMessage("Machine Is Out Of Order Please get It Repaired...");
                } else if ($mach_isRegister=="FAULTY") {
                    $verification = 0;
                    $ajaxData .= "<Sub id=\"MESSAGE\">FAULTY</Sub>";
                    //displayMessage("This Machine Is Demaged.");
                } else if ($mach_isRegister=="TRANSFER") {
                    $ajaxData .= "<Sub id=\"MESSAGE\">TRANSFER</Sub>";
                    $verification = 0;
                    //displayMessage("This Machine Is Transferred To Another Depot.");
                } else if (!$isVersion) {
                    $verification = 0;
                    $ajaxData .= "<Sub id=\"MESSAGE\">UPDATE VERSION</Sub>";
                    //displayMessage("Please update machine version on server.");
                } else if ($mach_isRegister=="OLD") {
                    $verification = 0;
                    $ajaxData .= "<Sub id=\"MESSAGE\">OLD</Sub>";
                    //displayMessage("Machine version is older please update machine version.");
                } else if ($mach_isRegister=="N") {
                    $verification = 0;
                    $ajaxData .= "<Sub id=\"MESSAGE\">NOT REGISTER</Sub>";
                    //displayMessage("Machine is not register please register first.");
                }
                
                $patch_status = $this->nusoapClient(WS_URL,'getPatchStatus');
                if ($patch_status == "1") {
                    $ajaxData .= "<Sub id=\"PATCHSTATUS\">OUTDATED</Sub>";
                } else {
                    $ajaxData .= "<Sub id=\"PATCHSTATUS\">WORKING</Sub>";
                }
            }
        }
        $ajaxData .= "</root>";
        return $ajaxData;        
    }
    
    function getMachineNoPrefix($machineNo) {
        $preFix = "";
        if ($machineNo != null && $machineNo!="") {
            for ($i = 0; $i < strlen($machineNo); $i++) {
                $charAt = substr($machineNo, $i, 1);
                if(is_numeric($charAt)){
                    $preFix .= $charAt;
                }
            }
        }
        return $preFix;
    }    
}
?>