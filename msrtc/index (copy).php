<?php
header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");

session_start();
$new_user=array('AGT00380','AGT00395');
/*
 *---------------------------------------------------------------
 * SESSION TIMEOUT START
 *---------------------------------------------------------------
 */
$_SESSION['is_collected'] = true; //IMP DONT REMOVE

$inactive = 6000;

if(!isset($_SESSION['timeout']))
{
    $_SESSION['timeout'] = time() + $inactive; 
}

$session_life = time() - $_SESSION['timeout'];

if($session_life > $inactive)
{  
    session_destroy();

    header("Location:index.php?page=main");
    die;
}

$_SESSION['timeout'] = time();
/*
 *---------------------------------------------------------------
 * SESSION TIMEOUT END
 *---------------------------------------------------------------
 */

define('ENVIRONMENT', 'production');
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('FCPATH', str_replace(SELF, '', __FILE__));
define('COMMON','controller/system/common.php');

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

ini_set("short_open_tags",true);

if (defined('ENVIRONMENT'))
{
    switch (ENVIRONMENT)
    {
        case 'development':
            error_reporting(E_ALL);
            ini_set('display_errors',1);
        break;

        case 'testing':
        case 'production':
            error_reporting(0);
        break;

        default:
           exit('The application environment is not set correctly.');
    }
}

global $page,$action,$allowed_pages,$page_labels;

$page_labels = array('main' => "Main Page",
            'machine_unregistration' => "Machine Unregister",
            'machine_report' => "Report",
            'machine_registration' => "Machine Register",
            'waybill_allocation' => "Waybill Allocation",
            'master_sync' => "Master Sync",
            'dutystart' => "Duty Start",
            'mcollection' => "Collection"
          );

$allowed_pages = array('main' => array("main"), 
            'machine_unregistration'=>array("main","machine_unregistration"),
            'machine_registration' => array("main","machine_registration"),
            'waybill_allocation' => array("main","waybill_allocation","master_sync","dutystart","machine_report"),
	 //'waybill_allocation' => array("main","waybill_allocation","master_sync","dutystart"),
            'machine_report'=>array("main","machine_report"),
	// 'machine_report'=>array("main"),
            // 'master_sync' => array("master_sync","dutystart"),
            // 'dutystart' => array("dutystart"),
            'mcollection' => array("main","mcollection","machine_report"));
//'mcollection' => array("main","mcollection"));

if(!empty($_REQUEST['p']))
{

    if(!isset($_REQUEST['page']) || $_REQUEST['page'] == ""){
        $_SESSION['errMsg'] = "Page cannot be blank.";
        header("Location:index.php?page=login");
        die;
    }

    require_once("controller/system/constants.php");
    require_once("controller/system/model.php");
    require_once("controller/system/helper.php");

    $crypted = trim(decrypt(urldecode($_REQUEST['p'])));

    // $crypted = trim(base64_decode(urldecode($_REQUEST['p'])));

    $rdata = convertToArray($crypted,'###');

    $logindata['REQUEST'] = $_REQUEST;
    $logindata['URL'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $logindata['REMOTEADDR'] = $_SERVER['REMOTE_ADDR'];
    $logindata['REMOTEHOST'] = $_SERVER['REMOTE_HOST'];

    log_data("login_".date('d-m-Y').".log",$logindata,'API LOGIN RAW DATA');
    log_data("login_".date('d-m-Y').".log",$rdata,'API LOGIN DECRYPTED DATA');
    $decodedArr['key']          = $rdata['key'];
    $decodedArr['agent_code']   = $rdata['agent_code'];
    $decodedArr['op_code']      = $rdata['op_code'];
    $decodedArr['bus_stop']     = $rdata['bus_stop'];
    $decodedArr['balance']      = $rdata['balance'];
    $decodedArr['remaining_balance']      = $rdata['remaining_balance'];
 $decodedArr['opening_balance'] = $rdata['opening_balance'];  
    log_data("login_".date('d-m-Y').".log",$decodedArr,'API LOGIN DECRYPTED DATA');

    $modObj = new Model();
    $fields['username'] = $decodedArr['agent_code'];
	
    $agentDetails = $modObj->nusoapClient(WS_URL,'getAgentDetailsByUserName',$fields);
  
    log_data("login_".date('d-m-Y').".log",$agentDetails,'AGENT ARR');
    $agentArr = json_decode($agentDetails);
 
    if($decodedArr['bus_stop']!=""){    
        $busfields['bus_stop'] = $decodedArr['bus_stop'];    
        $busStopDetails = $modObj->nusoapClient(WS_URL,'getBusStopDetails',$busfields);
        log_data("login_".date('d-m-Y').".log",$busStopDetails,'BU STOP DETAIL');
        $busStopArr = json_decode($busStopDetails);
    }

    if(empty($agentArr)){
        echo "Invalid User Id";
        $_SESSION['errMsg'] = "Invalid agent details.";
        header("Location:index.php?page=login");    
        die;
    }

    $setStatus = $modObj->setSessionVars($agentArr);
    
    $_SESSION['BUS_STOP'] = $decodedArr['bus_stop']; 
    if($decodedArr['bus_stop']!=""){    
        $_SESSION['BUSSTOPNAME'] = $busStopArr[0]->BUS_STOP_NM; 
        $_SESSION['BUSSTOPID'] = $busStopArr[0]->BUS_STOP_ID; 
    }
    $_SESSION['OP_CODE'] = $decodedArr['op_code']; 
    $_SESSION['BALANCE'] = $decodedArr['balance']; 
    $_SESSION['REM_BALANCE'] = $decodedArr['remaining_balance']; 
    // $_SESSION['REM_BALANCE'] = "100"; 
    $_SESSION['TOTAL_BALANCE'] = (float)$_SESSION['BALANCE']+(float)$_SESSION['REM_BALANCE']; 
    $_SESSION['KEY'] = $decodedArr['key'];
    $_SESSION['OPENING_BALANCE'] = $decodedArr['opening_balance'];  
    $_SESSION['PAGE'] = $_REQUEST['page']; 
if (in_array($rdata['agent_code'], $new_user))
  {
   header("Location:http://180.92.175.107/app/bosetim_17/index.php?page=main");    
   die;
  }

/* if($rdata['agent_code']=='AGT00337'){
        header("Location:http://180.92.175.107/app/bosetim_17/index.php?page=main");    
        die;
    }*/
    header("Location:index.php?page=main");    
    die;
}

if(isset($_REQUEST['page']) && $_REQUEST['page']!="")
{
    $page = ($_SESSION['is_collected']) ? $_REQUEST['page'] : "mcollection";
    // $page = $_REQUEST['page'];
}
else
{
    header("Location:index.php?page=main");
    die;
}

if(isset($_REQUEST['method']) && $_REQUEST['method']!="")
{
    $method = ($_SESSION['is_collected']) ? $_REQUEST['method'] : "index";
    // $method = $_REQUEST['method'];
}
else{
    $method = "index";
//    header("Location:index.php?page=main");
//    die;
}

if(!isset($_SESSION['USERID'])){
    $page = "login";
}else if(isset($_SESSION['USERID']) && $page == 'login'){

if (in_array($rdata['agent_code'], $new_user))
  {
   header("Location:http://180.92.175.107/app/bosetim_17/index.php?page=main");    
   die;
  }
/* if($rdata['agent_code']=='AGT00337'){
        header("Location:http://180.92.175.107/app/bosetim_17/index.php?page=main");    
        die;
    }*/
    header("Location:index.php?page=main");
}
require_once(COMMON);
