$(document).ready(function(){

    $('.date_of_jour_arr').bind("cut copy paste drop keyup",function(e) {
        e.preventDefault();
    });
    
    make_autocomplete('.frm-to-src-dest', cities);
    
    $.validator.addMethod("dateFormat",
        function(value, element) {
            var currVal = value;
            if(currVal == '')
                return false;
            //Declare Regex 
            var rxDatePattern = /^(\d{1,2})(\-)(\d{1,2})(\-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern); // is format OK?
            if (dtArray == null)
                return false;
            //Checks for dd-mm-yyyy format.
            dtDay = dtArray[1];
            dtMonth= dtArray[3];
            dtYear = dtArray[5];
            if (dtMonth < 1 || dtMonth > 12)
            {
                return false;
            }
            else if (dtDay < 1 || dtDay> 31)
            {
                return false;
            }
            else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
            {
                return false;  
            }
            else if (dtMonth == 2)
            {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay> 29 || (dtDay ==29 && !isleap))
                    return false;
            }

            return true;
        },"Please enter valid date.");


        $.validator.addMethod("checkDepend",
            function(value, element) {

            if($("#date_of_arr").val() != "")
            {
                var date_diff = date_operations($("#date_of_jour").val(),$("#date_of_arr").val());
                if(date_diff.date_compare)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        },"Return journey must be after onward journey.");

    
    $.validator.addMethod("notEqualTo", function (value, element, param)
    {
        var target = $(param);
        if (value) return value != target.val();
        else return this.optional(element);
    }, "Repeated field");
    
    $('#search_services').validate({
        ignore: "",
        rules: {
            from: {
                required: true,
                notEqualTo: "#to"
            },
            to : {
                required: true,
                notEqualTo: "#from"
            },
            date_of_jour : {
                required: true,
                dateFormat: true
            },
            date_of_arr : {
                required: "#round_trip:checked",
                checkDepend : true
            },
            provider : {
                required: true,
            }
        },
        messages: {
            from: {
              required: "Please enter from stop",
              notEqualTo: "From & to stop must be different"
            },
            to: {
              required: "Please enter to stop",
              notEqualTo: "From & to stop must be different"
            }
            ,
            date_of_jour: {
              required: "Please select date of journey",
              dateFormat: "Please enter date in dd-mm-yyyy format"
            }
            ,
            date_of_arr: {
              required: "Please select date of return journey",
            },
            provider: {
              required: "Please select provider",
            }
        },
        errorPlacement: function(error, element)
        {
          if(element.attr("name") == "provider")
          {
            error.appendTo('.provider_error');
          }
          else
          {
            error.insertAfter(element);
          }
        }
    });


    $('input[type=radio][name=triptype]').change(function() {

        if (this.value == 'single')
        {
            $("#date_of_arr").parent( "div.datepicker-wrap").removeClass("show-calender-datepicker").addClass("datepicker-wrap-return");
            $("input#date_of_arr").val("").prop( "disabled", true );
            $("input#date_of_arr").next("label#date_of_arr-error").html("");
        }
        else if (this.value == 'round')
        {
            $( "#date_of_arr" ).parent( "div.datepicker-wrap").addClass("show-calender-datepicker").removeClass("datepicker-wrap-return");
            $("#date_of_arr").parent(".datepicker-wrap::after").css({"background":"#cccccc none repeat scroll 0 0"});
            $("input#date_of_arr").prop( "disabled", false );
        }
    });


    $(document).off('click','.switch_cities').on('click','.switch_cities', function(e){
        e.preventDefault();
        var from_city   =   $("#from").val();
        var to_city     =   $("#to").val();

        if(from_city != "" || to_city != "")
        {
            $("#from").val(to_city);
            $("#to").val(from_city);
        }
    });

}); // end of document ready