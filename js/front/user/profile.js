
    
$(document).ready(function () {

    $("#oldpass").val("");
    
    $(document).off("click", ".change_form").on("click", ".change_form", function (data) {
        $(".profile_form").hide();
        var to_show = $(this).attr("data-attr");
        $("#" + to_show).show();
    });

    $(document).off("click", ".change_pass").on("click", ".change_pass", function (data) {

         
        if (jQuery("#chanpass").valid())
        {   
            $("#reseterr").html("");
            var detail = {};
            var div = "";
            var ajax_url = 'admin/users/change_pass';
            var form = 'chanpass';
            var err = "";
           
            get_data(ajax_url, form, div, detail, function (response)
            {
                if(response.flag != "")
                {
                    showNotification(response.msg_type, response.msg);
                    $("#oldpass").val('');
                    $("#newpass").val('');
                    $("#connewpass").val('');
                     
                }
                else
                {
                    showNotification("error", "Some unexpected has happened. Please try again");
                }
                /*if (response.flag=="@#error@")
                {
                    if(response.error.newpass)
                    {
                        $("#errnew").html(response.error.newpass);
                    }
                    else
                    {
                        $("#errnew").html("");
                    }
                        
                    if(response.error.oldpass)
                    {
                        $("#errold").html(response.error.oldpass);
                    }
                    else
                    {
                        $("#errold").html("");
                    }


                }
               
                else if(response.flag=="success")
                {
           
                    showNotification(response.msg_type, response.msg);
                             
                    $("#oldpass").val("");
                    $(".error").html("");
                    $("#newpass").val("");
                    $("#connewpass").val("");
                }*/

            }, '', false)
        }
        else
        {
            $('#chanpass').validate();
        }
    });

    $(document).off("keyup", "#email").on("keyup", "#email", function (data)
    {
        var detail = {};
        var div = "";
        var ajax_url = 'admin/users/email_check_ajax';
        var form = 'usr_prof';

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.status == "failed")
            {
                $("#erremail").html("This email ID is already exist");
                $(".prof_upd").attr('disabled','disabled');
            

            }
            else
            {
                $("#erremail").html("");
                $(".prof_upd").removeAttr('disabled');

            }

        }, '', false)

    });


    //form validation user_profile

  
    $(document).off('keypress', '.num').on('keypress', '.num', function(e) {
        
       var keyCode=e.charCode? e.charCode : e.keyCode;
       
        var tabback = [8,9];
         
        if((keyCode >= 48 && keyCode <= 57) || (tabback.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;
        
    });
   
    $(document).off('keydown', '.address').on('keydown', '.address', function(e) 
     {
        
     var validch4add = [8,9];
      var keyCode=e.charCode? e.charCode : e.keyCode;
      alert(keyCode);
        
    });
      
    $(document).off('keypress', '#email').on('keypress', '#email', function(e) {
        
        var regex = new RegExp("^[a-zA-Z0-9@.\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
             
        
    });
    
    $(document).off('keypress', '.alpha').on('keypress', '.alpha', function(e) 
        {
            var keyCode=e.charCode? e.charCode : e.keyCode;
           
            var tabback = [8,9];        
            if((keyCode >= 65 && keyCode <=  90)  || (keyCode >= 97 && keyCode <= 122) || (tabback.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
             {
                return true //disable key press
             }
          return false;       
      });
      
    
    $.validator.addMethod('filesize', function(value, element, param) {

        return this.optional(element) || (element.files[0].size <= param) 
    });

    
    $.validator.setDefaults({ ignore: ":hidden:not(.chosen_select)" });
    $("#edit-profile-form").validate({
       
        rules: {
        
            add1: {
                required: true,
//                validaddress:true
            },
           /* add2: {
                required: true,
                validaddress:true
    
            },*/
            fnm: {
                required: true,
                lettersonly:true
            },
            lnm: {
                required: true,
                lettersonly:true
            },   
            city: {
                required: true,
              
            },
             country: {
                required: true,
              
            },
            state: {
                required: true,
               
            },
            dob: {
                required: true
                
            },
            month: {
                required: true
                
            },
            year: {
                required: true
                
            },
            userfile:
            {
               
                extension: "jpeg|png|jpg",
                filesize:2000000,
                accept: "image/*"
            },
            pcode: {
                required: true,
                number: true,
                validpin:true,
                minlength:6,
                maxlength:6,
            },
      
            gen: {
                required: true
            },
            //            country: {
            //                required: true
            //            },
//            mob: {
//                required: true,
//                number: true,
//                minlength: 10,
//                maxlength: 10
//
//            }

        },
        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "city")
            {
                error.appendTo(".city_err"); 
              
            }
         
            else if(element.attr("name") == "country")
            {
                error.appendTo(".coun_err");  
            }
            else if(element.attr("name") == "state")
            {
                error.appendTo(".state_err"); 
            }
            else if (element.attr("name") === "gen") {
                error.appendTo(".errgen");
            }
            else if (element.attr("name") === "userfile") {
                error.appendTo(".file_err");
            }
            else {
                error.insertAfter(element);
            }
                
        },
 
        messages: {
               
            "add1": {
                
                required:"Address1 should not be empty",
//                validaddress:"Please enter valid address"
            },
            /*"add2": {
                
                required:"Address2 should not be empty",
                validaddress:"Please enter valid address"
            },*/
            "city": {
                
                required:"Please select city name"
            },
            dob: "Please select valid date of birth",
       
            "state": {
               
                required:"Please select state Name"
            },
            country: "Please select country name",
            pcode: "Pincode Number should not be empty",
            "fnm": {
                
                required:"First Name should not be empty",
                lettersonly:"Please enter valid First  Name"
            },
            "userfile":
            {
                extension:"Please upload valid image",
                filesize:"Please upload file less than 10 KB",
                accept:"Please upload valid image"
            },
            "lnm": {
                
                required:"First Name should not be empty",
                lettersonly:"Please enter valid Last  Name"
            },
            gen: "Please select gender",
            mob: "Please enter mobile Number",
            alno: "Please enter alternate mobile number",
            "email": {
                required: "Please enter email address",
                internet_email: "Please enter  valid email address"
            },
//            "mob": {
//                required: "Please enter mobile number",
//                number: "Please enter  valid mobile number",
//                minlength:"Please enter 10 digit mobile number",
//                maxlength:"Please enter 10 digit mobile number"
//            },
            "pcode": {
                required: "Please enter pincode number",
                number: "Please enter  valid pincode number</font>",
                validpin:"Please enter  valid pincode number</font>",
                minlength:"Please enter 6 digit Pincode number",
                maxlength:"Please enter 6 digit Pincode number",
            }
        }
         




    });
    
    $("#chanpass").validate({
            
        rules: {
            oldpass: {
                required: true
               
            },
            newpass:
            {
                required:true,
                minlength:8
            },
            connewpass:
            {
                required:true,
                equalTo: "#newpass"
            }
        },
        messages: {
            oldpass: "old password should not be empty",
            "newpass":  { required:"New password should not be empty</font>",
                          minlength:"Password should be 8 character"
            },
            
            "connewpass": {
                required: "Confirm password should not be empty",
                equalTo: "Both password must be same"
            }
        }
    }); 

    //form update 
    $("#email_update").validate({
            
        rules: {
            pemail: {
                required: true,
                internet_email: true
            },
            semail:
            {
               
                internet_email: true,
                notEqual: "#pemail" 
            }
       
        },
        messages: {
            "pemail": {
                required: "Primary email  should not be empty",
                internet_email: "Please enter valid email address"
            },
            "semail": {
               
                notEqual: "Primary  Email ID & Secondary Email ID  must not  be same",
                internet_email: "Please enter valid email address"
            },
          
        }
    });


    $(document).off("click", ".prof_upd").on("click", ".prof_upd", function (response) {

      
        if (jQuery("#edit-profile-form").valid())
        {
            $(".errormsg").html("");
            var detail = {};
            var div = "";
            var ajax_url = 'admin/users/update_user_profile';
            var form = 'edit-profile-form';
            var err = "";
           
              
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@success')
                {
                    showNotification(response.msg_type,response.msg);
                    var url=BASE_URL+'admin/users/get_profile';
                    setTimeout("location.href ='"+url+"';",500);
                }
                else if (response.flag == "@#error@")
                {
                    if(response.error.fnm)
                    {
                        $(".errfir").html(response.error.fnm);  
                    }
                    else
                    {
                        $(".errfir").html("");      
                    }
                    if(response.error.lnm)
                    {
                        $(".errlast").html(response.error.lnm);  
                    }
                    else
                    {
                        $(".errlast").html("");      
                    }
                    if(response.error.mob)
                    {
                        $(".errphno").html(response.error.mob);  
                    }
                    else
                    {
                        $(".errphno").html("");      
                    }
                    if(response.error.dob)
                    {
                        $(".errdob").html(response.error.dob);  
                    }
                    else
                    {
                        $(".errdob").html("");      
                    }
                    if(response.error.gen)
                    {
                        $(".errgen").html(response.error.gen);  
                    }
                    else
                    {
                        $(".errgen").html("");      
                    }
                    if(response.error.add1)
                    {
                        $(".erradd1").html(response.error.add1);  
                    }
                    else
                    {
                        $(".erradd1").html("");      
                    }
                    /*if(response.error.add2)
                    {
                        $(".erradd2").html(response.error.add2);  
                    }
                    else
                    {
                        $(".erradd2").html("");      
                    }*/
                    if(response.error.country)
                    {
                        $(".coun_err").html(response.error.country);  
                    }
                    else
                    {
                        $(".coun_err").html("");      
                    }
                    if(response.error.state)
                    {
                        $(".state_err").html(response.error.state);  
                    }
                    else
                    {
                        $(".state_err").html("");      
                    }
                    if(response.error.city)
                    {
                        $(".city_err").html(response.error.city);  
                    }
                    else
                    {
                        $(".city_err").html("");      
                    }
                    if(response.error.pcode)
                    {
                        $(".pcode_err").html(response.error.pcode);  
                    }
                    else
                    {
                        $(".pcode_err").html("");      
                    }
                    if(response.error.userfile)
                    {
                        $(".img_err").html(response.error.userfile);  
                       
                    }
                    else
                    {
                        $(".img_err").html("");      
                    }
                }
            }, '', false)
        }
        else                      {
            $('#edit-profile-form').validate();
        }
                  
    });

   
    $(document).off("click", ".upd_email").on("click", ".upd_email", function (response) {
        if ($("#email_update").valid())
        {

            var detail = {};
            $("#st").removeClass();
            var div = "";
            var ajax_url = 'admin/users/change_email';
            var form = 'email_update';
        
            // detail['hddemail']=$("#hddemail").val();
            get_data(ajax_url, form, div, detail, function (response)
            {
                    if (response.flag == '@success#')
                    {
                        $("#semail").val(response.secemail);
                        if(response.verify_st=='N')
                            {
                                $("#st").addClass('veri glyphicon glyphicon-remove');
                                $("#st").css("color","red");
                            }
                          else if(response.verify_st=='Y')
                              {
                                  $("#st").addClass('veri glyphicon glyphicon-ok');
                                  $("#st").css("color","green");
                              }  
                        
                    }
                    else if (response.flag == "@#error@")
                    {
                        if(response.error.pemail) 
                        {
                        $("#errpemail").html(response.error.pemail);
                        }
                        else
                         {
                            $("#errpemail").html("");
                         }
                        if(response.error.semail) 
                        {
                            $("#errsemail").html(response.error.semail);
                        }
                       else
                        {
                            $("#errsemail").html("");
                        }
                    }
                
                showNotification(response.msg_type, response.msg);
                
            }, '', false)
        }
        else
        {
            $('#email_update').validate();
        }
    
    });
    
    //clear text box
    $(document).off("focus", "#pemail").on("focus", "#pemail", function (response) {
        $(".error").html("");
    });
    $(document).off("focus", "#semail").on("focus", "#semail", function (response) {
        $(".error").html("");
    });
    
    $(document).off("focus", "#dob").on("focus", "#dob", function (e) {
        e.preventDefault();
    });
        //Date of birth selector
        var year=new Date().getFullYear()-18;
        var past_year= year-100;
        var yearran=past_year+':'+year;
        var dt=$('#dob').val();
        var  spilt_dt=[];
        
        
   
       $('#dob').datepicker();
     
       $("#dob").datepicker( "option", "changeYear", true );
       $("#dob").datepicker( "option", "changeMonth", true );
       $("#dob").datepicker( "option", "dateFormat", "dd-mm-yy" );
       $("#dob").datepicker( "option", "yearRange",yearran);
       
       if(dt=='')
           {
                $("#dob").datepicker("option","defaultDate", new Date(1990,04,11));
           }    
       else
           {
               spilt_dt=dt.split("-");
               $("#dob").datepicker("option","defaultDate", new Date(spilt_dt[2],spilt_dt[1],spilt_dt[0]));
               $("#dob").val(spilt_dt[0]+"-"+spilt_dt[1]+"-"+spilt_dt[2]);
           }
      
    //jquery validation  custom function
    $.validator.addMethod("internet_email", function (value, element) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
       
            if(value!="")
                {
            if (filter.test(value)) {
                return true;
            }
            else {
                return false;
            }
                }
                else
                    {
                        return true;
                    }
            
    }, "Please enter valid email Address!");
    
    $.validator.addMethod("lettersonly", function(value, element) 
        {
            return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "");
    
//    $.validator.addMethod("validaddress", function(value, element) 
//        {
//            return this.optional(element) || /^[a-zA-Z-0-9-/(), ]+$/i.test(value);
//        }, "");

    $.validator.addMethod("notEqual", function(value, element) 
        {
            return $('#pemail').val() != $('#semail').val()
        }, 
    "Primary Email & secondary Email should not be same");



    $.validator.addMethod("validpin", function(value, element) 
    {
        return value !=0;
    }, "Please Enter valid  pincode number");


    //change state  name depending upon country
        $(document).off('change', '#country').on('change', '#country', function(e) 
        {
            
                /*$('#state option').remove();
                $("#city").html( $('<option>').html('select City').attr({
                'selected':true, 
                'value':''
            }));*/
            $("#state").html("<option value=''></option>"); 
            $("#city").html("<option value=''></option>");
            
            var detail = {};
            var div = "";
            var ajax_url = 'admin/bus_stop_master/get_state';
            var form = 'chanpass';
            var err = "";
            detail['country_id']=$("#country").val();
            get_data(ajax_url, form, div, detail, function (response)
            {
        
                 if(response.state.length !=0)
                  {
                    /*$("#state").html("<option value='' selected>Select state</option>");
                    $("#state").next("span.custom-select").html("Select state");*/
                    $.each(response.state, function(i,value)
                    {
                        $("#state").append("<option value="+value.intStateId+">"+value.stState+"</option>");
                        $(".chosen_select").trigger("chosen:updated");
                    });
                }
                else
                 {
                    $("#state").html("<option value=''></option>");  
                    /*$("#state").next("span.custom-select").html("Select state");*/
                    $("#city").html("<option value=''></option>");  
                    /*$("#city").next("span.custom-select").html("Select city");*/
                 }

                $(".chosen_select").trigger("chosen:updated");
    
            }, '', false)
          
    });
     
    //get state
    $(document).off('change', '#state').on('change', '#state', function(e) 
    {
        $('#city option').remove();
        var detail = {};
        var div = "";
        var ajax_url = 'admin/bus_stop_master/get_city';
        var err = "";
        var form='';
        detail['state_id']=$("#state").val();
        get_data(ajax_url, form, div, detail, function (response)
            {
     
                if(response.city.length != 0)
                    {
               
                        $("#city").html("<option value=''></option>");
                        /*$("#city").next("span.custom-select").html("Select city");*/
                        $.each(response.city, function(i,value)
                        {
                            $("#city").append("<option value="+value.intCityId+">"+value.stCity+"</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                   else
                    {
                        $("#city").html("<option  value=''></option>");    
                        // $("#city").next("span.custom-select").html("Select city");
               
                    }

                    $(".chosen_select").trigger("chosen:updated");
    
            }, '', false)
          
    });
    
         $(document).off("click", "#res").on("click", "#res", function (response) {
             
             var detail = {};
            var div = "";
            var ajax_url = 'admin/users/resend_link';
            var err = "";
            var form='';
            
            get_data(ajax_url, form, div, detail, function (response)
                {
         
                 if (response.flag == '@success#')
                     {
                       showNotification(response.msg_type, response.msg);  
                     }
         
              }, '', false)
         });
     
    

});
