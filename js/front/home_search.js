
$().ready(function() {
    var max_fare = $("#txtmax_fare").val();
    var min_fare = $("#txtmin_fare").val();

    var max_time = $("#txtmax_time").val();
    var min_time = $("#txtmin_fare").val();

    // alert(min_time);
    /*Fare range slider starts here*/
    $("#fare_range").slider({
        range: true,
        min: min_fare,
        max: max_fare,
        values: [100, 500],
        slide: function(event, ui) {
            //alert($(this).attr('data-fare'));
            $('.sort_list').each(function(index) {
                if ($(this).attr('data-fare') >= ui.values[ 0 ] && $(this).attr('data-fare') <= ui.values[ 1 ])
                {
                    $(this).show();
                }
                else
                {
                    $(this).hide();
                }
            });
            $("#fare_input").val("₹ " + ui.values[ 0 ] + " - ₹ " + ui.values[ 1 ]);
        }
    });


    $("#fare_input").val("₹ " + $("#fare_range").slider("values", 0) +
            " - ₹ " + $("#fare_range").slider("values", 1));
    /*Fare range slider ends here*/

    /*Departure time range slider starts here*/

    $("#dept_tm_range").slider({
        range: true,
        min: 0,
        max: 1440,
        step: 15,
        values: [600, 1200],
        slide: function(e, ui) {
            //alert($(this).attr('data-dept'));
            $('.sort_list').each(function(index) {
                if ($(this).attr('data-dept') >= ui.values[ 0 ] && $(this).attr('data-dept') <= ui.values[ 1 ])
                {
                    // alert('abcd');
                    $(this).show();
                }
                else
                {
                    //alert('pqrs');
                    $(this).hide();
                }
            });
            $("#dept_tm_input").val("₹ " + ui.values[ 0 ] + " - ₹ " + ui.values[ 1 ]);
            var hours1 = Math.floor(ui.values[0] / 60);
            var minutes1 = ui.values[0] - (hours1 * 60);

            if (hours1.length == 1)
                hours1 = '0' + hours1;
            if (minutes1.length == 1)
                minutes1 = '0' + minutes1;
            if (minutes1 == 0)
                minutes1 = '00';

            if (hours1 >= 12) {
                if (hours1 == 12) {
                    hours1 = hours1;
                    minutes1 = minutes1 + " PM";
                } else {
                    hours1 = hours1 - 12;
                    minutes1 = minutes1 + " PM";
                }
            } else {
                hours1 = hours1;
                minutes1 = minutes1 + " AM";
            }
            if (hours1 == 0) {
                hours1 = 12;
                minutes1 = minutes1;
            }



            $('.slider-time').html(hours1 + ':' + minutes1 + ' - ');

            var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);

            if (hours2.length == 1)
                hours2 = '0' + hours2;
            if (minutes2.length == 1)
                minutes2 = '0' + minutes2;
            if (minutes2 == 0)
                minutes2 = '00';

            if (hours2 >= 12) {
                if (hours2 == 12) {
                    hours2 = hours2;
                    minutes2 = minutes2 + " PM";
                } else if (hours2 == 24) {
                    hours2 = 11;
                    minutes2 = "59 PM";
                } else {
                    hours2 = hours2 - 12;
                    minutes2 = minutes2 + " PM";
                }
            } else {
                hours2 = hours2;
                minutes2 = minutes2 + " AM";
            }

            $('.slider-time2').html(hours2 + ':' + minutes2);



        }
    });
    $("#dept_tm_input").val("₹ " + $("#dept_tm_range").slider("values", 0) +
            " - ₹ " + $("#dept_tm_range").slider("values", 1));

    /*Departure time range slider ends here*/

    /*time slot */

    $(document).off('change', '.dept_tm_slot').on('change', '.dept_tm_slot', function(e) {

        $('.sort_list').hide();
        var array = $(this).val();

        if (array != null) {
            //alert(array.length);
            $.each(array, function(i) {
                // alert(array[i]);
                var myString = array[i];
                var str = myString.toString().split('-');
                $('.sort_list').each(function(index) {

                    if ($(this).attr('data-dept') >= parseInt(str[0]) && $(this).attr('data-dept') <= parseInt(str[1]))
                    {

                        $(this).show();
                    }

//             
                });
            });
        }
        else {
            $('.sort_list').show();
        }


    });
    /*time slot */

    $(document).off('click', '.book_seat_btn').on('click', '.book_seat_btn', function(e) {
        

        var trip_no = $(this).val();
        var selected_seat = $('#trip_no_' + trip_no + ' ul .seat.selected').length;

        if (selected_seat > 0)
        {
            return true;
        }
        else
        {
            e.preventDefault();
            alert('Pls Select the seat');
        }

    });

    $(document).off('click', '.selectbusbtn').on('click', '.selectbusbtn', function(e) {

        var ind = $(".selectbusbtn").index(this);

        $('ul li.seat.selected').removeClass('selected');
        $('.bus_seat_div').not($('.bus_seat_div').eq(ind)).hide();
        $('.bus_seat_div').eq(ind).toggle();


//      $('.bus_seat_div').hide().eq(ind).show();


        var detail = {};
        var div = "";
        var ajax_url = 'front/booking/get_seat_detail';

        var form = '';

        trip_op_data = $(this).val().split(',');

        detail['trip_no'] = trip_op_data[0];
        detail['op_name'] = trip_op_data[1];
        // detail['trip_no'] = 675;
        detail['date'] = $('#date').val();
        detail['from'] = $('#from').val();
        detail['to'] = $('#to').val();


        get_data(ajax_url, form, div, detail, function(response)
        {
            if (response.flag == '@#success#@')
            {
                // console.log(response.view);
                $('.bus_seat_div').eq(ind).html(response.view);
                $('#seat_' + detail['trip_no']).html("<h4>" + response.tot_seats_update + " Seats" + "<h4>");
                $('#fare_' + detail['trip_no']).html("<h4>" + "Rs. " + response.fare_update + "<h4>");
            }
        }, '', false);

    });

    $(document).ready(function() {
        $('.SlectBox').SumoSelect({csvDispCount: 3});
        window.test = $('.testsel').SumoSelect({okCancelInMulti: true});
    });


    $(document).off('change', '.filter').on('change', '.filter', function(e) {

        filter();
    });

    $(document).off('click', '.bus_seat_div .deckrow .seat').on('click', '.bus_seat_div .deckrow .seat', function(e) {

        var trip_no = $(this).attr('data-trip-no');

        if ($(this).hasClass('available'))
        {
            $(this).removeClass('available');
            $(this).addClass('selected');
        }
        else if ($(this).hasClass('selected'))
        {
            $(this).removeClass('selected');
            $(this).addClass('available');

        }
        cal_fare(trip_no);

    });


    $(document).off('click', '.sort_col').on('click', '.sort_col', function() {
        var col = $(this).attr('ref');
        var stype = $(this).attr('data-sort');
        $(this).attr('data-sort', stype == 'asc' ? 'desc' : 'asc');

        $('.sort_list').sort(function(a, b) {


            var a = $(a).attr(col);
            var b = $(b).attr(col);

            if (stype == 'asc')
            {
                return a > b ? 1 : -1;
            } else
            {
                return a > b ? -1 : 1;
            }


        });
    });
})

function cal_fare(trip_no)
{
    var $par_elem = $('#trip_no_' + trip_no + '');
    var $deck_row = $par_elem.find('.deckrow_' + trip_no + '');

    var fare = $deck_row.attr('data-fare');


    var total_sel_seat = $deck_row.find('.seat.selected').length;

    // console.log(fare);
    // console.log(total_sel_seat);

    var seat_ids = new Array();
    $('#trip_no_' + trip_no + ' .seat.selected').each(function(ind) {
        if ($(this).hasClass('selected'))
        {
            seat_ids.push($(this).attr('data-seat-no'));
        }
    });


    $par_elem.find('.seat_booked_inp').val(seat_ids.join(','));
    // $par_elem.find('.seat_fare_inp').val(fare);


    $par_elem.find('.sel_seat').html(seat_ids.join(', '));
    $par_elem.find('.tot_fare').html(total_sel_seat * fare);

    // console.log(seat_ids);
}

function filter()
{
    var detail = {};
    var div = "bus_detail_div";
    var ajax_url = 'front/home/filter';

    var form = 'filterform';

    $('.popup_loader').removeClass('hide');

    get_data(ajax_url, form, div, detail, function(response)
    {
        if (response.flag == '@#success#@')
        {

        }
    }, '', false);
    $('.popup_loader').addClass('hide');
}

//Get fare starts
$(document).off('click', '.getfare').on('click', '.getfare', function(e) {
    e.preventDefault();
    var ind = $(".getfare").index(this);
    var getfare = $(this).attr('href');
   
    var detail = {};
    var div = "";
    var ajax_url = 'front/booking/get_seat_fare';

    var form = '';

    trip_op_data = getfare.split(",");
    detail['trip_no'] = trip_op_data[0];
    detail['op_name'] = trip_op_data[1];
    detail['date'] = $('#date').val();
    detail['from'] = $('#from').val();
    detail['to'] = $('#to').val();


    get_data(ajax_url, form, div, detail, function(response)
    {
       
        $('#seat_' + detail['trip_no']).html("<h4>" + response.tot_seats + " Seats" + "<h4>");
        $('#fare_' + detail['trip_no']).html("<h4>" + "Rs. " + response.fare + "<h4>");
        //}
    }, '', false);

});
//Get Fare ends

