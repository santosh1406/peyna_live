
        
$().ready(function () {
     $( "#transaction_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    });
    
//    $('#op_name').hide();
//    $('#api_provider').hide();
    
$('.alert-dismissable').delay(5000).fadeOut("slow")

    $(document).on('change', '#payment_type', function(){
        var detail = {};
            var div = "";
            var ajax_url = 'admin/op_topup/get_payment_mode';
            var form = '';
          
            detail["payment_type_id"]= $(this).val();
            
            get_data(ajax_url, form, div, detail, function (response)
            {
                if(response.payment_mode) 
                {
                    var option_data = "";
                    $.each(response.payment_mode, function(i, item) {
                        option_data += "<option value='"+item.id+"'>"+item.payment_mode+"</option>";
                    });
                    $(".payment_mode").css('display','block');
                    $('#payment_mode').html(option_data);
                }
                else
                {
                    $(".payment_mode").css('display','none');
                }
            });
    });
    
    $(document).on('change', '#provider_type', function(){
       if($(this).val()=='operator'){
           $('.api_provider').css('display','none');
           $('.op_name').css('display','block');
           
       } else if($(this).val()=='api_provide') {
           $('.api_provider').css('display','block');
           $('.op_name').css('display','none');
       }
       else {
           $('.api_provider').css('display','none');
           $('.op_name').css('display','none');
       }
    });
    
    /*
     * Enable and disable topup data
     */
    $(document).off('click', '.activate_operator').on('click', '.activate_operator', function (e) {
        e.preventDefault();
        var operator_status = $(this).attr("data-status");
        var id = $(this).attr("ref");
        var $this = $(this);
        if(operator_status == 'Pending')
        {
            var yesno = confirm("Are you sure you want to Approve this Topup?");
        }
        else if(operator_status == 'Approved')
        {
            //var yesno = confirm("Are you sure you want to disable this Topup?");
            alert("You are not allowed to change status once it is approved");
            return false;
        }
       
        if(yesno)
        {
            var detail = {};
            var div = "";
            var ajax_url = 'admin/op_topup/enableDisableTopup';
            var form = '';
          
            detail["id"]= id;
            
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    if(response.msg_type == "success")
                    {   $this.attr("ref",response.updated_data);
                  
                        window.location.href =BASE_URL+'admin/op_topup/';
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        showLoader(response.msg_type,response.msg); 
                    }
                    else
                    {
                        alert(response.msg);
                    }     
                }
            }, '', false);
        }
       
    });
    
    
    
    $(document).off('click', '.reject_operator').on('click', '.reject_operator', function (e) {
        e.preventDefault();
        var operator_status = $(this).attr("data-status");
        var id = $(this).attr("ref");
        var $this = $(this);
        if(operator_status == 'Pending')
        {
            var yesno = prompt("Are you sure you want to Reject this Topup?");
        }
        
        if(yesno==''){
            alert('Please enter the reason');
            var yesno = prompt("Are you sure you want to Reject this Topup?");
        }
          
        if(yesno)
        {
            var detail = {};
            var div = "";
            var ajax_url = 'admin/op_topup/reject_topup';
            var form = '';
          
            detail["id"]= id;
            detail["remark"]= yesno;
            
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    if(response.msg_type == "success")
                    {   $this.attr("ref",response.updated_data);
                  
                        window.location.href =BASE_URL+'admin/op_topup/';
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        showLoader(response.msg_type,response.msg); 
                    }
                    else
                    {
                        alert(response.msg);
                    }     
                }
            }, '', false);
        }
       
    });
    
    /*
     * Edit data
     */
    
     $(document).off('click', '.edit_topup').on('click', '.edit_topup', function (e) {
        var operator_status = $(this).attr("data-status");
        if(operator_status == 'Approved')
        {   
            e.preventDefault();
            alert("It is already approved. Not allow to update");
            return false;
        }
    });
    
    var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        processing: true,
        serverSide: true,
        ajax: {
            "url": BASE_URL + "admin/op_topup/list_topup_data",
            "type": "POST",
            "dataType": "json",
            data: function (d) {
                  d.from_date = $("#from_date").val();
                  d.to_date = $("#to_date").val();
                  d.rokad_token= rokad_token;

                                      
            }
        },
        columnDefs: [
            {
                "searchable": false
            }
        ],
        order: [[0, "desc"]],
        iDisplayLength: 10,
        aLengthMenu: [[5, 10, 50, -1], [5, 10, 50, "All"]],
        paginate: true,
        paging: true,
        searching: false,
        scrollX: true,
        aoColumnDefs: [
            {
                "bSortable": false,
                "aTargets": [1]
            }
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            var tot_amt = 0;
            var a4 = aData[4] * 1.00;
            var a5 = aData[5] * 1.00;
            var a6 = aData[6] * 1.00;
            var a7 = aData[7] * 1.00;
            var a8 = aData[8] * 1.00;
            var a9 = aData[9] * 1.00;
            //var a10 = aData[10] * 1.00;
            $("td:eq(4)", nRow).html(a4.toFixed(2));
            $("td:eq(5)", nRow).html(a5.toFixed(2));
            $("td:eq(6)", nRow).html(a6.toFixed(2));
            $("td:eq(7)", nRow).html(a7.toFixed(2));
            $("td:eq(8)", nRow).html(a8.toFixed(2));
            $("td:eq(9)", nRow).html(a9.toFixed(2));
            //$("td:eq(10)", nRow).html(a10.toFixed(2));
            
            //$("td:eq(14)", nRow).html(aData[17]);
            if ($("td:eq(10)", nRow).html() == 'Approved' || $("td:eq(10)", nRow).html() == 'Rejected') {
                $("td:eq(11)", nRow).html('&nbsp;')
            } else {
                $("td:eq(11)", nRow).html(aData[14]);
            }

            //debugger;
            return nRow;
        },footerCallback: function (tfoot,nRow, aData, start, end, display ) {
         
            var api = this.api(), aData;


            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
           
            
            $(tfoot).find('th').eq(0).html("Total");
              
            tax_without_disc = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
                
           disc_price = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
             disc_price1 = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 disc_price2 = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
disc_price3 = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
disc_price4= api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
disc_price5 = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );



                       
            $( api.column( 4 ).footer() ).html(
                tax_without_disc +' '
            );
            $( api.column( 5 ).footer() ).html(
                        disc_price +' '
                    );
 $( api.column( 6 ).footer() ).html(
                        disc_price1 +' '
                    );
            $( api.column(7).footer() ).html(
                        disc_price2 +' '
                    ); 
        }
    };
    var oTable = $('#topup_data').dataTable(tconfig); 
    
    
    //$(document).off('click', '#show_rpt').on('click', '#show_rpt', function (e) {
    $(document).on('click', '#show_rpt', function (e) {
        debugger;
        oTable.fnDraw();
        e.preventDefault();
    });
    
    $(document).on('click', '#add_operator', function () 
    {
        var detail          = {};
        var div             = "";
        var ajax_url        = 'admin/op_topup/check_approved_topup/';
        var form            = '';

        get_data(ajax_url, form, div, detail, function(response)
        {   
            if(response.approved_status=='approved')
            {
                window.location.href =BASE_URL+'admin/op_topup/add_topup';
            }
            else
            {
                alert("Please approve the previous transactions before adding new request.");
                return false;
            }
        });
    });
    
   
 //validation for Operator Data

     jQuery.validator.addMethod("internet_email", function (value, element) {
  if(value == '') 
        return true;
    var temp1;
    temp1 = true;
    var ind = value.indexOf('@');
    var str2=value.substr(ind+1);
    var str3=str2.substr(0,str2.indexOf('.'));
    if(str3.lastIndexOf('-')==(str3.length-1)||(str3.indexOf('-')!=str3.lastIndexOf('-')))
        return false;
    var str1=value.substr(0,ind);
    if((str1.lastIndexOf('_')==(str1.length-1))||(str1.lastIndexOf('.')==(str1.length-1))||(str1.lastIndexOf('-')==(str1.length-1)))
        return false;
    str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
    temp1 = str.test(value);
    return temp1;



}, "Please enter valid email address!");

jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z. ]+$/i.test(value);
}, "");

jQuery.validator.addMethod("lfsccode", function(value, element) {
    return this.optional(element) || /^[a-z]{4}\d{7}$/i .test(value);
}, "");

jQuery.validator.addMethod("amt", function(value, element) {
    return this.optional(element) || /^\d{0,7}(\.\d{0,2})?$/.test(value);
}, "");




 $("#op_topup_form").validate({
        rules: {
            provider_type: {
                required: true,
            },
            api_provider: {
                required: true,
            },
            transaction_type: {
                required: true,
            },
            service: {
                required: true,
            },
            op_name: {
                required: true,
            },
            amt: {
                required: true,
                amt: true
            },
            depositor_bank_name: {
                lettersonly: true,
                maxlength:200
            },
            depositor_acc_no: {
                number: true,
                maxlength:50
            },
            operator_bank_name: {
                lettersonly: true,
                maxlength:200
            },
            operator_acc_no:
            {
                number: true,
                maxlength:50
            },
            transaction_date:
            {
                required: true
            }
        },
        errorPlacement: function (error, element) {
                error.insertAfter(element);
        },
        messages: {
            "provider_type":{ 
                required:"Please select provider"
            },
            "api_provider":{ 
                required:"Please select API provider"
            },
            "transaction_type":{ 
                required:"Please select Transaction Type"
            },
            "services":{ 
                required:"Please select Service"
            },
            "op_name":{ 
                required:"Name should not be empty"
            },
            amt: {
                required:"Amount should not be empty",
                amt: "Please enter valid amount"
            },
           depositor_bank_name: {
                lettersonly:"Alphabets and spaces are allowed for depositor bank name.",
                maxlength: "Bank name should not be more than 200 characters"
            },
            "depositor_acc_no":{ 
                maxlength: "Account name should not be more than 50 characters",
                number: "Only numbers are allowed"
            },
           "operator_bank_name":{ 
                lettersonly:"Alphabets and space is allowed for depositor bank name.",
                maxlength: "Bank name should not be more than 200 characters"
            },
            "operator_acc_no": {
                maxlength: "Account name should not be more than 50 characters",
                number: "Only numbers are allowed"
            },
            "transaction_date":
            {
                required:"Please select Transaction Date",    
            }
        }

    });
  
    $(document).off('keypress', '#agfnm').on('keypress', '#agfnm', function(e) {
        
       var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
        
    });
   
    $(document).off('keypress', '#phono_no').on('keypress', '#phono_no', function(e) {
        
        var unicode=e.charCode? e.charCode : e.keyCode
        if (unicode!=8 && unicode!=9){ //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
                return false //disable key press
        }
        
    });
    $(document).off('keypress', '#pincode').on('keypress', '#pincode', function(e) {
        
        var unicode=e.charCode? e.charCode : e.keyCode
        if (unicode!=8 && unicode!=9){ //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
                return false //disable key press
        }
        
    });
   
 });
