$(document).ready(function() {
    $("#datatable_form").validate();
   
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
    });
    $( "#to_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
    });
    
     $(document).on('click','.passen_details', function(e){
         e.preventDefault();
            var detail = {};
            var div = "";
            var ajax_url ='report/booked_tickets_report/get_passenger_details';
            detail['ticket_id'] = $(this).attr("ref");
            var form = 'agform';
            var htmls = '';
                htmls += '<table class="table table-bordered"><tbody>';
                htmls += '<tr><th>Name</th><th>Age</th><th>Gender</th><th>Adult/Child</th><th>Seat No</th><th>seat Fare</th></tr>'
            get_data(ajax_url, form, div, detail, function (response)
            {
                $(".passengers-data").html('');
                $(".cancel-data").html('');
                if (response.flag == '@#success#@')
                { 
                   $.each(response['passenger'], function(i, item) {
                        var pas_type = (item.psgr_type=="A")?"Adult":"child";
                        htmls += '<tr><td>'+item.psgr_name+'</td>';
                        htmls += '<td>'+item.psgr_age+'</td>';
                        htmls += '<td>'+item.psgr_sex+'</td>';
                        htmls += '<td>'+pas_type+'</td>';
                        htmls += '<td>'+item.seat_no+'</td>';
                        htmls += '<td>'+item.fare_amt+'</td></tr>';
                       
                    });
                        htmls += '<tbody></table>';
                        if(response['cancel_ticket']!='')
                        {
                            var htmls_cancel = '';
                            htmls_cancel += '<h4 class="modal-title">Cancelled Ticket Details</h4>';
                            htmls_cancel += '<table class="table table-bordered"><tbody>';
                            htmls_cancel += '<tr><th>Refund Amt</th><th>Cancel Charges</th><th>cancellation Date</th></tr>';
                            htmls_cancel += '<tr><td>'+response['cancel_ticket']['actual_refund_paid']+'</td>';
                            htmls_cancel += '<td>'+response['cancel_ticket']['cancel_charge']+'</td>';
                            htmls_cancel += '<td>'+response['cancel_ticket']['created_at']+'</td></tr>';
                            htmls_cancel += '<tbody></table>';
                        } 
                       
                }  
                else
                {
                    htmls += '<tr><td>Data Not Available</td>';
                }
                        
                        $(".passengers-data").html(htmls);
                        $(".cancel-data").html(htmls_cancel);
                        $('#bus_stop').modal('show');

            }, '', false)
                
       
    });
  
    $('#from_date').bind("cut copy paste drop",function(e) {
        e.preventDefault();
    });

    $.validator.addMethod("dateFormat",
        function(value, element) {
           return date_validation(value);
          
        },"Please enter valid date.");
   
    if(($("#total_tkt").val())==0)
        {
          $(".prin").css('display','none');
        }
        
        $('#extoexcel').on('click', function(e) 
            {
              op_id   =   $("#op_id option:selected").val();
              ticket_status  =   $("#ticket_status option:selected").val();
              from_date  = $("#from_date").val();
              to_date    = $("#to_date").val();
              window.location.href=BASE_URL+'report/booked_tickets_report/export_excel?op_id='+op_id+"&ticket_status="+ticket_status+"&from_date="+from_date+"&to_date="+to_date;   
            });

        $('#extopdf').on('click', function(e) 
            {
               op_id   =   $("#op_id option:selected").val();
              ticket_status  =   $("#ticket_status option:selected").val();
              from_date  = $("#from_date").val();
              to_date    = $("#to_date").val();
              window.location.href=BASE_URL+'report/booked_tickets_report/export_pdf?op_id='+op_id+"&ticket_status="+ticket_status+"&from_date="+from_date+"&to_date="+to_date;   
            }); 
        
          $(".adve").click(function(){
            $("#"+$(this).attr("data-attr")).show();
            $("#"+$(this).attr("attr-data")).hide();
        });
        
         $('#reset_btn').on('click', function(e) 
            {
                $('#op_id').find('option:first').attr('selected', 'selected');
                $("#ticket_status").find('option:first').attr('selected', 'selected');
                $("#from_date").attr('value','');
                $("#to_date").attr('value','');
            });
        
         $(document).off('click', '.print_file').on('click', '.print_file', function (e) 
            {
                $('.noprint').hide();
                Popup($("#table_show").html(),"Ticket booked Report");
                $('.noprint').show();   
            });
        
        //date validation
         $.validator.addMethod("checkDepend",
            function(value, element) {
            if($("#from_date").val() != "" && $("#to_date").val() != "" )
            {
                var date_diff = date_operations($("#from_date").val(),$("#to_date").val());
                if(date_diff.date_compare)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        },"To date must be greater than from date.");
        
         $('#view_booked_tickets').validate({
        ignore: "",
        rules: {
            
              from_date : {
            },
            to_date : {
                checkDepend : true
            }
        },
        messages: {
            
            from_date: {
              required: "Please Enter from date"
            
            }
        }
    });
        
        
        
        
        
   });
   
   function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }
function Popup(data, pagetitle)
{
    //alert(data);
    var mywindow = window.open('', pagetitle, 'height=400,width=600, menubar=0, scrollbars=1');
    mywindow.document.write('<html><head><title>' + pagetitle + '</title>');
    /*optional stylesheet add */
    mywindow.document.write('<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" />');
    mywindow.document.write('<div><h2>' + pagetitle + '</h2></div>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.print();
    return true;
}

