var oTables = {};
$(document).ready(function() {
    
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    });
    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    });
    var tconfig = {
        processing: true,
        serverSide: true,
        ajax: {
            url: BASE_URL+"admin/cb_report_md/list_cb_report_data",
            type: 'POST',
            data    : function (d) {
                        d.distributor_type = $("#distributor_type option:selected").val();
                        d.user_type = $("#user_type option:selected").val();
                        d.from_date = $("#from_date").val();
                        d.till_date = $("#till_date").val();                     
                      },
        },
        columnDefs: [
            {
                searchable: false
            }
        ],
        order: [[ 1, "desc" ]],
        iDisplayLength: 5,
        aLengthMenu: [[5, 10, 50, -1], [5, 10, 50, "All"]],
        paginate: true,
        paging: true,
        searching: true,        
        aoColumnDefs: [
            {
                targets: [0], visible: true, searchable: false

            },
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },
    };
    oTables = $('#cb_report_data').dataTable(tconfig);
	$('#cb_report_md_view').on('submit', function(e) {
        e.preventDefault();
        oTables.fnDraw();
    });    
  
    $(document).off('click', '#extoexcel').on('click', '#extoexcel', function (e) {
        var from_date=$('#from_date').val();
        var till_date=$('#till_date').val();
        var user_type=$("#user_type option:selected").val();
        var distributor_type=$("#distributor_type option:selected").val();  

        var show_url= BASE_URL+'admin/cb_report_md/export_excel?from_date='+from_date+'&till_date='+till_date+'&user_type='+user_type+'&distributor_type='+distributor_type;

        $("#extoexcel").attr("href", show_url);

    });

});

$(document).ready(function() {

    $('#from_date,#till_date').on('change', function() {
        $('select[name="user_type"]').empty(); 
        $('select[name="user_type"]').append('<option value="">'+'Select Agent'+'</option>');
        var from_date =  $('#from_date').val();
        var till_date =  $('#till_date').val();
      
        $.ajax({
                url:  BASE_URL+"admin/cb_report_md/list_agent_ajax_data?from_date="+from_date+"&till_date"+till_date,                
                type: "GET",
                dataType: "json",
                success:function(data) {
                    $('select[name="user_type"]').empty();
                    $('select[name="user_type"]').append('<option value="">'+'Select Agent'+'</option>');
                    $.each(data, function(key, value) {                        	
                        $('select[name="user_type"]').append('<option value="'+ value.id +'">'+ value.agent_code + ' --- '+value.first_name+' '+value.last_name+'</option>');

                    });
                }
            });            

    });

      $('#from_date,#till_date').on('change', function() {
        $('select[name="distributor_type"]').empty(); 
        $('select[name="distributor_type"]').append('<option value="">'+'Select Distributor'+'</option>');
        var from_date =  $('#from_date').val();
        var till_date =  $('#till_date').val();
      
        $.ajax({
                url:  BASE_URL+"admin/cb_report_md/list_distributor_ajax_data?from_date="+from_date+"&till_date"+till_date,                
                type: "GET",
                dataType: "json",
                success:function(data) {
                    $('select[name="distributor_type"]').empty();
                    $('select[name="distributor_type"]').append('<option value="">'+'Select Distributor'+'</option>');
                    $.each(data, function(key, value) {                        	
                        $('select[name="distributor_type"]').append('<option value="'+ value.id +'">'+ value.agent_type+'</option>');

                    });
                }
            });            

    });

});
