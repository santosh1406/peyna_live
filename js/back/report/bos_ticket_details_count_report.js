$(document).ready(function() {
    $("#datatable_form").validate();
     
    $("#from_date").datepicker({
                numberOfMonths: 1,
                
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $("#till_date").datepicker("option", "minDate", dt);
                }
            });
            $("#till_date").datepicker({
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() - 1);
                    $("#from_date").datepicker("option", "maxDate", dt);
                }
            });
    

   
    if(($("#total_tkt").val())==0)
        {
          $(".prin").css('display','none');
        }
        
        $('#extoexcel').on('click', function(e) 
            { //alert(4444);
            
              from_date  = $("#from_date").val();
              till_date    = $("#till_date").val();
              tran_type = $("#trans_type option:selected").val();
              role_id = $("#user_type").find('option:selected').attr('value');
              
              user_id = $("#select_user").find('option:selected').attr('value');
              
              window.location.href=BASE_URL+'report/report/bos_tkt_details_count_export_excel?from_date='+from_date+"&till_date="+till_date+"&tran_type="+tran_type+"&role_id="+role_id+"&user_id="+user_id;   
            });

        $('#extopdf').on('click', function(e) 
            {
               
              from_date  = $("#from_date").val();
              till_date    = $("#till_date").val();
             
              window.location.href=BASE_URL+'report/booked_tickets_report/export_pdf?from_date='+from_date+"&till_date="+till_date;   
            }); 
        
          $(".adve").click(function(){
            $("#"+$(this).attr("data-attr")).show();
            $("#"+$(this).attr("attr-data")).hide();
        });
        
         
        
        
   });
   
   function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }


