$(document).ready(function() {

   //Jquery validation
   
    $( "#view_operator_report" ).validate({
            rules: {
                op_id: {
                    require_from_group: [1, ".op_class"]
                },
                from_date: {
                    require_from_group: [1, ".op_class"]
                },
                to_date: {
                    require_from_group: [1, ".op_class"]
                }
            }
        });
    
    
    //operator report
    
    $("#datatable_form").validate();
   
    $( "#from_date" ).datepicker({  maxDate: new Date(), dateFormat: "dd-mm-yy" });
    $( "#to_date" ).datepicker({  maxDate: new Date() , dateFormat: "dd-mm-yy"});
    
  
             var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"report/report/operator_list",
            "type": "POST",
          
             data   : function (d) {
                        d.from_date     = $("#from_date").val();
                        d.till_date     = $("#to_date").val();
                        d.op_nm        = $("#op_id option:selected").val();
                        d.rokad_token = rokad_token;                       
                     }
        },
        "columnDefs": [
        {
            "searchable": false
        }
        ],
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
         "scrollX": true,
        "bFilter": false,
       "bPaginate": true,
        "aoColumnDefs": [
        {
            "targets": [0],
           // "visible": true,
            "searchable": false
                
        },
       
        {
            "bSearchable": false, 
            "aTargets": [0]
            },

            {
            "bSortable": false,  
            "aTargets": [10]
            },

            {
            "aTargets": [1]
            }
        ],
          "oLanguage": {
                    "sProcessing": '<img src="'+BASE_URL+'assets/adminlte/back/img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                    },
        fnDrawCallback :function(data)
            {
                if(op_data.fnGetData().length == 0)
                {
                    $('.print').css('display','none');

                }else{
                    $('.print').css('display','inline');     

                }
            }
   
    };
        
       var op_data= $('#op_rept').dataTable(tconfig);
               
         $('#show_rpt').on('click', function(e) 
            {
                op_data.fnDraw();
                e.preventDefault();
            });
    
  
    $('#from_date').bind("cut copy paste drop",function(e) {
        e.preventDefault();
    });

    $.validator.addMethod("dateFormat",
        function(value, element) {
           return date_validation(value);
          
        },"Please enter valid date.");
   
   $('#extoexcel').on('click', function(e) 
            {
              from_date = $("#from_date").val();
              to_date = $("#to_date").val();
              window.location.href=BASE_URL+'report/report/operator_excel?from_date='+from_date+"&to_date="+to_date ;   
            });


    $('#extopdf').on('click', function(e) 
            {
              from_date = $("#from_date").val();
              to_date = $("#to_date").val();
              window.location.href=BASE_URL+'report/report/operator_pdf?from_date='+from_date+"&to_date="+to_date ;   
            });

   
   
   
   });
   
   function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }


function printMaster(elem, pagetitle)
{
    $('.noprint').hide();
    Popup($(elem).html(), pagetitle);
    $('.noprint').show();
}
function Popup(data, pagetitle)
{
    var mywindow = window.open('', pagetitle, 'height=400,width=600, menubar=0, scrollbars=1');
    mywindow.document.write('<html><head><title>' + pagetitle + '</title>');
    /*optional stylesheet add */
    mywindow.document.write('<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" />');
    mywindow.document.write('<div><h2>' + pagetitle + '</h2></div>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.print();
    return true;
}
