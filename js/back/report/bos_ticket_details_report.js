$(document).ready(function() {
    $("#datatable_form").validate();
   
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate':0
    });
    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate':0
    });
    
     $("#from_date").datepicker({
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $("#till_date").datepicker("option", "minDate", dt);
                }
            });
            $("#till_date").datepicker({
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() - 1);
                    $("#from_date").datepicker("option", "maxDate", dt);
                }
            });
    
    
    $('#from_date').bind("cut copy paste drop",function(e) {
        e.preventDefault();
    });

    $.validator.addMethod("dateFormat",
        function(value, element) {
           return date_validation(value);
          
        },"Please enter valid date.");
   
    if(($("#total_tkt").val())==0)
        {
          $(".prin").css('display','none');
        }
        
        $('#extoexcel').on('click', function(e) 
            {
            
              var operator_id  = $("#operator_id").val();
              var trans_type    = $("#trans_type").val();
              var from_date  = $("#from_date").val();
              var till_date    = $("#till_date").val();
              //console.log(from_date);
              if(from_date==''){
                  var from_date = $("#from_date").attr('placeholder');  
              }
              if(till_date==''){
                  var till_date = $("#till_date").attr('placeholder');  
              }
              window.location.href=BASE_URL+'report/report/bos_tkt_details_export_excel?from_date='+from_date+"&till_date="+till_date+"&operator_id="+operator_id+"&trans_type="+trans_type;   
            });

        $('#extopdf').on('click', function(e) 
            {
               
              from_date  = $("#from_date").val();
              till_date    = $("#till_date").val();
              window.location.href=BASE_URL+'report/booked_tickets_report/export_pdf?from_date='+from_date+"&till_date="+till_date;   
            }); 
        
          $(".adve").click(function(){
            $("#"+$(this).attr("data-attr")).show();
            $("#"+$(this).attr("attr-data")).hide();
        });
        
   });
   
   function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }


