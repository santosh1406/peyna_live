$(document).ready(function() {

    $("#datatable_form").validate();
    
    $( "#from_date" ).datepicker({  dateFormat: "dd-mm-yy" });
    $( "#to_date" ).datepicker({ dateFormat: "dd-mm-yy" });
   
    

    make_autocomplete('.auto_list', 'report/report/autocomplete_bus_stop', function(ui) {
          if (ui.item.value == 'Invalid Stop Code')
         {
             ui.item.value = '';
             $('#tbl_route_show').css('display','none');

         }else{
             $('#tbl_route_show').css('display','none');
         }
      });
     
    /*  $('#generate_btn').click(function(){
            
            $( "#view_reservation_report" ).validate({
                rules: {
                    from_date: {
                        require_from_group: [1, ".res_rpt"]
                    },
                    to_date: {
                        require_from_group: [1, ".res_rpt"]
                    },
                    txt_frm_stop_cd: {
                        require_from_group: [1, ".res_rpt"]
                    },
                    txt_to_stop_cd: {
                        require_from_group: [1, ".res_rpt"]
                    }
                }
            });
        });*/
    
       $('#from_date').bind("cut copy paste drop",function(e) {
        e.preventDefault();
    });

    $.validator.addMethod("dateFormat",
        function(value, element) {
           return date_validation(value);
          
        },"Please enter valid date.");
   
   //export data
   
    $('.exlrpt').on('click', function(e) 
            {
             
             from_date      =    $("#from_date").val();
             to_date       =   $("#to_date").val();
             from_stop_cd =   $("#txt_frm_stop_cd").val();
             till_stop_cd  =   $("#txt_to_stop_cd").val();
             transaction_type  =   $("#tran_type").val();
             window.location.href=BASE_URL+'report/report/reservation_export_excel?from_date='+from_date+"&to_date="+to_date+"&from_stop_cd="+from_stop_cd +"&till_stop_cd="+till_stop_cd+"&transaction_type="+ transaction_type;   
            });

        $('.pdfrpt').on('click', function(e) 
            {
              from_date      =    $("#from_date").val();
             to_date       =   $("#to_date").val();
             from_stop_cd =   $("#txt_frm_stop_cd").val();
             till_stop_cd  =   $("#txt_to_stop_cd").val();
             transaction_type  =   $("#tran_type").val();
             
              window.location.href=BASE_URL+'report/report/reservation_export_pdf?from_date='+from_date+"&to_date="+to_date+"&from_stop_cd="+from_stop_cd +"&till_stop_cd="+till_stop_cd+"&transaction_type="+ transaction_type;   
            });
            
        $('#reset_btn').on('click', function(e) 
            {
                //$('#tran_type').find('option:first').attr('selected', 'selected');
                $("#tran_type option:first").prop('selected','selected');
                $("#txt_frm_stop_cd").attr('value','');
                $("#txt_to_stop_cd").val('');
                $("#from_date").attr('value','');
                $("#to_date").attr('value','');
            });
            
   
    if(($("#total_tkt").val())==0)
        {
          $(".prin").css('display','none');
        }
        
        //slide up/down
        
         $(".adve").click(function(){
            $("#"+$(this).attr("data-attr")).show();
            $("#"+$(this).attr("attr-data")).hide();
        });
        
       //validation 
       
            //date validation
         $.validator.addMethod("checkDepend",
            function(value, element) {
            if($("#from_date").val() != "" && $("#to_date").val() != "" )
            {
                var date_diff = date_operations($("#from_date").val(),$("#to_date").val());
                    if(date_diff.date_compare)
                        {
                            return true;
                        }
                       else
                        {
                            return false;                       
                        }
            }
            else
            {
                return true;
            }

        },"To date must be greater than from date.");
        
         $('#view_reservation_report').validate({
        ignore: "",
        rules: {
            
              from_date : {

            },
            to_date : {
                checkDepend : true
            }
        },
        messages: {
            
            from_date: {
              required: "Please Enter from date"
            
            }
        }
    });
               
   });
   
   function showAdvSearch()
{
    $("#showDivAdvSearch").toggle();
}


 $(document).off('click', '.print_file').on('click', '.print_file', function (e) 
        {
            $('.noprint').hide();
            Popup($("#table_show").html(),"Operator Report");
            $('.noprint').show();   
        });
function Popup(data, pagetitle)
{
    var mywindow = window.open('', pagetitle, 'height=400,width=600, menubar=0, scrollbars=1');
    mywindow.document.write('<html><head><title>' + pagetitle + '</title>');
    /*optional stylesheet add */
    mywindow.document.write('<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" />');
    mywindow.document.write('<div><h2>' + pagetitle + '</h2></div>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.print();
    return true;
}
