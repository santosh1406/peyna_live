$(document).ready(function() {

    $("#datatable_form").validate();
   
   // $( "#from_date" ).datepicker({  maxDate: new Date() , dateFormat: "dd-mm-yy"});
  // $( "#to_date" ).datepicker({  maxDate: new Date() , dateFormat: "dd-mm-yy"});
    $('#op_generate_btn').click(function(){
            
        $("#view_operator_report").validate({
            rules: {
                op_id: {
                    require_from_group: [1, ".op_class"]
                },
                from_date: {
                    require_from_group: [1, ".op_class"]
                },
                to_date: {
                    require_from_group: [1, ".op_class"]
                }
            }
        });
        });
    
    $('#from_date').bind("cut copy paste drop",function(e) {
        e.preventDefault();
    });
    
    $("#from_date").datepicker({

        numberOfMonths: 2,
        dateFormat: "dd-mm-yy",
        onSelect: function(selected) {

          $("#to_date").datepicker("option","minDate", selected)

        }
    });

    $("#to_date").datepicker({
        numberOfMonths: 2,
        dateFormat: "dd-mm-yy",

         onSelect: function(selected) {
            $("#from_date").datepicker("option","maxDate", selected)

         }
    });

        
          
    $('#extoexcel').on('click', function(e) 
        {
          bus_service_no   =   $("#bus_service_no option:selected").val();
        
          from_date  = $("#from_date").val();
          to_date    = $("#to_date").val();
          window.location.href=BASE_URL+'report/bus_service_report/export_excel?bus_service_no='+bus_service_no+"&from_date="+from_date+"&to_date="+to_date;   
        });

    $('#extopdf').on('click', function(e) 
        {
           bus_service_no   =   $("#bus_service_no option:selected").val();
          
          from_date  = $("#from_date").val();
          to_date    = $("#to_date").val();
          window.location.href=BASE_URL+'report/bus_service_report/export_excel?bus_service_no='+bus_service_no+"&from_date="+from_date+"&to_date="+to_date;   
        }); 

      $(".adve").click(function(){
        $("#"+$(this).attr("data-attr")).show();
        $("#"+$(this).attr("attr-data")).hide();
        });

     $('#reset_btn').on('click', function(e) 
        {
          // $('#bus_service_no').find('option:first').attr('selected', 'selected');
           $('#bus_service_no').removeAttr('selected').find('option:first').attr('selected', 'selected');
            //$('#bus_service_no').prop('selectedIndex',0);
           // $("#bus_service_no").val($("#bus_service_no").data("default-value"));
         //  $("#bus_service_no").val("");
          
          //  $("#bus_service_no").data("default-value",$("#bus_service_no").val());
            $("#from_date").attr('value','');
            $("#to_date").attr('value','');
        });

   
   
   });
   
   function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }


function printMaster(elem, pagetitle)
{
    $('.noprint').hide();
    Popup($(elem).html(), pagetitle);
    $('.noprint').show();
}
function Popup(data, pagetitle)
{
    var mywindow = window.open('', pagetitle, 'height=400,width=600, menubar=0, scrollbars=1');
    mywindow.document.write('<html><head><title>' + pagetitle + '</title>');
    /*optional stylesheet add */
    mywindow.document.write('<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" />');
    mywindow.document.write('<div><h2>' + pagetitle + '</h2></div>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.print();
    return true;
}
