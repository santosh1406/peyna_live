$(document).ready(function () {
    $("#role_form").validate({
        rules: {
            role_name: {
                required: true     
            },
            home_page: {
                required: true
            }
        }
    });

    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/role/get_list",
            "type": "POST",
            "data": "json",
			data: function(d){
                d.rokad_token = rokad_token;
            }
        },
        // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true,
        "searching": true
    // "aoColumns": [
    //     { "mData": "id" },
    //     { "mData": "amount" },
    //     { "mData": "status" },
    //     { "mData": "action" }
    //  ]        

    };

    var oTable = $('#example').dataTable(tconfig);

    $(document).off('click', '.add_btn').on('click', '.add_btn', function () {
        //$('#role_form').reset_form();
        $('#add_edit_popup').modal('show');
    });


    $(document).off('click', '#submit_btn').on('click', '#submit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
    
        var detail = {};
        var div = "";
        var ajax_url = 'admin/role/save/';
        var form = 'role_form';
        
        if($('#role_form').valid()) 
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    //$('#role_form').reset_form();
                    $('.popup').modal('hide');
                
                    if(response.msg_type == "success")
                    {
                        showLoader(response.msg_type,response.msg);
						setTimeout(location.reload(), 5000);
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        showLoader(response.msg_type,response.msg); 
                    }
                    else
                    {
                        alert(response.msg);
                    }     
                }
            }, '', false);
            oTable.fnDraw();
        }
        
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');

        var detail = {};
        var div = "";
        var ajax_url = 'admin/role/edit_role/';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                role_data = response.role_data;
                $("#role_form #id").val(role_data.id);
                $("#role_form #role_name").val(role_data.role_name);
                $("#role_form #home_page").val(role_data.home_page);


                $('#add_edit_popup').modal('show');

            // $('#submit_btn').removeClass('disable');
            }
            else
            {
                alert(response.msg);
            }
        }, '', false);
    });

    $(document).off('click', '.del_btn').on('click', '.del_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var yn = $(this).attr('data-ref');
        if (yn == "N")
        {
            var postyn = "Y"
        }
        else if (yn == "Y")
        {
            var postyn = "N"
        }
            
        var detail = {};
        var div = "";
        var ajax_url = 'admin/role/delete_role/';
        var form = '';

        detail['id'] = id;
        detail['postyn'] = postyn;
            
        var yesno = confirm("Are you sure you want to delete this data");
        if (yesno)
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                }
                else
                {
                    alert(response.msg);
                }
            }, '', false);
        }
    });
});