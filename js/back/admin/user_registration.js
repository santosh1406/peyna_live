$(document).ready(function () {

    $("#admin_registration").validate({
        rules: {
            first_name: {
                required: true,
                lettersonly: true
            },
            last_name: {
                required: true,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
               email_exists: true
            },
            role_id: {
                required: true,
            },
            mobile_no: {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                valid_mobile_no: true,
                mobileno_exists: true,
                mobileno_start_with:true
            },
            dob: {
                required: true,
            },
            address1: {
                required: true,
            },
            agent_state: {
                required: true,
            },
            agent_city: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6,
                pincode_start_with: true,
            },
        },
        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "agent_state") {
                error.appendTo(element.parent("div") );
            } else if(element.attr("name") === "agent_city") {
                error.appendTo(element.parent("div") );
            } else {
                error.insertAfter(element);
            }
                
        },
        messages: {
                    
            first_name: {
                required: "Please Enter First Name",
                lettersonly: "Please Enter Valid First Name",
            },
            last_name: {
                required: "Please Enter Last Name",
                lettersonly: "Please Enter Valid Last Name",
            },
            email: {
                required: "Please Enter Email Address",
                email_exists:"Email id already registered with us"
            },
            dob: {
                required: "Please Select The DOB (DD-MM-YYYY)",
            },
            mobile_no: {
                required: "Please Enter Mobile No",
                number: "Please Enter Valid Mobile No",
                minlength: "Please Enter 10 Digit Mobile No",
                maxlength: "Please Enter 10 Digit Mobile No",
                valid_mobile_no: "Please Enter Valid Mobile No",
                mobileno_exists: 'Mobile No Already Registered with us',
                mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
            },
            role_id: {
                required: "Please Select Role",
            },
            address1: {
                required: "Please Enter The  Address1",
            },
            agent_state: {
                required: "Please Select State Name",
            },
            agent_city: {
                required: "Please Select City Name",
            },
            pincode: {
                required: "Please Enter The Pin Code",
                number: "Please Enter The Valid Pin Code",
                minlength: "Please enter 6 digit Pin Code",
                maxlength: "Please enter 6 digit Pin Code",
                pincode_start_with: "Pin Code should start between 1 and 9"
            },
        }
    });

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "");

    $.validator.addMethod("valid_mobile_no", function (value, element) {
        return (value == "0000000000" || value == "9999999999") ? false : true;
    }, 'Please enter valid mobile number');

    $.validator.addMethod("mobileno_start_with", function (value, element) {
        var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
        if (value.length > 0) {
            if (regex.test(value)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, 'Mobile No. should start with 7 or 8 or 9 ');

    $.validator.addMethod("pincode_start_with", function (value, element) {
        var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
        if (value.length > 0) {
            if (regex.test(value)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, 'PIN Code. should start with 1-9 ');

    /******** DATE OF BIRTH DATEPICKER ********/
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    var endDate = dt;
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        showMonthAfterYear: true,
        yearRange: "-100:-18",
        dateFormat: "dd-mm-yy",
        maxDate: endDate
    });
    $('#dob').keydown(function (event) {
        event.preventDefault();    
    });
       
    /******** mobile validation start here ********/
     $.validator.addMethod("mobileno_exists", function (value, element) {
        var flag;

        $.ajax({
            url: BASE_URL + 'admin/registration/is_mobile_no_registered',
            data: {mobile_no: value,rokad_token : rokad_token},
            async: false,
            type: 'POST',
            success: function (r) {
                flag = (r === 'true') ? false : true;
            }
        });

            return flag;
    }, 'Mobile no. already exsist with us');
    /******** mobile validation start here ********/

    /******** email validation start here ********/
     $.validator.addMethod("email_exists", function (value, element) {
        var flag;

        $.ajax({
            url: BASE_URL + 'admin/registration/is_email_registered',
            data: {email: value,rokad_token : rokad_token},
            async: false,
            type: 'POST',
            success: function (r) {
                flag = (r === 'true') ? false : true;
            }
        });

        return flag;
    }, 'Email id already exsist with us');
    /******** email validation start here ********/
    
    /*************STATE-CITY DROPDOWN **********/
    $(document).off('change', '.state').on('change', '.state', function (e) {
        $("#agent_city").html("<option value=''>Please wait..</option>");

        var detail = {};
        var div = '';
        var str = "";
        var form = '';
        var ajax_url = 'report/report/get_statewise_city';

        detail['state_id'] = $("#agent_state").find('option:selected').attr('value');

        if (detail['state_id'] != '') {
            $("#agent_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function (response) {
                if (response.city.length != 0) {
                    $("#agent_city").html("<option value=''>Select City</option>");
                    $.each(response.city, function (i, value) {
                        $("#agent_city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                        $(".chosen_select").trigger("chosen:updated");
                    });
                } else {
                    $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                }
            }, '', false);
        } else {
            $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
        }
    });

});