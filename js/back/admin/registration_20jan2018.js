$(document).ready(function () {

    $("#registration_form").validate({
         //ignore: "none:not(select)",
        rules: {
            firm_name: {
                required: true,
                letterswithspecialchar: true
            },
             service_name: {
                required: true
            },
            first_name: {
                required: true,
                lettersonly: true
            },
            last_name: {
                required: true,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
               email_exists: true
            },
            company_gst_no: {
                required: true,
                valid_gst_no: true
            },
            dob: {
                required: true,
            },
            adharcard: {
                required: true,
                digits: true,
                minlength: 12,
                maxlength: 12,
                valid_adharcard_no : true
            },
            pancard: {
                required: true,
                minlength: 10,
                maxlength: 10,
                valid_pancard_no : true
            },
            mobile_no: {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                valid_mobile_no: true,
                mobileno_exists: true,
                mobileno_start_with:true
            },
            address1: {
                required: true,
            },
            agent_state: {
                required: true,
            },
            agent_city: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6,
                pincode_start_with: true,
            },
            cmp:{
                required: true,
            },
            md:{
                required: true,
            },
            ad:{
                required: true,
            },
            distributor:{
                required: true,
            },
            bType:{
                required:true,
            },
            scheme:{
                required:true,
            },
            sellerpoint:{
                required:true,
            },
            sellerdesc:{
                required:true,
            },
            /*topUpLimit:{
                required:true,
            },
            hhmLimit:{
                required:true,
            },*/
            depot_code:{
                required:true,
            },
            securityDep:{
                required:true,
            },
            accno:{
                required:true,
                digits: true,
                maxlength: 12,
            },
            accname:{
                required:true,
                lettersonly: true,
            },
            brname:{
                required:true,
                lettersonly: true,
            },
            ifsc:{
                required:true,
                lfsccode:true
            },
            bname:{
                required:true,
                lettersonly: true,
            }
        },
        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "agent_state")
            {
                error.appendTo(element.parent("div") );
            }
            else if(element.attr("name") === "agent_city")
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "cmp") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "md") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "ad") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "distributor") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "bType") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "scheme") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "sellerpoint") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "service_name") 
            {
                error.appendTo(element.parent("div") );
            }
            else if (element.attr("name") === "depot_code") 
            {
                error.appendTo(element.parent("div") );
            }
            else {
                error.insertAfter(element);
            }
                
        },
        messages:
                {
                    firm_name: {
                        required: "Please Enter Firm/Company Name",
                        letterswithspecialchar: "Please Enter Valid Firm/Company Name",
                    },
                    first_name: {
                        required: "Please Enter First Name",
                        lettersonly: "Please Enter Valid First Name",
                    },
                    last_name: {
                        required: "Please Enter Last Name",
                        lettersonly: "Please Enter Valid Last Name",
                    },
                    email: {
                        required: "Please Enter Email Address",
                        email_exists:"Email id already registered with us"
                    },
                    company_gst_no: {
                        required: "Please Enter Company GST No",
                        valid_gst_no : "Please Enter Valid Company GST No"
                    },
                    adharcard: {
                        required: "Please Enter Aadhaar Card Number",
                        digits:"Please enter 12 digit Aadhaar card number",
                        minlength: "Please enter 12 digit Aadhaar card number",
                        maxlength: "Please enter 12 digit Aadhaar card number",
                        valid_adharcard_no: "Please Enter Valid Aadhaar Card Number."
                    },                   
                    pancard: {
                        required: "Please Enter PAN/TAN Card Number",
                        minlength: "Please enter 10 digit pan card number",
                        maxlength: "Please enter 10 digit pan card number",
                        valid_pancard_no: "Please Enter Valid PAN/TAN Card Number."
                    },      
                    dob: {
                        required: "Please Select The DOB (DD-MM-YYYY)",
                    },
                    mobile_no: {
                        required: "Please Enter Mobile No",
                        number: "Please Enter Valid Mobile No",
                        minlength: "Please Enter 10 Digit Mobile No",
                        maxlength: "Please Enter 10 Digit Mobile No",
                        valid_mobile_no: "Please Enter Valid Mobile No",
                        mobileno_exists: 'Mobile No Already Registered with us',
                        mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                    },
                    address1: {
                        required: "Please Enter The  Address1",
                    },
                    agent_state: {
                        required: "Please Select State Name",
                    },
                    agent_city: {
                        required: "Please Select City Name",
                    },
                    pincode: {
                        required: "Please Enter The Pin Code",
                        number: "Please Enter The Valid Pin Code",
                        minlength: "Please enter 6 digit Pin Code",
                        maxlength: "Please enter 6 digit Pin Code",
                        pincode_start_with: "Pin Code should start between 1 and 9"
                    },
                    cmp:{
                        required: "Please Select the Company",
                    },
                    md:{
                        required: "Please Select the Master Distributor",
                    },
                    ad:{
                        required: "Please Select the Area Distributor",
                    },
                    distributor:{
                        required: "Please Select the Distributor",
                    },
                    service_name:{
                        required: "Please Select the service_name",
                    },
                    bType:{
                        required: "Please Select the Buisness type",
                    },
                    scheme:{
                        required: "Please Select the scheme type",
                    },
                    sellerpoint:{
                        required: "Please Select the sellerpoint",
                    },
                    sellerdesc:{
                        required: "Please Enter The sellerpoint description",
                    },
                    /*topUpLimit:{
                        required: "Please Enter The topUpLimit",
                    },
                    hhmLimit:{
                        required: "Please Enter The hhmLimit",
                    },*/
                    depot_code:{
                        required: "Please Select the depot_code",
                    },
                    securityDep:{
                        required: "Please Enter The security Deposit",
                    },
                    accname:{
                        required: "Please Enter The account name",
                    },
                    accno:{
                        required: "Please Enter The account number",
                    },
                    brname:{
                        required: "Please Enter The branch name",
                    },
                    bname:{
                        required: "Please Enter bank name",
                    },
                    ifsc:{
                        lfsccode: "Please enter valid IFSC CODE ",
                    }
                }

                
    });

  /// fetch the master distributor based on company ///////
    if(cmp_on_change_flag == true) {
        $(document).off('change', '.cmp').on('change', '.cmp', function (e) {
            if(show_retailor_flag == true) {
                $('.retailer_show').show();
            } else { 
                $('.retailer_show').hide();
            }
            $("#md").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_md_data';

            detail['id'] = $("#cmp").find('option:selected').attr('value');

            if (detail['id'] != '') {
                $("#md").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.md.length != 0) {
                        $("#md").html("<option value=''>Select Master Distributor</option>");
                        $.each(response.md, function (i, value) {
                            $("#md").append("<option value=" + value.id + ">" + value.display_name + ": " + value.mobile_no +"</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                        }
                    else {
                        $("#md").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#md").html("<option value=''>Select Area Distributor</option>").trigger('chosen:updated');
            }
        });
    }    
    
    /// fetch the area distributor based on md ///////
    if(md_on_change_flag == true) {
        $(document).off('change', '.md').on('change', '.md', function (e) {
            if(show_retailor_flag == true) {
                $('.retailer_show').show();
            } else { 
                $('.retailer_show').hide();
            }
           
            $("#ad").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_ad_data';

            detail['id'] = $("#md").find('option:selected').attr('value');

            if (detail['id'] != '') {
                $("#ad").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.ad.length != 0) {
                        $("#ad").html("<option value=''>Select Area Distributor</option>");
                        $.each(response.ad, function (i, value) {
                            $("#ad").append("<option value=" + value.id + ">" + value.display_name + ": " + value.mobile_no +"</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                        }
                    else {
                        $("#ad").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#ad").html("<option value=''>Select Area Distributor</option>").trigger('chosen:updated');
            }
        });
    }
        /// fetch the distributor based on ad ///////
    if (ad_on_change_flag == true) {
        $(document).off('change', '.ad').on('change', '.ad', function (e) {
            if(show_retailor_flag == true) {
                $('.retailer_show').show();
            } else { 
                $('.retailer_show').hide();
            }
          
            $("#distributor").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_distributor';

            detail['id'] = $("#ad").find('option:selected').attr('value');

            if (detail['id'] != '') {
                $("#distributor").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.distributor.length != 0) {
                        $("#distributor").html("<option value=''>Select Distributor</option>");
                        $.each(response.distributor, function (i, value) {
                            $("#distributor").append("<option value=" + value.id + ">" + value.display_name +  ": " + value.mobile_no +"</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else {
                        $("#distributor").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#distributor").html("<option value=''>Select Distributor</option>").trigger('chosen:updated');
            }
        });
    }


    jQuery.validator.addMethod("lettersonly", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "");

    jQuery.validator.addMethod("letterswithdigit", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
    }, "");

    jQuery.validator.addMethod("lfsccode", function(value, element) {
        return this.optional(element) || /^[a-z]{4}\d{7}$/i.test(value);
    }, "");

    $.validator.addMethod("letterswithspecialchar", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z.,@#&_\-\s]+$/i.test(value);
    }, "");
    
    $.validator.addMethod("valid_mobile_no", function (value, element) {
        return (value == "0000000000" || value == "9999999999") ? false : true;
    }, 'Please enter valid mobile number');
    $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');

    $.validator.addMethod("pincode_start_with", function (value, element) {
            var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'PIN Code. should start with 1-9 ');
    /******** DATE OF BIRTH DATEPICKER ********/
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    var endDate = dt;
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        showMonthAfterYear: true,
        yearRange: "-100:-18",
        dateFormat: "dd-mm-yy",
        maxDate: endDate
    });
    $('#dob').keydown(function (event) {
    event.preventDefault();    
});
       
    /******** aadhar card Validation start here ********/
    $.validator.addMethod("valid_adharcard_no", function (value, element) {
            var regex = new RegExp("^[0-9]{12}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid pancard number');
     /******** aadhar card Validation end here ********/    
    /******** Pancard Validation start here ********/
    $.validator.addMethod("valid_pancard_no", function (value, element) {
            var regex = new RegExp("^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid pancard number');
     /******** Pancard Validation end here ********/
     /******** mobile validation start here ********/
     $.validator.addMethod("mobileno_exists", function (value, element) {
            var flag;

            $.ajax({
                url: BASE_URL + 'admin/registration/is_mobile_no_registered',
                data: {mobile_no: value,rokad_token : rokad_token},
                async: false,
                type: 'POST',
                success: function (r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Mobile no. already exsist with us');
    /******** mobile validation start here ********/
    /******** email validation start here ********/
     $.validator.addMethod("email_exists", function (value, element) {
            var flag;

            $.ajax({
                url: BASE_URL + 'admin/registration/is_email_registered',
                data: {email: value,rokad_token : rokad_token},
                async: false,
                type: 'POST',
                success: function (r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Email id already exsist with us');
    /******** email validation start here ********/
    /******** GST number Validation start here ********/
    $.validator.addMethod("valid_gst_no", function (value, element) {
            var regex = new RegExp("^[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[0-9]{1}Z[0-9]{1}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid GST number');
     /******** GST number Validation end here ********/
    /*************STATE-CITY DROPDOWN **********/
    $(document).off('change', '.state').on('change', '.state', function (e)
    {
        $("#agent_city").html("<option value=''>Please wait..</option>");

        var detail = {};
        var div = '';
        var str = "";
        var form = '';
        var ajax_url = 'report/report/get_statewise_city';

        detail['state_id'] = $("#agent_state").find('option:selected').attr('value');

        if (detail['state_id'] != '')
        {
            $("#agent_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.city.length != 0)
                {
                    $("#agent_city").html("<option value=''>Select City</option>");
                    $.each(response.city, function (i, value)
                    {
                        $("#agent_city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                        $(".chosen_select").trigger("chosen:updated");
                    });
                }
                else
                {
                    $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                }
            }, '', false);
        }
        else
        {
            $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
        }
    });
});