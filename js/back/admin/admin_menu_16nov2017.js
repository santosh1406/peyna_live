$(document).ready(function() {
    $.validator.setDefaults({
        ignore: ":hidden:not(select)"
    });
    
    $("#menu_form").validate({
        rules: {
            parent: {
                required: true     
            },
            menu_name: {
                required: true
            },
            menu_link: {
                required: true
            }
        }
    });

    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/menu/get_list",
            "type": "POST",
            "data": "json",
            data: function(d){
                d.rokad_token = rokad_token;
            }
        },
        // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true,
        "searching": true
    // "aoColumns": [
    //     { "mData": "id" },
    //     { "mData": "amount" },
    //     { "mData": "status" },
    //     { "mData": "action" }
    //  ]        

    };

    var oTable = $('#menu_display_table').dataTable(tconfig);
        
    $(document).off('click', '.add_btn').on('click', '.add_btn', function () {
    	//$('#menu_form').reset_form();
        $('#add_edit_popup').modal('show');
    });


    $(document).off('click', '#submit_btn').on('click', '#submit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');

        var detail = {};
        var div = "";
        var ajax_url = 'admin/menu/savemenu';
        var form = 'menu_form';
        
        if($('#menu_form').valid()) 
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    //$('#menu_form').reset_form();
                    $('.popup').modal('hide');
                    if(response.msg_type == "success")
                    {
                        showLoader(response.msg_type,response.msg);
						setTimeout(location.reload(), 5000);
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        showLoader(response.msg_type,response.msg); 
                    }
                    else
                    {
                        alert(response.msg);
                    }     
                }
            }, '', false);
            oTable.fnDraw();
        }
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
            
        var detail = {};
        var div = "";
        var ajax_url = 'admin/menu/edit_menu';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                menu_data = response.menu_data;
                
                $("#menu_form #id").val(menu_data.id);
                $("#menu_form #menu_parent").val(menu_data.parent);
                //to update chosen select
                $('select').trigger("chosen:updated");
                $("#menu_form #menu_name").val(menu_data.display_name);
                $("#menu_form #menu_link").val(menu_data.link);
                $("#menu_form #menu_class").val(menu_data.class);
                $('#add_edit_popup').modal('show');

            // $('#submit_btn').removeClass('disable');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });

    $(document).off('click', '.del_btn').on('click', '.del_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var yn = $(this).attr('data-ref');
        if (yn == "N")
        {
            var postyn = "Y"
        }
        else if (yn == "Y")
        {
            var postyn = "N"
        }
            
        var detail = {};
        var div = "";
        var ajax_url = 'admin/menu/delete_menu';
        var form = '';

        detail['id'] = id;
        detail['postyn'] = postyn;
            
        var yesno = confirm("Are you sure you want to delete this data");
        if (yesno)
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                }
                else
                {
                    alert(response.msg);
                }
            }, '', false);
        }
    });

       
});