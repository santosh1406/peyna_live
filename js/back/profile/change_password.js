$(document).ready(function () {
	document.getElementById("change_password").reset();
    $("#change_password").validate({
        rules: {            
            old_password: {
                required: true                             
            },
			new_password: {
                required: true,				            
                minlength: 8,
                maxlength: 15
            },
			cnf_password: {
                required: true,				            
                minlength: 8,
                maxlength: 15,
				equalTo: "#new_password"
            }
        },
		messages:{
			old_password: {
				required: "Please enter old password",				
				minlength: "Please enter valid old password",
				maxlength: "Please enter valid old password"
			},
			new_password: {
				required: "Please enter new password",				
				minlength: "Please enter password having minimum length 8",
				maxlength: "Please enter password having minimum length 15"
			},
			cnf_password: {
				required: "Please enter confirm password",				
				minlength: "Please enter password having minimum length 8",
				maxlength: "Please enter password having minimum length 15",
				equalTo: "Confirm password should match new password"
			}
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {
					document.getElementById("change_password").reset();
					if(response.msg_type == 'success')
					{						
						if(back == 'Y')
						{
							showLoader(response.msg_type, response.msg);
						}
						else
						{
							showNotification(response.msg_type, response.msg);
						}
						location.reload();
					}
					else if(response.msg_type == 'error')
					{							
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });	 
});