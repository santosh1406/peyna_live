$(document).ready(function () {
	document.getElementById("change_email").reset();
    $("#change_email").validate({
        rules: {            
            inp_email: {
                required: true,
                email: true
             }			
        },
		messages:{
			inp_email: {
				required: "Please enter primary email",				
				email: "Please enter valid email",				
			}
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {										
					if(response.msg_type == 'success')
					{
						document.getElementById("change_email").reset();
						if(back == 'Y')
						{
							showLoader(response.msg_type, response.msg);
						}
						else
						{
							showNotification(response.msg_type, response.msg);
						}
						location.reload();
					}
					else if(response.msg_type == 'error')
					{							
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });	 
});