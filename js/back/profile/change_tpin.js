$(document).ready(function () {
	document.getElementById("change_tpin").reset();
    $("#change_tpin").validate({
        rules: {            
            old_tpin: {
                required: true,
				digits: true,
                minlength: 4,
				maxlength: 4                
            },
			new_tpin: {
                required: true,
				digits: true,
                minlength: 4,
                maxlength: 4
            },
			cnf_tpin: {
                required: true,
				digits: true,
                minlength: 4,
                maxlength: 4,
				equalTo: "#new_tpin"
            }
        },
		messages:{
			old_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter valid T-PIN",
				maxlength: "Please enter valid T-PIN"
			},
			new_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter T-PIN having exact length 4",
				maxlength: "Please enter T-PIN having exact length 4"
			},
			cnf_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter T-PIN having exact length 4",
				maxlength: "Please enter T-PIN having exact length 4",
				equalTo: "Confirm T-PIN should match new T-PIN"
			}
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {
					console.log(response);
					document.getElementById("change_tpin").reset();
					if(response.msg_type == 'success')
					{
						$('#change_tpin').hide();						
						$('#change_tpin2').show();
						$('#change_tpin2').find('input[name="username_hidden"]').val(response.username_hidden);
						$('#change_tpin2').find('input[name="tpin_hidden"]').val(response.tpin_hidden);
					}
					else if(response.msg_type == 'error')
					{					
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });

	$("#change_tpin2").validate({
        rules: {            
            new_otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },            
        },
		messages:{
			new_otp: {
				required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
			}                    
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {					
					if(response.msg_type == 'success')
					{					
						document.getElementById("change_tpin2").reset();
						if(back == 'Y')
						{
							showLoader(response.msg_type, response.msg);
							window.location = BASE_URL;
						}
						else
						{
							showNotification(response.msg_type, response.msg);
							window.location = BASE_URL;
						}					
					}
					else if(response.msg_type == 'error')
					{					
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });
});

$(document).on("click", ".back_btn", function() {
	var form = $(this).closest("form");	
	console.log(form);

	if(form.attr('id') == 'change_tpin2')
	{		
		document.getElementById("change_tpin2").reset();		
		$('#change_tpin2').hide();						
		$('#change_tpin').show();
	}	
});