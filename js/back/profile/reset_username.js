$(document).ready(function () {
	document.getElementById("reset_username_step1").reset();
	document.getElementById("reset_username_step2").reset();
	document.getElementById("reset_username_step3").reset();
    $("#reset_username_step1").validate({
        rules: {            
            username: {
                required: true,
				digits: true,                
                minlength: 10,
                maxlength: 10
            },            
        },
		messages:{
			username: {
				required: "Please enter mobile number",
				digits: "Please enter valid mobile number",
				minlength: "Please enter 10 digit mobile number",
				maxlength: "Please enter 10 digit mobile number"
			}                    
		},
		submitHandler: function(form) {			
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();			
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {
					if(response.msg_type == 'success')
					{								
						$('#reset_username_step1').hide();
						$('#reset_username_step2').show();
						$('#reset_username_step2').find('#old_username').html(response.username);
						$('#reset_username_step2').find('input[name="username_hidden"]').val(response.username_hidden);
					}
					else if(response.msg_type == 'error')
					{
						console.log('vdv');
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });
	 
	 $("#reset_username_step2").validate({
        rules: {             
			otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },
			new_username: {
                required: true,
				digits: true,                
                minlength: 10,
                maxlength: 10
            }			
        },
		messages:{			
			otp: {
                required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
            },
			new_username: {
                required: "Please enter new mobile number",
				digits: "Please enter valid new mobile number",
				minlength: "Please enter 10 digit new mobile number",
				maxlength: "Please enter 10 digit new mobile number"
            }			
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {					
					if(response.msg_type == 'success')
					{						
						$('#reset_username_step2').hide();						
						$('#reset_username_step3').show();
						$('#reset_username_step3').find('input[name="username_hidden"]').val(response.username_hidden);
					}
					else if(response.msg_type == 'error')
					{					
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}					
				}            
			});
		}
     });
	 
	 $("#reset_username_step3").validate({
        rules: {            
            new_otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },            
        },
		messages:{
			new_otp: {
				required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
			}                    
		},
		submitHandler: function(form) {
			$(form).find('.error_box').find(".msg").html("");
			$(form).find('.success_box').find(".msg").html("");
			$(form).find('.error_box').hide();
			$(form).find('.success_box').hide();
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				dataType:'json',
				success: function(response) {					
					if(response.msg_type == 'success')
					{					
						document.getElementById("reset_username_step3").reset();
						if(back == 'Y')
						{
							showLoader(response.msg_type, response.msg);
							window.location = BASE_URL;
						}
						else
						{
							showNotification(response.msg_type, response.msg);
							window.location = BASE_URL;
						}									
					}
					else if(response.msg_type == 'error')
					{					
						$(form).find('.error_box').find(".msg").html(response.msg);
						$(form).find('.error_box').show();
					}
				}            
			});
		}
     });
});


$(document).on("click", ".resend_otp", function() {
	var form = $(this).closest("form");	

	form.find('.error_box').find(".msg").html("");
	form.find('.success_box').find(".msg").html("");
	form.find('.error_box').hide();
	form.find('.success_box').hide();
			
	var mno = form.find('input[name="username_hidden"]').val();

	if(mno == "")
	return false;	
	
	resend_otp(form, mno);	
});

function resend_otp(form, mno)
{
	$.ajax({
		url: BASE_URL + "admin/users/resend_otp",
		method: "POST",
		data: { mno : mno  },
		success: function(response) {
			var response = jQuery.parseJSON( response );
			if(response.msg_type == 'success')
			{				
				showNotification(response.msg_type, "OTP sent successfully");			
			}
			else if(response.msg_type == 'error')
			{						
				form.find('.error_box').find(".msg").html(response.msg);
				form.find('.error_box').show();		
			}
		}            
	});
}

$(document).on("click", ".back_btn", function() {
	var form = $(this).closest("form");	
	console.log(form);

	if(form.attr('id') == 'reset_username_step2')
	{		
		document.getElementById("reset_username_step2").reset();
		document.getElementById("reset_username_step3").reset();
		$('#reset_username_step2').hide();						
		$('#reset_username_step1').show();
	}
	else if(form.attr('id') == 'reset_username_step3')
	{		
		document.getElementById("reset_username_step3").reset();
		$('#reset_username_step3').hide();						
		$('#reset_username_step2').show();
	}
});
