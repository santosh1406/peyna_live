$(document).ready(function () {
	document.getElementById("change_password_inp").reset();
	document.getElementById("change_password").reset();
	
	document.getElementById("change_tpin_inp").reset();
	document.getElementById("change_tpin").reset();
	
	document.getElementById("tpin_otp_inp").reset();
	document.getElementById("tpin_otp").reset();
	
	document.getElementById("change_email_inp").reset();
	document.getElementById("change_email").reset();
	
    $("#change_password_inp").validate({
        rules: {            
            old_password: {
                required: true                             
            },
			new_password: {
                required: true,				            
                minlength: 8,
                maxlength: 15				
            },
			cnf_password: {
                required: true,				            
                minlength: 8,
                maxlength: 15,
				equalTo: "#new_password"
            }
        },
		messages:{
			old_password: {
				required: "Please enter old password",				
				minlength: "Please enter valid old password",
				maxlength: "Please enter valid old password"
			},
			new_password: {
				required: "Please enter new password",				
				minlength: "Your password should be at least 8 characters that include upper and lower case characters, numbers, and symbols.",
				maxlength: "Your password should be at least 8 characters that include upper and lower case characters, numbers, and symbols."
			},
			cnf_password: {
				required: "Please enter confirm password",				
				minlength: "Confirm password should match new password",
				maxlength: "Confirm password should match new password",
				equalTo: "Confirm password should match new password"
			}
		}		
     });	 
	 
	$(document).on("click","#change_password_submit",function() {		
		if($('#change_password_inp').valid())
		{
			disable_button('change_password_submit');
			
			var inp_form = $("#change_password_inp");
			var main_form = $("#change_password");				
			
			main_form.find("#old_password_hidden").val( btoa(inp_form.find("#old_password").val()) );
			main_form.find("#new_password_hidden").val( btoa(inp_form.find("#new_password").val()) );
			main_form.find("#cnf_password_hidden").val( btoa(inp_form.find("#cnf_password").val()) );
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {
					document.getElementById("change_password_inp").reset();
					document.getElementById("change_password").reset();
					
					if(response.msg_type == 'success')
					{						
						if(back == 'Y')
						{							
							showLoader(response.msg_type, response.msg);
						}
						else
						{
							showNotification(response.msg_type, response.msg);
						}						
						setTimeout(location.reload(), 5000);
					}
					else if(response.msg_type == 'error')
					{							
						swal("Oops...", response.msg, "error");
						enable_button('change_password_submit');
					}
				}            
			});			
		}
	});	 
		
	/* $.validator.addMethod("strong_password", function (value, element) {
		var flag;
		$.ajax({
			url: BASE_URL + 'admin/users/is_strong_password',
			data: {password_inp: value},
			async: false,
			type: 'POST',
			success: function (r) {
			console.log(r);
			flag = (r === 'true') ? true : false;
			}
		});
		return flag;
	}, 'Password should be of minimum 8 character with atlest 1 capital, 1 small, 1 special character [!@#%&._$*], and 1 numeric character.'); */
	
    $("#change_tpin_inp").validate({
        rules: {            
            old_tpin: {
                required: true,
				digits: true,
                minlength: 4,
				maxlength: 4                
            },
			new_tpin: {
                required: true,
				digits: true,
                minlength: 4,
                maxlength: 4
            },
			cnf_tpin: {
                required: true,
				digits: true,
                minlength: 4,
                maxlength: 4,
				equalTo: "#new_tpin"
            }
        },
		messages:{
			old_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter valid T-PIN",
				maxlength: "Please enter valid T-PIN"
			},
			new_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter T-PIN having exact length 4",
				maxlength: "Please enter T-PIN having exact length 4"
			},
			cnf_tpin: {
				required: "Please enter old T-PIN",
				digits: "Please enter valid T-PIN",	
				minlength: "Please enter T-PIN having exact length 4",
				maxlength: "Please enter T-PIN having exact length 4",
				equalTo: "Confirm T-PIN should match new T-PIN"
			}
		}		
     });
	 
	 $(document).on("click","#change_tpin_submit",function() {		
		if($('#change_tpin_inp').valid())
		{
			disable_button('change_tpin_submit');
			
			var inp_form = $("#change_tpin_inp");
			var main_form = $("#change_tpin");				
			
			main_form.find("#old_tpin_hidden").val( btoa(inp_form.find("#old_tpin").val()) );
			main_form.find("#new_tpin_hidden").val( btoa(inp_form.find("#new_tpin").val()) );
			main_form.find("#cnf_tpin_hidden").val( btoa(inp_form.find("#cnf_tpin").val()) );
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {
					document.getElementById("change_tpin_inp").reset();
					document.getElementById("change_tpin").reset();
					if(response.msg_type == 'success')
					{						
						$('#change_tpin_inp').hide();						
						$('#tpin_otp_inp').show();
						enable_button('change_tpin_submit');
					}
					else if(response.msg_type == 'error')
					{							
						swal("Oops...", response.msg, "error");
						enable_button('change_tpin_submit');
					}
				}            
			});			
		}
	});
	
	$("#tpin_otp_inp").validate({
        rules: {            
            new_otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },            
        },
		messages:{
			new_otp: {
				required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
			}                    
		}		
     });
	 
	 $(document).on("click","#tpin_otp_submit",function() {		
		if($('#tpin_otp_inp').valid())
		{
			disable_button('tpin_otp_submit');
			
			var inp_form = $("#tpin_otp_inp");
			var main_form = $("#tpin_otp");				
			
			main_form.find("#new_otp_hidden").val( btoa(inp_form.find("#new_otp").val()) );			
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {	
					document.getElementById("tpin_otp_inp").reset();
					document.getElementById("tpin_otp").reset();
					if(response.msg_type == 'success')
					{	
						if(back == 'Y')
						{
							showLoader(response.msg_type, response.msg);						
						}
						else
						{
							showNotification(response.msg_type, response.msg);
						}
						window.setTimeout(function(){
						window.location = BASE_URL;
						}, 5000);
					}
					else if(response.msg_type == 'error')
					{							
						swal("Oops...", response.msg, "error");
						enable_button('tpin_otp_submit');
					}
				}            
			});			
		}
	}); 
	 
	 $("#change_email_inp").validate({
        rules: {            
            inp_email: {
                required: true                
             }			
        },
		messages:{
			inp_email: {
				required: "Please enter primary email",				
				email: "Please enter valid email",				
			}
		}		
     });	 
	 
	 $(document).on("click","#change_email_submit",function() {		
		if($('#change_email_inp').valid())
		{
			disable_button('change_email_submit');
			
			var inp_form = $("#change_email_inp");
			var main_form = $("#change_email");				
			
			main_form.find("#inp_email_hidden").val( btoa(inp_form.find("#inp_email").val()) );			
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {	
					document.getElementById("change_email_inp").reset();
					document.getElementById("change_email").reset();
					if(response.msg_type == 'success')
					{						
						if(back == 'Y')
						{							
							showLoader(response.msg_type, response.msg);
						}
						else
						{
							showNotification(response.msg_type, response.msg);
						}						
						setTimeout(location.reload(), 5000);
					}
					else if(response.msg_type == 'error')
					{							
						swal("Oops...", response.msg, "error");
						enable_button('change_email_submit');
					}
				}            
			});			
		}
	}); 
});

$(document).on("click", ".back_btn", function() {
	var form = $(this).closest("form");
	var formid = form.attr('id');
	console.log(formid);
	if(formid == "tpin_otp_inp")
	{
		document.getElementById("change_tpin_inp").reset();
		document.getElementById("change_tpin").reset();

		document.getElementById("tpin_otp_inp").reset();
		document.getElementById("tpin_otp").reset();
		
		form.hide();
		$("#change_tpin_inp").show();
	}	
});

$(document).on("click", ".resend_otp", function() {
	var form = $(this).closest("form");		
	$.ajax({
		url		: BASE_URL + "admin/users/resend_otp",
		method	: "POST",
		data	: { resend_key : 'change_tpin', rokad_token : rokad_token },		
		success	: function(response) {			
			if(response.msg_type == 'success')
			{	
				document.getElementById("tpin_otp").reset();
				if(back == 'Y')
				{
					showLoader(response.msg_type, "OTP sent successfully");					
				}
				else
				{
					showNotification(response.msg_type, "OTP sent successfully");					
				}					
			}
			else if(response.msg_type == 'error')
			{						
				swal("Oops...", response.msg, "error");
				enable_button('submit_btn');
			}
		}            
	});	
});

$(document).off('keypress', '#old_tpin, #new_tpin, #cnf_tpin, #new_otp').on('keypress', '#old_tpin, #new_tpin, #cnf_tpin, #new_otp', function(e) 
{
   var regex = new RegExp("^[0-9\b]+$");//accept only _
   var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	 key=e.charCode? e.charCode : e.keyCode;
	if(key==9 )
	{
		return true; 
	}
	if (regex.test(str)) 
	{
		return true;
	}
	e.preventDefault();
	return false;
});