$().ready(function () {

    //display list of  agent
    $.validate({

        onError: function () {

        },
        onSuccess: function () {


            var div = "";
            var detail = {};
            var ajax_url = 'admin/agent/upload_docs';

            var form = 'docs_upload';
            var id = $("#agid").val();
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@success')
                {
                    showLoader(response.msg_type, response.msg);
                    location.href = BASE_URL + 'admin/agent/agent_docs_display?id=' + id;
                } else
                {
                    showLoader(response.msg_type, response.msg);
                }

            }, '', false)
        },
        modules: 'file'
    });

    //Display document list by agent
    var agent_id = $("#agid").val();
    var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/agent/docs_list_view?id=" + agent_id,
            "type": "POST",
            "data": "json",
			 data : function (d) {                        
				d.rokad_token = rokad_token;
			}
        },
        "columnDefs": [
            {
                "searchable": true
            }
        ],
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": true,
        "aoColumnDefs": [

            {"bSearchable": false, "aTargets": [0]},
            {"bSortable": false, "aTargets": [5]},
            {"aTargets": [1]}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

            $("td:first", nRow).html(iDisplayIndex + 1);

            return nRow;
        },
    };


    var oTable = $('#allagent').dataTable(tconfig);
    oTable.fnDraw();


    $(document).off('click', '#sel_ag').on('click', '#sel_ag', function (e)
    {
        var agent = $(".agent").val();

        window.location.href = BASE_URL + 'admin/agent/agent_docs_add?id=' + agent;

    });


    $(document).off('click', '.status a').on('click', '.status a', function (e) {

        var btnidx = $('.status').index(this);
        var $parent = $(this).parent().parent().parent();

        $parent.closest('tr').eq(btnidx).find('.status').html("<input type=file name='idprof[]' class='span3 fi_le'><input type=button class='btn can_upd'  value=Cancel >");


    });
    $(document).off('click', '.can_upd').on('click', '.can_upd', function (e) {

        var btnidx = $('.status').index(this);
        var $parent = $(this).parent().parent().parent();

        $parent.closest('tr').eq(btnidx).find('.status').html("<a href='javascript:void(0)' title='Click to reupload image'>Reupload</a>");


    });

    $(document).off('click', '.ins_row').on('click', '.ins_row', function (e) {

        var btnidx = $('.ins_row').index(this);
        var $parent = $(this).parent().parent().parent();
        ddl = $('#doccat').clone().attr('id', 'doccat');

        // var appendTxt = "<tr><td></td><td>"+$ddl+"</td><td><select name=doc[]><option>Select document format</option><option value='pancard'>PAN Card</option><option value='passport'>Valid Indian passport</option><option value=vicard>Voters Identity card </option><option value=dlie>Driving license </option><option value=bpassbook>Bank Pass Book</option></select></td><td><input id='idprof' name='idprof[]'  type='file'></td><td><input type='button' class='btn btn-primary ins_row' value='Add New' /><td><input type='button' class='del btn btn-danger' value='Delete' /></td></tr>";
        var appendTxt = $('tbody tr:first').clone();

        ($parent.find('tr').eq(btnidx)).after(appendTxt);


        //$parent.find('tr').eq(btnidx).next().find('td:eq(5)').html('<input type=button class=del btn btn-primary value=Delete>');
        $parent.find('tr').eq(btnidx).next().find('input[type="text"]').val('');
        $parent.find('tr').eq(btnidx).next().find('input[type="file"]').val('');
        $parent.find('tr').eq(btnidx).next().find('select[name="docnm[]"]').html("<option value=''>Please select document Name</option>")
        $parent.find('tr').eq(btnidx).next().find('select[name="docnm[]"]').removeClass('error');
        $parent.find('tr').eq(btnidx).next().find('select[name="docnm[]"]').removeAttr('style');
        $parent.find('tr').eq(btnidx).next().find('select[name="doccat[]"]').removeClass('error');
        $parent.find('tr').eq(btnidx).next().find('select[name="doccat[]"]').removeAttr('style');
        $parent.find('tr').eq(btnidx).next().find('input[name="idprof[]"]').removeAttr('style');
        $parent.find('tr').eq(btnidx).next().find('td').removeClass('has-error');
        $parent.find('tr').eq(btnidx).next().find('span').html("");

        ser('#idtable');

    });

    $(document).off('change', '.doccat').on('change', '.doccat', function (e)
    {
        var detail = {};
        var btnidx = $('.doccat').index(this);
        var $parent = $(this).parent().parent().parent();

        $parent.find('tr').eq(btnidx).find('select[name="docnm[]" ] option:gt(0)').remove();
        var div = "";
        var ajax_url = 'admin/agent/get_doc_category';
        detail['cat_id'] = $(this).val();
        var form = '';

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.res.length > 0)
            {

                $.each(response.res, function (i, value)
                {
                    $parent.find('tr').eq(btnidx).find('select[name="docnm[]"]').append("<option value=" + value.agent_docs_list + ">" + value.docs_name + "</option>");

                });
            } else
            {

                $parent.find('tr').eq(btnidx).find('select[name="docnm[]"] option:gt(0)').remove();
            }

        }, '', false)

    });

    $(document).off('click', '.upload').on('click', '.upload', function (e)
    {


        $(".alert-danger ul").html("");
        var vals = [];
        var results = [];
        $('#idtable select.docnm').each(function () {

            var val = $(this).find('option:selected').text();
            vals.push(val);

        });


        var sorted_arr = vals.sort();
        for (var i = 0; i < vals.length - 1; i++)
        {
            if (sorted_arr[i + 1] == sorted_arr[i])
            {
                if (sorted_arr[i] != 'Please select document Name')
                {
                    results.push(sorted_arr[i]);
                }
            }
        }


        if (results.length > 0)
        {
            $(".alert-danger").css('display', 'block');
            var str = "";
            str = "<p>You can not upload same document twice</p>";
            str += "<ul>";
            str += "<li>" + results + "</li>";
            str += "<ul>";
            $(".alert-danger p").html(str);
            return false;
        } else if (results.length == 0)
        {

            $(".alert-danger").hide();
        }

        return true;

    });


    $(document).off('click', '.del').on('click', '.del', function (e) {

        var i = 0;
        $('#idtable tr').each(function () {
            i++;
        });

        if (i - 1 == 1)
        {
            alert("can not delete rows");
        } else
        {
            $(this).parent().parent().remove();
        }
        ser('#idtable');
    });




    $(document).off('click', '.vw_img').on('click', '.vw_img', function (e) {
        $("#board_stop").modal('show');
    });

    $(document).off('change', '.docnm').on('change', '.docnm', function (e) {

        var btnidx = $('.doccat').index(this);
        var $parent = $(this).parent().parent().parent();
        var str = $parent.find('tr').eq(btnidx).find('select[name="docnm[]"] option:selected').text();
        $parent.find('tr').eq(btnidx).find('input[name="dropdowntext[]"]').val(str);

    });

    $(document).off('click', '.go').on('click', '.go', function (e) {

        agid = $('select.ag_nm option:selected').val();
        window.location.href = BASE_URL + 'admin/agent/agent_docs_view?id=' + agid;
    });

    $(document).off('click', '.add_new').on('click', '.add_new', function (e) {

        id = $("#agid").val();
        window.location.href = BASE_URL + 'admin/agent/add_new_doc?id=' + id;
    });

    $(document).off('click', '.docs_display').on('click', '.docs_display', function (e) {

        id = $("#agid").val();
        window.location.href = BASE_URL + 'admin/agent/agent_docs_display?id=' + id;
    });


    $(document).off('click', '.backto_list').on('click', '.backto_list', function (e) {

        id = $("#agid").val();
        window.location.href = BASE_URL + 'admin/agent/agent_docs_view?agid=' + id;
    });

    $(document).off('click', '.back_add_doc').on('click', '.back_add_doc', function (e) {

        id = $("#agid").val();
        window.location.href = BASE_URL + 'admin/agent/add_new_doc?id=' + id;
    });



    $(document).off('click', '.del_file').on('click', '.del_file', function (e) {


        $(".alert-success span").html("");
        var s = $(this).attr("data-img-path");
        var doc = $(this).attr("data-doc-name");
        bootbox.confirm("Do you want to delete " + doc + " document?", function (result) {
            if (result == true)
            {
                var detail = {};
                var div = "";
                var ajax_url = 'admin/agent/delete_img';

                detail['ag_id'] = $("#agid").val();
                detail['docs_id'] = s;
                var form = '';
                get_data(ajax_url, form, div, detail, function (response)
                {
                    if (response.msg_status == 'success')
                    {
                        $(".alert-success span").html("Document deleted successfully");
                        $(".alert-success").css('display', 'block');
                        oTable.fnDraw();
                        $(".alert-success").fadeOut("slow");
                    }

                }, '', false)

            }
        });
    });

    $(document).off('click', '.back').on('click', '.back', function (e)
    {

        window.location.href = BASE_URL + 'admin/agent/';
    });

    //kyc verification

    $(document).off('click', '#approve_kyc_button').on('click', '#approve_kyc_button', function (e)
    {
        var flag= confirm("Are you sure you want to approve KYC?");
        if(flag==true)
        {
            var div = "";
            var detail = {};
            var ajax_url = 'admin/agent/kyc_status';

            var form = '';
            var id = $("#agid").val();
            detail['agid'] = id;

            get_data(ajax_url, form, div, detail, function (response)
            {				
                if (response.flag == '@#success#@')
                {
                    showLoader(response.msg_type, response.msg);
                    setTimeout(location.reload(), 3000);
                } else
                {
                    showLoader(response.msg_type, response.msg);
                }

            }, '', false);
        }
    }); //kyc approval
});

function ser(id)
{

    var i = 0;
    $(id + ' tr').each(function () {
        $(this).find("td:first").text(i);
        i++;
    });
}