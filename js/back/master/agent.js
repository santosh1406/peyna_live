
$(document).ready(function() {

    $.resetinput = function() {

        $('form').find('input[type=text], input[type=password], input[type=number], input[type=email], textarea').removeAttr('disabled');
    };

    $('#booking_agent').on('hidden.bs.modal', function() {
        $(this).find('#booking_agent_form')[0].reset();
        $(this).removeData('bs.modal');
        $('#booking_agent_form').find('input[type=text]').val('');
        $("#agem").removeAttr('disabled', 'disabled');
        $(".error").html("");
        $(".err").html("");
        $("label.error").hide();
        $(".error").removeClass("error");

    });


    var dateToday = new Date();
    var mon = 12 - dateToday.getMonth();
    var currentMonth = dateToday.getMonth() + mon;

    var currentYear = dateToday.getFullYear();



    var year20 = currentYear - 18;
    var dt = 31 + "/" + currentMonth + "/" + year20;

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        endDate: dt
    });



    //validation for agent parameter

    jQuery.validator.addMethod("internet_email", function(value, element) {
        if (value == '')
            return true;
        var temp1;
        temp1 = true;
        var ind = value.indexOf('@');
        var str2 = value.substr(ind + 1);
        var str3 = str2.substr(0, str2.indexOf('.'));
        if (str3.lastIndexOf('-') == (str3.length - 1) || (str3.indexOf('-') != str3.lastIndexOf('-')))
            return false;
        var str1 = value.substr(0, ind);
        if ((str1.lastIndexOf('_') == (str1.length - 1)) || (str1.lastIndexOf('.') == (str1.length - 1)) || (str1.lastIndexOf('-') == (str1.length - 1)))
            return false;
        str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
        temp1 = str.test(value);
        return temp1;



    }, "Please enter valid email addtess!");

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "");

    jQuery.validator.addMethod("lfsccode", function(value, element) {
        return this.optional(element) || /^[a-z]{4}\d{7}$/i.test(value);
    }, "");

    $.validator.addMethod("validaddress", function(value, element) {
        return this.optional(element) || /^[a-zA-Z-0-9-/(.,) ]+$/i.test(value);
    }, "");

    $(document).off('keypress', '#agadd').on('keypress', '#agadd', function(e) {

        var regex = new RegExp("^[a-zA-Z)0-9(-/\b ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;

    });

    $("#booking_agent_form").validate({
        rules: {
            agent_level: {
                required: true

            },
            agfnm: {
                required: true,
                lettersonly: true
            },
            main_level:
                    {
                        required: true
                    },
            level_1:
                    {
                        required: true
                    },
            level_2:
                    {
                        required: true
                    },
            level_3:
                    {
                        required: true
                    },
            level_4:
                    {
                        required: true
                    },
            agmnm: {
                required: true,
                lettersonly: true
            },
            aglnm: {
                required: true,
                lettersonly: true
            },
            accname:
                    {
                        required: true,
                        lettersonly: true
                    },
            gen:
                    {
                        required: true
                    },
            dob:
                    {
                        required: true
                    },
            pincd: {
                number: true,
                required: true,
                maxlength: 6,
                minlength: 6
            },
            agem:
                    {
                        required: true,
                        internet_email: true

                    },
            mobno:
                    {
                        required: true,
                        number: true,
                        minlength: 10
                    },
            agadd:
                    {
                        required: true,
                        validaddress: true
                    },
            accno:
                    {
                        required: true,
                        number: true,
                    //    minlength: 21
                    },
            bname:
                    {
                        required: true,
                        lettersonly: true
                    },
            brname:
                    {
                        required: true,
                        lettersonly: true
                    },
            ifsc:
                    {
                        required: true,
                        lfsccode: true
                    },
            connm:
                    {
                        required: true
                    },
            stnm:
                    {
                        required: true
                    },
            photo:
                    {
                        filesize: 2000000,
                        extension: "jpeg|png|jpg",
                    },
            citynm:
                    {
                        required: true

                    }



        },
        errorPlacement: function(error, element) {
            if (element.attr("name") === "gen") {
                error.appendTo("#aggenmsg");
            }

            else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).removeClass("error");
        },
        messages: {
            "main_level": {
                required: "Please select valid Agent Level",
                lettersonly: "Please select valid Agent name"
            },
            "level_1":{
                required: "Please select valid Agent name"
            },
            "level_2":{
                required: "Please select valid Agent name"
            },
            "level_3":{
                required: "Please select valid Agent name"
            },
            "level_4":{
                required: "Please select valid Agent name"
            },
            
            "agent_level": {
                required: "Agent name should not be empty",
                lettersonly: "Please select valid Agent name"
            },
            "agfnm": {
                required: "First name should not be empty",
                lettersonly: "Please enter valid first name"
            },
            "agmnm": {
                required: "First name should not be empty",
                lettersonly: "Please enter valid first name"
            },
            "aglnm": {
                required: "First name should not be empty",
                lettersonly: "Please enter valid first name"
            },
            "accname": {
                required: "Account name should not be empty",
                lettersonly: "Please enter valid Account name"
            },
            gen: "Please select gender",
            dob: "Please select date of birth",
            "accno": {
                required: "Account number should not be empty",
                number: "Please enter valid Account number",
               // minlength: "Account number  should be 21 digit",
            },
            "pincd": {
                required: "Pincode number should not be empty",
                number: "Please enter valid pincode number",
                minlength: "Pin code number should be 6 digit",
            },
            "agem": {
                required: "Email address should not be empty",
                email: "Please enter valid email"
            },
            "mobno": {
                required: "You must enter Contact Number",
                minlength: "Contact number must be 10  digit",
                number: "Please enter valid contact number"
            },
            "agadd": {
                required: "Agent address should not be empty",
                validaddress: "Please enter valid address"
            },
            "ifsc": {
                lfsccode: "Please enter valid IFSC CODE "
            },
            "bname":
                    {
                        required: "Bank Name should not be empty",
                        lettersonly: "Please enter valid Bank name"
                    },
            "brname":
                    {
                        required: "Bank Branch  Name should not be empty",
                        lettersonly: "Please enter valid Bank Branch"
                    },
            connm:
                    {
                        required: "Please select Country Name"
                    },
            stnm:
                    {
                        required: "Please select State Name"
                    },
            citynm:
                    {
                        required: "Please select City Name"

                    },
            level1:
                    {
                        required: "Please select Direct agent"
                    },
            level2:
                    {
                        required: "Please select Principal agent"
                    },
            level3:
                    {
                        required: "Please select Sub agent"
                    },
            level4:
                    {
                        required: "Please select terminal agent"
                    },
            "photo":
                    {
                        extension: "Please uplaod valid image"
                    }
        }

    });
    $.validator.addMethod('filesize', function(value, element, param) {

        return this.optional(element) || (element.files[0].size <= param)
    });



    var specialKeys = [8, 9, 46, 36, 35, 37, 39];


    $(document).off('keydown', '#agfnm').on('keydown', '#agfnm', function(e) {

        var keyCode = e.charCode ? e.charCode : e.keyCode;

        if ((keyCode >= 65 && keyCode <= 90) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;

    });

    $(document).off('keydown', '#agmnm').on('keydown', '#agmnm', function(e) {

        var keyCode = e.charCode ? e.charCode : e.keyCode;

        if ((keyCode >= 65 && keyCode <= 90) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;

    });



    $(document).off('keydown', '#aglnm').on('keydown', '#aglnm', function(e) {

        var keyCode = e.charCode ? e.charCode : e.keyCode;

        if ((keyCode >= 65 && keyCode <= 90) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;

    });
    $(document).off('keypress', '#mobno').on('keypress', '#mobno', function(e)
    {

        var unicode = e.charCode ? e.charCode : e.keyCode;
        if (unicode != 8 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
            if (unicode < 48 || unicode > 57) { //if not a number
                return false //disable key press
            }
        }
    });
    $(document).off('keypress', '#accno').on('keypress', '#accno', function(e)
    {

        var unicode = e.charCode ? e.charCode : e.keyCode;
        if (unicode != 8 && unicode != 9)
        { //if the key isn't the backspace key (which we should allow)
            if (unicode < 48 || unicode > 57)
            { //if not a number
                return false //disable key press
            }
        }
    });

    var charwithspace = [8, 9, 46, 36, 35, 37, 39, 32];
    $(document).off('keydown', '#bname').on('keydown', '#bname', function(e)
    {

        var keyCode = e.charCode ? e.charCode : e.keyCode;
        if ((keyCode >= 65 && keyCode <= 90) || (charwithspace.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;
    });

    $(document).off('keydown', '#brname').on('keydown', '#brname', function(e)
    {

        var keyCode = e.charCode ? e.charCode : e.keyCode;
        if ((keyCode >= 65 && keyCode <= 90) || (charwithspace.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;
    });


    $(document).off('keydown', '#accname').on('keydown', '#accname', function(e)
    {

        var keyCode = e.charCode ? e.charCode : e.keyCode;
        if ((keyCode >= 65 && keyCode <= 90) || (charwithspace.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode))
        {
            return true //disable key press
        }
        return false;
    });


    $(document).off('keypress', '#pincd').on('keypress', '#pincd', function(e) {

        var unicode = e.charCode ? e.charCode : e.keyCode
        if (unicode != 8 && unicode != 9) { //if the key isn't the backspace key (which we should allow)
            if (unicode < 48 || unicode > 57) //if not a number
                return false //disable key press
        }

    });

    $('#dob').keydown(function(e) {
        e.preventDefault();
        return false;
    });



    //go to level


    //get state list 

    $(document).off('change', '#connm').on('change', '#connm', function(e)
    {

        $('#stnm option').remove();
        $("#citynm").html($('<option>').html('select City').attr({
            'selected': true,
            'value': ''
        }));

        var detail = {};
        var div = "";
        var ajax_url = 'admin/bus_stop_master/get_state';
        var form = 'chanpass';
        var err = "";
        detail['country_id'] = $(this).val();
        get_data(ajax_url, form, div, detail, function(response)
        {
            if (response.state.length != 0)
            {
                $("#stnm").html("<option value='' selected>select state</option>");
                $.each(response.state, function(i, value)
                {
                    $("#stnm").append("<option value=" + value.intStateId + ">" + value.stState + "</option>");
                });
            }
            else
            {
                $("#stnm").html("<option value='' selected>Select State</option>");
            }

        }, '', false)

    });

    //get city name
    $(document).off('change', '#stnm').on('change', '#stnm', function(e)
    {
        $('#citynm option').remove();
        var detail = {};
        var div = "";
        var ajax_url = 'admin/bus_stop_master/get_city';
        var form = 'chanpass';
        var err = "";
        detail['state_id'] = $(this).val();
        get_data(ajax_url, form, div, detail, function(response)
        {

            if (response.city.length != 0)
            {

                $("#citynm").html("<option value=''>select City</option>");
                $.each(response.city, function(i, value)
                {
                    $("#citynm").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                });
            }
            else
            {


                $("#citynm").html("<option selected=seleced  value='' >Select City</option>");
            }

        }, '', false)

    });

    $(document).off('change', '#main_level').on('change', '#main_level', function(e)
    {
        var sel_level = $(this).val();
        var i;
        var html_div = '<div class="clearfix" style="height: 10px;clear: both;"></div>';
        for (i = 1; i < sel_level; i++) {
            html_div += '<div class="form-group"><label class="col-lg-2 control-label" >Agent Level' + i + ': </label>';
            html_div += '<div class="col-lg-6"><select id="level_' + i + '" name="level_' + i + '"  no_of_level=' + i + ' class="form-control agent_level"><option value="">Please Select Agent</option>';
            html_div += '</select></div> </div><div class="clearfix" style="height: 10px;clear: both;"></div>';
        }
        $("#add_levels").html(html_div);
        var detail = {};
        var div = "";
        var ajax_url = 'admin/agent/get_agent_data';
        var form = '';
        var err = "";
        detail['agent_id'] = $(this).val();
        get_data(ajax_url, form, div, detail, function(response)
        {

            if (response.agent_data.length != 0)
            {

                $("#level_1").html("<option value=''>select Agent</option>");
                $.each(response.agent_data, function(i, value)
                {
                    $("#level_1").append("<option value=" + value.agent_id + ">" + value.fname + ' ' + value.mname + ' ' + value.lname + "</option>");
                });
            }


        }, '', false)

    });

    $(document).off('change', '.agent_level').on('change', '.agent_level', function(e)
    {

        var i;
        var sel_level = $(this).attr('no_of_level');
        var act_level = parseInt(sel_level) + 1;

        var detail = {};
        var div = "";
        var ajax_url = 'admin/agent/get_agent_level_data';
        var form = '';
        var err = "";
        detail['agent_level'] = $(this).attr('no_of_level');

        detail['main_agent_id'] = $('#main_level').val();
        detail['sel_agent_val'] = $(this).val();

        get_data(ajax_url, form, div, detail, function(response)
        {

            if (response.agent_data.length != 0)
            {

                $("#level_" + act_level).html("<option value=''>select Agent</option>");
                $.each(response.agent_data, function(i, value)
                {
                    $("#level_" + act_level).append("<option value=" + value.agent_id + ">" + value.fname + ' ' + value.mname + ' ' + value.lname + "</option>");
                });
            }


        }, '', false)

    });

    $(document).off('click', '.create_login_btn').on('click', '.create_login_btn', function(e) {
        e.preventDefault();
        var detail = {};
        var form = '';
        var div = '';


        detail['agent_id'] = $(this).attr('data-ref');
        var ajax_url = 'admin/agent/create_login/' + detail['agent_id'];

        bootbox.confirm("Do you want to create login?", function(result) {
            if (result)
            {
                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        bootstrapGrowl(response.msg, 'success');


                    }
                    else
                    {
                        bootstrapGrowl(response.error, 'danger');
                    }
                });
            }
        });
    });

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + 'admin/agent/';
    });


    var tconfig = {
        //  "sDom": '<"toolbar">hlfetlp',
        "processing": false,
        "serverSide": true,
        ajax: {
            url: BASE_URL + "admin/agent/list_agent_data",
            type: "POST",
            data: function(d) {
                d.bos_code = $('input[name=bos_code]').val();
                d.vender_code = $('input[name=vender_code]').val();
                d.email = $('input[name=email]').val();
                d.phone = $('input[name=phone]').val();
                d.bos_code = $('input[name=bos_code]').val();
                d.kyc = $("#kyc option:selected").val();
                d.created_by = $('input[name=created_by]').val();
                d.agent_level = $("#agent_level option:selected").val();
				d.rokad_token = rokad_token;
            }
        },
        "columnDefs": [
            {"searchable": false}
        ],
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": false,
        "scrollX": true,
        "aoColumnDefs": [
            {"bSearchable": true, "aTargets": [0]},
            {"targets": [0,10], "orderable": false}
        ],
        "order": [[0, "desc"]],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            // $("td:first", nRow).html(iDisplayIndex + 1);
            // return nRow;
            if (aData[7] == '1')
            {

                $("td:eq(7)", nRow).html("Active");
            }
            else
            {

                $("td:eq(7)", nRow).html("Deactive");
            }


            if (role_name == '0') {
                $("td:eq(9)", nRow).html(aData[9]);
            }

            else if (aData[9] == 'Y')
            {

                $("td:eq(9)", nRow).html("<i class='btn btn-info btn-xs fa fa-key kyc_st' title='Documents Approved'  data-kyc-flag='" + aData[9] + "' data-ref=" + aData[0] + " data-level=1  ></i>");
            }
            else
            {

                $("td:eq(9)", nRow).html("<i class='btn btn-warning btn-xs fa fa-key kyc_st' title='Documents Pending' data-kyc-flag='" + aData[9] + "' data-ref=" + aData[0] + " data-level=1  ></i>");
            }
            
            $("td:eq(10)", nRow).html(aData[11]);
            
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
    };
    var oTable = $('#agent_view').dataTable(tconfig);


    $(document).off('click', '.kyc_st').on('click', '.kyc_st', function(e)
    {
        var detail = {};
        detail['agent_id'] = $(this).attr('data-ref');
        detail['kyc_status'] = $(this).attr('data-kyc-flag');


        var div = "";
        var ajax_url = 'admin/agent/kyc_status';
        var str = "";
        var form = '';
        bootbox.confirm("Do you want to approve the channel partner?", function(result) {
            if (result == true)
            {
                get_data(ajax_url, form, div, detail, function(response)
                {

                    if (response.flag == '@#success#@')
                    {
                        if (response.msg_type == "success")
                        {

                            oTable.fnDraw();


                        }
                    }
                    else
                    {
                        if (response.msg_type == "error")
                        {
                            // showLoader(response.msg_type,response.msg); 
                            bootstrapGrowl(response.msg, 'danger');
                        }
                    }

                }, '', false)

            }
        });
    });

    $('#agent_filter_view').on('submit', function(e) {
        oTable.fnDraw();
        e.preventDefault();
    });

    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
        e.preventDefault();
        var elem = $(this);

        bootbox.confirm("Please confirm to delete agent?", function(result) {
            if (result)
            {
                var detail = {};
                var form = '';
                var div = '';


                detail['agent_id'] = elem.attr('data-ref');
                var ajax_url = 'admin/agent/delete/' + detail['agent_id'];

                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        bootstrapGrowl(response.msg, 'success');

                    }
                    else
                    {
                        bootstrapGrowl(response.msg, 'danger');
                    }
                });
            }
        });
    });
    
    $(document).off('click', '.change_cb_flag').on('click', '.change_cb_flag', function(e) { 
        e.preventDefault();
        var elem = $(this);
        
        var detail = {};

        detail['agent_id'] = elem.data('ref');
        detail['flag'] = elem.data('flag');
        
        if(elem.data('flag') == 0) {
            var msg = "Agent is not CB Agent. Do you really want to make him CB Agent?";
        }
        else if(elem.data('flag') == 1) {
            var msg = "Agent is CB Agent. Do you really want to remove him from CB Agent?";
        }
        bootbox.confirm(msg, function(result) {
            if (result) { 
                $.ajax({
                    url: base_url + "admin/agent/change_cb_flag?" + Math.random(),
                    type: "post",
                    data: detail,
                    //dataType: "json",
                    success: function(msg) {
                        var res = $.parseJSON(msg);
                        alert(res.msg);
                        location.reload();
                    },
                    error: function(t,e,x) {
                        //$('#overlay').css('display','none');console.log(t);console.log(e);console.log(x);
                        bootstrapGrowl(msg, 'danger');
                    }
                });
            }
        });
    });
    
    $(document).off('click', '.create_login_btn').on('click', '.create_login_btn', function(e) {
        e.preventDefault();
        var detail = {};
        var form = '';
        var div = '';


        detail['agent_id'] = $(this).attr('data-ref');
        var ajax_url = 'admin/agent/create_login/' + detail['agent_id'];

        bootbox.confirm("Do you want to create login?", function(result) {
            if (result)
            {
                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        bootstrapGrowl(response.msg, 'success');
                        // $('#alert_msg').removeClass('hide').find('.body').html(response.msg);
                    }
                    else
                    {
                        bootstrapGrowl(response.error, 'danger');
                    }
                });
            }
        });
    });

    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
        e.preventDefault();
        var elem = $(this);

        bootbox.confirm("Please confirm to delete agent?", function(result) {
            if (result)
            {
                var detail = {};
                var form = '';
                var div = '';


                detail['agent_id'] = elem.attr('data-ref');
                var ajax_url = 'admin/agent/delete/' + detail['agent_id'];

                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        bootstrapGrowl(response.msg, 'success');
                        oTable.fnDraw();

                    }
                    else
                    {
                        bootstrapGrowl(response.msg, 'danger');
                    }
                });
            }
        });
    });
    // end of document


});
/*

function displaysearchingblock(msg)
{    
    if(msg!='')
        document.getElementById('srchBlckMsg').innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;"+msg+"&nbsp;&nbsp;&nbsp;&nbsp;";
    else
        document.getElementById('srchBlckMsg').innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;Processing your request!&nbsp;&nbsp;&nbsp;&nbsp;";
    
    document.getElementById("updatestatus").style.display="block";
}
function hidesearchingblock()
{
//    console.trace();
    document.getElementById("updatestatus").style.display="none";
}*/