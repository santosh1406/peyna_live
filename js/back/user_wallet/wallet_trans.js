
$(document).ready(function() {
    var oTables;
     var date = new Date();
     var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	
     $("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
     $("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));

   var wallet_trans_config = {
        processing: true,
        serverSide: true,
        ajax: {
            url: BASE_URL+"admin/user_wallet/get_wallet_trans/"+wallet_id,
            type: 'POST',
             data    : function (d) {
                        d.from_date = $("#from_date").val();
                        d.till_date = $("#till_date").val();
                        d.tran_type = $("#tran_type option:selected").val();
                        d.tran_reason = $("#tran_reason option:selected").val();
                        d.rokad_token = rokad_token;
                      },     
        },
        columnDefs: [
        {
            searchable: false
        }
        ],
        order: [[ 5, "asc" ]],
        iDisplayLength: 50,
        aLengthMenu: [[5, 10, 50, -1], [5, 10, 50, "All"]],
        paginate: true,
        paging: true,
        searching: true,
        aoColumnDefs: [
            {
                bSortable: false,  
                aTargets: [0,1,2,3,4,6]
            },
        ],
        "oLanguage": {
                "sProcessing": '<center><img src="'+ base_url +'assets/adminlte/back/img/ajax-loader.gif" class="img-circle centered" alt="user image"/></center>' 
            },  
        
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
          
        },
        fnDrawCallback: function(data)
        {

            if (oTables.fnGetData().length == 0)
            {
                $("#extoexcel").attr("disabled", "disabled"); 

            } else {
                 $("#extoexcel").removeAttr("disabled"); 

            }
        },
      
       
    };
    oTables = $('#user_wallet_trans').dataTable(wallet_trans_config);

    $('#user_wallet_filter').on('submit', function(e) {
        
        e.preventDefault();
        oTables.fnDraw();
    });
    
    $.validator.addMethod("checkDepend",
    function(value, element) {
    if($("#from_date").val() != "" &&  $("#till_date").val() != "")
    {   
        var date_diff = date_operations($("#from_date").val(),$("#till_date").val());
        if(date_diff.date_compare)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }

  },"To date must be greater than from date.");

         $('#user_wallet_filter').validate({
        ignore: "",
        rules: {
                till_date : {
                checkDepend : true
                }
        },
        messages: {
            
            till_date:
            {

            }
        }
    }); 
        
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
        });
    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate:0
     });
     
    /* code added for exporing total result set in excel and pdf format starts here*/

     $('#extoexcel').on('click', function(e) {
                var formData = $("#user_wallet_filter").serialize();
                window.location.href=BASE_URL+"admin/user_wallet/wallet_transaction_report_excel/"+wallet_id+"?"+formData;
     });
     
    /* code added for exporting total result set in excel and pdf format ends here*/
});
