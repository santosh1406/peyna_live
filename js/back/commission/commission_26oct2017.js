
$(document).ready(function ()
{
    //list commission ajax starts here.
    $('.alert-dismissable').delay(10000).fadeOut("slow");
    var tconfig =
            {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": BASE_URL + "admin/commission/list_commission",
                    "type": "POST",
                    "data": "json",
					data: function(d){                     
					d.rokad_token = rokad_token;
					}
                },
                "columnDefs": [
                    {"searchable": false}
                ],
                "iDisplayLength": 5,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "paginate": true,
                "paging": true,
                "searching": true,
                "aoColumnDefs": [
                    {"bSearchable": false, "aTargets": [0]},
                    {"targets": [2, 3, -1], "orderable": false},
                    {"bVisible": false, "aTargets": [6]}
                ],
                "order": [[0, "desc"]],
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $("td:first", nRow).html(iDisplayIndex + 1);
                    if (aData[3] != null)
                    {
                        var till_date_data = aData[3].split('-');
                        var till_date = new Date(till_date_data[2] + '-' + till_date_data[1] + '-' + till_date_data[0]);
                        var today = new Date();
                    }


                    if (aData[6] == '1')
                    {
                        $("td:nth-last-child(2)", nRow).html('expired');
                        if (till_date >= today)
                        {
                            if (aData[5] == '1')
                            {
                                $("td:nth-last-child(2)", nRow).html('Active');
                            } else if (aData[5] == '0')
                            {
                                $("td:nth-last-child(2)", nRow).html('Inactive');
                            }
                        }
                    } else if (aData[6] == '0')
                    {
                        $("td:nth-child(3)", nRow).html('');
                        $("td:nth-child(4)", nRow).html('');
                        if (aData[5] == '1')
                        {
                            $("td:nth-last-child(2)", nRow).html('Active');
                        } else if (aData[5] == '0')
                        {
                            $("td:nth-last-child(2)", nRow).html('Inactive');
                        }
                    }
                    var active_btn = 'fa fa-square-o';
                    if (aData[5] == '1')
                    {
                        active_btn = 'fa fa-check-square-o';
                    }

                    var btn = '';
                    btn += '<a title="Change status" href="' + BASE_URL + 'admin/commission/change_status/' + aData[5] + '" ref-id="' + aData[0] + '" ref="' + aData[5] + '" class="change_status"><i class="' + active_btn + '"></i></a>';
                    btn += '<a title="View commission" href="' + BASE_URL + 'admin/commission/view/' + aData[0] + '" class="edit_btn margin" ref="' + aData[0] + '"><i class="glyphicon glyphicon-eye-open"></i> </a>';
                    btn += '<a class="deletebtn" title="Delete commission" href="' + BASE_URL + 'admin/commission/delete/' + aData[0] + '" ref="' + aData[0] + '"><i class="glyphicon glyphicon-trash"></i></a>';

                    $("td:last", nRow).html(btn);
                },
            };

    var oTable = $('#listdata').dataTable(tconfig);
    //search on single hit only
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').bind('keyup', function (e) {
        if (e.keyCode == 13) {
            oTable.fnFilter(this.value);
        }
    });
    //list commission ajax ends here

    //delete commission starts here
    $(document).off('click', '.deletebtn').on('click', '.deletebtn', function (e)
    {
        // e.preventDefault();
        var flag = confirm('Do you want to delete');
        return flag;
    });
    //delete commission ends here

    //change status starts here
    $(document).off('click', '.change_status').on('click', '.change_status', function (e)
    {
        e.preventDefault();
        var detail = {};
        var div = "";
        var ajax_url = 'admin/commission/change_status/' + $(this).attr('ref');
        var form = 'view_commission_form';
        var err = "";
        detail['id'] = $(this).attr('ref-id');
        detail['status'] = $(this).attr('ref');
        get_data(ajax_url, form, div, detail, function (response)
        {
            show(response);
            if (response.flag == '@#success#@')
            {
                bootstrapGrowl(response.msg, 'success');
                oTable.fnDraw();
            }
        }, '', false)
    });
    //change status ends here

    var date = new Date();
    date.setDate(date.getDate() - 1);
    $('input[name="from_till"]').on('click.daterangepicker', function (ev, picker) {
        $(".input-mini").hide();
    });

    $("#from_till").daterangepicker({
        startDate: date,
        minDate: date,
        autoUpdateInput: false,
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear'
        }},
            function (start, end, label) {
                // show("A new date range was chosen: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
            }
    );

    $('input[name="from_till"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('input[name="from"]').val(picker.startDate.format('DD-MM-YYYY'));
        $('input[name="till"]').val(picker.endDate.format('DD-MM-YYYY')).attr("data-validate-date", picker.startDate.format('DD-MM-YYYY'));
    });

    $('input[name="from_till"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val("");
        $('input[name="from"]').val("").attr("data-validate-date", "");
        $('input[name="till"]').val("");
    });

});

//on change of service tax show hide div
$(document).off('change', '#commission_with_service_tax').on('change', '#commission_with_service_tax', function () {

    if ($('#commission_with_service_tax').val() == '1')
    {
        $("#service_tax_hide_show").show();
        $("#service_tax_hide_show").find('input').attr('required', true);
    } else
    {
        $("#service_tax_hide_show").hide();
        $("#service_tax_hide_show").find('input').attr('required', false);
    }
});

//On change of default Commission value date show hide here start //
$(document).off('change', '#default_template').on('change', '#default_template', function () {

    if ($('#default_template').val() == '1')
    {
        $("#show_hide_date").show();
        $("#show_hide_date").find('input').attr('required', true);
    } else
    {
        $("#show_hide_date").hide();
        $("#show_hide_date").find('input').attr('required', false);
    }
});

//validation starts here
$(document).on('submit', '#create_commission_form', function () {
    var flag = 1;
    var total_flag = 1;
    //commission name should not be null
    if ($('#name').val() == '') {
        alert("Enter commission name");
        return false;
    }
    
    var name = $('#name').val();
    if(/^[a-zA-Z0-9_-]*$/.test(name) == false) {
        alert('Commission Name should only contain alphanumeric or alphabetic characters.');
        return false;
    }
    //service tax field should not be null
    if ($('#service_tax').is(":visible"))
    {
        if ($('#service_tax').val() == '') {
            alert("Enter Service Tax rate");
            return false;
        }
    }

    //from-till should be selected
    if ($('#from_till').is(":visible"))
    {
        if ($('#from_till').val() == '') {
            alert("Please select From-Till.");
            return false;
        }

    }

    //pos_commission validation
    $('.pos_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
            var pos_value = $(this).val();
            var trimax_value = $(this).parent('td').closest('td').prev('td').find('input').val();
            var md_value = $(this).parent('td').closest('td').next('td').find('input').val();
            var ad_value = $(this).parent('td').closest('td').next('td').next('td').find('input').val();
            var dist_value = $(this).parent('td').closest('td').next('td').next('td').next('td').find('input').val();
            var retailer_value = $(this).parent('td').closest('td').next('td').next('td').next('td').next('td').find('input').val();
            console.log(parseFloat(trimax_value) + parseFloat(pos_value) + parseFloat(md_value) + parseFloat(ad_value) + parseFloat(dist_value) + parseFloat(retailer_value));
            if ((parseFloat(trimax_value) + parseFloat(pos_value) + parseFloat(md_value) + parseFloat(ad_value) + parseFloat(dist_value) + parseFloat(retailer_value)) != 100)
            {
                $(this).addClass('shadow_blue');
                $(this).parent('td').closest('td').prev().find('input').addClass('shadow_blue');
                $(this).parent('td').closest('td').next().find('input').addClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().find('input').addClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().next().find('input').addClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().next().next().find('input').addClass('shadow_blue');
                total_flag = 0;
            } else
            {
                $(this).removeClass('shadow_blue');
                $(this).parent('td').closest('td').prev().find('input').removeClass('shadow_blue');
                $(this).parent('td').closest('td').next().find('input').removeClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().find('input').removeClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().next().find('input').removeClass('shadow_blue');
                $(this).parent('td').closest('td').next().next().next().next().find('input').removeClass('shadow_blue');
            }

        }

    });
    //pos_commission validation
    $('.md_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
        }


    });
    //pos_commission validation
    $('.ad_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
        }


    });
    //pos_commission validation
    $('.dist_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
        }


    });
    //pos_commission validation
    $('.retailer_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
        }


    });

    //pos_commission validation
    $('.trimax_commission').each(function () {
        if ($(this).val() == '' || $(this).val() < 0) {
            $(this).addClass('shadow_red');
            flag = 0;
        } else {
            $(this).removeClass('shadow_red');
        }


    });

    if (flag == 0) {
        alert('Please fill all the values');
        return false;
    }
    if (total_flag == 0)
    {
        alert('Total Amount should be exact 100.');
        return false;
    }
    
    
    //check whether commission exists or not.
    var commission_exists_flag = 1;
    var commission_name = $('#name').val();
    $.ajax({
        url: BASE_URL + 'admin/Commission/is_commission_exists',
        data: {
                name: commission_name,
                rokad_token : rokad_token
             },
        async: false,
        type: 'POST',
        success: function (r) {
            commission_exists_flag = (r === 'true') ? false : true;
        }

    });
    if (commission_exists_flag == false)
    {
        alert("Commission name already exists.")
    }
    if(commission_exists_flag==true)
    {
        var confirm_flag = confirm('Are you sure you want to create commission?');
        return confirm_flag;
    }
    else
    {
        return false;
    }
    //check whether commission exists or not ends here.
    
});
