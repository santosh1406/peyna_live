/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function ()
{
    $(document).off('change', '#select_service_select').on('change', '#select_service_select', function (e)
    {
       var library=this.value;
       if(library!="")
       {
            e.preventDefault();
            var detail = {};
            var div = "";
            var ajax_url = 'admin/service/redirect_commission/';
            var err = "";
            detail['library'] = library;
            get_data(ajax_url, "", div, detail, function (response)
            {
              window.open(response.redirect, '_blank');
            }, '', false)
       }
       
    });
});
