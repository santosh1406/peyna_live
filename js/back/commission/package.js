
$(document).ready(function ()
{   
      //list commission ajax starts here.
    $('.alert-dismissable').delay(10000).fadeOut("slow");
    var tconfig =
            {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": BASE_URL + "admin/package/list_package",
                    "type": "POST",
                    "data": "json",
					data: function(d){                     
					d.rokad_token = rokad_token;
					}
                },
                "columnDefs": [
                    {"searchable": false}
                ],
                "iDisplayLength": 5,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "paginate": true,
                "paging": true,
                "searching": true,
                 "oLanguage": {
                "sProcessing": img 
            },
                "aoColumnDefs": [
                    {"bSearchable": false, "aTargets": [0]},
                    {"targets": [2, 3, -1], "orderable": false},
                    {"bVisible": false, "aTargets": [6]}
                ],
                "order": [[0, "desc"]],
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var btn = '';
                    $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                    if (aData[3] != null) {
                        var till_date = aData[3];
                        var today = new Date();
                    }

                    if (aData[6] == '1') {
                        $("td:nth-last-child(2)", nRow).html('Yes');
                    } else if (aData[6] == '0') {
                        btn += '<a class="deletebtn" title="Delete package" href="' + BASE_URL + 'admin/package/delete/' + aData[0] + '" ref="' + aData[0] + '"><i class="glyphicon glyphicon-trash"></i></a>';
                        $("td:nth-last-child(2)", nRow).html('No');
                    }
                    var active_btn = 'fa fa-square-o';
                    if (aData[5] == '1')
                    {
                        active_btn = 'fa fa-check-square-o';
                    }

                    // btn += '<a title="Change status" href="' + BASE_URL + 'admin/package/change_status/' + aData[5] + '" ref-id="' + aData[0] + '" ref="' + aData[5] + '" class="change_status"><i class="' + active_btn + '"></i></a>';
                    btn += '<a title="View package" href="' + BASE_URL + 'admin/package/view/' + aData[0] + '" class="edit_btn margin" ref="' + aData[0] + '"><i class="glyphicon glyphicon-eye-open"></i> </a>';

                    $("td:last", nRow).html(btn);
                },
            };

    var oTable = $('#listdata').dataTable(tconfig);
    //search on single hit only
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').bind('keyup', function (e) {
        if (e.keyCode == 13) {
            oTable.fnFilter(this.value);
        }
    });
    //list commission ajax ends here

    //delete commission starts here
    $(document).off('click', '.deletebtn').on('click', '.deletebtn', function (e)
    {
        // e.preventDefault();
        var flag = confirm('Do you want to delete');
        return flag;
    });
    //delete commission ends here

    //change status starts here
    $(document).off('click', '.change_status').on('click', '.change_status', function (e)
    {
        e.preventDefault();
        var detail = {};
        var div = "";
        var ajax_url = 'admin/package/change_status/' + $(this).attr('ref');
        var form = 'view_commission_form';
        var err = "";
        detail['id'] = $(this).attr('ref-id');
        detail['status'] = $(this).attr('ref');
        get_data(ajax_url, form, div, detail, function (response)
        {
           
            if (response.flag == '@#success#@')
            {
                bootstrapGrowl(response.msg, 'success');
                oTable.fnDraw();
            }
        }, '', false)
    });
    //change status ends here

//     var date = new Date();
//     date.setDate(date.getDate() - 1);
//     $('input[name="from_till"]').on('click.daterangepicker', function (ev, picker) {
//         $(".input-mini").hide();
//     });

//     $("#from_till").daterangepicker({
//         startDate: date,
//         minDate: date,
//         autoUpdateInput: false,
//         locale: {
//             format: 'DD-MM-YYYY',
//             cancelLabel: 'Clear'
//         }},
//             function (start, end, label) {
//                 // show("A new date range was chosen: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
//             }
//     );

//     $('input[name="from_till"]').on('apply.daterangepicker', function (ev, picker) {
//         $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
//         $('input[name="from"]').val(picker.startDate.format('DD-MM-YYYY'));
//         $('input[name="till"]').val(picker.endDate.format('DD-MM-YYYY')).attr("data-validate-date", picker.startDate.format('DD-MM-YYYY'));
//     });

//     $('input[name="from_till"]').on('cancel.daterangepicker', function (ev, picker) {
//         $(this).val("");
//         $('input[name="from"]').val("").attr("data-validate-date", "");
//         $('input[name="till"]').val("");
//     });

// });

// //on change of service tax show hide div
// $(document).off('change', '#commission_with_service_tax').on('change', '#commission_with_service_tax', function () {

//     if ($('#commission_with_service_tax').val() == '1')
//     {
//         $("#service_tax_hide_show").show();
//         $("#service_tax_hide_show").find('input').attr('required', true);
//     } else
//     {
//         $("#service_tax_hide_show").hide();
//         $("#service_tax_hide_show").find('input').attr('required', false);
//     }
// });

// //On change of default Commission value date show hide here start //
// $(document).off('change', '#default_template').on('change', '#default_template', function () {

//     if ($('#default_template').val() == '1')
//     {
//         $("#show_hide_date").show();
//         $("#show_hide_date").find('input').attr('required', true);
//     } else
//     {
//         $("#show_hide_date").hide();
//         $("#show_hide_date").find('input').attr('required', false);
//     }
// });

//validation starts here
// $(document).on('submit', '#create_commission_form', function () {
//     var flag = 1;
//     var total_flag = 1;
//     //commission name should not be null
//     if ($('#name').val() == '') {
//         alert("Enter commission name");
//         return false;
//     }
    
//     var name = $('#name').val();
//     if(/^[a-zA-Z0-9- ]*$/.test(name) == false) {
//         alert('Commission Name should only contain alphanumeric or alphabetic characters.');
//         return false;
//     }
//     //service tax field should not be null
//     if ($('#service_tax').is(":visible"))
//     {
//         if ($('#service_tax').val() == '') {
//             alert("Enter Service Tax rate");
//             return false;
//         }
//     }

//     //from-till should be selected
//     if ($('#from_till').is(":visible"))
//     {
//         if ($('#from_till').val() == '') {
//             alert("Please select From-Till.");
//             return false;
//         }

//     }

//     //pos_commission validation
//     $('.pos_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//             var pos_value = $(this).val();
//             var trimax_value = $(this).parent('td').closest('td').prev('td').find('input').val();
//             var md_value = $(this).parent('td').closest('td').next('td').find('input').val();
//             var ad_value = $(this).parent('td').closest('td').next('td').next('td').find('input').val();
//             var dist_value = $(this).parent('td').closest('td').next('td').next('td').next('td').find('input').val();
//             var retailer_value = $(this).parent('td').closest('td').next('td').next('td').next('td').next('td').find('input').val();
//             if ((parseFloat(trimax_value) + parseFloat(pos_value) + parseFloat(md_value) + parseFloat(ad_value) + parseFloat(dist_value) + parseFloat(retailer_value)) > 100)
//             {
//                 $(this).addClass('shadow_blue');
//                 $(this).parent('td').closest('td').prev().find('input').addClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().find('input').addClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().find('input').addClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().next().find('input').addClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().next().next().find('input').addClass('shadow_blue');
//                 total_flag = 0;
//             } else
//             {
//                 $(this).removeClass('shadow_blue');
//                 $(this).parent('td').closest('td').prev().find('input').removeClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().find('input').removeClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().find('input').removeClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().next().find('input').removeClass('shadow_blue');
//                 $(this).parent('td').closest('td').next().next().next().next().find('input').removeClass('shadow_blue');
//             }

//         }

//     });
//     //pos_commission validation
//     $('.md_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//         }


//     });
//     //pos_commission validation
//     $('.ad_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//         }


//     });
//     //pos_commission validation
//     $('.dist_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//         }


//     });
//     //pos_commission validation
//     $('.retailer_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//         }


//     });

//     //pos_commission validation
//     $('.trimax_commission').each(function () {
//         if ($(this).val() == '' || $(this).val() < 0) {
//             $(this).addClass('shadow_red');
//             flag = 0;
//         } else {
//             $(this).removeClass('shadow_red');
//         }


//     });

//     if (flag == 0) {
//         alert('Please fill all the values');
//         return false;
//     }
//     if (total_flag == 0)
//     {
//         alert('Total should be less than  or equal to 100.');
//         return false;
//     }
    
    
//     //check whether commission exists or not.
//     var commission_exists_flag = 1;
//     var commission_name = $('#name').val();
//     $.ajax({
//         url: BASE_URL + 'admin/Commission/is_commission_exists',
//         data: {
//                 name: commission_name,
//                 rokad_token : rokad_token
//              },
//         async: false,
//         type: 'POST',
//         success: function (r) {
//             commission_exists_flag = (r === 'true') ? false : true;
//         }

//     });
//     if (commission_exists_flag == false)
//     {
//         alert("Commission name already exists.")
//     }
  
//     if(commission_exists_flag==true && flag== 1 && total_flag == 1)
//     {
//         var confirm_flag = confirm('Are you sure you want to create commission?');
//         return confirm_flag;
//     }
//     else
//     {
//         return false;
//     }
//     //check whether commission exists or not ends here.


//@ Author: Lilawati start here
 //form validation code start here
    $('#create_package_form').validate({ 
            rules: {
                      name: {
                        required: true,
                        alphanumericonly : true,
                        package_exists : true
                      },
                      default_package : {
                        required : true
                      },
                      from :{
                        required : true,                      
                      },
                      till : {
                        required : true,
                        enddate: true                       
                      },
                      "service_name[]" :{
                        required : true,
                      },
                      "select_commission[]" :{
                        required : true
                      } 
                    },
            messages: {
                        name: {
                          required: "Please enter package name",
                          alphanumericonly : "Please enter valid package name",
                          package_exists : "Package name already exists"
                        },
                        default_package: {
                          required: "Please select default package",
                        },
                        from :{
                            required : "Please select form date",
                        },
                        till : {
                            required : "Please select till date",
                            enddate : "Till date must be greater than from date"
                        },
                        "service_name[]" : {
                            required: "Please select at least one service",
                        },
                        "select_commission[]" : {
                            required: "Please select commission",
                        }
                      },
            submitHandler: function(form) {
                $('.img-circle').show();
                $('#save_package_data').html('Processing....');
                $("#save_package_data").attr("disabled", "disabled");
                return true;
            },
            errorPlacement: function (error, element) {
              if ($(element).hasClass("service_type")) { 
                  var lastCheckBox = $('[name="'+element.attr("name")+'"]:last');
                  error.insertAfter(lastCheckBox.closest('.intent'));
              } else {
                  error.appendTo(element.parent("div") );
              }
            }
      });

    // check package already exists code start here
    $.validator.addMethod("package_exists",function(value, element) { 
      var flag;

      $.ajax({ 
        url : BASE_URL + "admin/package/package_unique",
        data : {name : value},
        async : false,
        type : 'POST',
        success : function(r) {
          flag = (r === 'true') ? false : true;
        }
      });
      return flag;
    } ,"Package name already exists");

    jQuery.validator.addMethod("alphanumericonly", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z0-9_\-\s]+$/i.test(value);
    }, "");    

    $.validator.addMethod("enddate", function(value, element) {

    var startDate = $('#from').val();
    var newstartdate = startDate.split("-").reverse().join("-");

    var endDate = value;
    var newenddate = endDate.split("-").reverse().join("-");

    newstartdate = new Date(newstartdate);
    newenddate = new Date(newenddate);
    if (newenddate <= newstartdate) {
        return false;
       
    }else {
        return true;
    }
}, "Till Date must be greater than from Date.");

    // load datepicker
    $( "#from" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        minDate : new Date(),
        //maxDate :0
    });
    $( "#till" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        minDate : new Date(),
        //maxDate:0
    });
    $(".from").show();
    $(".till").show();
    // on change of default package value date show hide code start here 
    $(document).off('change', '#default_package').on('change', '#default_package', function () {
        if (this.value == '1')
        {
          $(".from").hide();
          $(".till").hide();
        }
        else
        {
          $(".from").show();
          $(".till").show();
        }
    });     
    
//     $(document).off('change', '#till').on('change', '#till', function () { alert("hi");
//     var startDate = $('#from').val();
//     var newstartdate = startDate.split("-").reverse().join("-");
//     var endDate = $('#till').val();
//     var newenddate = endDate.split("-").reverse().join("-");
//     var t = new Date(newstartdate);
//     var s = new Date(newenddate);
//     if (s <= t) {
//         alert("End date should be greater than Start date");
//         $('#till').val = "";
//     }
// });
   


    //On change of service value load commission value in dropdown from db using ajax here start //
    $(document).off('ifChanged', '.service_type').on('ifChanged', '.service_type', function (e) {
        $('.img-circle').show();
        service_id = $(this).attr('value');
        pkg_status = $('select[name=default_package]').val()
        if ($(this).is(':checked')){
                e.preventDefault();
                this.setAttribute('checked',this.checked);
                $("#select_commission_"+service_id).empty();
                $("#select_commission").html("");
                var elem = $(this);
                var detail = {};
                var form = '';
                var div = '';
                detail['service_id'] = $(this).attr('value');
                detail['default_package'] = pkg_status;

                var ajax_url = "admin/package/list_commission_detail";
                get_data(ajax_url, form, div, detail, function(response) { 
                    $("#select_commission_"+detail['service_id']).show();
                       $("#select_commission_"+detail['service_id']).append("<option value=''>Select Commission</option>");
                    $.each(response, function(index, value) {  
                        $("#select_commission_"+detail['service_id']).append("<option value=" + value.id + ">" + value.name + "</option>");
                        $("#service_name_"+detail['service_id']).removeClass('checked');
                    });
                    $('.img-circle').hide();
                });
            }
            else 
            {
              $("#select_commission_"+service_id).hide();
              $('.img-circle').hide();
            }
    });
    
});
